package com.atlassian.webhooks.internal.rest;

import java.util.LinkedHashMap;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

@JsonSerialize
public class RestWebhookRequestResponse extends LinkedHashMap<String, Object> {

    public static RestWebhookRequestResponse EXAMPLE =
            new RestWebhookRequestResponse(RestWebhookRequest.EXAMPLE, RestWebhookResponse.EXAMPLE);
    private final String ERROR = "error";
    private final String REQUEST = "request";
    private final String RESPONSE = "response";

    @SuppressWarnings("unused") // Required by Jersey
    public RestWebhookRequestResponse() {}

    public RestWebhookRequestResponse(WebhookHttpRequest request, WebhookHttpResponse response) {
        put(REQUEST, new RestWebhookRequest(request));
        put(RESPONSE, new RestWebhookResponse(response));
    }

    public RestWebhookRequestResponse(WebhookHttpRequest request, String errorMessage) {
        put(REQUEST, new RestWebhookRequest(request));
        put(ERROR, new RestWebhookError(errorMessage));
    }

    public RestWebhookRequestResponse(RestWebhookRequest request, RestWebhookResponse response) {
        put(REQUEST, request);
        put(RESPONSE, response);
    }
}
