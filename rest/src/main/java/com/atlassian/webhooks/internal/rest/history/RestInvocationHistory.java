package com.atlassian.webhooks.internal.rest.history;

import java.util.LinkedHashMap;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.atlassian.webhooks.history.InvocationHistory;

@JsonSerialize
public class RestInvocationHistory extends LinkedHashMap<String, Object> {

    public static RestInvocationHistory EXAMPLE = new RestInvocationHistory(
            RestHistoricalInvocation.EXAMPLE_SUCCESS,
            RestHistoricalInvocation.EXAMPLE_FAILURE,
            RestHistoricalInvocation.EXAMPLE_ERROR,
            100);
    private final String COUNTS = "counts";
    private final String LAST_ERROR = "lastError";
    private final String LAST_FAILURE = "lastFailure";
    private final String LAST_SUCCESS = "lastSuccess";

    @SuppressWarnings("unused") // Required by Jersey
    public RestInvocationHistory() {}

    public RestInvocationHistory(@Nonnull InvocationHistory history) {
        history.getLastError().ifPresent(lastError -> put(LAST_ERROR, new RestHistoricalInvocation(lastError)));
        history.getLastFailure().ifPresent(lastFail -> put(LAST_FAILURE, new RestHistoricalInvocation(lastFail)));
        history.getLastSuccess().ifPresent(lastSuccess -> put(LAST_SUCCESS, new RestHistoricalInvocation(lastSuccess)));
        put(COUNTS, new RestInvocationCounts(history.getCounts()));
    }

    private RestInvocationHistory(
            RestHistoricalInvocation success,
            RestHistoricalInvocation failure,
            RestHistoricalInvocation error,
            int counts) {
        put(LAST_SUCCESS, success);
        put(LAST_FAILURE, failure);
        put(LAST_ERROR, error);
        put(COUNTS, counts);
    }
}
