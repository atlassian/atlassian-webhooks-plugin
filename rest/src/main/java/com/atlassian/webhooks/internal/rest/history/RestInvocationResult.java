package com.atlassian.webhooks.internal.rest.history;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.webhooks.history.InvocationResult;

public class RestInvocationResult extends LinkedHashMap<String, Object> {

    public static final RestInvocationResult EXAMPLE = new RestInvocationResult("200", "SUCCESS");
    public static final RestInvocationResult EXAMPLE_FAILURE = new RestInvocationResult("404", "FAILURE");

    public static final RestInvocationResult EXAMPLE_ERROR =
            new RestInvocationResult("Detailed error message", "ERROR");

    private static final String DESCRIPTION = "description";
    private static final String OUTCOME = "outcome";

    @SuppressWarnings("unused") // Required by Jersey
    public RestInvocationResult() {}

    public RestInvocationResult(@Nonnull InvocationResult result) {
        put(DESCRIPTION, result.getDescription());
        put(OUTCOME, result.getOutcome());
    }

    public RestInvocationResult(String description, String outcome) {
        put(DESCRIPTION, description);
        put(OUTCOME, outcome);
    }

    public static RestInvocationResult valueOf(Object value) {
        if (value instanceof RestInvocationResult) {
            return (RestInvocationResult) value;
        } else if (value instanceof InvocationResult) {
            return new RestInvocationResult((InvocationResult) value);
        } else if (value instanceof Map) {
            Map<String, String> map = (Map<String, String>) value;
            return new RestInvocationResult(map.get(DESCRIPTION), map.get(OUTCOME));
        }
        return null;
    }
}
