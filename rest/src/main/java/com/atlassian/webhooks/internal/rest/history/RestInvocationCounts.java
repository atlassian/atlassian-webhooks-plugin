package com.atlassian.webhooks.internal.rest.history;

import java.util.LinkedHashMap;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.ImmutableMap;

import com.atlassian.webhooks.history.InvocationCounts;

@JsonSerialize
public class RestInvocationCounts extends LinkedHashMap<String, Object> {

    @SuppressWarnings("unused") // Required by Jersey
    public RestInvocationCounts() {}

    public RestInvocationCounts(@Nonnull InvocationCounts counts) {
        put("errors", counts.getErrors());
        put("failures", counts.getFailures());
        put("successes", counts.getSuccesses());
        put(
                "window",
                ImmutableMap.of(
                        "start", counts.getWindowStart().toEpochMilli(),
                        "duration", counts.getWindowDuration().toMillis()));
    }
}
