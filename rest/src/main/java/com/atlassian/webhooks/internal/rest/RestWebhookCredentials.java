package com.atlassian.webhooks.internal.rest;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class RestWebhookCredentials extends LinkedHashMap<String, Object> {

    public static final String PASSWORD = "password";
    public static final String USERNAME = "username";

    // for Jersey
    public RestWebhookCredentials() {}

    private RestWebhookCredentials(String username, String password) {
        put(USERNAME, username);
        put(PASSWORD, password);
    }

    public String getPassword() {
        return getStringProperty(PASSWORD);
    }

    public String getUsername() {
        return getStringProperty(USERNAME);
    }

    @Nullable
    public static RestWebhookCredentials valueOf(@Nullable Object value) {
        if (value instanceof RestWebhookCredentials) {
            return (RestWebhookCredentials) value;
        } else if (value instanceof Map) {
            Map<String, String> map = (Map<String, String>) value;
            return new RestWebhookCredentials(map.get(USERNAME), map.get(PASSWORD));
        }

        return null;
    }

    private String getStringProperty(String key) {
        return Objects.toString(get(key), null);
    }
}
