package com.atlassian.webhooks.internal.rest.history;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.webhooks.history.InvocationRequest;

/**
 * A record of a previously completed {@link com.atlassian.webhooks.WebhookInvocation}
 */
public class RestInvocationRequest extends LinkedHashMap<String, Object> {

    public static RestInvocationRequest EXAMPLE = new RestInvocationRequest("POST", "http://example.com/callback");

    private static final String METHOD = "method";
    private static final String URL = "url";

    @SuppressWarnings("unused") // Required by Jersey
    public RestInvocationRequest() {}

    public RestInvocationRequest(@Nonnull InvocationRequest request) {
        put(METHOD, request.getMethod());
        put(URL, request.getUrl());
    }

    public RestInvocationRequest(String method, String url) {
        put(METHOD, method);
        put(URL, url);
    }

    public static RestInvocationRequest valueOf(Object value) {
        if (value instanceof RestInvocationRequest) {
            return (RestInvocationRequest) value;
        } else if (value instanceof InvocationRequest) {
            return new RestInvocationRequest((InvocationRequest) value);
        } else if (value instanceof Map) {
            Map<String, String> map = (Map<String, String>) value;
            return new RestInvocationRequest(map.get(METHOD), map.get(URL));
        }
        return null;
    }
}
