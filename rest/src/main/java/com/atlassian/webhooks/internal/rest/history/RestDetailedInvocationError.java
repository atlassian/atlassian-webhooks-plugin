package com.atlassian.webhooks.internal.rest.history;

import java.util.LinkedHashMap;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.atlassian.webhooks.history.DetailedInvocationError;

@JsonSerialize
public class RestDetailedInvocationError extends LinkedHashMap<String, Object> {

    @SuppressWarnings("unused") // Required by Jersey
    public RestDetailedInvocationError() {}

    public RestDetailedInvocationError(DetailedInvocationError result) {
        put("content", result.getContent());
        put("description", result.getDescription());
        put("outcome", result.getOutcome());
        put("errorMessage", result.getErrorMessage());
    }
}
