package com.atlassian.webhooks.internal.rest;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.atlassian.webhooks.WebhookScope;

@JsonSerialize
public class RestWebhookScope extends LinkedHashMap<String, Object> {

    public static RestWebhookScope EXAMPLE = new RestWebhookScope("repository", "1");

    public static final String ID = "id";
    public static final String TYPE = "type";

    // for Jersey
    public RestWebhookScope() {}

    public RestWebhookScope(@Nonnull WebhookScope scope) {
        this(scope.getType(), scope.getId().orElse(null));
    }

    private RestWebhookScope(@Nonnull String type, @Nullable String id) {
        put(TYPE, type);
        put(ID, id);
    }

    public String getId() {
        return getStringProperty(ID);
    }

    public String getType() {
        return getStringProperty(TYPE);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public static RestWebhookScope valueOf(Object value) {
        if (value instanceof RestWebhookScope) {
            return (RestWebhookScope) value;
        } else if (value instanceof WebhookScope) {
            return new RestWebhookScope((WebhookScope) value);
        } else if (value instanceof Map) {
            Map<String, String> map = (Map<String, String>) value;
            return new RestWebhookScope(map.get(TYPE), map.getOrDefault(ID, null));
        }
        return null;
    }

    private String getStringProperty(String key) {
        return Objects.toString(get(key), null);
    }
}
