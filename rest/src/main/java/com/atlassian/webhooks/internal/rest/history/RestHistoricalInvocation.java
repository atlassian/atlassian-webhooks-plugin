package com.atlassian.webhooks.internal.rest.history;

import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.OptionalInt;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.atlassian.webhooks.history.HistoricalInvocation;
import com.atlassian.webhooks.internal.rest.RestWebhookScope;

/**
 * A record of a previously completed {@link com.atlassian.webhooks.WebhookInvocation}
 */
@JsonSerialize
public class RestHistoricalInvocation extends LinkedHashMap<String, Object> {

    public static final RestHistoricalInvocation EXAMPLE_ERROR = new RestHistoricalInvocation(
            12,
            "repo:modified",
            RestWebhookScope.EXAMPLE,
            100,
            1513106011,
            1513106111,
            RestInvocationRequest.EXAMPLE,
            RestInvocationResult.EXAMPLE_ERROR);
    public static final RestHistoricalInvocation EXAMPLE_FAILURE = new RestHistoricalInvocation(
            11,
            "repo:refs_changed",
            RestWebhookScope.EXAMPLE,
            100,
            1513106011,
            1513106111,
            RestInvocationRequest.EXAMPLE,
            RestInvocationResult.EXAMPLE_FAILURE);
    public static final RestHistoricalInvocation EXAMPLE_SUCCESS = new RestHistoricalInvocation(
            10,
            "repo:created",
            RestWebhookScope.EXAMPLE,
            155,
            1513106011,
            1513106166,
            RestInvocationRequest.EXAMPLE,
            RestInvocationResult.EXAMPLE);
    private final String DURATION = "duration";
    private final String EVENT = "event";
    private final String EVENT_SCOPE = "eventScope";
    private final String FINISH = "finish";
    private final String ID = "id";
    private final String REQUEST = "request";
    private final String RESULT = "result";
    private final String START = "start";

    @SuppressWarnings("unused") // Required by Jersey
    public RestHistoricalInvocation() {}

    public RestHistoricalInvocation(@Nonnull HistoricalInvocation invocation) {
        put(ID, invocation.getId());
        put(EVENT, invocation.getEvent().getId());
        invocation.getEventScope().ifPresent(eventScope -> put(EVENT_SCOPE, new RestWebhookScope(eventScope)));
        put(DURATION, invocation.getDuration().toMillis());
        put(START, invocation.getStart().toEpochMilli());
        put(FINISH, invocation.getFinish().toEpochMilli());
        putRequest(invocation);
        putResponse(invocation);
    }

    private RestHistoricalInvocation(
            int id,
            String event,
            RestWebhookScope eventScope,
            int duration,
            int start,
            int finish,
            RestInvocationRequest request,
            RestInvocationResult result) {
        put(ID, id);
        put(EVENT, event);
        put(EVENT_SCOPE, eventScope);
        put(DURATION, duration);
        put(START, start);
        put(FINISH, finish);
        put(REQUEST, request);
        put(RESULT, result);
    }

    public int getDuration() {
        return getIntProperty(DURATION);
    }

    public String getEvent() {
        return getStringProperty(EVENT);
    }

    public RestWebhookScope getEventScope() {
        return RestWebhookScope.valueOf(get(EVENT_SCOPE));
    }

    public int getFinish() {
        return getIntProperty(FINISH);
    }

    public int getId() {
        return getIntProperty(ID);
    }

    public RestInvocationRequest getRequest() {
        return RestInvocationRequest.valueOf(REQUEST);
    }

    public RestInvocationResult getResult() {
        return RestInvocationResult.valueOf(RESULT);
    }

    public int getStart() {
        return getIntProperty(START);
    }

    protected void putRequest(@Nonnull HistoricalInvocation invocation) {
        put(REQUEST, new RestInvocationRequest(invocation.getRequest()));
    }

    protected void putResponse(@Nonnull HistoricalInvocation invocation) {
        put(RESULT, new RestInvocationResult(invocation.getResult()));
    }

    private int getIntProperty(String property) {
        return getOptionalIntProperty(property).orElse(-1);
    }

    private OptionalInt getOptionalIntProperty(String property) {
        Object value = get(property);
        if (value instanceof Number) {
            return OptionalInt.of(((Number) value).intValue());
        } else if (value instanceof String) {
            return OptionalInt.of(Integer.parseInt((String) value));
        }
        return OptionalInt.empty();
    }

    private String getStringProperty(String key) {
        return Objects.toString(get(key), null);
    }
}
