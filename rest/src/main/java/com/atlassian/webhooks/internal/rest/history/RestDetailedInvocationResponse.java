package com.atlassian.webhooks.internal.rest.history;

import java.util.LinkedHashMap;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.history.DetailedInvocationResponse;
import com.atlassian.webhooks.request.WebhookHttpResponse;

/**
 * The recorded {@link WebhookHttpResponse response} of a previous {@link WebhookInvocation webhook invocation}
 */
@JsonSerialize
public class RestDetailedInvocationResponse extends LinkedHashMap<String, Object> {

    @SuppressWarnings("unused") // Required by Jersey
    public RestDetailedInvocationResponse() {}

    public RestDetailedInvocationResponse(@Nonnull DetailedInvocationResponse response) {
        put("description", response.getDescription());
        put("headers", response.getHeaders());
        put("outcome", response.getOutcome());
        response.getBody().ifPresent(body -> put("body", body));
        put("statusCode", response.getStatusCode());
    }
}
