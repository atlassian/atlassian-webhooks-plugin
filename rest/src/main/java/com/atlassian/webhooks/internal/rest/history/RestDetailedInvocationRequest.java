package com.atlassian.webhooks.internal.rest.history;

import java.util.LinkedHashMap;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.history.DetailedInvocationRequest;
import com.atlassian.webhooks.request.WebhookHttpRequest;

/**
 * The recorded {@link WebhookHttpRequest request} of a previous {@link WebhookInvocation webhook invocation}
 */
@JsonSerialize
public class RestDetailedInvocationRequest extends LinkedHashMap<String, Object> {

    @SuppressWarnings("unused") // Required by Jersey
    public RestDetailedInvocationRequest() {}

    public RestDetailedInvocationRequest(@Nonnull DetailedInvocationRequest request) {
        put("url", request.getUrl());
        put("headers", request.getHeaders());
        put("method", request.getMethod().name());
        request.getBody().ifPresent(body -> put("body", body));
    }
}
