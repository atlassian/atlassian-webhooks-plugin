package com.atlassian.webhooks.internal.rest;

import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.net.ssl.SSLException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.Iterables;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webhooks.NoSuchWebhookException;
import com.atlassian.webhooks.PingRequest;
import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookCreateRequest;
import com.atlassian.webhooks.WebhookCredentials;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.WebhookSearchRequest;
import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.WebhookUpdateRequest;
import com.atlassian.webhooks.diagnostics.WebhookDiagnosticsResult;
import com.atlassian.webhooks.history.DetailedInvocation;
import com.atlassian.webhooks.history.HistoricalInvocationRequest;
import com.atlassian.webhooks.history.InvocationHistory;
import com.atlassian.webhooks.history.InvocationHistoryByEventRequest;
import com.atlassian.webhooks.history.InvocationHistoryRequest;
import com.atlassian.webhooks.history.InvocationHistoryService;
import com.atlassian.webhooks.history.InvocationOutcome;
import com.atlassian.webhooks.internal.rest.history.RestDetailedInvocation;
import com.atlassian.webhooks.internal.rest.history.RestInvocationHistory;
import com.atlassian.webhooks.util.WebhookScopeUtil;

import static com.google.common.collect.ImmutableSet.toImmutableSet;
import static java.util.stream.Collectors.toList;

public class WebhooksResourceHelper {

    private static final Pattern URL_VARIABLE_REGEX = Pattern.compile("\\{(.*?)}/?");

    protected final Logger log;

    private final I18nResolver i18nResolver;
    private final RestResponseBuilder restBuilder;
    private final Supplier<Object> invocationHistoryServiceProvider;
    private final Supplier<Object> webhooksServiceProvider;
    private final Consumer<WebhookScope> webhooksScopeAdminValidator;

    public WebhooksResourceHelper(
            I18nResolver i18nResolver,
            RestResponseBuilder restBuilder,
            Supplier<Object> invocationHistoryServiceProvider,
            Supplier<Object> webhookServiceProvider,
            Consumer<WebhookScope> webhookScopeAdminValidator) {
        this.i18nResolver = i18nResolver;
        this.restBuilder = restBuilder;
        this.invocationHistoryServiceProvider = invocationHistoryServiceProvider;
        this.webhooksScopeAdminValidator = webhookScopeAdminValidator;
        this.webhooksServiceProvider = webhookServiceProvider;

        log = LoggerFactory.getLogger(getClass());
    }

    private InvocationHistoryService getInvocationHistoryService() {
        return (InvocationHistoryService) invocationHistoryServiceProvider.get();
    }

    private WebhookService getWebhookService() {
        return (WebhookService) webhooksServiceProvider.get();
    }

    private void validateCanAdmin(WebhookScope scope) {
        webhooksScopeAdminValidator.accept(scope);
    }

    public Response createWebhook(UriInfo uriInfo, WebhookScope scope, RestWebhook webhook) {
        WebhookService service = getWebhookService();
        validateCanAdmin(scope);

        WebhookCreateRequest.Builder builder = WebhookCreateRequest.builder()
                .configuration(webhook.getConfiguration())
                .event(toEvents(service, webhook.getEvents()))
                .name(webhook.getName())
                .scope(scope)
                .url(webhook.getUrl());

        Boolean active = webhook.getActive();
        Boolean sslVerificationRequired = webhook.getSslVerificationRequired();

        if (active != null) {
            builder.active(active);
        }
        if (sslVerificationRequired != null) {
            builder.sslVerificationRequired(sslVerificationRequired);
        }

        webhook.getCredentials().ifPresent(credentials -> builder.credentials(toCredentials(credentials)));

        Webhook result = service.create(builder.build());
        return Response.created(uriInfo.getRequestUriBuilder()
                        .path(Integer.toString(result.getId()))
                        .build())
                .entity(new RestWebhook(result))
                .build();
    }

    public Response deleteWebhook(WebhookScope scope, int webhookId) {
        WebhookService service = getWebhookService();
        validateCanAdmin(scope);

        // We first have to check that they're deleting a webhook *from* the scope that they're authorised in
        if (!scopeMatchesWebhook(service, webhookId, scope) || !service.delete(webhookId)) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(noSuchWebhook(webhookId))
                    .build();
        }
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    public Response findWebhooks(
            UriInfo uriInfo,
            WebhookScope scope,
            List<WebhookScope> scopes,
            List<String> eventIds,
            boolean statistics,
            int start,
            int limit) {
        WebhookService service = getWebhookService();
        validateCanAdmin(scope);

        List<String> decodedEventIds =
                eventIds.stream().map(WebhooksResourceHelper::decodeURL).collect(toList());
        Set<WebhookEvent> events = toEvents(service, decodedEventIds);
        List<Webhook> hooks = service.search(WebhookSearchRequest.builder()
                .event(events)
                .scope(scopes) // Allow search across set of scopes. This is useful for inherited webhooks
                .offset(start)
                .limit(limit + 1) // Request one more to determine whether there's a next page
                .build());

        boolean hasNextPage = hooks.size() > limit;
        if (hasNextPage) {
            hooks = hooks.subList(0, limit);
        }

        Map<Integer, InvocationHistory> hookStats;
        if (statistics && !hooks.isEmpty()) {
            // load the statistics for each of the retrieved webhooks
            InvocationHistoryService historyService = getInvocationHistoryService();
            Set<Integer> ids = hooks.stream().map(Webhook::getId).collect(toImmutableSet());
            // isPrimaryScope checks if the scope in question is the only scope for which to search for webhooks. If we
            // are searching
            // across a wider set of scopes (e.g. if we have inherited webhooks), stats will need to be loaded for the
            // specific scope in question.
            hookStats = isPrimaryScope(scope, scopes)
                    ? historyService.getByWebhook(ids)
                    : historyService.getByWebhookAndScope(ids, scope);
        } else {
            hookStats = Collections.emptyMap();
        }
        // transform to RestWebhooks and enrich with statistics if available
        List<RestWebhook> restHooks = hooks.stream()
                .map(hook -> {
                    RestWebhook restHook = new RestWebhook(hook);
                    InvocationHistory stats = hookStats.get(hook.getId());
                    if (stats != null) {
                        restHook.setStatistics(new RestInvocationHistory(stats));
                    }
                    return restHook;
                })
                .collect(toList());

        return Response.ok(restBuilder.page(uriInfo, start, limit, restHooks, hasNextPage))
                .build();
    }

    public Response findWebhooks(
            UriInfo uriInfo, WebhookScope scope, List<String> eventIds, boolean statistics, int start, int limit) {
        return findWebhooks(uriInfo, scope, Collections.singletonList(scope), eventIds, statistics, start, limit);
    }

    public Response getLatestInvocation(WebhookScope scope, int webhookId, String eventId, Set<String> outcomeNames) {
        InvocationHistoryService historyService = getInvocationHistoryService();
        validateCanAdmin(scope);

        // convert manually - Jersey will send a 404 with HTML if we use the enum
        // for the query parameter and it's spelt incorrectly
        Set<InvocationOutcome> outcomes = new HashSet<>();
        for (String outcomeName : outcomeNames) {
            try {
                outcomes.add(InvocationOutcome.valueOf(outcomeName.toUpperCase(Locale.ROOT)));
            } catch (IllegalArgumentException e) {
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity(unknownOutcome(outcomeName))
                        .build();
            }
        }

        HistoricalInvocationRequest latestInvocationRequest = HistoricalInvocationRequest.builder(webhookId)
                .eventId(StringUtils.trimToNull(eventId))
                .outcome(outcomes)
                .build();
        try {
            Optional<DetailedInvocation> latestInvocation = historyService.getLatestInvocation(latestInvocationRequest);

            return latestInvocation
                    .map(RestDetailedInvocation::new)
                    .map(Response::ok)
                    .orElseGet(Response::noContent)
                    .build();
        } catch (NoSuchWebhookException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(noSuchWebhook(webhookId))
                    .build();
        }
    }

    public Response getStatistics(WebhookScope scope, int webhookId, String eventId) {
        InvocationHistoryService historyService = getInvocationHistoryService();
        WebhookService service = getWebhookService();
        validateCanAdmin(scope);

        if (!scopeMatchesWebhook(service, webhookId, scope)) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(noSuchWebhook(webhookId))
                    .build();
        }

        InvocationHistoryRequest historyRequest = InvocationHistoryRequest.builder()
                .eventId(StringUtils.trimToNull(eventId))
                .webhookId(webhookId)
                .build();
        try {
            InvocationHistory history = historyService.get(historyRequest);
            return Response.ok(new RestInvocationHistory(history)).build();
        } catch (NoSuchWebhookException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(noSuchWebhook(webhookId))
                    .build();
        }
    }

    public Response getStatisticsSummary(WebhookScope scope, int webhookId) {
        InvocationHistoryService historyService = getInvocationHistoryService();
        WebhookService service = getWebhookService();
        validateCanAdmin(scope);

        if (!scopeMatchesWebhook(service, webhookId, scope)) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(noSuchWebhook(webhookId))
                    .build();
        }

        InvocationHistoryByEventRequest request =
                InvocationHistoryByEventRequest.builder(webhookId).build();
        try {
            Map<String, RestInvocationHistory> invocationHistories =
                    historyService.getByEvent(request).entrySet().stream()
                            .collect(Collectors.toMap(
                                    entry -> entry.getKey().getId(),
                                    entry -> new RestInvocationHistory(entry.getValue())));

            return Response.ok(invocationHistories).build();
        } catch (NoSuchWebhookException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(noSuchWebhook(webhookId))
                    .build();
        }
    }

    public Response getWebhook(WebhookScope scope, int webhookId, boolean includeStatistics) {
        WebhookService service = getWebhookService();
        validateCanAdmin(scope);
        if (!scopeMatchesWebhook(service, webhookId, scope)) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(noSuchWebhook(webhookId))
                    .build();
        }

        Webhook webhook = service.findById(webhookId).orElse(null);
        if (webhook == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(noSuchWebhook(webhookId))
                    .build();
        }
        RestWebhook restWebhook = new RestWebhook(webhook);
        if (includeStatistics) {
            InvocationHistoryService historyService = getInvocationHistoryService();
            InvocationHistoryRequest historyRequest =
                    InvocationHistoryRequest.builder().webhookId(webhookId).build();
            InvocationHistory history = historyService.get(historyRequest);
            restWebhook.setStatistics(new RestInvocationHistory(history));
        }
        return Response.ok(restWebhook).build();
    }

    public Response testWebhook(WebhookScope scope, String url) {
        return testWebhook(scope, url, true, null, null, null);
    }

    public Response testWebhook(
            WebhookScope scope,
            String url,
            boolean sslVerificationRequired,
            Integer webhookId,
            String username,
            String password) {
        WebhookService service = getWebhookService();
        validateCanAdmin(scope);

        String parsedURL = removeURLParameters(url);
        PingRequest.Builder request =
                PingRequest.builder(parsedURL).scope(scope).sslVerificationRequired(sslVerificationRequired);

        // If credentials are specified, we want to add these to the ping request.
        if (username != null || password != null) {
            request.credentials(new SimpleWebhookCredentials(username, password));

            // If a webhookId is specified, we want to retrieve any credentials stored against the given webhook and
            // add these to the ping request. The reason we preference a username and password passed in first, is if
            // a user is testing the connection with different credentials before saving the given credentials.
        } else if (webhookId != null) {
            Webhook webhook = service.findById(webhookId).orElse(null);
            if (webhook == null) {
                log.warn("No webhook was found for ID {}. Connection will be tested without credentials.", webhookId);
            } else {
                webhook.getCredentials()
                        .ifPresent(credentials -> request.credentials(new SimpleWebhookCredentials(
                                credentials.getUsername().orElse(null),
                                credentials.getPassword().orElse(null))));
            }
        }

        Future<WebhookDiagnosticsResult> future = service.ping(request.build());

        try {
            WebhookDiagnosticsResult result = future.get();
            if (result.isError()) {
                String errorMessage = getErrorMessage(parsedURL, result.getError());
                return Response.ok(new RestWebhookRequestResponse(result.getRequest(), errorMessage))
                        .build();
            } else {
                return Response.ok(new RestWebhookRequestResponse(result.getRequest(), result.getResponse()))
                        .build();
            }
        } catch (InterruptedException | ExecutionException e) {
            String errorMessage = getErrorMessage(parsedURL, e.getCause());
            return Response.ok(new RestWebhookRequestResponse(null, errorMessage))
                    .build();
        }
    }

    public Response update(WebhookScope scope, int webhookId, RestWebhook webhook) {
        WebhookService service = getWebhookService();
        validateCanAdmin(scope);
        if (!scopeMatchesWebhook(service, webhookId, scope)) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(noSuchWebhook(webhookId))
                    .build();
        }

        WebhookUpdateRequest.Builder builder = WebhookUpdateRequest.builder()
                .configuration(webhook.getConfiguration())
                .event(toEvents(service, webhook.getEvents()))
                .name(webhook.getName())
                .scope(scope)
                .url(webhook.getUrl());

        Boolean active = webhook.getActive();
        Boolean sslVerificationRequired = webhook.getSslVerificationRequired();

        if (active != null) {
            builder.active(active);
        }
        if (sslVerificationRequired != null) {
            builder.sslVerificationRequired(sslVerificationRequired);
        }
        webhook.getCredentials().ifPresent(credentials -> builder.credentials(toCredentials(credentials)));

        try {
            Webhook result = service.update(webhookId, builder.build());
            return Response.ok().entity(new RestWebhook(result)).build();
        } catch (NoSuchWebhookException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(noSuchWebhook(webhookId))
                    .build();
        }
    }

    private static String decodeURL(final String fragment) {
        try {
            return URLDecoder.decode(fragment, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    private String getErrorMessage(String url, Throwable error) {
        // We're going to try be nice, if it's a known error we'll translate it to a human readable string
        if (error.getCause() instanceof UnknownHostException) {
            return i18nResolver.getText("webhooks.rest.unknown.host", url);
        }
        if (error.getCause() instanceof SocketException) {
            return i18nResolver.getText("webhooks.rest.connect.error");
        }
        if (error.getCause() instanceof SSLException) {
            return i18nResolver.getText("webhooks.rest.ssl.error");
        }
        // Never return the error from error.getMessage() to the user because it might result in showing the user
        // more information than they would normally be able to see, since the request is being made from a
        // privileged instance (SSRF)
        log.warn("Request to {} resulted in an error: {}", url, error.getLocalizedMessage());
        return i18nResolver.getText("webhooks.rest.other.error");
    }

    private boolean isPrimaryScope(WebhookScope scope, List<WebhookScope> scopes) {
        return scopes.size() == 1 && Iterables.getOnlyElement(scopes).equals(scope);
    }

    private Object noSuchWebhook(int webhookId) {
        return restBuilder.error("webhookId", i18nResolver.getText("webhooks.rest.nosuchwebhook", webhookId));
    }

    private String removeURLParameters(String url) {
        return URL_VARIABLE_REGEX.matcher(url).replaceAll(StringUtils.EMPTY);
    }

    private boolean scopeMatchesWebhook(WebhookService service, int webhookId, WebhookScope scope) {
        Optional<Webhook> possibleWebhook = service.findById(webhookId);

        return possibleWebhook
                .map(webhook -> WebhookScopeUtil.equals(scope, webhook.getScope()))
                .orElse(false);
    }

    private WebhookCredentials toCredentials(RestWebhookCredentials credentials) {
        return new SimpleWebhookCredentials(credentials.getUsername(), credentials.getPassword());
    }

    private Set<WebhookEvent> toEvents(WebhookService service, Collection<String> eventIds) {
        return eventIds.stream()
                .map(eventId -> service.getEvent(eventId).orElseGet(() -> new UnknownWebhookEvent(eventId)))
                .collect(toImmutableSet());
    }

    private Object unknownOutcome(String outcomeName) {
        return restBuilder.error("outcome", i18nResolver.getText("webhooks.rest.nosuchoutcome", outcomeName));
    }

    private static class SimpleWebhookCredentials implements WebhookCredentials {

        private final String password;
        private final String username;

        private SimpleWebhookCredentials(String username, String password) {
            this.password = password;
            this.username = username;
        }

        @Nonnull
        @Override
        public Optional<String> getPassword() {
            return Optional.ofNullable(password);
        }

        @Nonnull
        @Override
        public Optional<String> getUsername() {
            return Optional.ofNullable(username);
        }
    }

    private static class UnknownWebhookEvent implements WebhookEvent {

        private final String id;

        private UnknownWebhookEvent(String id) {
            this.id = id;
        }

        @Nonnull
        @Override
        public String getI18nKey() {
            return id;
        }

        @Nonnull
        @Override
        public String getId() {
            return id;
        }
    }
}
