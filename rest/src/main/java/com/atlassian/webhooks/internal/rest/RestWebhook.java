package com.atlassian.webhooks.internal.rest;

import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.internal.rest.history.RestInvocationHistory;

import static java.util.Optional.ofNullable;

@JsonSerialize
public class RestWebhook extends LinkedHashMap<String, Object> {

    private static final String ACTIVE = "active";
    private static final String CONFIGURATION = "configuration";
    private static final String CREDENTIALS = "credentials";
    private static final String CREATED_DATE = "createdDate";
    private static final String EVENTS = "events";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String SCOPE_TYPE = "scopeType";
    private static final String SSL_VERIFICATION_REQUIRED = "sslVerificationRequired";
    private static final String STATISTICS = "statistics";
    private static final String UPDATED_DATE = "updatedDate";
    private static final String USERNAME = "username";
    private static final String URL = "url";

    private static final Date exampleDate = Date.from(Instant.ofEpochSecond(1513106011L));

    public static RestWebhook EXAMPLE = new RestWebhook(
            10,
            "Webhook Name",
            exampleDate,
            exampleDate,
            ImmutableSet.of("repo:refs_changed", "repo:modified"),
            ImmutableMap.of("secret", "password"),
            ImmutableMap.of("username", "admin"),
            "http://example.com",
            true,
            "repository",
            true);

    @SuppressWarnings("unused") // Required by Jersey
    public RestWebhook() {}

    public RestWebhook(Webhook webhook) {
        if (webhook.getId() > 0) {
            put(ID, webhook.getId());
        }
        put(NAME, webhook.getName());
        put(CREATED_DATE, webhook.getCreatedDate());
        put(UPDATED_DATE, webhook.getUpdatedDate());
        put(
                EVENTS,
                webhook.getEvents().stream()
                        .map(WebhookEvent::getId)
                        .collect(Collectors.collectingAndThen(Collectors.toSet(), Collections::unmodifiableSet)));
        put(CONFIGURATION, webhook.getConfiguration());
        webhook.getCredentials()
                .ifPresent(credentials ->
                        // Don't set the password. We never want to return the password in the REST representation.
                        put(CREDENTIALS, ImmutableMap.of(USERNAME, credentials.getUsername())));
        put(URL, webhook.getUrl());
        put(ACTIVE, webhook.isActive());
        put(SCOPE_TYPE, webhook.getScope().getType());
        put(SSL_VERIFICATION_REQUIRED, webhook.isSslVerificationRequired());
    }

    private RestWebhook(
            int id,
            String name,
            Date createdDate,
            Date updatedDate,
            Set<String> events,
            Map<String, String> configuration,
            Map<String, String> credentials,
            String url,
            boolean isActive,
            String scopeType,
            boolean sslVerificationRequired) {
        put(ID, id);
        put(NAME, name);
        put(CREATED_DATE, createdDate);
        put(UPDATED_DATE, updatedDate);
        put(EVENTS, events);
        put(CONFIGURATION, configuration);
        put(CREDENTIALS, credentials);
        put(URL, url);
        put(ACTIVE, isActive);
        put(SCOPE_TYPE, scopeType);
        put(SSL_VERIFICATION_REQUIRED, sslVerificationRequired);
    }

    @Nullable
    public Boolean getActive() {
        Object value = get(ACTIVE);
        return value instanceof Boolean ? (Boolean) value : null;
    }

    @Nonnull
    public Map<String, String> getConfiguration() {
        Object config = get(CONFIGURATION);
        if (config instanceof Map) {
            //noinspection unchecked
            return (Map<String, String>) config;
        }
        return Collections.emptyMap();
    }

    @Nonnull
    public Set<String> getEvents() {
        Object events = get(EVENTS);
        if (events instanceof Collection) {
            //noinspection unchecked
            return new HashSet<>((Collection) events);
        }
        return Collections.emptySet();
    }

    @Nonnull
    public Optional<RestWebhookCredentials> getCredentials() {
        return ofNullable(RestWebhookCredentials.valueOf(get(CREDENTIALS)));
    }

    public String getName() {
        return StringUtils.trimToNull(getStringProperty(NAME));
    }

    public String getScopeType() {
        return StringUtils.trimToNull(getStringProperty(SCOPE_TYPE));
    }

    public Boolean getSslVerificationRequired() {
        Object value = get(SSL_VERIFICATION_REQUIRED);
        return value instanceof Boolean ? (Boolean) value : true;
    }

    public String getUrl() {
        return StringUtils.trimToNull(getStringProperty(URL));
    }

    public void setStatistics(RestInvocationHistory statistics) {
        if (statistics != null) {
            put(STATISTICS, statistics);
        }
    }

    private String getStringProperty(String key) {
        return Objects.toString(get(key), null);
    }
}
