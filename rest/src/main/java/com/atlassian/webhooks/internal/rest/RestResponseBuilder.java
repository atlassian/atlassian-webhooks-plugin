package com.atlassian.webhooks.internal.rest;

import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.UriInfo;

/**
 * Interface that abstracts away the product-specific REST representations used for returning errors and pages of
 * data.
 */
public interface RestResponseBuilder {

    Object error(@Nullable String field, @Nonnull String errorMessage);

    Object page(@Nonnull UriInfo uriInfo, int start, int limit, @Nonnull List<?> data, boolean hasNextPage);
}
