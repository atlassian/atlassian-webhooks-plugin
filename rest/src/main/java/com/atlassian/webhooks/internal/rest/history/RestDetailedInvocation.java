package com.atlassian.webhooks.internal.rest.history;

import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.atlassian.webhooks.history.*;

/**
 * A detailed record of a previously completed {@link com.atlassian.webhooks.WebhookInvocation}
 */
@JsonSerialize
public class RestDetailedInvocation extends RestHistoricalInvocation {

    @SuppressWarnings("unused") // Required by Jersey
    public RestDetailedInvocation() {}

    public RestDetailedInvocation(@Nonnull DetailedInvocation invocation) {
        super(invocation);
    }

    @Override
    protected void putRequest(@Nonnull HistoricalInvocation invocation) {
        put("request", new RestDetailedInvocationRequest((DetailedInvocationRequest) invocation.getRequest()));
    }

    @Override
    protected void putResponse(@Nonnull HistoricalInvocation invocation) {
        InvocationResult result = invocation.getResult();
        if (result instanceof DetailedInvocationError) {
            put("result", new RestDetailedInvocationError((DetailedInvocationError) result));
        } else {
            put("result", new RestDetailedInvocationResponse((DetailedInvocationResponse) result));
        }
    }
}
