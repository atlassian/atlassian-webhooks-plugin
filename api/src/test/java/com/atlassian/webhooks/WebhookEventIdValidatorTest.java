package com.atlassian.webhooks;

import javax.annotation.Nonnull;
import javax.validation.ConstraintValidatorContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class WebhookEventIdValidatorTest {

    @Mock
    private ConstraintValidatorContext.ConstraintViolationBuilder builder;

    @Mock
    private ConstraintValidatorContext context;

    @Mock
    private WebhookService webhookService;

    @Before
    public void setup() {
        WebhookEventIdValidator.setWebhookService(webhookService);
        when(context.buildConstraintViolationWithTemplate(anyString())).thenReturn(builder);
        when(webhookService.getEvent(anyString())).thenReturn(empty());
    }

    @After
    public void tearDown() {
        WebhookEventIdValidator.setWebhookService(null);
    }

    @Test
    public void testUnknownEventIdIsInvalid() {
        SimpleWebhookEvent event = new SimpleWebhookEvent("test_event");

        assertFalse(isValid(event));
        verify(context).disableDefaultConstraintViolation();
        verify(context).buildConstraintViolationWithTemplate(anyString());
        verify(builder).addConstraintViolation();
    }

    @Test
    public void testValidEventIdValidatesOk() {
        SimpleWebhookEvent event = new SimpleWebhookEvent("test_event");
        when(webhookService.getEvent("test_event")).thenReturn(of(event));

        assertTrue(isValid(event));
        verifyZeroInteractions(context, builder);
    }

    @Test
    public void testWebhookServiceNotSet() {
        WebhookEventIdValidator.setWebhookService(null);
        SimpleWebhookEvent event = new SimpleWebhookEvent("test_event");

        assertTrue(isValid(event));
    }

    private boolean isValid(WebhookEvent event) {
        try {
            WebhookEventId annotation = WebhookEvent.class.getMethod("getId").getAnnotation(WebhookEventId.class);
            WebhookEventIdValidator validator = new WebhookEventIdValidator();
            validator.initialize(annotation);
            return validator.isValid(event.getId(), context);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException(e);
        }
    }

    private static class SimpleWebhookEvent implements WebhookEvent {

        private final String id;

        private SimpleWebhookEvent(String id) {
            this.id = id;
        }

        @Nonnull
        @Override
        public String getId() {
            return id;
        }

        @Nonnull
        @Override
        public String getI18nKey() {
            return "i18n." + id;
        }
    }
}
