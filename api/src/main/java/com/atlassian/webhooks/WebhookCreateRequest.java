package com.atlassian.webhooks;

import javax.annotation.Nonnull;

/**
 * @since 5.0
 */
public class WebhookCreateRequest extends AbstractWebhookRequest {

    private WebhookCreateRequest(Builder builder) {
        super(builder);
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends AbstractBuilder<Builder> {

        private Builder() {
            scope(WebhookScope.GLOBAL);
        }

        @Nonnull
        public WebhookCreateRequest build() {
            return new WebhookCreateRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
