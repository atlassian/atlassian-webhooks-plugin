package com.atlassian.webhooks;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebhookEventIdValidator implements ConstraintValidator<WebhookEventId, String> {

    private static final Logger log = LoggerFactory.getLogger(WebhookEventIdValidator.class);

    private static WebhookService webhookService;

    private String message;

    public WebhookEventIdValidator() {}

    @Override
    public void initialize(WebhookEventId constraintAnnotation) {
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (webhookService == null) {
            log.warn(
                    "Cannot validate whether '{}' is a valid webhook event because the validator has not (yet)"
                            + " been initialized. Assuming it's fine.",
                    value);
            return true;
        }
        if (!webhookService.getEvent(value).isPresent()) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
            return false;
        }

        return true;
    }

    /**
     * Sets the {@link WebhookService} to ensure that {@link WebhookEventIdValidator} can have a no-arg public
     * constructor as required by javax.validation
     *
     * @param value the webhookservice to use
     */
    protected static void setWebhookService(WebhookService value) {
        webhookService = value;
    }
}
