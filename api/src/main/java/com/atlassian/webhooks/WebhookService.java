package com.atlassian.webhooks;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;
import javax.annotation.Nonnull;

import com.atlassian.webhooks.diagnostics.WebhookDiagnosticsResult;

/**
 * A service for managing and publishing webhooks
 *
 * @since 5.0
 */
public interface WebhookService {

    /**
     * Create a webhook.
     *
     * @param request the parameters for the webhook to be created
     * @return the created webhook
     */
    @Nonnull
    Webhook create(@Nonnull WebhookCreateRequest request);

    /**
     * Delete a registered webhook.
     *
     * @param id unique id of registered webhook
     * @return {@code true} if webhook was deleted, otherwise {@code false}
     */
    boolean delete(int id);

    /**
     * Delete all webhooks that match the associated search
     *
     * @param request search details
     * @return number of deleted items
     */
    int delete(@Nonnull WebhookDeleteRequest request);

    /**
     * @param id the webhook ID
     * @return the webhook, or {@link Optional#empty} if the webhook does not exist
     */
    @Nonnull
    Optional<Webhook> findById(int id);

    /**
     * @param eventId the {@link WebhookEvent#getId event ID}
     * @return the matching {@link WebhookEvent}, or {@link Optional#empty()} if no matching event is available
     */
    @Nonnull
    Optional<WebhookEvent> getEvent(@Nonnull String eventId);

    /**
     * @return a list of supported {@link WebhookEvent webhook events}
     */
    @Nonnull
    List<WebhookEvent> getEvents();

    /**
     * @return webhook statistics if statistics recording is enabled
     */
    @Nonnull
    Optional<WebhookStatistics> getStatistics();

    /**
     * Send a simple ping webhook to the listener described by {@code request}
     *
     * @param request describes the listener to ping
     * @return details of attempting to send a request to the endpoint
     */
    @Nonnull
    Future<WebhookDiagnosticsResult> ping(@Nonnull PingRequest request);

    /**
     * Publishes all webhooks associated with the provided request.
     *
     * @see #publish(WebhookPublishRequest)
     */
    void publish(@Nonnull WebhookPublishRequest request);

    /**
     * Searches for webhooks matching provided details.
     *
     * @param request search details
     * @return a list of webhooks matching requested details
     */
    @Nonnull
    List<Webhook> search(@Nonnull WebhookSearchRequest request);

    /**
     * @param enabled whether statistics should be enabled
     */
    void setStatisticsEnabled(boolean enabled);

    /**
     * Update an existing webhook.
     *
     * @param id      unique id of the webhook
     * @param request updated parameters of the webhook
     * @return the updated webhook
     * @throws NoSuchWebhookException if the webhook was not found
     */
    @Nonnull
    Webhook update(int id, @Nonnull WebhookUpdateRequest request);
}
