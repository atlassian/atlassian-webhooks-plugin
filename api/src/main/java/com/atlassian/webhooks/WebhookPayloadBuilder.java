package com.atlassian.webhooks;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since 5.0
 */
public interface WebhookPayloadBuilder {

    /**
     * Set the body of the HTTP request
     *
     * @param body the body to send in the HTTP request
     * @param contentType the content-type of the body
     */
    @Nonnull
    WebhookPayloadBuilder body(@Nullable byte[] body, @Nullable String contentType);

    /**
     * Sets or updates the value for an header
     *
     * @param name  name of header
     * @param value value of header
     */
    @Nonnull
    WebhookPayloadBuilder header(@Nonnull String name, @Nullable String value);
}
