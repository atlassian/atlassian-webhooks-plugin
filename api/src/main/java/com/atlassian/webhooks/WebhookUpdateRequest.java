package com.atlassian.webhooks;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Request for {@link WebhookService#update updating} a registered webhook.
 *
 * @since 5.0
 */
public class WebhookUpdateRequest extends AbstractWebhookRequest {

    private WebhookUpdateRequest(Builder builder) {
        super(builder);
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    public static Builder builder(@Nonnull Webhook webhook) {
        return new Builder(webhook);
    }

    public static class Builder extends AbstractBuilder<Builder> {

        private Builder() {}

        private Builder(@Nonnull Webhook webhook) {
            this.active(requireNonNull(webhook, "webhook").isActive())
                    .configuration(webhook.getConfiguration())
                    .event(webhook.getEvents())
                    .name(webhook.getName())
                    .scope(webhook.getScope())
                    .sslVerificationRequired(requireNonNull(webhook, "webhook").isSslVerificationRequired())
                    .url(webhook.getUrl());

            webhook.getCredentials().ifPresent(this::credentials);
        }

        @Nonnull
        public WebhookUpdateRequest build() {
            return new WebhookUpdateRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
