package com.atlassian.webhooks;

import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.annotation.Nonnull;

/**
 * Represents a external HTTP endpoint that is to be called whenever one of the configured application events occurs.
 *
 * @since 5.0
 */
public interface Webhook {

    /**
     * Get configured context for this webhook, this can be used to store configuration against this webhook. Context
     * will be loaded from the state that exists in the {@link AbstractWebhookRequest} that created/updated the webhook.
     * Further changes must be made via an update, any changes to a loaded webhook will be kept for the lifetime of
     * that webhook request, but will not be persisted.
     *
     * @return the webhook context
     */
    @Nonnull
    Map<String, String> getConfiguration();

    /**
     * @return the date at which the webhook was created
     */
    @Nonnull
    Date getCreatedDate();

    /**
     * Optional credentials if the given webhook URL is authenticated.
     *
     * @return credentials for the authenticated webhook URL, or {@link Optional#empty()} if none exist
     * @since 7.1
     */
    @Nonnull
    default Optional<WebhookCredentials> getCredentials() {
        return Optional.empty();
    }

    /**
     * Returns a list of events that will trigger this webhook. These will match against the triggers specified in a
     * {@link AbstractWebhookRequest#getEvents()}.
     *
     * @return list of triggers
     */
    @Nonnull
    Set<WebhookEvent> getEvents();

    /**
     * Returns the ID of the webhook, this ID is unique per webhook, not per webhook trigger
     *
     * @return ID of the webhook
     */
    int getId();

    /**
     * Retrieves the name of the webhook, which can be used to easily recognize what the webhook is used for.
     *
     * @return the webhook name
     */
    @Nonnull
    String getName();

    /**
     * Returns the scope that specifies at what contextual level this webhook takes place. This is used to filter
     * webhooks as soon as possible so that webhooks are only loaded if the event matches their scope.
     *
     * @return the webhook scope
     */
    @Nonnull
    WebhookScope getScope();

    /**
     * @return the date at which the webhook was created or last updated
     */
    @Nonnull
    Date getUpdatedDate();

    /**
     * @return the HTTP(s) URL the webhook should invoke
     */
    @Nonnull
    String getUrl();

    /**
     * @return {@code true} if the webhook is active, {@code false} otherwise. This value is only relevant for Webhooks
     * that are persisted to the database. Webhooks submitted directly to the {@link WebhookService} will be fired
     * regardless of what this method returns.
     */
    boolean isActive();

    /**
     * Whether SSL verification is required for the given URL the webhook should invoke. By default, SSL verification
     * is required.
     *
     * @return {@code true} if SSL verification is required, {@code false} if not
     */
    default boolean isSslVerificationRequired() {
        return true;
    }
}
