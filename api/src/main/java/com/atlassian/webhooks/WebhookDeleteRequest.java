package com.atlassian.webhooks;

import javax.annotation.Nonnull;

/**
 * Request for {@link WebhookService#delete deleting} registered {@link Webhook}s
 *
 * @since 5.0
 */
public class WebhookDeleteRequest extends AbstractBulkWebhookRequest {

    WebhookDeleteRequest(Builder builder) {
        super(builder);
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    public static Builder builder(AbstractBulkWebhookRequest request) {
        return new Builder(request);
    }

    public static class Builder extends AbstractBuilder<Builder> {

        private Builder() {}

        private Builder(@Nonnull AbstractBulkWebhookRequest request) {
            super(request);
        }

        @Nonnull
        public WebhookDeleteRequest build() {
            return new WebhookDeleteRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
