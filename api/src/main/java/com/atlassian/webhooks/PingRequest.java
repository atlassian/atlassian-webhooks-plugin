package com.atlassian.webhooks;

import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.URL;

/**
 * A request to {@link WebhookService#ping send} a ping message to a webhook listener
 *
 * @since 5.0
 */
public class PingRequest {

    private final String url;
    private final WebhookScope scope;
    private final boolean sslVerificationRequired;
    private final WebhookCredentials credentials;

    private PingRequest(Builder builder) {
        url = builder.url;
        scope = builder.scope;
        sslVerificationRequired = builder.sslVerificationRequired;
        credentials = builder.credentials;
    }

    @Nonnull
    public static Builder builder(@Nonnull String url) {
        return new Builder(url);
    }

    @Nonnull
    public static Builder builder(@Nonnull Webhook webhook) {
        Builder builder = new Builder(webhook.getUrl())
                .scope(webhook.getScope())
                .sslVerificationRequired(webhook.isSslVerificationRequired());

        webhook.getCredentials().ifPresent(builder::credentials);

        return builder;
    }

    @Nonnull
    public Optional<WebhookCredentials> getCredentials() {
        return Optional.ofNullable(credentials);
    }

    @NotNull
    public WebhookScope getScope() {
        return scope;
    }

    @NotNull
    @URL
    public String getUrl() {
        return url;
    }

    @NotNull
    public boolean isSslVerificationRequired() {
        return sslVerificationRequired;
    }

    public static class Builder {

        private final String url;
        private WebhookScope scope;
        private boolean sslVerificationRequired;
        private WebhookCredentials credentials;

        public Builder(@Nonnull String url) {
            this.url = url;
            this.scope = WebhookScope.GLOBAL;
            this.sslVerificationRequired = true;
        }

        @Nonnull
        public Builder credentials(@Nullable WebhookCredentials credentials) {
            this.credentials = credentials;
            return this;
        }

        @Nonnull
        public Builder scope(@Nonnull WebhookScope scope) {
            this.scope = scope;
            return this;
        }

        @Nonnull
        public Builder sslVerificationRequired(boolean sslVerificationRequired) {
            this.sslVerificationRequired = sslVerificationRequired;
            return this;
        }

        @Nonnull
        public PingRequest build() {
            return new PingRequest(this);
        }
    }
}
