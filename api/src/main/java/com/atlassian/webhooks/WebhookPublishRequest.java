package com.atlassian.webhooks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import com.atlassian.webhooks.util.BuilderUtil;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

/**
 * Used to request webhooks that match the search parameters are fired with the trigger event as the payload.
 *
 * @since 5.0
 */
public class WebhookPublishRequest {

    private final List<WebhookCallback> callbacks;
    private final Map<String, Object> context;
    private final WebhookEvent event;
    private final Supplier<?> payloadSupplier;

    private WebhookScope eventScope;
    private List<WebhookScope> scopes;
    private Webhook webhook;
    private Optional<Runnable> invocationCompletedCallback;

    private WebhookPublishRequest(SearchBuilder searchBuilder) {
        this.callbacks = searchBuilder.callbacks;
        this.context = searchBuilder.context;
        this.event = searchBuilder.event;
        this.eventScope = searchBuilder.eventScope;
        this.scopes = searchBuilder.scopes;
        this.payloadSupplier = searchBuilder.payloadSupplier;
        this.invocationCompletedCallback = searchBuilder.invocationCompletedCallback;
    }

    private WebhookPublishRequest(SingleWebhookBuilder singleWebhookBuilder) {
        this.callbacks = singleWebhookBuilder.callbacks;
        this.context = singleWebhookBuilder.context;
        this.event = singleWebhookBuilder.event;
        this.payloadSupplier = singleWebhookBuilder.payloadSupplier;
        this.webhook = singleWebhookBuilder.webhook;
        this.eventScope = singleWebhookBuilder.eventScope;
        this.invocationCompletedCallback = singleWebhookBuilder.invocationCompletedCallback;
    }

    @Nonnull
    public static SearchBuilder builder(@Nonnull WebhookEvent event, @Nullable Object payload) {
        return builder(event, payload, Collections.emptyMap());
    }

    /**
     * @since 7.2
     */
    @Nonnull
    public static SearchBuilder builder(@Nonnull WebhookEvent event, @Nullable Supplier<?> payloadSupplier) {
        return builder(event, payloadSupplier, Collections.emptyMap());
    }

    /**
     * @since 7.1
     */
    @Nonnull
    public static SearchBuilder builder(
            @Nonnull WebhookEvent event, @Nullable Object payload, @Nonnull Map<String, Object> context) {
        return new SearchBuilder(event, payload, context);
    }

    /**
     * @since 7.2
     */
    @Nonnull
    public static SearchBuilder builder(
            @Nonnull WebhookEvent event, @Nullable Supplier<?> payloadSupplier, @Nonnull Map<String, Object> context) {
        return new SearchBuilder(event, payloadSupplier, context);
    }

    @Nonnull
    public static SingleWebhookBuilder builder(
            @Nonnull Webhook webhook, @Nonnull WebhookEvent event, @Nullable Object payload) {
        return builder(webhook, event, payload, Collections.emptyMap());
    }

    /**
     * @since 7.2
     */
    @Nonnull
    public static SingleWebhookBuilder builder(
            @Nonnull Webhook webhook, @Nonnull WebhookEvent event, @Nullable Supplier<?> payloadSupplier) {
        return builder(webhook, event, payloadSupplier, Collections.emptyMap());
    }

    /**
     * @since 7.1
     */
    @Nonnull
    public static SingleWebhookBuilder builder(
            @Nonnull Webhook webhook,
            @Nonnull WebhookEvent event,
            @Nullable Object payload,
            @Nonnull Map<String, Object> context) {
        return new SingleWebhookBuilder(webhook, event, payload, context);
    }

    /**
     * @since 7.2
     */
    @Nonnull
    public static SingleWebhookBuilder builder(
            @Nonnull Webhook webhook,
            @Nonnull WebhookEvent event,
            @Nullable Supplier<?> payloadSupplier,
            @Nonnull Map<String, Object> context) {
        return new SingleWebhookBuilder(webhook, event, payloadSupplier, context);
    }

    @Nonnull
    public List<WebhookCallback> getCallbacks() {
        return callbacks;
    }

    /**
     * @since 7.1
     */
    @Nonnull
    public Map<String, Object> getContext() {
        return context;
    }

    @NotNull
    public WebhookEvent getEvent() {
        return event;
    }

    /**
     * @since 7.1
     */
    @Nonnull
    public Optional<WebhookScope> getEventScope() {
        return ofNullable(eventScope);
    }

    @Nonnull
    public Optional<Object> getPayload() {
        Object payload = ofNullable(payloadSupplier).map(Supplier::get).orElse(null);
        return ofNullable(payload);
    }

    @Nonnull
    public List<WebhookScope> getScopes() {
        return scopes;
    }

    @Nonnull
    public Optional<Webhook> getWebhook() {
        return ofNullable(webhook);
    }

    public Optional<Runnable> getInvocationCompletedCallback() {
        return invocationCompletedCallback;
    }

    public static class SearchBuilder {

        private final List<WebhookCallback> callbacks;
        private final Map<String, Object> context;
        private final Supplier<?> payloadSupplier;
        private final List<WebhookScope> scopes;

        private WebhookEvent event;
        private WebhookScope eventScope;
        private Optional<Runnable> invocationCompletedCallback = Optional.empty();

        public SearchBuilder(@Nonnull WebhookEvent event, @Nullable Object payload) {
            this(event, payload, Collections.emptyMap());
        }

        /**
         * @since 7.1
         */
        public SearchBuilder(
                @Nonnull WebhookEvent event, @Nullable Object payload, @Nonnull Map<String, Object> context) {
            this(event, () -> payload, context);
        }

        public SearchBuilder(
                @Nonnull WebhookEvent event,
                @Nullable Supplier<?> payloadSupplier,
                @Nonnull Map<String, Object> context) {
            this.callbacks = new ArrayList<>();
            this.context = context;
            this.event = requireNonNull(event, "event");
            this.payloadSupplier = payloadSupplier;
            this.scopes = new ArrayList<>();
        }

        @Nonnull
        public WebhookPublishRequest build() {
            return new WebhookPublishRequest(this);
        }

        @Nonnull
        public SearchBuilder callback(@Nonnull WebhookCallback value, @Nonnull WebhookCallback... values) {
            BuilderUtil.addIf(Objects::nonNull, callbacks, value, values);

            return this;
        }

        @Nonnull
        public SearchBuilder eventScope(@Nonnull WebhookScope value) {
            this.eventScope = value;
            return this;
        }

        @Nonnull
        public SearchBuilder scopes(@Nonnull WebhookScope value, @Nonnull WebhookScope... values) {
            BuilderUtil.addIf(Objects::nonNull, scopes, value, values);

            return this;
        }

        @Nonnull
        public SearchBuilder invocationCompletedCallback(Runnable callback) {
            this.invocationCompletedCallback = Optional.of(callback);
            return this;
        }
    }

    public static class SingleWebhookBuilder {

        private final List<WebhookCallback> callbacks;
        private final Map<String, Object> context;
        private final WebhookEvent event;
        private final Supplier<?> payloadSupplier;
        private final Webhook webhook;
        private Optional<Runnable> invocationCompletedCallback = Optional.empty();
        private WebhookScope eventScope;

        public SingleWebhookBuilder(@Nonnull Webhook webhook, @Nonnull WebhookEvent event, @Nullable Object payload) {
            this(webhook, event, payload, Collections.emptyMap());
        }

        /**
         * @since 7.1
         */
        public SingleWebhookBuilder(
                @Nonnull Webhook webhook,
                @Nonnull WebhookEvent event,
                @Nullable Object payload,
                @Nonnull Map<String, Object> context) {
            this(webhook, event, () -> payload, context);
        }

        public SingleWebhookBuilder(
                @Nonnull Webhook webhook,
                @Nonnull WebhookEvent event,
                @Nullable Supplier<?> payloadSupplier,
                @Nonnull Map<String, Object> context) {
            this.context = requireNonNull(context, "context");
            this.event = requireNonNull(event, "event");
            this.payloadSupplier = payloadSupplier;
            this.webhook = requireNonNull(webhook, "webhook");

            callbacks = new ArrayList<>();
        }

        @Nonnull
        public WebhookPublishRequest build() {
            return new WebhookPublishRequest(this);
        }

        @Nonnull
        public SingleWebhookBuilder callback(@Nonnull WebhookCallback value, @Nonnull WebhookCallback... values) {
            BuilderUtil.addIf(Objects::nonNull, callbacks, value, values);

            return this;
        }

        @Nonnull
        public SingleWebhookBuilder invocationCompletedCallback(Runnable callback) {
            this.invocationCompletedCallback = Optional.of(callback);
            return this;
        }

        @Nonnull
        public SingleWebhookBuilder eventScope(@Nonnull WebhookScope value) {
            this.eventScope = value;
            return this;
        }
    }
}
