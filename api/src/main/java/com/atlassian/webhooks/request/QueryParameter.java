package com.atlassian.webhooks.request;

import java.util.Optional;
import javax.annotation.Nonnull;

/**
 * HTTP request query parameter
 *
 * @since 5.0
 */
public interface QueryParameter {

    @Nonnull
    String getName();

    @Nonnull
    Optional<String> getValue();
}
