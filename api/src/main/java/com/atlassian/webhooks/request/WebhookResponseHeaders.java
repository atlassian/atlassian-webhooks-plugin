package com.atlassian.webhooks.request;

import java.util.Map;
import javax.annotation.Nonnull;

/**
 * An HTTP client agnostic representation of response headers from a request
 *
 * @since 5.0
 */
public interface WebhookResponseHeaders {

    /**
     * Get a header values for the provided name
     *
     * @param name name of the header
     * @return values for the {@code name} provided
     */
    @Nonnull
    String getHeader(@Nonnull String name);

    /**
     * Get all headers in the response
     *
     * @return the headers in the response
     */
    @Nonnull
    Map<String, String> getHeaders();
}
