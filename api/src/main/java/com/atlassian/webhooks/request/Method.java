package com.atlassian.webhooks.request;

/**
 * The HTTP method used to dispatch the webhook
 *
 * @since 5.0
 */
public enum Method {
    GET,
    POST,
    PUT,
    DELETE,
    OPTIONS,
    HEAD
}
