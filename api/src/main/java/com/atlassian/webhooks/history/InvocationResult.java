package com.atlassian.webhooks.history;

import javax.annotation.Nonnull;

/**
 * Information on the result of {@link InvocationHistory past invocation}
 *
 * @since 6.1
 */
public interface InvocationResult {

    /**
     * @return the kind of result - either {@link InvocationOutcome#SUCCESS success},
     * {@link InvocationOutcome#FAILURE failure} or an {@link InvocationOutcome#ERROR error}
     */
    @Nonnull
    InvocationOutcome getOutcome();

    /**
     * @return a concise description of the invocation's result e.g. "200 OK" or "400 BAD REQUEST" or
     * "NullPointerException: foo must not be null"
     */
    @Nonnull
    String getDescription();
}
