package com.atlassian.webhooks.history;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Nonnull;

import com.atlassian.webhooks.NoSuchWebhookException;
import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookScope;

/**
 * Provides various historical information about {@link com.atlassian.webhooks.WebhookInvocation invocations}
 * for {@link Webhook webhooks}.
 *
 * @since 6.1
 */
public interface InvocationHistoryService {

    /**
     * @return a historical summary of invocations for the supplied {@link Webhook webhook} restricted to only those
     *         invoked for the supplied {@link WebhookEvent event} if non-null
     * @throws NoSuchWebhookException if history is requested for a webhook that does not exist
     */
    @Nonnull
    InvocationHistory get(@Nonnull InvocationHistoryRequest request);

    /**
     * @return historical summaries of invocations for all {@link WebhookEvent events} for the supplied
     *         {@link Webhook webhook}
     * @throws NoSuchWebhookException if history is requested for a webhook that does not exist
     */
    @Nonnull
    Map<WebhookEvent, InvocationHistory> getByEvent(@Nonnull InvocationHistoryByEventRequest request);

    /**
     * Returns a map of Webhook invocation histories for a specified default number of days going back from today
     * based on UTC
     * @param webhookIds the IDs of the webhooks for which the invocation history is requested
     * @return a {@link Map map} of {@link Webhook#getId() webhook IDs} to the corresponding
     *         {@link InvocationHistory invocation history}
     */
    @Nonnull
    Map<Integer, InvocationHistory> getByWebhook(Collection<Integer> webhookIds);

    /**
     * Returns a map of Webhook invocation histories for a specified default number of days going back from today
     * based on UTC
     * @param webhookIds the IDs of the webhooks for which the invocation history is requested
     * @param scope the scope for which the invocation history is requested
     * @return a {@link Map map} of {@link Webhook#getId() webhook IDs} to the corresponding
     *         {@link InvocationHistory invocation history}
     * @since 7.1
     */
    @Nonnull
    Map<Integer, InvocationHistory> getByWebhookAndScope(Collection<Integer> webhookIds, WebhookScope scope);

    /**
     * Returns a map of Webhook invocation histories for a specified number of days going back from today based on UTC
     * @param webhookIds the IDs of the webhooks for which the invocation history is requested
     * @param days the number of preceding days inclusive of today in UTC to retrieve history for.
     *             For example, 1 will give data starting from yesterday's date in UTC and 0 will include the
     *             data of today's date based on UTC
     * @return a {@link Map map} of {@link Webhook#getId() webhook IDs} to the corresponding
     *         {@link InvocationHistory invocation history} for the number of preceding days
     */
    @Nonnull
    Map<Integer, InvocationHistory> getByWebhookForDays(Collection<Integer> webhookIds, int days);

    /**
     * Returns a map of Webhook invocation histories for a specified number of days going back from today based on UTC
     * @param webhookIds the IDs of the webhooks for which the invocation history is requested
     * @param scope the scope for which the invocation history is requested
     * @param days the number of preceding days inclusive of today in UTC to retrieve history for.
     *             For example, 1 will give data starting from yesterday's date in UTC and 0 will include the
     *             data of today's date based on UTC
     * @return a {@link Map map} of {@link Webhook#getId() webhook IDs} to the corresponding
     *         {@link InvocationHistory invocation history} for the number of preceding days
     * @since 7.1
     */
    @Nonnull
    Map<Integer, InvocationHistory> getByWebhookAndScopeForDays(
            Collection<Integer> webhookIds, WebhookScope scope, int days);

    /**
     * @return the most recent {@link DetailedInvocation webhook invocation} matching the specified
     *         {@link InvocationOutcome result} and other requested criteria or
     *         {@link Optional#empty() an empty result} if there have been no such invocations
     * @throws NoSuchWebhookException if invocation data is requested for a webhook that does not exist
     */
    @Nonnull
    Optional<DetailedInvocation> getLatestInvocation(@Nonnull HistoricalInvocationRequest request);
}
