package com.atlassian.webhooks.history;

import java.util.Map;
import java.util.Optional;
import javax.annotation.Nonnull;

/**
 * The details of a {@link DetailedInvocation invocation's} HTTP request
 *
 * @since 6.1
 */
public interface DetailedInvocationRequest extends InvocationRequest {

    /**
     * @return the request body, if any
     */
    @Nonnull
    Optional<String> getBody();

    /**
     * @return the HTTP headers sent with the request
     */
    @Nonnull
    Map<String, String> getHeaders();
}
