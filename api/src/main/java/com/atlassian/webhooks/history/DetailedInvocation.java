package com.atlassian.webhooks.history;

import javax.annotation.Nonnull;

import com.atlassian.webhooks.WebhookInvocation;

/**
 * A detailed {@link WebhookInvocation webhook invocation} including detailed HTTP request, response and error
 * information
 *
 * @since 6.1
 */
public interface DetailedInvocation extends HistoricalInvocation {

    /**
     * @return the details of the invocation's HTTP request
     */
    @Nonnull
    DetailedInvocationRequest getRequest();

    /**
     * @return the details of the result of the invocation - the {@link DetailedInvocationResponse HTTP response}
     * if one was received or the {@link DetailedInvocationError error} otherwise
     */
    @Nonnull
    DetailedInvocationResult getResult();
}
