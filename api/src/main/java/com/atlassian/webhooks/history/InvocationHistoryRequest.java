package com.atlassian.webhooks.history;

import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookInvocation;

import static java.util.Optional.ofNullable;

/**
 * A request for retrieving the {@link InvocationHistory history} of {@link WebhookInvocation invocations}
 * for a {@link Webhook webhook}
 *
 * @since 6.1
 */
public class InvocationHistoryRequest {

    private final String eventId;
    private final int webhookId;

    private InvocationHistoryRequest(Builder builder) {
        eventId = builder.eventId;
        webhookId = builder.webhookId;
    }

    @Nonnull
    public static Builder builder() {
        return new Builder();
    }

    @Nonnull
    public Optional<String> getEventId() {
        return ofNullable(eventId);
    }

    public int getWebhookId() {
        return webhookId;
    }

    public static class Builder {

        private String eventId;
        private int webhookId;

        @Nonnull
        public InvocationHistoryRequest build() {
            return new InvocationHistoryRequest(this);
        }

        @Nonnull
        public Builder eventId(@Nullable String value) {
            eventId = value;
            return this;
        }

        @Nonnull
        public Builder webhookId(int id) {
            webhookId = id;
            return this;
        }
    }
}
