package com.atlassian.webhooks.history;

import java.util.Map;
import java.util.Optional;
import javax.annotation.Nonnull;

/**
 * The details of an {@link DetailedInvocation invocation's} HTTP response
 *
 * @since 6.1
 */
public interface DetailedInvocationResponse extends DetailedInvocationResult {

    /**
     * @return the response body, if any
     */
    @Nonnull
    Optional<String> getBody();

    /**
     * @return the HTTP headers sent with the response
     */
    @Nonnull
    Map<String, String> getHeaders();

    /**
     * @return the HTTP status code
     */
    int getStatusCode();
}
