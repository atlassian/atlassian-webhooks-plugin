package com.atlassian.webhooks;

import java.util.Optional;
import javax.annotation.Nonnull;

/**
 * Credentials for authenticated webhook URLs.
 *
 * @since 7.1
 */
public interface WebhookCredentials {

    /**
     * @return the password for the authenticated webhook URL, or {@link Optional#empty()} if it does not exist
     */
    @Nonnull
    Optional<String> getPassword();

    /**
     * @return the username for the authenticated webhook URL, or {@link Optional#empty()} if it does not exist
     */
    @Nonnull
    Optional<String> getUsername();
}
