package com.atlassian.webhooks.diagnostics;

import javax.annotation.Nonnull;

import com.atlassian.webhooks.WebhookEvent;

/**
 * Built-in {@link WebhookEvent} that's used for {@link com.atlassian.webhooks.WebhookService#ping testing} webhooks.
 *
 * @since 5.0
 */
public enum WebhookDiagnosticsEvent implements WebhookEvent {
    PING("diagnostics:ping");

    private static final String I18N_PREFIX = "webhooks.diagnostics.event.";
    private final String i18nKey;
    private final String id;

    WebhookDiagnosticsEvent(String id) {
        this.id = id;
        this.i18nKey = I18N_PREFIX + name().toLowerCase();
    }

    @Nonnull
    @Override
    public String getI18nKey() {
        return i18nKey;
    }

    @Nonnull
    @Override
    public String getId() {
        return id;
    }
}
