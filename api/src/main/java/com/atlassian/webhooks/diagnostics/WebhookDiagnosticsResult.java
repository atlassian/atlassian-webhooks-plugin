package com.atlassian.webhooks.diagnostics;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

/**
 * The result of {@link com.atlassian.webhooks.WebhookService#ping sending} a {@link WebhookDiagnosticsEvent#PING ping}
 * message to a webhook.
 *
 * @since 5.0
 */
public class WebhookDiagnosticsResult {

    private WebhookHttpRequest request;
    private WebhookHttpResponse response;
    private Throwable error;

    private WebhookDiagnosticsResult() {}

    @Nonnull
    public static WebhookDiagnosticsResult build(WebhookHttpRequest request, WebhookHttpResponse response) {
        WebhookDiagnosticsResult result = new WebhookDiagnosticsResult();
        result.request = request;
        result.response = response;
        return result;
    }

    @Nonnull
    public static WebhookDiagnosticsResult build(WebhookHttpRequest request, Throwable error) {
        WebhookDiagnosticsResult result = new WebhookDiagnosticsResult();
        result.request = request;
        result.error = error;
        return result;
    }

    /**
     * @return the error that occurred during diagnosing the webhook or {@code null} if no error occurred
     */
    @Nullable
    public Throwable getError() {
        return error;
    }

    /**
     * @return the request dispatched to the HTTP client
     */
    @Nonnull
    public WebhookHttpRequest getRequest() {
        return request;
    }

    /**
     * @return the response from the remote endpoint. {@code null} if an error occurred.
     */
    @Nullable
    public WebhookHttpResponse getResponse() {
        return response;
    }

    /**
     * @return {@code true} if an error occurred while processing the webhook, otherwise {@code false}
     */
    public boolean isError() {
        return error != null;
    }
}
