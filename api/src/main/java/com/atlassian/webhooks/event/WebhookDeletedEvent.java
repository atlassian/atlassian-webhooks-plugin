package com.atlassian.webhooks.event;

import javax.annotation.Nonnull;

import com.atlassian.webhooks.Webhook;

import static java.util.Objects.requireNonNull;

/**
 * This event is raised after a webhook has been deleted.
 *
 * @since 5.0
 */
public class WebhookDeletedEvent extends AbstractWebhookEvent {

    private final Webhook webhook;

    public WebhookDeletedEvent(@Nonnull Object source, @Nonnull Webhook webhook) {
        super(requireNonNull(source, "source"));
        this.webhook = requireNonNull(webhook, "webhook");
    }

    @Nonnull
    public Webhook getWebhook() {
        return webhook;
    }
}
