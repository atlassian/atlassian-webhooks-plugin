package com.atlassian.webhooks.event;

import javax.annotation.Nonnull;

import com.atlassian.event.api.AsynchronousPreferred;
import com.atlassian.webhooks.Webhook;

import static java.util.Objects.requireNonNull;

/**
 * This event is raised after a webhook has been created.
 *
 * @since 5.0
 */
@AsynchronousPreferred
public class WebhookCreatedEvent extends AbstractWebhookEvent {

    private final Webhook webhook;

    public WebhookCreatedEvent(@Nonnull Object source, @Nonnull Webhook webhook) {
        super(requireNonNull(source, "source"));
        this.webhook = requireNonNull(webhook, "webhook");
    }

    @Nonnull
    public Webhook getWebhook() {
        return webhook;
    }
}
