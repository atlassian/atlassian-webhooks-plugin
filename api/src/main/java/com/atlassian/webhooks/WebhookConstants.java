package com.atlassian.webhooks;

/**
 * Constants used in the webhooks API
 *
 * @since 5.0
 */
public class WebhookConstants {

    private WebhookConstants() {
        throw new UnsupportedOperationException("WebhookConstants is a utility class that should not be instantiated");
    }

    /**
     * Key used in {@link Webhook#getConfiguration()} that configures whether the payload for the webhook should be
     * empty. Possible values are {@code "true"}, {@code "false"} or {@code null} (which is interpreted as
     * {@code false})
     */
    public static final String CONFIG_EXCLUDE_BODY = "excludeBody";

    /**
     * Key used in {@link Webhook#getConfiguration()} that contains the plugin key of the plugin that registered the
     * &lt;webhook&gt; module.
     */
    public static final String CONFIG_PLUGIN_KEY = "pluginKey";
}
