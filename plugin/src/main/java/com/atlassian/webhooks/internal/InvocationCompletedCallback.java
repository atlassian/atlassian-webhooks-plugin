package com.atlassian.webhooks.internal;

import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.Nonnull;

import com.atlassian.webhooks.WebhookCallback;
import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

class InvocationCompletedCallback implements WebhookCallback {

    private final AtomicInteger counter;
    private final Runnable callback;

    public InvocationCompletedCallback(int expectedInvocationCount, Runnable callback) {
        counter = new AtomicInteger(expectedInvocationCount);
        this.callback = callback;
    }

    @Override
    public void onError(WebhookHttpRequest request, @Nonnull Throwable error, @Nonnull WebhookInvocation invocation) {
        decrementAndCallback();
    }

    @Override
    public void onFailure(
            @Nonnull WebhookHttpRequest request,
            @Nonnull WebhookHttpResponse response,
            @Nonnull WebhookInvocation invocation) {
        decrementAndCallback();
    }

    @Override
    public void onSuccess(
            @Nonnull WebhookHttpRequest request,
            @Nonnull WebhookHttpResponse response,
            @Nonnull WebhookInvocation invocation) {
        decrementAndCallback();
    }

    private void decrementAndCallback() {
        if (counter.decrementAndGet() == 0) {
            callback.run();
        }
    }
}
