package com.atlassian.webhooks.internal.dao.ao;

import java.util.Date;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.OneToMany;
import net.java.ao.schema.Default;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

@Table(AoWebhook.TABLE_NAME)
public interface AoWebhook extends Entity {

    String ACTIVE_COLUMN = "ACTIVE";
    String CREATED_COLUMN = "CREATED";
    String NAME_COLUMN = "NAME";
    String PASSWORD_COLUMN = "PASSWORD";
    String SCOPE_ID_COLUMN = "SCOPE_ID";
    String SCOPE_TYPE_COLUMN = "SCOPE_TYPE";
    String SSL_VERIFICATION_REQUIRED_COLUMN = "SSL_VERIFICATION_REQUIRED";
    String TABLE_NAME = "WEBHOOK";
    String UPDATED_COLUMN = "UPDATED";
    String URL_COLUMN = "URL";
    String USERNAME_COLUMN = "USERNAME";

    @NotNull
    @OneToMany(reverse = "getWebhook")
    AoWebhookConfigurationEntry[] getConfiguration();

    @NotNull
    @OneToMany(reverse = "getWebhook")
    AoWebhookEvent[] getEvents();

    @Accessor(CREATED_COLUMN)
    @NotNull
    Date getCreatedDate();

    @Accessor(NAME_COLUMN)
    @NotNull
    @StringLength(255)
    String getName();

    @Accessor(PASSWORD_COLUMN)
    @Nullable
    String getPassword();

    @Accessor(SCOPE_ID_COLUMN)
    @StringLength(255)
    String getScopeId();

    @Accessor(SCOPE_TYPE_COLUMN)
    @NotNull
    @StringLength(255)
    String getScopeType();

    @Accessor(UPDATED_COLUMN)
    @NotNull
    Date getUpdatedDate();

    @Accessor(URL_COLUMN)
    @NotNull
    @StringLength(StringLength.UNLIMITED)
    String getUrl();

    @Accessor(USERNAME_COLUMN)
    @Nullable
    String getUsername();

    @Accessor(ACTIVE_COLUMN)
    boolean isActive();

    @Accessor(SSL_VERIFICATION_REQUIRED_COLUMN)
    @Default("true")
    @NotNull
    boolean isSslVerificationRequired();

    @Mutator(ACTIVE_COLUMN)
    void setActive(boolean active);

    @Mutator(NAME_COLUMN)
    void setName(@Nonnull String name);

    @Mutator(PASSWORD_COLUMN)
    void setPassword(@Nullable String password);

    @Mutator(SCOPE_ID_COLUMN)
    void setScopeId(@Nullable String id);

    @Mutator(SCOPE_TYPE_COLUMN)
    void setScopeType(@Nullable String type);

    @Mutator(SSL_VERIFICATION_REQUIRED_COLUMN)
    void setSslVerificationRequired(boolean sslVerificationRequired);

    @Mutator(UPDATED_COLUMN)
    void setUpdatedDate(@Nonnull Date date);

    @Mutator(URL_COLUMN)
    void setUrl(@Nonnull String url);

    @Mutator(USERNAME_COLUMN)
    void setUsername(@Nullable String username);
}
