package com.atlassian.webhooks.internal.history;

import java.util.Optional;
import javax.annotation.Nonnull;

import com.atlassian.webhooks.history.HistoricalInvocation;
import com.atlassian.webhooks.history.InvocationCounts;
import com.atlassian.webhooks.history.InvocationHistory;

import static java.util.Objects.requireNonNull;

public class SimpleInvocationHistory implements InvocationHistory {

    private final HistoricalInvocation lastError;
    private final HistoricalInvocation lastFailure;
    private final HistoricalInvocation lastSuccess;
    private final InvocationCounts counts;

    public SimpleInvocationHistory(
            @Nonnull InvocationCounts counts,
            HistoricalInvocation lastError,
            HistoricalInvocation lastFailure,
            HistoricalInvocation lastSuccess) {
        this.counts = requireNonNull(counts, "counts");
        this.lastError = lastError;
        this.lastFailure = lastFailure;
        this.lastSuccess = lastSuccess;
    }

    @Nonnull
    @Override
    public InvocationCounts getCounts() {
        return counts;
    }

    @Nonnull
    @Override
    public Optional<HistoricalInvocation> getLastError() {
        return Optional.ofNullable(lastError);
    }

    @Nonnull
    @Override
    public Optional<HistoricalInvocation> getLastFailure() {
        return Optional.ofNullable(lastFailure);
    }

    @Nonnull
    @Override
    public Optional<HistoricalInvocation> getLastSuccess() {
        return Optional.ofNullable(lastSuccess);
    }
}
