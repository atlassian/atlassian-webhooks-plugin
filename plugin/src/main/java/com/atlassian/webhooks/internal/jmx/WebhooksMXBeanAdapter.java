package com.atlassian.webhooks.internal.jmx;

import java.util.Optional;

import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.WebhookStatistics;
import com.atlassian.webhooks.internal.publish.WebhookDispatcher;

public class WebhooksMXBeanAdapter implements WebhooksMXBean {

    private final WebhookDispatcher dispatcher;
    private final WebhookService webhookService;

    WebhooksMXBeanAdapter(WebhookDispatcher dispatcher, WebhookService webhookService) {
        this.dispatcher = dispatcher;
        this.webhookService = webhookService;
    }

    @Override
    public long getDispatchCount() {
        return getStatistics().map(WebhookStatistics::getDispatchedCount).orElse(-1L);
    }

    @Override
    public long getDispatchErrorCount() {
        return getStatistics().map(WebhookStatistics::getDispatchErrorCount).orElse(-1L);
    }

    @Override
    public long getDispatchFailureCount() {
        return getStatistics().map(WebhookStatistics::getDispatchFailureCount).orElse(-1L);
    }

    @Override
    public long getDispatchInFlightCount() {
        return dispatcher.getInFlightCount();
    }

    @Override
    public long getDispatchLastRejectedTimestamp() {
        return dispatcher.getLastRejectedTimestamp();
    }

    @Override
    public long getDispatchRejectedCount() {
        return getStatistics().map(WebhookStatistics::getDispatchRejectedCount).orElse(-1L);
    }

    @Override
    public long getDispatchSuccessCount() {
        return getStatistics().map(WebhookStatistics::getDispatchSuccessCount).orElse(-1L);
    }

    @Override
    public long getPublishCount() {
        return getStatistics().map(WebhookStatistics::getPublishedCount).orElse(-1L);
    }

    private Optional<WebhookStatistics> getStatistics() {
        return webhookService.getStatistics();
    }
}
