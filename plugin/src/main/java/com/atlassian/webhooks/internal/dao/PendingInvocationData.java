package com.atlassian.webhooks.internal.dao;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.atlassian.webhooks.history.DetailedInvocation;

/**
 * Represents a webhook's invocation data that has not yet been persisted.
 */
class PendingInvocationData {

    private final AtomicInteger errorCount;
    private final ReadWriteLock freezeLock;
    private final AtomicInteger failureCount;
    private final AtomicInteger successCount;
    private boolean frozen;
    private AtomicReference<DetailedInvocation> latestError;
    private AtomicReference<DetailedInvocation> latestFailure;
    private AtomicReference<DetailedInvocation> latestSuccess;

    PendingInvocationData() {
        errorCount = new AtomicInteger();
        failureCount = new AtomicInteger();
        freezeLock = new ReentrantReadWriteLock();
        successCount = new AtomicInteger();
        latestError = new AtomicReference<>();
        latestFailure = new AtomicReference<>();
        latestSuccess = new AtomicReference<>();
    }

    /**
     * Freezes the instance, preventing further modifications
     */
    void freeze() {
        if (frozen) {
            // once frozen, the instance can never be unfrozen
            return;
        }
        Lock lock = freezeLock.writeLock();
        lock.lock();
        frozen = true;
        lock.unlock();
    }

    int getErrorCount() {
        return errorCount.get();
    }

    int getFailureCount() {
        return failureCount.get();
    }

    DetailedInvocation getLatestError() {
        return latestError.get();
    }

    DetailedInvocation getLatestFailure() {
        return latestFailure.get();
    }

    DetailedInvocation getLatestSuccess() {
        return latestSuccess.get();
    }

    int getSuccessCount() {
        return successCount.get();
    }

    boolean addAll(PendingInvocationData other) {
        return unlessFrozen(() -> {
            errorCount.accumulateAndGet(other.getErrorCount(), PendingInvocationData::add);
            latestError.accumulateAndGet(other.getLatestError(), PendingInvocationData::getLatest);
            failureCount.accumulateAndGet(other.getFailureCount(), PendingInvocationData::add);
            latestFailure.accumulateAndGet(other.getLatestFailure(), PendingInvocationData::getLatest);
            successCount.accumulateAndGet(other.getSuccessCount(), PendingInvocationData::add);
            latestSuccess.accumulateAndGet(other.getLatestSuccess(), PendingInvocationData::getLatest);
        });
    }

    /**
     * @param errors the number of errors to add
     * @param failures the number of failures to add
     * @param successes the number of successes to add
     *
     * @return {@code true} if successful, {@code false} if this instance is frozen
     */
    boolean addCounts(int errors, int failures, int successes) {
        return unlessFrozen(() -> {
            errorCount.addAndGet(errors);
            failureCount.addAndGet(failures);
            successCount.addAndGet(successes);
        });
    }

    /**
     * @param invocation the invocation to save
     * @return {@code true} if successful, {@code false} if this instance is frozen
     */
    boolean onInvocation(DetailedInvocation invocation) {
        return unlessFrozen(() -> {
            switch (invocation.getResult().getOutcome()) {
                case SUCCESS:
                    latestSuccess.accumulateAndGet(invocation, PendingInvocationData::getLatest);
                    break;
                case FAILURE:
                    latestFailure.accumulateAndGet(invocation, PendingInvocationData::getLatest);
                    break;
                case ERROR:
                    latestError.accumulateAndGet(invocation, PendingInvocationData::getLatest);
            }
        });
    }

    /**
     * Executes {@code action} unless the instance has already been frozen
     *
     * @param action the action to execute
     * @return {@code true} if the action was executed
     */
    private boolean unlessFrozen(Runnable action) {
        if (frozen) {
            return false;
        }

        // acquire a read lock to ensure that the action is fully completed before the instance can be frozen
        Lock readLock = freezeLock.readLock();
        if (readLock.tryLock() && !frozen) {
            try {
                action.run();
                return true;
            } finally {
                readLock.unlock();
            }
        }
        return false;
    }

    private static DetailedInvocation getLatest(DetailedInvocation first, DetailedInvocation second) {
        if (first == null) {
            return second;
        }
        if (second == null) {
            return first;
        }
        return first.getFinish().isAfter(second.getFinish()) ? first : second;
    }

    private static int add(int value1, int value2) {
        return value1 + value2;
    }
}
