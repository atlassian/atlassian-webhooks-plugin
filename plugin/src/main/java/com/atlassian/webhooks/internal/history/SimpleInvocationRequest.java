package com.atlassian.webhooks.internal.history;

import javax.annotation.Nonnull;

import com.atlassian.webhooks.history.InvocationRequest;
import com.atlassian.webhooks.request.Method;

import static java.util.Objects.requireNonNull;

public class SimpleInvocationRequest implements InvocationRequest {

    private final Method method;
    private final String url;

    public SimpleInvocationRequest(Method method, String url) {
        this.method = requireNonNull(method, "method");
        this.url = requireNonNull(url, "url");
    }

    @Nonnull
    @Override
    public Method getMethod() {
        return method;
    }

    @Nonnull
    @Override
    public String getUrl() {
        return url;
    }
}
