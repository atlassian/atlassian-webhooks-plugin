package com.atlassian.webhooks.internal.module;

import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.CharMatcher;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookConstants;
import com.atlassian.webhooks.WebhookCreateRequest;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.internal.Validator;
import com.atlassian.webhooks.internal.model.SimpleWebhook;
import com.atlassian.webhooks.internal.model.SimpleWebhookScope;
import com.atlassian.webhooks.internal.model.UnknownWebhookEvent;
import com.atlassian.webhooks.module.WebhookModuleDescriptor;

import static java.util.Optional.empty;
import static java.util.Optional.of;

import static com.atlassian.webhooks.internal.UrlConstructor.escapeCurlyBraces;

public class SimpleWebhookModuleDescriptor extends AbstractModuleDescriptor<Webhook>
        implements WebhookModuleDescriptor {

    private static final AtomicInteger idGenerator = new AtomicInteger(-1);
    private static final Logger log = LoggerFactory.getLogger(SimpleWebhookModuleDescriptor.class);
    private static final CharMatcher SLASH = CharMatcher.is('/');

    private final ApplicationProperties applicationProperties;
    private final int id;
    private final Validator validator;
    private final WebhookService webhookService;

    private WebhookCreateRequest request;

    private volatile Webhook webhook;

    public SimpleWebhookModuleDescriptor(
            ApplicationProperties applicationProperties,
            ModuleFactory moduleFactory,
            Validator validator,
            WebhookService webhookService) {
        super(moduleFactory);

        this.applicationProperties = applicationProperties;
        this.validator = validator;
        this.webhookService = webhookService;

        // generate a unique ID (in this JVM) that cannot be confused with an ID from the database.
        // Module webhook IDs are < 0, DB IDs are > 0.
        id = idGenerator.decrementAndGet();
    }

    @Override
    public Webhook getModule() {
        return getOrCreateWebhook();
    }

    @Override
    public void disabled() {
        super.disabled();

        webhook = null;
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        super.init(plugin, element);

        // Use the create request as an intermediate step to allow all provided attributes to be validated
        request = WebhookCreateRequest.builder()
                .name(getKey())
                .configuration(WebhookConstants.CONFIG_PLUGIN_KEY, getPluginKey())
                .configuration(getConfiguration(element))
                .event(getEvents(getAttributeOrThrow(element, "event")))
                .scope(getScope(element.element("scope")))
                .url(toUrl(getAttributeOrThrow(element, "url")))
                .build();
    }

    private static Optional<String> getAttribute(Element element, String name) {
        String attribute = element.attributeValue(name);
        return attribute == null ? empty() : Optional.of(attribute);
    }

    private static String getAttributeOrThrow(Element element, String name) throws PluginParseException {
        return getAttribute(element, name)
                .orElseThrow(() -> new PluginParseException(
                        "Required attribute '" + name + "' on '" + element.getName() + "' is missing"));
    }

    private Map<String, String> getConfiguration(Element element) {
        Map<String, String> result = new HashMap<>();
        Multimap<String, String> parameters = getParameters(element);

        // parse and remove the legacy attributes and parameters that webhooks v2 supported
        getExcludeBody(element, parameters).ifPresent(value -> result.put("excludeBody", value.toString()));
        getFilter(element, parameters).ifPresent(value -> result.put("filter", value));

        parameters
                .asMap()
                .forEach((key, values) -> result.put(key, values.stream().collect(Collectors.joining(","))));

        return result;
    }

    private Set<WebhookEvent> getEvents(String eventId) {
        if (eventId.endsWith("*")) {
            String prefix = eventId.substring(0, eventId.length() - 1);
            Set<WebhookEvent> events = webhookService.getEvents().stream()
                    .filter(event -> event.getId().startsWith(prefix))
                    .collect(Collectors.toSet());
            if (!events.isEmpty()) {
                return events;
            }
        }

        WebhookEvent event = webhookService.getEvent(eventId).orElseGet(() -> new UnknownWebhookEvent(eventId));
        return Collections.singleton(event);
    }

    private Optional<Boolean> getExcludeBody(Element element, Multimap<String, String> parameters) {
        Optional<Boolean> excludeBodyAttr = getAttribute(element, "excludeBody").map(Boolean::valueOf);
        Collection<String> excludeBodyParam = parameters.removeAll("excludeBody");
        Collection<String> excludeIssueDetailsParam = parameters.removeAll("excludeIssueDetails");

        if (excludeBodyAttr.isPresent() || !excludeBodyParam.isEmpty() || !excludeIssueDetailsParam.isEmpty()) {
            // merge the settings
            Boolean excludeBody = excludeBodyAttr.orElse(null);
            if (excludeBodyParam != null) {
                for (String param : excludeBodyParam) {
                    excludeBody = merge(WebhookConstants.CONFIG_EXCLUDE_BODY, excludeBody, Boolean.valueOf(param));
                }
            }
            if (excludeIssueDetailsParam != null) {
                for (String param : excludeIssueDetailsParam) {
                    excludeBody = merge(WebhookConstants.CONFIG_EXCLUDE_BODY, excludeBody, Boolean.valueOf(param));
                }
            }
            return of(excludeBody);
        }
        return empty();
    }

    private Optional<String> getFilter(Element element, Multimap<String, String> parameters) {
        Optional<String> filterAttr = getAttribute(element, "filter");
        Collection<String> filterParam = parameters.removeAll("filter");
        Collection<String> jqlParam = parameters.removeAll("jql");

        if (filterAttr.isPresent() || !filterParam.isEmpty() || !jqlParam.isEmpty()) {
            // merge the settings
            String filter = filterAttr.orElse(null);
            if (filterParam != null) {
                for (String param : filterParam) {
                    filter = merge("filter", filter, param);
                }
            }
            if (jqlParam != null) {
                for (String param : jqlParam) {
                    filter = merge("filter", filter, param);
                }
            }
            return of(filter);
        }
        return empty();
    }

    private Webhook getOrCreateWebhook() {
        if (webhook != null) {
            return webhook;
        }

        synchronized (this) {
            if (webhook != null) {
                return webhook;
            }

            boolean valid;
            try {
                validator.validate(request);
                valid = true;
            } catch (ConstraintViolationException e) {
                String violations = e.getConstraintViolations().stream()
                        .map(violation -> violation.getPropertyPath().toString() + ": " + violation.getMessage())
                        .collect(Collectors.joining(", "));
                log.warn(
                        "De-activating webhook '{}' to '{}' because it's descriptor is invalid ({}).",
                        getCompleteKey(),
                        request.getUrl(),
                        violations);
                valid = false;
            }

            SimpleWebhook.Builder builder = SimpleWebhook.builder()
                    .id(id)
                    .name(request.getName())
                    .active(valid && request.isActive())
                    .configuration(request.getConfiguration())
                    .event(request.getEvents())
                    .scope(request.getScope())
                    .sslVerificationRequired(request.isSslVerificationRequired())
                    .url(request.getUrl());

            request.getCredentials().ifPresent(builder::credentials);

            webhook = builder.build();
        }
        return webhook;
    }

    private Multimap<String, String> getParameters(Element element) {
        ListMultimap<String, String> result =
                MultimapBuilder.hashKeys().arrayListValues().build();

        for (Element param : element.elements("param")) {
            String name = getAttributeOrThrow(param, "name");
            result.put(name, getAttribute(param, "value").orElse(param.getText()));
        }

        return result;
    }

    private WebhookScope getScope(Element scopeElement) {
        if (scopeElement == null) {
            return WebhookScope.GLOBAL;
        }

        return new SimpleWebhookScope(getAttributeOrThrow(scopeElement, "type"), scopeElement.getText());
    }

    private <T> T merge(String paramName, T object1, T object2) {
        if (object1 == null) {
            return object2;
        }
        if (object2 == null) {
            return object1;
        }
        if (object1.equals(object2)) {
            return object1;
        }
        throw new PluginParseException(paramName + " was defined multiple times with different values");
    }

    private String toUrl(String value) {
        try {
            URI uri = new URI(escapeCurlyBraces(value));
            if (!uri.isAbsolute()) {

                // turn it into an absolute URL
                return SLASH.trimTrailingFrom(applicationProperties.getBaseUrl(UrlMode.CANONICAL))
                        + '/'
                        + SLASH.trimLeadingFrom(value);
            }
        } catch (Exception e) {
            // not a valid URI, but the validator should determine that as well and throw an appropriate exception
        }
        return value;
    }
}
