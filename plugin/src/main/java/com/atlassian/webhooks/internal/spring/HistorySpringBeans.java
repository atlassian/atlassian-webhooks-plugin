package com.atlassian.webhooks.internal.spring;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.osgi.framework.ServiceRegistration;

import com.atlassian.event.api.EventListenerRegistrar;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.history.InvocationHistoryService;
import com.atlassian.webhooks.internal.dao.InvocationHistoryDao;
import com.atlassian.webhooks.internal.history.DefaultInvocationHistoryService;
import com.atlassian.webhooks.internal.history.InternalInvocationHistoryService;
import com.atlassian.webhooks.internal.history.WebhookInvocationHistorian;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

@Configuration
@SuppressWarnings("rawtypes")
public class HistorySpringBeans {

    /**
     * InternalInvocationHistoryService for internal use, InvocationHistoryService for external use.
     *
     * @see HistorySpringBeans#exportInvocationHistoryService(InvocationHistoryService)
     */
    @Bean
    public InternalInvocationHistoryService invocationHistoryService(
            @Qualifier("asyncInvocationHistoryDao") InvocationHistoryDao dao,
            EventListenerRegistrar eventListenerRegistrar,
            SchedulerService schedulerService,
            TransactionTemplate txTemplate,
            WebhookService webhookService) {
        return new DefaultInvocationHistoryService(
                dao, eventListenerRegistrar, schedulerService, txTemplate, webhookService);
    }

    @Bean
    public WebhookInvocationHistorian webhookInvocationHistorian(InternalInvocationHistoryService historyService) {
        return new WebhookInvocationHistorian(historyService);
    }

    // Exports

    @Bean
    public FactoryBean<ServiceRegistration> exportInvocationHistoryService(
            final InvocationHistoryService invocationHistoryService) {
        return exportOsgiService(invocationHistoryService, as(InvocationHistoryService.class));
    }
}
