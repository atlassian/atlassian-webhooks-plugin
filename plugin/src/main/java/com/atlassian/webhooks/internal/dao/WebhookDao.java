package com.atlassian.webhooks.internal.dao;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.webhooks.WebhookCreateRequest;
import com.atlassian.webhooks.WebhookSearchRequest;
import com.atlassian.webhooks.WebhookUpdateRequest;
import com.atlassian.webhooks.internal.dao.ao.AoWebhook;

/**
 * A DAO for storing webhooks
 *
 * @since 5.0
 */
public interface WebhookDao {

    /**
     * Store a new webhook with supplied details
     *
     * @param request the webhook details to store
     * @return the stored webhook
     */
    @Nonnull
    AoWebhook create(@Nonnull WebhookCreateRequest request);

    /**
     * Delete a webhook with the provided id
     *
     * @param id the id of webhook
     * @return {@code true} if a webhook was deleted, {@code false} otherwise
     */
    boolean delete(int id);

    /**
     * Delete multiple webhooks
     *
     * @param webhooks the webhooks to delete
     */
    void delete(@Nonnull AoWebhook[] webhooks);

    /**
     * Get a webhook by its id
     *
     * @param id the id of the webhook
     * @return the webhook if one is found, {@code null} otherwise
     */
    @Nullable
    AoWebhook getById(int id);

    /**
     * Get all webhooks that match a search request. All parameters are considered joined by AND. Parameters that take
     * a list of parameters such as {@link WebhookSearchRequest#getScopeTypes()} will be combined by an OR operator.
     *
     * @param search parameters for searching
     * @return an array of matching webhooks
     */
    @Nonnull
    AoWebhook[] search(@Nonnull WebhookSearchRequest search);

    /**
     * Update a webhook with provided details
     *
     * @param id the id of the webhook
     * @param request the updated parameters of the webhook
     * @return the updated webhook if one is found with the provided id, otherwise {@code null}
     */
    @Nullable
    AoWebhook update(int id, @Nonnull WebhookUpdateRequest request);
}
