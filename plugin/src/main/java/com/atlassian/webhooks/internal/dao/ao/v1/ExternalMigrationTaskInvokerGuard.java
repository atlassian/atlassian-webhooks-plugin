package com.atlassian.webhooks.internal.dao.ao.v1;

import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.activeobjects.external.ActiveObjects;

/**
 * This class is responsible for coordinating the data migration process between the atlassian-webhooks-plugin and
 * the jira-webhooks-plugin. It ensures a seamless synchronization of migration tasks across these plugins.
 *
 * <p>Upon completion of an internal upgrade task, the {@code onMigrationFinished} method is invoked. This method
 * either stores a reference to an {@code ActiveObject} handle or, if a handle is already registered, it triggers
 * an externally registered migration task. This mechanism ensures that either the internal state is prepared for
 * future actions, or immediate action is taken if the prerequisites are met.</p>
 *
 * <p>Conversely, the {@code migrationTask} method acts as the counterpart to {@code onMigrationFinished}. It either
 * stores an external migration task for later execution, or, if an {@code ActiveObject} handle is already available,
 * it executes the stored task immediately. This dual functionality facilitates a flexible migration process that
 * can adapt to the availability of necessary resources.</p>
 *
 * <p>It is important to note that the required {@code ActiveObject} instance cannot be provided through Dependency
 * Injection (DI) because the specific type needed is
 * {@code EntityManagedActiveObjects}, not {@code ActiveObjectsDelegate}. This distinction is crucial because
 * the synchronization mechanism relies on accessing the same {@code ActiveObject} instance that is used by
 * the {@code ActiveObjectUpgradeManagerImpl} for data migration tasks. This design choice ensures that the
 * migration process is consistently managed and executed across different components of the system.</p>
 *
 */
public class ExternalMigrationTaskInvokerGuard {

    private static final Logger log = LoggerFactory.getLogger(ExternalMigrationTaskInvokerGuard.class);

    private Consumer<ActiveObjects> migrationTask;
    private ActiveObjects ao;

    public synchronized void migrationTask(Consumer<ActiveObjects> task) {
        if (ao != null) {
            log.info("atlassian-webhooks migration already happened, perform external migration now");
            task.accept(ao);
            ao = null;
        } else {
            if (this.migrationTask != null) {
                log.warn("migration task already registered, ignoring");
            } else {
                this.migrationTask = task;
            }
        }
    }

    public synchronized void onMigrationFinished(ActiveObjects ao) {
        if (migrationTask != null) {
            log.info("External migration task present, invoking");
            migrationTask.accept(ao);
            migrationTask = null;
        } else {
            log.info("No external migration not registered");
            this.ao = ao;
        }
    }
}
