package com.atlassian.webhooks.internal.client;

import java.util.concurrent.CompletableFuture;
import javax.annotation.Nonnull;

import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

public interface RequestExecutor {

    /**
     * Execute the request to the external service
     *
     * @param request details about the request
     * @return a future detailing the response
     */
    @Nonnull
    CompletableFuture<WebhookHttpResponse> execute(@Nonnull WebhookHttpRequest request);
}
