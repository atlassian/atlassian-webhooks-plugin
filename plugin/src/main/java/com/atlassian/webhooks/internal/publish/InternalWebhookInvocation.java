package com.atlassian.webhooks.internal.publish;

import java.util.List;
import javax.annotation.Nonnull;

import com.atlassian.webhooks.WebhookCallback;
import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.internal.client.request.RawRequest;

public interface InternalWebhookInvocation extends WebhookInvocation {

    /**
     * Retrieve the callbacks that have been registered against a specific invocation of a webhook. Internal as
     * retrieving callbacks is only required from the inner workings of the publisher.
     *
     * @return list of registered callbacks
     */
    @Nonnull
    List<WebhookCallback> getCallbacks();

    /**
     * Overridden to return {@link RawRequest.Builder} which allows setting the body.
     *
     * @return the request builder
     */
    @Nonnull
    @Override
    RawRequest.Builder getRequestBuilder();
}
