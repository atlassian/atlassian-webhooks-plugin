package com.atlassian.webhooks.internal.dao.ao;

import javax.annotation.Nonnull;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;

@Table(AoWebhookConfigurationEntry.TABLE_NAME)
public interface AoWebhookConfigurationEntry extends Entity {

    String KEY_COLUMN = "KEY";
    String TABLE_NAME = "WEBHOOK_CONFIG";
    String VALUE_COLUMN = "VALUE";
    String WEBHOOK_COLUMN = "WEBHOOK";

    // This is used to allow foreign key style queries in AO.
    // It's standard for AO to just add 'ID' to the end of the column
    // specified by the Mutator/Accessor
    String WEBHOOK_COLUMN_QUERY = WEBHOOK_COLUMN + "ID";

    @NotNull
    @Accessor(KEY_COLUMN)
    String getKey();

    @NotNull
    @Accessor(VALUE_COLUMN)
    String getValue();

    @NotNull
    @Accessor(WEBHOOK_COLUMN)
    AoWebhook getWebhook();

    @Mutator(KEY_COLUMN)
    void setKey(@Nonnull String value);

    @Mutator(VALUE_COLUMN)
    void setValue(@Nonnull String value);

    @Mutator(WEBHOOK_COLUMN)
    void setWebhook(@Nonnull AoWebhook value);
}
