package com.atlassian.webhooks.internal.dao;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Qualifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Multimap;

import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.WebhooksConfiguration;
import com.atlassian.webhooks.history.DetailedInvocation;
import com.atlassian.webhooks.history.InvocationCounts;
import com.atlassian.webhooks.history.InvocationOutcome;
import com.atlassian.webhooks.internal.WebhookHostAccessor;
import com.atlassian.webhooks.internal.WebhooksLifecycleAware;
import com.atlassian.webhooks.internal.dao.ao.AoHistoricalInvocation;

/**
 * {@link InvocationHistoryDao} implementation that buffers invocation data in memory to be written to the database
 * with a delay. This ensures that storing invocation data is never blocking. In addition, if multiple dispatches
 * happen for the same webhook/event combination before being flushed to the database, only 1 update needs to be done.
 */
public class AsyncInvocationHistoryDao implements InvocationHistoryDao, WebhooksLifecycleAware {

    private static final Logger log = LoggerFactory.getLogger(AsyncInvocationHistoryDao.class);
    private static final int MAX_ATTEMPTS = 3;

    private final InvocationHistoryDao dao;
    private final WebhookHostAccessor hostAccessor;
    private final ConcurrentMap<WebhookAndEvent, PendingInvocationData> pendingByWebhookAndEvent;
    private final TransactionTemplate txTemplate;

    private long flushIntervalSeconds;

    private volatile boolean active;
    private volatile Future<?> future;

    public AsyncInvocationHistoryDao(
            @Qualifier("aoInvocationHistoryDao") InvocationHistoryDao dao,
            WebhookHostAccessor hostAccessor,
            TransactionTemplate txTemplate) {
        this.dao = dao;
        this.hostAccessor = hostAccessor;
        this.txTemplate = txTemplate;

        flushIntervalSeconds = 30L;
        pendingByWebhookAndEvent = new ConcurrentHashMap<>();
    }

    @Override
    public void addCounts(int webhookId, String eventId, Date date, int errors, int failures, int successes) {
        if (active && System.currentTimeMillis() - date.getTime() < TimeUnit.MINUTES.toMillis(5)) {
            boolean updated = false;
            while (!updated) {
                PendingInvocationData pending = getPending(webhookId, eventId);
                updated = pending.addCounts(errors, failures, successes);
            }
        } else {
            // counts are too old, write them directly to the dao
            dao.addCounts(webhookId, eventId, date, errors, failures, successes);
        }
    }

    @Override
    public int deleteDailyCountsOlderThan(int days) {
        return dao.deleteDailyCountsOlderThan(days);
    }

    @Override
    public void deleteForWebhook(int webhookId) {
        dao.deleteForWebhook(webhookId);
    }

    @Nonnull
    @Override
    public Map<String, String> decodeHeaders(String id, String headersString) {
        return dao.decodeHeaders(id, headersString);
    }

    @Nonnull
    @Override
    public InvocationCounts getCounts(int webhookId, String eventId, int days) {
        flush(webhookId, eventId);
        return dao.getCounts(webhookId, eventId, days);
    }

    @Nonnull
    @Override
    public Map<String, InvocationCounts> getCountsByEvent(
            int webhookId, @Nonnull Collection<String> eventIds, int days) {
        for (String eventId : eventIds) {
            flush(webhookId, eventId);
        }
        return dao.getCountsByEvent(webhookId, eventIds, days);
    }

    @Nonnull
    @Override
    public Map<Integer, InvocationCounts> getCountsByWebhook(@Nonnull Collection<Integer> webhookIds, int days) {
        pendingByWebhookAndEvent.keySet().stream()
                .filter(key -> webhookIds.contains(key.getWebhookId()))
                .forEach(key -> flush(key.getWebhookId(), key.getEventId()));

        return dao.getCountsByWebhook(webhookIds, days);
    }

    @Override
    public AoHistoricalInvocation getLatestInvocation(
            int webhookId, String eventId, Collection<InvocationOutcome> outcomes) {
        flush(webhookId, eventId);
        return dao.getLatestInvocation(webhookId, eventId, outcomes);
    }

    @Nonnull
    @Override
    public List<AoHistoricalInvocation> getLatestInvocations(
            int webhookId, String eventId, Collection<InvocationOutcome> outcomes) {
        flush(webhookId, eventId);
        return dao.getLatestInvocations(webhookId, eventId, outcomes);
    }

    @Nonnull
    @Override
    public Multimap<String, AoHistoricalInvocation> getLatestInvocationsByEvent(
            int webhookId, @Nonnull Collection<String> eventIds) {
        for (String eventId : eventIds) {
            flush(webhookId, eventId);
        }
        return dao.getLatestInvocationsByEvent(webhookId, eventIds);
    }

    @Nonnull
    @Override
    public Multimap<Integer, AoHistoricalInvocation> getLatestInvocationsByWebhook(
            @Nonnull Collection<Integer> webhookIds) {
        pendingByWebhookAndEvent.keySet().stream()
                .filter(key -> webhookIds.contains(key.getWebhookId()))
                .forEach(key -> flush(key.getWebhookId(), key.getEventId()));

        return dao.getLatestInvocationsByWebhook(webhookIds);
    }

    @Nonnull
    @Override
    public Multimap<Integer, AoHistoricalInvocation> getLatestInvocationsByWebhookAndScope(
            @Nonnull Collection<Integer> webhookIds, @Nonnull WebhookScope scope) {
        pendingByWebhookAndEvent.keySet().stream()
                .filter(key -> webhookIds.contains(key.getWebhookId()))
                .forEach(key -> flush(key.getWebhookId(), key.getEventId()));

        return dao.getLatestInvocationsByWebhookAndScope(webhookIds, scope);
    }

    @Override
    public void onStart(WebhooksConfiguration configuration) {
        active = true;
        flushIntervalSeconds = configuration.getStatisticsFlushInterval().getSeconds();
        scheduleFlush();
    }

    @Override
    public void onStop() {
        if (future != null) {
            Future<?> f = future;
            future = null;
            f.cancel(false);
        }
        // write everything that's pending to the database upon shutdown
        flush();
        active = false;
    }

    @Override
    public void saveInvocation(int webhookId, @Nonnull DetailedInvocation invocation) {
        boolean updated = false;
        while (!updated) {
            if (!active) {
                dao.saveInvocation(webhookId, invocation);
                return;
            }
            updated = getPending(webhookId, invocation.getEvent().getId()).onInvocation(invocation);
        }
    }

    @VisibleForTesting
    void flush() {
        if (pendingByWebhookAndEvent.isEmpty()) {
            return;
        }

        // move all items from the pending map to a local map
        Map<WebhookAndEvent, PendingInvocationData> data = new HashMap<>();
        data.putAll(pendingByWebhookAndEvent);
        data.keySet().forEach(pendingByWebhookAndEvent::remove);

        // flush all of the pending data to the database
        Iterator<Map.Entry<WebhookAndEvent, PendingInvocationData>> it =
                data.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<WebhookAndEvent, PendingInvocationData> entry = it.next();
            WebhookAndEvent key = entry.getKey();
            try {
                flushPending(key.getWebhookId(), key.getEventId(), entry.getValue());
                it.remove();
            } catch (RuntimeException e) {
                log.warn(
                        "Failed to write webhook invocation data for {}:{} to the database",
                        key.getWebhookId(),
                        key.getEventId(),
                        e);
            }
        }

        // return any failed items to the pending map
        data.forEach((key, value) -> returnPending(key.getWebhookId(), key.getEventId(), value));
    }

    private void flush(int webhookId, String eventId) {
        WebhookAndEvent key = new WebhookAndEvent(webhookId, eventId);
        PendingInvocationData pending = pendingByWebhookAndEvent.remove(key);
        if (pending != null) {
            try {
                flushPending(webhookId, eventId, pending);
            } catch (RuntimeException e) {
                log.warn("Failed to flush webhook invocation data for {}:{} to the database", webhookId, eventId, e);
                // put it back
                returnPending(webhookId, eventId, pending);
            }
        }
    }

    private void flushAndReschedule() {
        try {
            flush();
        } finally {
            if (!Thread.currentThread().isInterrupted()) {
                scheduleFlush();
            }
        }
    }

    private void flushPending(int webhookId, String eventId, PendingInvocationData pending) {
        log.trace("Flushing webhook invocation data for {}:{} to the database", webhookId, eventId);
        pending.freeze();

        txTemplate.execute(() -> {
            maybeFlush(webhookId, pending.getLatestError());
            maybeFlush(webhookId, pending.getLatestFailure());
            maybeFlush(webhookId, pending.getLatestSuccess());
            return null;
        });

        for (int attempt = 1; attempt <= MAX_ATTEMPTS; ++attempt) {
            try {
                txTemplate.execute(() -> {
                    dao.addCounts(
                            webhookId,
                            eventId,
                            new Date(),
                            pending.getErrorCount(),
                            pending.getFailureCount(),
                            pending.getSuccessCount());
                    return null;
                });
                break;
            } catch (RuntimeException e) {
                if (attempt == MAX_ATTEMPTS) {
                    throw e;
                } else {
                    log.debug("Update of invocation counts for {}:{} failed. Retrying", webhookId, eventId, e);
                }
            }
        }
    }

    private PendingInvocationData getPending(int webhookId, String eventId) {
        return pendingByWebhookAndEvent.computeIfAbsent(
                new WebhookAndEvent(webhookId, eventId), key -> new PendingInvocationData());
    }

    private void maybeFlush(int webhookId, DetailedInvocation invocation) {
        if (invocation != null) {
            dao.saveInvocation(webhookId, invocation);
        }
    }

    private void returnPending(int webhookId, String eventId, PendingInvocationData pending) {
        boolean updated = false;
        while (!updated) {
            updated = getPending(webhookId, eventId).addAll(pending);
        }
    }

    private void scheduleFlush() {
        if (active) {
            future = hostAccessor
                    .getExecutorService()
                    .schedule(this::flushAndReschedule, flushIntervalSeconds, TimeUnit.SECONDS);
        }
    }
}
