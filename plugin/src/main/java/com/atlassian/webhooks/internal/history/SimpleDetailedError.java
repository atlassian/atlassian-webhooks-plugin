package com.atlassian.webhooks.internal.history;

import javax.annotation.Nonnull;

import com.atlassian.webhooks.history.DetailedInvocationError;
import com.atlassian.webhooks.history.InvocationOutcome;

import static java.util.Objects.requireNonNull;

import static com.atlassian.webhooks.history.InvocationOutcome.ERROR;

public class SimpleDetailedError implements DetailedInvocationError {

    private final String content;
    private final String description;

    public SimpleDetailedError(@Nonnull String content, @Nonnull String description) {
        this.content = requireNonNull(content, "content");
        this.description = requireNonNull(description, "description");
    }

    @Nonnull
    @Override
    public String getContent() {
        return content;
    }

    @Nonnull
    @Override
    public String getDescription() {
        return description;
    }

    @Nonnull
    @Override
    public InvocationOutcome getOutcome() {
        return ERROR;
    }
}
