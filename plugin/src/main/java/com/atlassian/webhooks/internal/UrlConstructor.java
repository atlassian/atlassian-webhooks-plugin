package com.atlassian.webhooks.internal;

import java.net.URLEncoder;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.nio.charset.StandardCharsets.UTF_8;

public class UrlConstructor {

    private UrlConstructor() {}

    /**
     * Regular expression pattern that matches URL template variables.
     * <p>
     * The pattern ensures that:
     * - Variables can be written as {name} or %7Bname%7D (case-insensitive)
     * - Variable names cannot contain curly braces or percent signs to prevent incorrect matching across URL segments
     * - Matches are non-greedy to handle multiple variables in the same URL
     * <p>
     * Example: "/api/users/{userId}/posts/{postId}" or "/api/users/%7BuserId%7D/posts/%7BpostId%7D"
     */
    private static final Pattern URL_VARIABLE_PATTERN = Pattern.compile("(?:%7[Bb]|\\{)([^{}%]+?)(?:%7[Dd]|})");

    /**
     * Can be used to disable any additional re-encoding of the target URLs configured by the admin,
     * in order fix problems like escaping '%25' into '%2525'.
     * <p>
     * Note that this affects only the base URL, not the URL replacements which we still escape.
     */
    public static final String USE_RAW_URLS_FEATURE_FLAG = "plugin.webhooks.use.raw.urls";

    /**
     * Constructs a URL by replacing placeholders in the URL with values from the provided map.
     *
     * @param skipUrlReEncoding whether the target base URL encoding will be skipped or not
     */
    public static String constructUrl(String url, Map<String, Object> urlReplacements, boolean skipUrlReEncoding) {
        StringBuilder result = new StringBuilder();
        Matcher m = URL_VARIABLE_PATTERN.matcher(url);

        while (m.find()) {
            Object replacement = urlReplacements.get(m.group(1));
            if (replacement == null) {
                m.appendReplacement(result, m.group());
            } else {
                m.appendReplacement(result, escapeQueryParamValue(String.valueOf(replacement), skipUrlReEncoding));
            }
        }

        m.appendTail(result);
        return result.toString();
    }

    /**
     * Escapes a query parameter value according to RFC 1738 and RFC 3986.
     * <p>
     * For example, a token {@code "a=b%c"} will be escaped to {@code "a%3Db%25c"}.
     */
    public static String escapeQueryParamValue(String rawParameter, boolean skipUrlReEncoding) {
        if (!skipUrlReEncoding) {
            // the entire URL will be re-encoded in RawRequest.getUrl() so we don't need to encode it here
            return rawParameter;
        }

        // the URL will not be re-encoded, so we need to encode the URL replacement here
        return URLEncoder.encode(rawParameter, UTF_8)
                .replace("+", "%20"); // replace '+' with '%20' as per RFC 3986 for consistency
    }

    public static String escapeCurlyBraces(String url) {
        return url.replace("{", "%7b").replace("}", "%7d");
    }
}
