package com.atlassian.webhooks.internal.history;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import com.google.common.io.CharStreams;

import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.history.DetailedInvocationRequest;
import com.atlassian.webhooks.request.Method;
import com.atlassian.webhooks.request.WebhookHttpRequest;

import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

public class SimpleDetailedRequest implements DetailedInvocationRequest {

    private static final Logger log = getLogger(SimpleDetailedRequest.class);

    private final String body;
    private final Map<String, String> headers;
    private final Method method;
    private final String url;

    public SimpleDetailedRequest(@Nonnull WebhookInvocation invocation, @Nonnull WebhookHttpRequest request) {
        String body;
        try {
            body = loadBody(requireNonNull(request, "request"));
        } catch (IOException e) {
            log.debug("Failed to load request body for webhook invocation {}", invocation.getId());
            body = null;
        }
        this.body = body;
        headers = request.getHeaders();
        method = request.getMethod();
        url = request.getUrl();
    }

    public SimpleDetailedRequest(
            String body, @Nonnull Map<String, String> headers, @Nonnull Method method, @Nonnull String url) {
        this.body = body;
        this.headers = headers;
        this.method = method;
        this.url = url;
    }

    @Nonnull
    @Override
    public Optional<String> getBody() {
        return Optional.ofNullable(body);
    }

    @Nonnull
    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Nonnull
    @Override
    public Method getMethod() {
        return method;
    }

    @Nonnull
    @Override
    public String getUrl() {
        return url;
    }

    private String loadBody(WebhookHttpRequest request) throws IOException {
        String contentType = request.getContentType().orElse(null);

        if (!BodyUtils.isTextContent(contentType)) {
            throw new IllegalArgumentException(
                    String.format("Request payload with content type \"%s\" is not text", contentType));
        }

        byte[] body = request.getContent();
        if (body == null || body.length == 0) {
            return null;
        } else {
            return CharStreams.toString(new BufferedReader(
                    new InputStreamReader(new ByteArrayInputStream(body), BodyUtils.getCharset(contentType))));
        }
    }
}
