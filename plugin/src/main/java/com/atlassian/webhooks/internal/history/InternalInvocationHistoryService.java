package com.atlassian.webhooks.internal.history;

import java.time.Instant;
import javax.annotation.Nonnull;

import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.history.InvocationHistoryService;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

/**
 * Internal interface for the {@link InvocationHistoryService} which allows for logging
 * {@link WebhookInvocation webhook invocations}
 */
public interface InternalInvocationHistoryService extends InvocationHistoryService {

    /**
     * Logs that an error occurred while attempting to invoke a webhook. Information retained includes the
     * request and the error message/exception and any timing info. Derived information such as tallies and historical
     * points of interest are also updated.
     *
     * @param request the HTTP request made
     * @param error the exception or error encountered while attempting the HTTP request
     * @param invocation the invocation
     * @param start when the HTTP request was made
     * @param finish when processing of the error completed
     */
    void logInvocationError(
            @Nonnull WebhookHttpRequest request,
            @Nonnull Throwable error,
            @Nonnull WebhookInvocation invocation,
            @Nonnull Instant start,
            @Nonnull Instant finish);

    /**
     * Logs a webhook invocation's failure including the request/response and any timing info. Derived information such
     * as tallies and historical points of interest are also updated.
     *
     * @param request the HTTP request made
     * @param response the HTTP response made
     * @param invocation the invocation
     * @param start when the HTTP request was made
     * @param finish when the HTTP response completed
     */
    void logInvocationFailure(
            @Nonnull WebhookHttpRequest request,
            @Nonnull WebhookHttpResponse response,
            @Nonnull WebhookInvocation invocation,
            @Nonnull Instant start,
            @Nonnull Instant finish);

    /**
     * Logs a webhook invocation's success including the request/response and any timing info. Derived information such
     * as tallies and historical points of interest are also updated.
     *
     * @param request the HTTP request made
     * @param response the HTTP response made
     * @param invocation the invocation
     * @param start when the HTTP request was made
     * @param finish when the HTTP response completed
     */
    void logInvocationSuccess(
            @Nonnull WebhookHttpRequest request,
            @Nonnull WebhookHttpResponse response,
            @Nonnull WebhookInvocation invocation,
            @Nonnull Instant start,
            @Nonnull Instant finish);
}
