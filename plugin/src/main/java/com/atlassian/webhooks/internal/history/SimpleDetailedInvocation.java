package com.atlassian.webhooks.internal.history;

import java.time.Instant;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.history.*;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

public class SimpleDetailedInvocation extends SimpleHistoricalInvocation implements DetailedInvocation {

    public SimpleDetailedInvocation(
            @Nonnull String id,
            @Nonnull WebhookEvent event,
            @Nullable WebhookScope eventScope,
            @Nonnull InvocationRequest request,
            @Nonnull InvocationResult result,
            @Nonnull Instant start,
            @Nonnull Instant finish) {
        super(id, event, eventScope, start, finish, request, result);
    }

    public SimpleDetailedInvocation(
            @Nonnull WebhookInvocation invocation,
            @Nonnull InvocationOutcome resultKind,
            @Nonnull WebhookHttpRequest request,
            @Nonnull WebhookHttpResponse response,
            @Nonnull Instant start,
            @Nonnull Instant finish) {
        super(
                invocation.getId(),
                invocation.getEvent(),
                invocation.getEventScope().orElse(null),
                start,
                finish,
                new SimpleDetailedRequest(invocation, request),
                new SimpleDetailedResponse(resultKind, invocation, response));
    }

    @Nonnull
    @Override
    public DetailedInvocationRequest getRequest() {
        return (DetailedInvocationRequest) super.getRequest();
    }

    @Nonnull
    @Override
    public DetailedInvocationResult getResult() {
        return (DetailedInvocationResult) super.getResult();
    }
}
