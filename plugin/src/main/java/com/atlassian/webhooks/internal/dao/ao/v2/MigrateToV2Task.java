package com.atlassian.webhooks.internal.dao.ao.v2;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.java.ao.Accessor;
import net.java.ao.Mutator;
import net.java.ao.Preload;
import net.java.ao.Query;
import net.java.ao.schema.Table;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import com.atlassian.webhooks.internal.dao.ao.AoWebhook;
import com.atlassian.webhooks.internal.dao.ao.v1.AoHistoricalInvocationV1;

import static com.google.common.base.Preconditions.checkState;

public class MigrateToV2Task implements ActiveObjectsUpgradeTask {

    private static final int PAGE_SIZE = 500;

    private static final Logger log = LoggerFactory.getLogger(MigrateToV2Task.class);

    @Override
    public ModelVersion getModelVersion() {
        return ModelVersion.valueOf("2");
    }

    @Override
    public void upgrade(ModelVersion currentVersion, ActiveObjects ao) {
        checkState(currentVersion.isSame(ModelVersion.valueOf("1")), "Can only migrate from v1 to v2");

        log.info("Migrating webhooks to version 2");

        ao.migrate(AoWebhook.class, AoHistoricalInvocationV1toV2.class);
        addScopeToInvocations(ao);

        log.info("Migration is complete");
    }

    private void addScopeToInvocations(ActiveObjects ao) {
        // Page to limit memory usage
        boolean fullPageRetrieved;
        do {
            fullPageRetrieved = ao.executeInTransaction(() -> {
                String whereClause = AoHistoricalInvocationV1toV2.COLUMN_EVENT_SCOPE_TYPE + " IS NULL AND "
                        + AoHistoricalInvocationV1toV2.COLUMN_EVENT_SCOPE_ID + " IS NULL";
                Query query = Query.select()
                        .where(whereClause)
                        .order(AoHistoricalInvocationV1toV2.COLUMN_ID + " ASC")
                        .limit(PAGE_SIZE);
                AoHistoricalInvocationV1toV2[] invocations = ao.find(AoHistoricalInvocationV1toV2.class, query);
                for (AoHistoricalInvocationV1toV2 invocation : invocations) {
                    AoWebhook aoWebhook = ao.get(AoWebhook.class, invocation.getWebhookId());
                    if (aoWebhook != null) {
                        String scopeId = aoWebhook.getScopeId() == null ? StringUtils.EMPTY : aoWebhook.getScopeId();
                        invocation.setEventScopeId(scopeId);
                        invocation.setEventScopeType(aoWebhook.getScopeType());
                        invocation.save();
                    } else {
                        // If there is no webhook then the mapping is dead so let's delete it so we don't get
                        // errors later on.
                        ao.delete(invocation);
                    }
                }
                return invocations.length == PAGE_SIZE;
            });
        } while (fullPageRetrieved);
    }

    @Table(AoHistoricalInvocationV1.TABLE_NAME)
    @Preload
    public interface AoHistoricalInvocationV1toV2 extends AoHistoricalInvocationV1 {

        static final String COLUMN_EVENT_SCOPE_ID = "EVENT_SCOPE_ID";
        static final String COLUMN_EVENT_SCOPE_TYPE = "EVENT_SCOPE_TYPE";

        // NOTE: @NotNull not null yet so that we can migrate.
        @Accessor(COLUMN_EVENT_SCOPE_ID)
        String getEventScopeId();

        // NOTE: @NotNull not null yet so that we can migrate.
        @Accessor(COLUMN_EVENT_SCOPE_TYPE)
        String getEventScopeType();

        @Mutator(COLUMN_EVENT_SCOPE_ID)
        void setEventScopeId(@Nonnull String eventScopeId);

        @Mutator(COLUMN_EVENT_SCOPE_TYPE)
        void setEventScopeType(@Nonnull String eventScopeType);
    }
}
