package com.atlassian.webhooks.internal;

import javax.annotation.Nonnull;

import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookPayloadBuilder;

/**
 * A payload manager is used to serialize the body for a specific webhook invocation. This aims to allow the body
 * to only be set a single time due to the expense of serialization.
 *
 * @since 5.0
 */
public interface WebhookPayloadManager {

    /**
     * Retrieve the body to be set for a specific invocation.
     *
     * @param invocation invocation context
     * @param builder the payload builder
     */
    void setPayload(@Nonnull WebhookInvocation invocation, @Nonnull WebhookPayloadBuilder builder);
}
