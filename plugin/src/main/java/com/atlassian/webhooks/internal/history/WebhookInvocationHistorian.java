package com.atlassian.webhooks.internal.history;

import java.time.Instant;
import java.util.concurrent.ConcurrentMap;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import com.google.common.collect.Maps;

import com.atlassian.webhooks.WebhookCallback;
import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookRequestEnricher;
import com.atlassian.webhooks.diagnostics.WebhookDiagnosticsEvent;
import com.atlassian.webhooks.history.InvocationHistoryService;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Composes the history of webhook invocations by registering as a {@link WebhookRequestEnricher} and, when presented
 * with a webhook to enrich, registeres{@link InvocationHistoryService} as an
 * {@link WebhookInvocation#registerCallback(WebhookCallback) invocation callback}
 */
public class WebhookInvocationHistorian implements WebhookRequestEnricher, WebhookCallback {

    private static final Logger log = getLogger(WebhookInvocationHistorian.class);

    private final InternalInvocationHistoryService historyService;
    private final ConcurrentMap<String, Instant> startTimes = Maps.newConcurrentMap();

    public WebhookInvocationHistorian(@Nonnull InternalInvocationHistoryService historyService) {
        this.historyService = historyService;
    }

    @Override
    public void enrich(@Nonnull WebhookInvocation invocation) {
        if (invocation.getEvent() instanceof WebhookDiagnosticsEvent
                || invocation.getWebhook().getId() < 0) {
            // don't track diagnostics requests or requests to webhooks from module descriptors (such as connect-based
            // webhooks)
            return;
        }

        startTimes.put(invocation.getId(), Instant.now());
        invocation.registerCallback(this);
    }

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public void onError(
            @Nonnull WebhookHttpRequest webhookHttpRequest,
            @Nonnull Throwable throwable,
            @Nonnull WebhookInvocation webhookInvocation) {

        Instant finishTime = Instant.now();
        Instant startTime = getAndClearStartTimeFor(webhookInvocation, finishTime);
        historyService.logInvocationError(webhookHttpRequest, throwable, webhookInvocation, startTime, finishTime);
    }

    @Override
    public void onFailure(
            @Nonnull WebhookHttpRequest webhookHttpRequest,
            @Nonnull WebhookHttpResponse webhookHttpResponse,
            @Nonnull WebhookInvocation webhookInvocation) {

        Instant finishTime = Instant.now();
        Instant startTime = getAndClearStartTimeFor(webhookInvocation, finishTime);
        historyService.logInvocationFailure(
                webhookHttpRequest, webhookHttpResponse, webhookInvocation, startTime, finishTime);
    }

    @Override
    public void onSuccess(
            @Nonnull WebhookHttpRequest webhookHttpRequest,
            @Nonnull WebhookHttpResponse webhookHttpResponse,
            @Nonnull WebhookInvocation webhookInvocation) {

        Instant finishTime = Instant.now();
        Instant startTime = getAndClearStartTimeFor(webhookInvocation, finishTime);
        historyService.logInvocationSuccess(
                webhookHttpRequest, webhookHttpResponse, webhookInvocation, startTime, finishTime);
    }

    private Instant getAndClearStartTimeFor(@Nonnull WebhookInvocation webhookInvocation, Instant defaultStartTime) {
        Instant startTime = startTimes.remove(webhookInvocation.getId());
        if (startTime == null) {
            log.debug(
                    "Unable to determine the starting time for webhook invocation {}. Assuming start and finish times equal",
                    webhookInvocation.getId());
            startTime = defaultStartTime;
        }
        return startTime;
    }
}
