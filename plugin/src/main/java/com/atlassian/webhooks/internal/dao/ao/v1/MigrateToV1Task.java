package com.atlassian.webhooks.internal.dao.ao.v1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import com.atlassian.webhooks.internal.dao.ao.v0.WebHookListenerAOV0;

import static com.google.common.base.Preconditions.checkState;

public class MigrateToV1Task implements ActiveObjectsUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(MigrateToV1Task.class);

    private final WebhookV1MigrateService webhookV1MigrateService;

    private final ExternalMigrationTaskInvokerGuard externalMigrationTaskInvokerService;

    public MigrateToV1Task(
            WebhookV1MigrateService webhookV1MigrateService,
            ExternalMigrationTaskInvokerGuard externalMigrationTaskInvokerService) {
        this.webhookV1MigrateService = webhookV1MigrateService;
        this.externalMigrationTaskInvokerService = externalMigrationTaskInvokerService;
    }

    @Override
    public ModelVersion getModelVersion() {
        return ModelVersion.valueOf("1");
    }

    @Override
    public void upgrade(ModelVersion currentVersion, ActiveObjects ao) {
        checkState(currentVersion.isSame(ModelVersion.valueOf("0")), "Can only migrate from v0 to v1");

        log.info("Migrating webhooks to version 1");

        ao.migrate(
                WebHookListenerAOV0.class,
                AoWebhookV1.class,
                AoWebhookConfigurationEntryV1.class,
                AoWebhookEventV1.class,
                AoHistoricalInvocationV1.class);
        ao.stream(WebHookListenerAOV0.class, hook -> webhookV1MigrateService.migrate(ao, hook));

        externalMigrationTaskInvokerService.onMigrationFinished(ao);

        log.info("Migration is complete");
    }
}
