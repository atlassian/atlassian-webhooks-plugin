package com.atlassian.webhooks.internal.configuration;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;

import com.atlassian.sal.api.ApplicationProperties;

import static java.util.Objects.requireNonNull;

/**
 * Finds and returns feature flag states either from the system or from SAL to account for different product practises.
 * <p>
 * System properties take precedence over those provided by SAL.
 */
public class FeatureFlagService {

    /**
     * This feature flag can be used to disable any additional re-encoding of target URLs configured by the admin.
     * It addresses issues such as unnecessary escaping of '%25' into '%2525'.
     * <p>
     * Note: This affects only the base URL. URL replacements are still subject to escaping.
     */
    private static final String SKIP_URL_REENCODING_FLAG = "plugin.webhooks.skip.url.reencoding";

    private final ApplicationProperties applicationProperties;

    public FeatureFlagService(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    /**
     * Searches for the property with the specified key in the system properties and iff not found then using SAL. The
     * method returns null if the property is not found.
     *
     * @param key the property key.
     * @return the value of the property or null.
     */
    @Nullable
    @VisibleForTesting
    String getProperty(@Nonnull String key) {
        requireNonNull(key, "key to get property");
        final String systemPropertyValue = System.getProperty(key);
        if (systemPropertyValue != null) {
            return systemPropertyValue;
        }

        try {
            // Replace deprecated method when the new SAL API is created and supported by all products
            return applicationProperties.getPropertyValue(key);
        } catch (UnsupportedOperationException e) { // Confluence does not support #getPropertyValue
            return null;
        }
    }

    /**
     * Searches for the property with the specified key in the system properties and iff not found then using SAL.
     *
     * @param key the property key.
     * @return true if and only if the property is set to "true" (case-insensitive)
     */
    @VisibleForTesting
    boolean getBoolean(@Nonnull String key) {
        requireNonNull(key, "key to get boolean property");
        return Boolean.parseBoolean(this.getProperty(key));
    }

    /**
     * Determines if the feature flag for disabling URL re-encoding is active.
     * <p>
     * When enabled, the webhook's target base URL set by the admin will not undergo re-encoding,
     * preventing issues such as escaping '%25' to '%2525'.
     */
    public boolean shouldSkipUrlReEncoding() {
        return getBoolean(SKIP_URL_REENCODING_FLAG);
    }
}
