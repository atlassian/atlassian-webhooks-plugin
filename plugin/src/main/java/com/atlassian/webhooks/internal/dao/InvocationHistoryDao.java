package com.atlassian.webhooks.internal.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.Multimap;

import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.history.DetailedInvocation;
import com.atlassian.webhooks.history.InvocationCounts;
import com.atlassian.webhooks.history.InvocationHistory;
import com.atlassian.webhooks.history.InvocationOutcome;
import com.atlassian.webhooks.internal.dao.ao.AoDailyInvocationCounts;
import com.atlassian.webhooks.internal.dao.ao.AoHistoricalInvocation;

/**
 * The DAO for persisting {@link DetailedInvocation} and {@link InvocationHistory} instances
 */
public interface InvocationHistoryDao {

    /**
     * Creates or updates the {@link AoDailyInvocationCounts counts record} for the provided webhookId, eventId and
     * date combination/
     */
    void addCounts(int webhookId, String eventId, Date date, int errors, int failures, int successes);

    /**
     * Helper method that deserializes the request or response headers from their encoded string
     */
    @Nonnull
    Map<String, String> decodeHeaders(String id, String headersString);

    /**
     * Deletes all {@link AoDailyInvocationCounts counts records} older than {@code days} days
     *
     * @param days number of days
     * @return the number of deleted records
     */
    int deleteDailyCountsOlderThan(int days);

    /**
     * Deletes all invocation history records that exist for the provided webhook
     *
     * @param webhookId the webhook ID
     */
    void deleteForWebhook(int webhookId);

    /**
     * @param webhookId the webhook ID
     * @param eventId the {@link com.atlassian.webhooks.WebhookEvent#getId() webhook ID}
     * @param days the number of preceding days (from now) to retrieve counts for
     * @return the invocation counts for the specified (webhook,event) combination.
     */
    @Nonnull
    InvocationCounts getCounts(int webhookId, @Nullable String eventId, int days);

    /**
     * @param webhookId webhook ID
     * @param eventIds the event IDs to retrieve counts for
     * @param days the number of preceding days (from now) to retrieve counts for
     * @return a map counts for each of the (webhook, event) combinations
     */
    @Nonnull
    Map<String, InvocationCounts> getCountsByEvent(int webhookId, @Nonnull Collection<String> eventIds, int days);

    /**
     * @param webhookIds webhook IDs
     * @param days the number of preceding days (from now) to retrieve counts for
     * @return a map of total counts for each of the webhooks
     */
    @Nonnull
    Map<Integer, InvocationCounts> getCountsByWebhook(@Nonnull Collection<Integer> webhookIds, int days);

    /**
     * @param webhookId webhook ID
     * @param eventId the {@link WebhookEvent#getId() event ID}. A null event means all events are considered
     * @param outcomes the outcomes to consider. A null collection means all outcomes are considered
     * @return the latest recorded invocation for the provided webhook/event/outcome(s), if any exists
     */
    @Nullable
    AoHistoricalInvocation getLatestInvocation(
            int webhookId, @Nullable String eventId, @Nullable Collection<InvocationOutcome> outcomes);

    /**
     * @param webhookId webhook ID
     * @param eventId the {@link WebhookEvent#getId() event ID}. A null event means all events are considered
     * @param outcomes the outcomes to consider. A null collection means all outcomes are considered
     * @return the latest recorded invocation for each outcome, for the provided webhook/event pair
     */
    @Nonnull
    List<AoHistoricalInvocation> getLatestInvocations(
            int webhookId, @Nullable String eventId, @Nullable Collection<InvocationOutcome> outcomes);

    /**
     * @param webhookId webhook ID
     * @param eventIds the {@link WebhookEvent#getId() event IDs} to consider
     * @return a map of event ID to the latest recorded invocation for the webhook/event pair
     */
    @Nonnull
    Multimap<String, AoHistoricalInvocation> getLatestInvocationsByEvent(
            int webhookId, @Nonnull Collection<String> eventIds);

    /**
     * @param webhookIds webhook IDs
     * @return a map of webhook ID to the latest recorded invocation for the webhook (across all events)
     */
    @Nonnull
    Multimap<Integer, AoHistoricalInvocation> getLatestInvocationsByWebhook(@Nonnull Collection<Integer> webhookIds);

    /**
     * @param webhookIds webhook IDs
     * @param scope the scope for which the latest invocation is requested
     * @return a map of webhook ID to the latest recorded invocation for the webhook (across all events)
     * @since 7.1
     */
    @Nonnull
    Multimap<Integer, AoHistoricalInvocation> getLatestInvocationsByWebhookAndScope(
            @Nonnull Collection<Integer> webhookIds, @Nonnull WebhookScope scope);

    /**
     * Stores the invocation details, updating the latest invocation for the webhook/event pair if this invocation
     * is finished later than the currently latest invocation in the database.
     *
     * @param webhookId webhook ID
     * @param invocation the invocation details to persist
     */
    void saveInvocation(int webhookId, @Nonnull DetailedInvocation invocation);
}
