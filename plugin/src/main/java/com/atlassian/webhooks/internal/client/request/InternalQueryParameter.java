package com.atlassian.webhooks.internal.client.request;

import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.webhooks.request.QueryParameter;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

public class InternalQueryParameter implements QueryParameter {

    private final String name;
    private final String value;

    public InternalQueryParameter(@Nonnull String name, @Nullable String value) {
        this.name = requireNonNull(name, "name");
        this.value = value;
    }

    @Nonnull
    @Override
    public String getName() {
        return name;
    }

    @Nonnull
    @Override
    public Optional<String> getValue() {
        return ofNullable(value);
    }
}
