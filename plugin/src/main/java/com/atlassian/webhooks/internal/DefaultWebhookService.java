package com.atlassian.webhooks.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.annotations.VisibleForTesting;
import io.atlassian.util.concurrent.ThreadFactories;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.webhooks.NoSuchWebhookException;
import com.atlassian.webhooks.PingRequest;
import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookCallback;
import com.atlassian.webhooks.WebhookCreateRequest;
import com.atlassian.webhooks.WebhookCredentials;
import com.atlassian.webhooks.WebhookDeleteRequest;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookFilter;
import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookPayloadBuilder;
import com.atlassian.webhooks.WebhookPublishRequest;
import com.atlassian.webhooks.WebhookRequestEnricher;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.WebhookSearchRequest;
import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.WebhookStatistics;
import com.atlassian.webhooks.WebhookUpdateRequest;
import com.atlassian.webhooks.WebhooksConfiguration;
import com.atlassian.webhooks.WebhooksNotInitializedException;
import com.atlassian.webhooks.diagnostics.WebhookDiagnosticsEvent;
import com.atlassian.webhooks.diagnostics.WebhookDiagnosticsResult;
import com.atlassian.webhooks.event.WebhookCreatedEvent;
import com.atlassian.webhooks.event.WebhookDeletedEvent;
import com.atlassian.webhooks.event.WebhookModifiedEvent;
import com.atlassian.webhooks.internal.concurrent.BackPressureBlockingQueue;
import com.atlassian.webhooks.internal.configuration.FeatureFlagService;
import com.atlassian.webhooks.internal.dao.WebhookDao;
import com.atlassian.webhooks.internal.dao.ao.AoWebhook;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookConfigurationEntry;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookEvent;
import com.atlassian.webhooks.internal.model.SimpleWebhook;
import com.atlassian.webhooks.internal.model.SimpleWebhookCredentials;
import com.atlassian.webhooks.internal.model.SimpleWebhookScope;
import com.atlassian.webhooks.internal.model.UnknownWebhookEvent;
import com.atlassian.webhooks.internal.publish.DefaultWebhookInvocation;
import com.atlassian.webhooks.internal.publish.InternalWebhookInvocation;
import com.atlassian.webhooks.internal.publish.WebhookDispatcher;
import com.atlassian.webhooks.module.WebhookModuleDescriptor;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

public class DefaultWebhookService implements WebhookService, WebhooksLifecycleAware {

    private static final Logger log = LoggerFactory.getLogger(DefaultWebhookService.class);

    private final WebhookDao dao;
    private final WebhookDispatcher dispatcher;
    private final EventPublisher eventPublisher;
    private final WebhookHostAccessor hostAccessor;
    private final PluginAccessor pluginAccessor;
    private final TransactionTemplate txTemplate;
    private final Validator validator;
    private final WebhookPayloadManager webhookPayloadManager;
    private final FeatureFlagService featureFlagService;

    private volatile ThreadPoolExecutor publishExecutor;
    private volatile SimpleWebhooksStatistics statistics;

    public DefaultWebhookService(
            WebhookDao dao,
            WebhookDispatcher dispatcher,
            EventPublisher eventPublisher,
            WebhookHostAccessor hostAccessor,
            PluginAccessor pluginAccessor,
            TransactionTemplate txTemplate,
            Validator validator,
            WebhookPayloadManager webhookPayloadManager,
            FeatureFlagService featureFlagService) {
        this.dao = dao;
        this.dispatcher = dispatcher;
        this.hostAccessor = hostAccessor;
        this.eventPublisher = eventPublisher;
        this.pluginAccessor = pluginAccessor;
        this.txTemplate = txTemplate;
        this.validator = validator;
        this.webhookPayloadManager = webhookPayloadManager;
        this.featureFlagService = featureFlagService;
    }

    @Override
    @Nonnull
    public Webhook create(@Nonnull WebhookCreateRequest request) {
        requireNonNull(request, "request");

        validator.validate(request);
        return txTemplate.execute(() -> {
            AoWebhook aoWebhook = dao.create(request);

            Webhook webhook = convert(aoWebhook);
            eventPublisher.publish(new WebhookCreatedEvent(this, webhook));
            return webhook;
        });
    }

    @Override
    public boolean delete(int id) {
        return txTemplate.execute(() -> {
            AoWebhook aoWebhook = dao.getById(id);
            if (aoWebhook == null) {
                return false;
            }

            // convert webhook object before deleting to retain values for event publishing
            Webhook webhook = convert(aoWebhook);

            // deleting by array saves us the internal (repeated) find that delete by id would do
            dao.delete(new AoWebhook[] {aoWebhook});

            eventPublisher.publish(new WebhookDeletedEvent(this, webhook));
            return true;
        });
    }

    @Override
    public int delete(@Nonnull WebhookDeleteRequest request) {
        requireNonNull(request, "request");

        // Search may come back with multiple pages, but we just want to make sure that we delete
        // all of them, so this must loop until there are no search results left
        WebhookSearchRequest search = WebhookSearchRequest.builder(request).build();

        return txTemplate.execute(() -> {
            int deleted = 0;
            AoWebhook[] searchResults;
            do {
                searchResults = dao.search(search);
                dao.delete(searchResults);
                Arrays.stream(searchResults).forEach((aoWebhook) -> {
                    Webhook webhook = convert(aoWebhook);
                    eventPublisher.publish(new WebhookDeletedEvent(this, webhook));
                });
                deleted += searchResults.length;
            } while (searchResults.length >= search.getLimit());
            return deleted;
        });
    }

    @Nonnull
    @Override
    public Optional<Webhook> findById(int id) {
        return txTemplate.execute(() -> ofNullable(dao.getById(id)).map(this::convert));
    }

    @Nonnull
    @Override
    public Optional<WebhookEvent> getEvent(@Nonnull String eventId) {
        WebhookEvent event = hostAccessor.getEvent(eventId);
        if (event instanceof UnknownWebhookEvent) {
            return empty();
        }
        return of(event);
    }

    @Nonnull
    @Override
    public List<WebhookEvent> getEvents() {
        return hostAccessor.getEvents();
    }

    @Nonnull
    @Override
    public Optional<WebhookStatistics> getStatistics() {
        return ofNullable(statistics);
    }

    @Override
    public void onStart(WebhooksConfiguration configuration) {
        if (configuration.isStatisticsEnabled()) {
            statistics = new SimpleWebhooksStatistics();
        }
        ThreadPoolExecutor executor = publishExecutor;
        if (executor != null) {
            executor.shutdown();
        }
        publishExecutor = new ThreadPoolExecutor(
                5,
                5,
                5,
                TimeUnit.SECONDS,
                new BackPressureBlockingQueue<>(configuration.getDispatchQueueSize()),
                ThreadFactories.namedThreadFactory("webhook-dispatcher", ThreadFactories.Type.DAEMON));
    }

    @Override
    public void onStop() {
        ThreadPoolExecutor executor = publishExecutor;
        publishExecutor = null;
        if (executor != null) {
            executor.shutdown();
        }
    }

    @Nonnull
    @Override
    public Future<WebhookDiagnosticsResult> ping(@Nonnull PingRequest request) {
        requireNonNull(request, "request");
        validator.validate(request);

        SimpleWebhook.Builder builder = SimpleWebhook.builder()
                .event(WebhookDiagnosticsEvent.PING)
                .url(request.getUrl())
                .scope(request.getScope())
                .sslVerificationRequired(request.isSslVerificationRequired());

        request.getCredentials().ifPresent(builder::credentials);

        Webhook webhook = builder.build();
        WebhookPublishRequest publishRequest = WebhookPublishRequest.builder(
                        webhook, WebhookDiagnosticsEvent.PING, null)
                .build();
        InternalWebhookInvocation invocation = createSingleInvocationFor(webhook, publishRequest);
        enrich(invocation);

        return dispatchForResult(invocation);
    }

    @Override
    public void publish(@Nonnull WebhookPublishRequest request) {
        requireNonNull(request, "request");
        validator.validate(request);

        // If we're tracing, we want to be able to track how long it took to get onto the queue
        final String debugIdentifier = getDebugString();
        log.trace("Adding to webhook service dispatch queue with queue id [{}]", debugIdentifier);

        getExecutorOrThrow().execute(() -> {
            try {
                log.trace("Webhook has started execution for debug queue id [{}]", debugIdentifier);
                incrementPublishCount();

                Collection<WebhookFilter> filters = hostAccessor.getFilters();

                List<InternalWebhookInvocation> webhookInvocations = createInvocationsFor(request);
                List<InternalWebhookInvocation> webhookInvocationsFiltered = webhookInvocations.stream()
                        .filter(invocation -> {
                            boolean filterResult = filters.stream().allMatch(filter -> {
                                boolean individualFilter = filter.filter(invocation);
                                if (log.isTraceEnabled()) {
                                    log.trace(
                                            "Filter [{}] has completed with result [{}] for invocation [{}]",
                                            filter.getClass().getSimpleName(),
                                            individualFilter,
                                            invocation.getId());
                                }
                                return individualFilter;
                            });

                            log.debug(
                                    "The overall result of the filter was [{}] for invocation [{}]",
                                    filterResult,
                                    invocation.getId());

                            return filterResult;
                        })
                        .collect(toList());

                request.getInvocationCompletedCallback().ifPresent(callback -> {
                    if (!webhookInvocationsFiltered.isEmpty()) {
                        InvocationCompletedCallback invocationCompletedCallback =
                                new InvocationCompletedCallback(webhookInvocationsFiltered.size(), callback);
                        webhookInvocationsFiltered.forEach(i -> i.registerCallback(invocationCompletedCallback));
                    } else {
                        // if all webhooks have been filtered out, invoke signal completeness
                        callback.run();
                    }
                });

                webhookInvocationsFiltered.forEach(filteredInvocation -> {
                    enrich(filteredInvocation);

                    dispatcher.dispatch(filteredInvocation);
                });
            } catch (Exception e) {
                // Current all failures are ignored due to being in an executor
                log.info(
                        "An error occurred while attempting to publish webhooks for queue id [{}]",
                        debugIdentifier,
                        log.isDebugEnabled() ? e : null);

                request.getInvocationCompletedCallback().ifPresent(Runnable::run);
            }
        });
    }

    private void enrich(WebhookInvocation invocation) {
        for (WebhookRequestEnricher enricher : hostAccessor.getEnrichers()) {
            try {
                enricher.enrich(invocation);
            } catch (Exception e) {
                log.info(
                        "Webhook enricher [{}] has failed with an error for invocation [{}]",
                        enricher.getClass().getSimpleName(),
                        invocation.getId(),
                        log.isDebugEnabled() ? e : null);
            }
        }
    }

    @Nonnull
    @Override
    public List<Webhook> search(@Nonnull WebhookSearchRequest request) {
        requireNonNull(request, "request");

        return txTemplate.execute(
                () -> Arrays.stream(dao.search(request)).map(this::convert).collect(Collectors.toList()));
    }

    @Override
    public void setStatisticsEnabled(boolean enabled) {
        if (enabled) {
            if (statistics == null) {
                statistics = new SimpleWebhooksStatistics();
            }
        } else {
            statistics = null;
        }
    }

    @Nonnull
    @Override
    public Webhook update(int id, @Nonnull WebhookUpdateRequest request) {
        requireNonNull(request, "request");
        validator.validate(request);

        return txTemplate.execute(() -> {
            AoWebhook aoCurrent = dao.getById(id);
            if (aoCurrent == null) {
                throw new NoSuchWebhookException("Webhook with ID " + id + " does not exist");
            }

            AoWebhook aoUpdated = dao.update(id, request);
            if (aoUpdated == null) {
                throw new NoSuchWebhookException("Webhook with ID " + id + " does not exist");
            }

            Webhook current = convert(aoCurrent);
            Webhook updated = convert(aoUpdated);
            eventPublisher.publish(new WebhookModifiedEvent(this, current, updated));
            return updated;
        });
    }

    @VisibleForTesting
    protected Executor getExecutorOrThrow() {
        ThreadPoolExecutor executor = publishExecutor;
        if (executor != null) {
            return executor;
        }

        throw new WebhooksNotInitializedException(
                "The webhooks plugin hasn't been initialized yet. " + "Webhook will not be published.");
    }

    private Webhook convert(AoWebhook aoWebhook) {
        SimpleWebhook.Builder builder = SimpleWebhook.builder()
                .id(aoWebhook.getID())
                .active(aoWebhook.isActive())
                .configuration(getContext(aoWebhook))
                .event(getEvents(aoWebhook.getEvents()))
                .name(aoWebhook.getName())
                .scope(getScope(aoWebhook))
                .sslVerificationRequired(aoWebhook.isSslVerificationRequired())
                .url(aoWebhook.getUrl())
                .createdDate(aoWebhook.getCreatedDate())
                .updatedDate(aoWebhook.getUpdatedDate());

        if (aoWebhook.getUsername() != null || aoWebhook.getPassword() != null) {
            builder.credentials(getCredentials(aoWebhook));
        }

        return builder.build();
    }

    private List<InternalWebhookInvocation> createInvocationsFor(WebhookPublishRequest request) {
        if (request.getWebhook().isPresent()) {
            return singletonList(createSingleInvocationFor(request.getWebhook().get(), request));
        }

        WebhookEvent event = request.getEvent();
        Set<WebhookScope> scopes = new HashSet<>(request.getScopes());
        scopes.add(WebhookScope.GLOBAL); // always dispatch to global scope webhooks

        List<InternalWebhookInvocation> invocations = new ArrayList<>();
        Stream<Webhook> pluginWebhooks =
                pluginAccessor.getEnabledModuleDescriptorsByClass(WebhookModuleDescriptor.class).stream()
                        .map(WebhookModuleDescriptor::getModule)
                        .filter(webhook -> webhook.getEvents().stream()
                                        .anyMatch(e -> e.getId().equals(event.getId()))
                                && scopes.contains(webhook.getScope())
                                && webhook.isActive());
        Stream<Webhook> subscribedWebhooks =
                search(WebhookSearchRequest.builder()
                                .active(true)
                                .event(event)
                                .scope(scopes)
                                .build())
                        .stream();

        Stream.concat(pluginWebhooks, subscribedWebhooks)
                .map(hook -> createSingleInvocationFor(hook, request))
                .forEach(invocations::add);

        return invocations;
    }

    private InternalWebhookInvocation createSingleInvocationFor(Webhook hook, WebhookPublishRequest request) {
        DefaultWebhookInvocation invocation = new DefaultWebhookInvocation(featureFlagService, hook, request);
        log.trace(
                "A new webhook invocation has been created for webhook [{}], invocation [{}]",
                hook.getId(),
                invocation.getId());

        // We have to create the request body here; we unfortunately cannot wait until
        // after the filtering, as filters might make a decision about the body
        WebhookPayloadBuilder payloadBuilder = invocation.getRequestBuilder().asPayloadBuilder();
        webhookPayloadManager.setPayload(invocation, payloadBuilder);

        maybeRegisterStatisticsCallback(invocation);
        return invocation;
    }

    private Future<WebhookDiagnosticsResult> dispatchForResult(InternalWebhookInvocation invocation) {
        CompletableFuture<WebhookDiagnosticsResult> result = new CompletableFuture<>();
        invocation.registerCallback(new WebhookCallback() {
            @Override
            public void onError(
                    WebhookHttpRequest request, @Nonnull Throwable error, @Nonnull WebhookInvocation webhook) {
                result.complete(WebhookDiagnosticsResult.build(request, error));
            }

            @Override
            public void onFailure(
                    @Nonnull WebhookHttpRequest request,
                    @Nonnull WebhookHttpResponse response,
                    @Nonnull WebhookInvocation webhook) {
                result.complete(WebhookDiagnosticsResult.build(request, response));
            }

            @Override
            public void onSuccess(
                    @Nonnull WebhookHttpRequest request,
                    @Nonnull WebhookHttpResponse response,
                    @Nonnull WebhookInvocation webhook) {
                result.complete(WebhookDiagnosticsResult.build(request, response));
            }
        });

        dispatcher.dispatch(invocation);
        return result;
    }

    private Map<String, String> getContext(AoWebhook aoWebhook) {
        return Arrays.stream(aoWebhook.getConfiguration())
                .collect(Collectors.toMap(AoWebhookConfigurationEntry::getKey, AoWebhookConfigurationEntry::getValue));
    }

    private WebhookCredentials getCredentials(AoWebhook aoWebhook) {
        return new SimpleWebhookCredentials(aoWebhook.getUsername(), aoWebhook.getPassword());
    }

    private String getDebugString() {
        if (log.isTraceEnabled()) {
            return UUID.randomUUID().toString();
        }
        return "";
    }

    private List<WebhookEvent> getEvents(AoWebhookEvent[] events) {
        return Arrays.stream(events)
                .map(AoWebhookEvent::getEventId)
                .map(hostAccessor::getEvent)
                .collect(toList());
    }

    private WebhookScope getScope(AoWebhook webhook) {
        return new SimpleWebhookScope(webhook.getScopeType(), webhook.getScopeId());
    }

    private void incrementPublishCount() {
        SimpleWebhooksStatistics stats = statistics;
        if (stats != null) {
            stats.onPublish();
        }
    }

    private void maybeRegisterStatisticsCallback(InternalWebhookInvocation invocation) {
        SimpleWebhooksStatistics stats = statistics;
        if (stats != null) {
            invocation.registerCallback(stats.asCallback());
        }
    }
}
