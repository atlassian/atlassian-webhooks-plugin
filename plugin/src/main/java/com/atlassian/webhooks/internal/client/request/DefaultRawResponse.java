package com.atlassian.webhooks.internal.client.request;

import javax.annotation.Nonnull;

import com.atlassian.httpclient.api.Response;
import com.atlassian.webhooks.request.WebhookHttpResponse;
import com.atlassian.webhooks.request.WebhookResponseBody;
import com.atlassian.webhooks.request.WebhookResponseHeaders;

import static java.util.Objects.requireNonNull;

public class DefaultRawResponse implements WebhookHttpResponse {

    private final Response result;
    private final int statusCode;
    private final long maxBytes;

    public DefaultRawResponse(@Nonnull Response result, long maxBytes) {
        this.result = requireNonNull(result, "result");
        this.statusCode = result.getStatusCode();
        this.maxBytes = maxBytes;
    }

    @Override
    public void close() {}

    @Nonnull
    @Override
    public WebhookResponseBody getBody() {
        return new AtlassianHttpResponseBody(result, maxBytes);
    }

    @Nonnull
    @Override
    public WebhookResponseHeaders getHeaders() {
        return new AtlassianHttpHeaders(result);
    }

    @Override
    public int getStatusCode() {
        return statusCode;
    }
}
