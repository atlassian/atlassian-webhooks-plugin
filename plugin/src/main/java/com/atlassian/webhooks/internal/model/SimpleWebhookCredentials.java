package com.atlassian.webhooks.internal.model;

import java.util.Objects;
import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.webhooks.WebhookCredentials;

import static com.google.common.base.Strings.nullToEmpty;

public class SimpleWebhookCredentials implements WebhookCredentials {

    private final String password;
    private final String username;

    public SimpleWebhookCredentials(@Nullable String username, @Nullable String password) {
        this.password = nullToEmpty(password);
        this.username = nullToEmpty(username);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SimpleWebhookCredentials that = (SimpleWebhookCredentials) o;
        return Objects.equals(getPassword(), that.getPassword()) && Objects.equals(getUsername(), that.getUsername());
    }

    @Nonnull
    @Override
    public Optional<String> getPassword() {
        return Optional.of(password);
    }

    @Nonnull
    @Override
    public Optional<String> getUsername() {
        return Optional.of(username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPassword(), getUsername());
    }
}
