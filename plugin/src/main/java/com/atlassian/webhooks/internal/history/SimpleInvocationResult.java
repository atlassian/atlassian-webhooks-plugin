package com.atlassian.webhooks.internal.history;

import javax.annotation.Nonnull;

import com.atlassian.webhooks.history.InvocationOutcome;
import com.atlassian.webhooks.history.InvocationResult;

import static java.util.Objects.requireNonNull;

public class SimpleInvocationResult implements InvocationResult {

    private final String description;
    private final InvocationOutcome outcome;

    public SimpleInvocationResult(String description, InvocationOutcome outcome) {
        this.description = requireNonNull(description, "description");
        this.outcome = requireNonNull(outcome, "outcome");
    }

    @Nonnull
    @Override
    public String getDescription() {
        return description;
    }

    @Nonnull
    @Override
    public InvocationOutcome getOutcome() {
        return outcome;
    }
}
