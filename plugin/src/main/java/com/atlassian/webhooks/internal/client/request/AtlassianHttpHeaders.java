package com.atlassian.webhooks.internal.client.request;

import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.httpclient.api.Response;
import com.atlassian.webhooks.request.WebhookResponseHeaders;

import static java.util.Objects.requireNonNull;

public class AtlassianHttpHeaders implements WebhookResponseHeaders {

    private final Response request;

    public AtlassianHttpHeaders(@Nonnull Response request) {
        this.request = requireNonNull(request, "request");
    }

    @Nonnull
    @Override
    public String getHeader(@Nonnull String name) {
        return request.getHeader(requireNonNull(name, "name"));
    }

    @Override
    @Nonnull
    public Map<String, String> getHeaders() {
        return request.getHeaders();
    }
}
