package com.atlassian.webhooks.internal.dao;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.time.Clock;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

import net.java.ao.EntityStreamCallback;
import net.java.ao.Query;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.history.DetailedInvocation;
import com.atlassian.webhooks.history.DetailedInvocationError;
import com.atlassian.webhooks.history.DetailedInvocationRequest;
import com.atlassian.webhooks.history.DetailedInvocationResponse;
import com.atlassian.webhooks.history.DetailedInvocationResult;
import com.atlassian.webhooks.history.InvocationCounts;
import com.atlassian.webhooks.history.InvocationOutcome;
import com.atlassian.webhooks.internal.dao.ao.AoDailyInvocationCounts;
import com.atlassian.webhooks.internal.dao.ao.AoHistoricalInvocation;
import com.atlassian.webhooks.internal.history.SimpleInvocationCounts;

import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

public class AoInvocationHistoryDao implements InvocationHistoryDao {

    // If the max length for a String field has not been specified then AO uses the default value of 255 (defined in
    // net.java.ao.types.StringType)
    @VisibleForTesting
    static final int DEFAULT_MAX_STRING_LENGTH = 255;

    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final long DAY_AS_MS = TimeUnit.DAYS.toMillis(1);
    private static final String COUNT_COLS = String.join(
            ",",
            AoDailyInvocationCounts.COLUMN_ID,
            AoDailyInvocationCounts.COLUMN_EVENT_ID,
            AoDailyInvocationCounts.COLUMN_ERRORS,
            AoDailyInvocationCounts.COLUMN_FAILURES,
            AoDailyInvocationCounts.COLUMN_SUCCESSES,
            AoDailyInvocationCounts.COLUMN_WEBHOOK_ID);
    private static final String MASKED_TEXT = "********";
    private static final String SIMPLE_INVOCATION_COLS = String.join(
            ",",
            AoHistoricalInvocation.COLUMN_ID,
            AoHistoricalInvocation.COLUMN_EVENT_ID,
            AoHistoricalInvocation.COLUMN_FINISH,
            AoHistoricalInvocation.COLUMN_OUTCOME,
            AoHistoricalInvocation.COLUMN_REQUEST_ID,
            AoHistoricalInvocation.COLUMN_REQUEST_METHOD,
            AoHistoricalInvocation.COLUMN_REQUEST_URL,
            AoHistoricalInvocation.COLUMN_RESULT_DESCRIPTION,
            AoHistoricalInvocation.COLUMN_START,
            AoHistoricalInvocation.COLUMN_WEBHOOK_ID,
            AoHistoricalInvocation.COLUMN_EVENT_SCOPE_ID,
            AoHistoricalInvocation.COLUMN_EVENT_SCOPE_TYPE);

    private static final Logger log = getLogger(AoInvocationHistoryDao.class);

    private final ActiveObjects ao;
    private final Clock clock;

    public AoInvocationHistoryDao(@Nonnull ActiveObjects ao) {
        this(ao, Clock.systemDefaultZone());
    }

    AoInvocationHistoryDao(ActiveObjects ao, Clock clock) {
        this.ao = ao;
        this.clock = clock;
    }

    @Override
    public void addCounts(
            int webhookId, @Nonnull String eventId, @Nonnull Date date, int errors, int failures, int successes) {
        requireNonNull(eventId, "eventId");
        requireNonNull(date, "date");

        long daysSinceEpoch = date.getTime() / DAY_AS_MS;
        updateCount(webhookId, eventId, daysSinceEpoch, errors, failures, successes);
    }

    @Nonnull
    @Override
    public Map<String, String> decodeHeaders(String id, String headersString) {
        if (StringUtils.isBlank(headersString)) {
            return Collections.emptyMap();
        }
        try {
            Properties properties = new Properties();
            properties.load(new StringReader(headersString));
            return Maps.fromProperties(properties);
        } catch (IOException e) {
            log.debug("Failed to parse headers for invocation {}", id, e);
            return Collections.emptyMap();
        }
    }

    @Override
    public int deleteDailyCountsOlderThan(int days) {
        long daysSinceEpoch = clock.millis() / DAY_AS_MS;
        long oldestToKeep = daysSinceEpoch - days;
        return ao.deleteWithSQL(
                AoDailyInvocationCounts.class, AoDailyInvocationCounts.COLUMN_DAYS_SINCE_EPOCH + " < ?", oldestToKeep);
    }

    @Override
    public void deleteForWebhook(int webhookId) {
        ao.deleteWithSQL(AoDailyInvocationCounts.class, AoDailyInvocationCounts.COLUMN_WEBHOOK_ID + " = ?", webhookId);
    }

    @Nonnull
    @Override
    public InvocationCounts getCounts(int webhookId, String eventId, int days) {
        long daysSinceEpoch = clock.millis() / DAY_AS_MS;
        long firstDay = daysSinceEpoch - days;

        String whereClause = AoDailyInvocationCounts.COLUMN_WEBHOOK_ID + " = ? AND "
                + AoDailyInvocationCounts.COLUMN_DAYS_SINCE_EPOCH + " >= ?";
        Object[] params;
        if (eventId == null) {
            params = new Object[] {webhookId, firstDay};
        } else {
            whereClause += " AND " + AoDailyInvocationCounts.COLUMN_EVENT_ID + " = ?";
            params = new Object[] {webhookId, firstDay, lower(eventId)};
        }

        int errors = 0;
        int failures = 0;
        int successes = 0;
        AoDailyInvocationCounts[] counts =
                ao.find(AoDailyInvocationCounts.class, Query.select().where(whereClause, params));
        for (AoDailyInvocationCounts count : counts) {
            errors += count.getErrors();
            failures += count.getFailures();
            successes += count.getSuccesses();
        }
        return new SimpleInvocationCounts(Duration.of(days, ChronoUnit.DAYS), errors, failures, successes);
    }

    @Nonnull
    @Override
    public Map<String, InvocationCounts> getCountsByEvent(
            int webhookId, @Nonnull Collection<String> eventIds, int days) {
        long daysSinceEpoch = clock.millis() / DAY_AS_MS;
        long firstDay = daysSinceEpoch - days;

        StringBuilder queryBuilder = new StringBuilder(AoDailyInvocationCounts.COLUMN_WEBHOOK_ID)
                .append(" = ? AND ")
                .append(AoDailyInvocationCounts.COLUMN_DAYS_SINCE_EPOCH)
                .append(" >= ?");
        List<Object> params = new ArrayList<>();
        params.add(webhookId);
        params.add(firstDay);
        addEventClause(queryBuilder, params, eventIds);

        CountsByEventCallback callback = new CountsByEventCallback(Duration.of(days, ChronoUnit.DAYS), eventIds);
        ao.stream(
                AoDailyInvocationCounts.class,
                Query.select(COUNT_COLS)
                        .where(queryBuilder.toString(), params.toArray())
                        .order(AoDailyInvocationCounts.COLUMN_EVENT_ID + " ASC"),
                callback);
        callback.onDone();
        return callback.getResult();
    }

    @Nonnull
    @Override
    public Map<Integer, InvocationCounts> getCountsByWebhook(@Nonnull Collection<Integer> webhookIds, int days) {
        long daysSinceEpoch = clock.millis() / DAY_AS_MS;
        long firstDay = daysSinceEpoch - days;

        String query = AoDailyInvocationCounts.COLUMN_DAYS_SINCE_EPOCH + " >= ?" + " AND "
                + AoDailyInvocationCounts.COLUMN_WEBHOOK_ID + inClauseWithPlaceholders(webhookIds);
        List<Object> params = new ArrayList<>();
        params.add(firstDay);
        params.addAll(webhookIds);

        CountsByWebhookCallback callback = new CountsByWebhookCallback(Duration.of(days, ChronoUnit.DAYS), webhookIds);
        ao.stream(
                AoDailyInvocationCounts.class,
                Query.select(COUNT_COLS)
                        .where(query, params.toArray())
                        .order(AoDailyInvocationCounts.COLUMN_WEBHOOK_ID + " ASC"),
                callback);
        callback.onDone();
        return callback.getResult();
    }

    @Override
    public AoHistoricalInvocation getLatestInvocation(
            int webhookId, String eventId, Collection<InvocationOutcome> outcomes) {
        List<Object> arguments = new ArrayList<>();
        StringBuilder queryBuilder = new StringBuilder(AoHistoricalInvocation.COLUMN_WEBHOOK_ID + " = ?");
        arguments.add(webhookId);
        addOutcomeClause(queryBuilder, arguments, outcomes);
        addEventClause(queryBuilder, arguments, eventId);
        AoHistoricalInvocation[] result = ao.find(
                AoHistoricalInvocation.class,
                Query.select()
                        .where(queryBuilder.toString(), arguments.toArray())
                        .order(AoHistoricalInvocation.COLUMN_FINISH + " DESC")
                        .limit(1));

        return result == null || result.length == 0 ? null : result[0];
    }

    @Nonnull
    @Override
    public List<AoHistoricalInvocation> getLatestInvocations(
            int webhookId, String eventId, Collection<InvocationOutcome> outcomes) {
        List<Object> arguments = new ArrayList<>();
        StringBuilder queryBuilder = new StringBuilder(AoHistoricalInvocation.COLUMN_WEBHOOK_ID + " = ?");
        arguments.add(webhookId);
        addOutcomeClause(queryBuilder, arguments, outcomes);
        addEventClause(queryBuilder, arguments, eventId);
        AoHistoricalInvocation[] result = ao.find(
                AoHistoricalInvocation.class,
                Query.select(SIMPLE_INVOCATION_COLS)
                        .where(queryBuilder.toString(), arguments.toArray())
                        .limit(InvocationOutcome.values().length));

        return result == null ? Collections.emptyList() : ImmutableList.copyOf(result);
    }

    @Nonnull
    @Override
    public Multimap<String, AoHistoricalInvocation> getLatestInvocationsByEvent(
            int webhookId, @Nonnull Collection<String> eventIds) {
        List<Object> arguments = new ArrayList<>();
        StringBuilder queryBuilder = new StringBuilder(AoHistoricalInvocation.COLUMN_WEBHOOK_ID + " = ?");
        arguments.add(webhookId);
        addEventClause(queryBuilder, arguments, eventIds);
        InvocationByEventCallback callback = new InvocationByEventCallback(eventIds);
        ao.stream(
                AoHistoricalInvocation.class,
                Query.select(SIMPLE_INVOCATION_COLS).where(queryBuilder.toString(), arguments.toArray()),
                callback);

        return callback.getResult();
    }

    @Nonnull
    @Override
    public Multimap<Integer, AoHistoricalInvocation> getLatestInvocationsByWebhook(@Nonnull Collection<Integer> ids) {
        InvocationByWebhookCallback callback = new InvocationByWebhookCallback();
        ao.stream(
                AoHistoricalInvocation.class,
                Query.select(SIMPLE_INVOCATION_COLS)
                        .where(AoHistoricalInvocation.COLUMN_WEBHOOK_ID + inClauseWithPlaceholders(ids), ids.toArray()),
                callback);

        return callback.getResult();
    }

    @Nonnull
    @Override
    public Multimap<Integer, AoHistoricalInvocation> getLatestInvocationsByWebhookAndScope(
            @Nonnull Collection<Integer> ids, @Nonnull WebhookScope scope) {

        String query = AoHistoricalInvocation.COLUMN_EVENT_SCOPE_ID + " = ?" + " AND "
                + AoHistoricalInvocation.COLUMN_EVENT_SCOPE_TYPE + " = ? AND "
                + AoDailyInvocationCounts.COLUMN_WEBHOOK_ID
                + inClauseWithPlaceholders(ids);

        List<Object> params = new ArrayList<>();
        params.add(scope.getId().orElse(null));
        params.add(scope.getType());
        params.addAll(ids);

        InvocationByWebhookCallback callback = new InvocationByWebhookCallback();
        ao.stream(
                AoHistoricalInvocation.class,
                Query.select(SIMPLE_INVOCATION_COLS).where(query, params.toArray()),
                callback);

        return callback.getResult();
    }

    @Override
    public void saveInvocation(int webhookId, @Nonnull DetailedInvocation invocation) {
        if (invocation
                .getEventScope()
                .map(scope -> !scope.getId().isPresent() && !scope.getType().equals(WebhookScope.TYPE_GLOBAL))
                .orElse(false)) {
            throw new IllegalArgumentException("Event scope must have an ID if it is not global");
        }

        InvocationOutcome outcome = invocation.getResult().getOutcome();
        DetailedInvocationRequest request = invocation.getRequest();
        DetailedInvocationResult result = invocation.getResult();
        String id = invocationPk(
                webhookId,
                invocation.getEvent().getId(),
                invocation.getEventScope().orElse(null),
                outcome);

        AoHistoricalInvocation current = ao.get(AoHistoricalInvocation.class, id);
        if (current != null && current.getFinish() > invocation.getFinish().toEpochMilli()) {
            // the persisted invocation is newer, nothing to do
            return;
        }

        ImmutableMap.Builder<String, Object> builder = ImmutableMap.<String, Object>builder()
                .put(AoHistoricalInvocation.COLUMN_ID, id)
                .put(
                        AoHistoricalInvocation.COLUMN_EVENT_ID,
                        lower(invocation.getEvent().getId()))
                .put(
                        AoHistoricalInvocation.COLUMN_FINISH,
                        invocation.getFinish().toEpochMilli())
                .put(AoHistoricalInvocation.COLUMN_OUTCOME, outcome)
                .put(AoHistoricalInvocation.COLUMN_REQUEST_ID, invocation.getId())
                .put(
                        AoHistoricalInvocation.COLUMN_REQUEST_METHOD,
                        request.getMethod().name())
                .put(AoHistoricalInvocation.COLUMN_REQUEST_URL, sanitise(request.getUrl()))
                .put(AoHistoricalInvocation.COLUMN_RESULT_DESCRIPTION, sanitise(result.getDescription()))
                .put(AoHistoricalInvocation.COLUMN_START, invocation.getStart().toEpochMilli())
                .put(AoHistoricalInvocation.COLUMN_WEBHOOK_ID, webhookId);

        invocation.getEventScope().ifPresent(eventScope -> {
            // We insert an empty string instead of null in the table to be consistent with the way the MigrateToV2Task
            // handled this case. On read, we interpret empty strings as null again.
            builder.put(
                    AoHistoricalInvocation.COLUMN_EVENT_SCOPE_ID,
                    eventScope.getId().orElse(StringUtils.EMPTY));
            builder.put(AoHistoricalInvocation.COLUMN_EVENT_SCOPE_TYPE, eventScope.getType());
        });

        maybePut(
                builder,
                AoHistoricalInvocation.COLUMN_REQUEST_BODY,
                formatJsonBody(request.getBody().orElse(null)));
        maybePut(builder, AoHistoricalInvocation.COLUMN_REQUEST_HEADERS, encodeHeaders(request.getHeaders()));

        if (result instanceof DetailedInvocationError) {
            maybePut(
                    builder,
                    AoHistoricalInvocation.COLUMN_ERROR_CONTENT,
                    ((DetailedInvocationError) result).getContent());
        } else if (result instanceof DetailedInvocationResponse) {
            DetailedInvocationResponse response = (DetailedInvocationResponse) result;
            maybePut(
                    builder,
                    AoHistoricalInvocation.COLUMN_RESPONSE_BODY,
                    formatJsonBody(response.getBody().orElse(null)));
            maybePut(builder, AoHistoricalInvocation.COLUMN_RESPONSE_HEADERS, encodeHeaders(response.getHeaders()));
            builder.put(AoHistoricalInvocation.COLUMN_STATUS_CODE, response.getStatusCode());
        }

        if (current != null) {
            ao.delete(current);
        }
        ao.create(AoHistoricalInvocation.class, builder.build());
    }

    @VisibleForTesting
    protected static Map<String, String> maskAuthorizationHeader(Map<String, String> headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER)) {
            return headers;
        }
        String authorizationHeader = headers.get(AUTHORIZATION_HEADER);
        String authorizationType = authorizationHeader.contains(StringUtils.SPACE)
                ? authorizationHeader.split(StringUtils.SPACE, 2)[0] + StringUtils.SPACE
                : StringUtils.EMPTY;
        // Need to make and return a copy of the map as headers is defined as an UnmodifiableMap
        Map<String, String> newHeaders = new HashMap<>(headers);
        newHeaders.put(AUTHORIZATION_HEADER, authorizationType + MASKED_TEXT);
        return newHeaders;
    }

    private static void addEventClause(StringBuilder queryBuilder, List<Object> arguments, String eventId) {
        if (StringUtils.isBlank(eventId)) {
            return;
        }

        queryBuilder
                .append(" AND ")
                .append(AoHistoricalInvocation.COLUMN_EVENT_ID)
                .append(" = ?");
        arguments.add(lower(eventId));
    }

    private static void addEventClause(
            StringBuilder queryBuilder, List<Object> arguments, Collection<String> eventIds) {
        if (eventIds == null || eventIds.isEmpty()) {
            return;
        }

        queryBuilder
                .append(" AND ")
                .append(AoHistoricalInvocation.COLUMN_EVENT_ID)
                .append(inClauseWithPlaceholders(eventIds));
        eventIds.forEach(eventId -> arguments.add(lower(eventId)));
    }

    private static void addOutcomeClause(
            StringBuilder queryBuilder, List<Object> arguments, Collection<InvocationOutcome> outcomes) {
        if (outcomes == null || outcomes.isEmpty()) {
            return;
        }

        queryBuilder
                .append(" AND ")
                .append(AoHistoricalInvocation.COLUMN_OUTCOME)
                .append(inClauseWithPlaceholders(outcomes));
        arguments.addAll(outcomes);
    }

    private static String countsPk(int webhookId, String eventId, long daysSinceEpoch) {
        return webhookId + "." + lower(eventId) + "." + daysSinceEpoch;
    }

    private static String encodeHeaders(Map<String, String> headers) {
        Properties properties = new Properties();
        properties.putAll(maskAuthorizationHeader(headers));

        StringWriter writer = new StringWriter();
        try {
            properties.store(writer, null);
            String encoded = writer.toString();
            // strip off the leading comment with the date
            if (encoded.startsWith("#")) {
                int eol = encoded.indexOf('\n');
                if (eol > 0 && eol + 1 < encoded.length()) {
                    encoded = encoded.substring(eol + 1);
                }
            }
            return encoded;
        } catch (IOException e) {
            return null;
        }
    }

    private static String formatJsonBody(String body) {
        String sanitised = body != null ? body.replace("\0", "") : null;

        try {
            return sanitised != null ? (new JSONObject(sanitised)).toString(4) : null;
        } catch (JSONException e) {
            return sanitised;
        }
    }

    private static String inClauseWithPlaceholders(Collection<?> items) {
        return " IN (" + items.stream().map(item -> "?").collect(Collectors.joining(", ")) + ")";
    }

    private static String invocationPk(
            int webhookId, String eventId, WebhookScope eventScope, InvocationOutcome outcome) {
        String eventScopePk = StringUtils.EMPTY;
        if (eventScope != null) {
            String eventScopeId = eventScope.getId().orElse(null);
            String eventScopeType = eventScope.getType();
            eventScopePk = eventScopeId != null ? eventScopeId + "." + eventScopeType + "." : eventScopeType + ".";
        }
        return webhookId + "." + lower(eventId) + "." + eventScopePk
                + outcome.name().substring(0, 1);
    }

    private static String lower(String value) {
        return StringUtils.lowerCase(value, Locale.ROOT);
    }

    private static void maybePut(ImmutableMap.Builder<String, Object> builder, String key, String value) {
        String trimmed = StringUtils.trimToNull(value);
        if (trimmed != null) {
            builder.put(key, value);
        }
    }

    private static String sanitise(String details) {
        // Better to store something than throw an exception
        if (StringUtils.length(details) > DEFAULT_MAX_STRING_LENGTH) {
            log.trace("Truncating to {} chars: {}", DEFAULT_MAX_STRING_LENGTH, details);
            return StringUtils.substring(details, 0, DEFAULT_MAX_STRING_LENGTH);
        }
        return details;
    }

    private void updateCount(
            int webhookId, String eventId, long daysSinceEpoch, int errors, int failures, int successes) {

        String dailyCountId = countsPk(webhookId, eventId, daysSinceEpoch);
        // retrieve, delete and recreate the count in a single transaction to ensure that we cannot lose
        // data if the count gets updated (on another node) concurrently. It's unfortunate that AO does not support
        // something simple as ao.updateWithSql
        ao.executeInTransaction(() -> {
            int updatedErrors = errors;
            int updatedFailures = failures;
            int updatedSuccesses = successes;

            AoDailyInvocationCounts current = ao.get(AoDailyInvocationCounts.class, dailyCountId);
            if (current != null) {
                updatedErrors += current.getErrors();
                updatedFailures += current.getFailures();
                updatedSuccesses += current.getSuccesses();
                ao.delete(current);
            }

            ao.create(
                    AoDailyInvocationCounts.class,
                    ImmutableMap.<String, Object>builder()
                            .put(AoDailyInvocationCounts.COLUMN_ID, dailyCountId)
                            .put(AoDailyInvocationCounts.COLUMN_DAYS_SINCE_EPOCH, daysSinceEpoch)
                            .put(AoDailyInvocationCounts.COLUMN_ERRORS, updatedErrors)
                            .put(AoDailyInvocationCounts.COLUMN_EVENT_ID, lower(eventId))
                            .put(AoDailyInvocationCounts.COLUMN_FAILURES, updatedFailures)
                            .put(AoDailyInvocationCounts.COLUMN_SUCCESSES, updatedSuccesses)
                            .put(AoDailyInvocationCounts.COLUMN_WEBHOOK_ID, webhookId)
                            .build());

            return null;
        });
    }

    private static class CountsByEventCallback implements EntityStreamCallback<AoDailyInvocationCounts, String> {

        private final Duration duration;
        private final Map<String, String> lowerToRequestedEventId;
        private final Map<String, InvocationCounts> result;

        private int errors;
        private int failures;
        private int successes;
        private String currentEvent;

        private CountsByEventCallback(Duration duration, Collection<String> eventIds) {
            this.duration = duration;
            lowerToRequestedEventId =
                    eventIds.stream().collect(Collectors.toMap(AoInvocationHistoryDao::lower, Function.identity()));
            result = new HashMap<>(eventIds.size());
        }

        @Override
        public void onRowRead(AoDailyInvocationCounts count) {
            String eventId = count.getEventId();
            if (!eventId.equals(currentEvent)) {
                popCurrentEvent();
                errors = failures = successes = 0;
                currentEvent = eventId;
            }
            errors += count.getErrors();
            failures += count.getFailures();
            successes += count.getSuccesses();
        }

        void onDone() {
            popCurrentEvent();
            InvocationCounts empty = new SimpleInvocationCounts(duration, 0, 0, 0);
            lowerToRequestedEventId.values().forEach(eventId -> result.putIfAbsent(eventId, empty));
        }

        Map<String, InvocationCounts> getResult() {
            return result;
        }

        private void popCurrentEvent() {
            if (currentEvent != null) {
                result.put(
                        lowerToRequestedEventId.get(currentEvent),
                        new SimpleInvocationCounts(duration, errors, failures, successes));
            }
            currentEvent = null;
        }
    }

    private static class CountsByWebhookCallback implements EntityStreamCallback<AoDailyInvocationCounts, String> {

        private final Duration duration;
        private final Collection<Integer> ids;
        private final Map<Integer, InvocationCounts> result;

        private int errors;
        private int failures;
        private int successes;
        private Integer currentId;

        private CountsByWebhookCallback(Duration duration, Collection<Integer> webhookIds) {
            this.duration = duration;
            this.ids = webhookIds;

            result = new HashMap<>(webhookIds.size());
        }

        @Override
        public void onRowRead(AoDailyInvocationCounts count) {
            Integer webhookId = count.getWebhookId();
            if (!webhookId.equals(currentId)) {
                popCurrentItem();
                errors = failures = successes = 0;
                currentId = webhookId;
            }
            errors += count.getErrors();
            failures += count.getFailures();
            successes += count.getSuccesses();
        }

        void onDone() {
            popCurrentItem();
            InvocationCounts empty = new SimpleInvocationCounts(duration, 0, 0, 0);
            ids.forEach(webhookId -> result.putIfAbsent(webhookId, empty));
        }

        Map<Integer, InvocationCounts> getResult() {
            return result;
        }

        private void popCurrentItem() {
            if (currentId != null) {
                result.put(currentId, new SimpleInvocationCounts(duration, errors, failures, successes));
            }
            currentId = null;
        }
    }

    private static class InvocationByEventCallback implements EntityStreamCallback<AoHistoricalInvocation, String> {

        private final Map<String, String> lowerToRequestedEventId;
        private final ListMultimap<String, AoHistoricalInvocation> result;

        private InvocationByEventCallback(Collection<String> eventIds) {
            lowerToRequestedEventId =
                    eventIds.stream().collect(Collectors.toMap(AoInvocationHistoryDao::lower, Function.identity()));
            result = ArrayListMultimap.create();
        }

        @Override
        public void onRowRead(AoHistoricalInvocation invocation) {
            result.put(lowerToRequestedEventId.get(invocation.getEventId()), invocation);
        }

        Multimap<String, AoHistoricalInvocation> getResult() {
            return result;
        }
    }

    private static class InvocationByWebhookCallback implements EntityStreamCallback<AoHistoricalInvocation, String> {

        private final ListMultimap<Integer, AoHistoricalInvocation> result;

        private InvocationByWebhookCallback() {
            result = ArrayListMultimap.create();
        }

        @Override
        public void onRowRead(AoHistoricalInvocation invocation) {
            result.put(invocation.getWebhookId(), invocation);
        }

        Multimap<Integer, AoHistoricalInvocation> getResult() {
            return result;
        }
    }
}
