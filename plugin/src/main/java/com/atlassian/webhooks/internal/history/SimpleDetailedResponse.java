package com.atlassian.webhooks.internal.history;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import com.google.common.io.CharStreams;

import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.history.DetailedInvocationResponse;
import com.atlassian.webhooks.history.InvocationOutcome;
import com.atlassian.webhooks.request.WebhookHttpResponse;
import com.atlassian.webhooks.request.WebhookResponseBody;

import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

public class SimpleDetailedResponse implements DetailedInvocationResponse {

    private static final Logger log = getLogger(SimpleDetailedResponse.class);

    private final String body;
    private final String description;
    private final Map<String, String> headers;
    private final InvocationOutcome outcome;
    private final int statusCode;

    public SimpleDetailedResponse(
            @Nonnull InvocationOutcome outcome,
            @Nonnull WebhookInvocation invocation,
            @Nonnull WebhookHttpResponse response) {
        this.outcome = requireNonNull(outcome, "outcome");
        description = describe(outcome, requireNonNull(response, "response"));
        headers = response.getHeaders().getHeaders();
        String body;
        try {
            body = loadBody(response);
        } catch (IOException e) {
            log.debug("Failed to load response body for webhook invocation {}", invocation.getId());
            body = null;
        }
        this.body = body;
        statusCode = response.getStatusCode();
    }

    public SimpleDetailedResponse(
            String body,
            @Nonnull String description,
            @Nonnull Map<String, String> headers,
            @Nonnull InvocationOutcome outcome,
            int statusCode) {

        this.body = body;
        this.description = description;
        this.headers = headers;
        this.outcome = outcome;
        this.statusCode = statusCode;
    }

    @Nonnull
    @Override
    public Optional<String> getBody() {
        return Optional.ofNullable(body);
    }

    @Nonnull
    @Override
    public String getDescription() {
        return description;
    }

    @Nonnull
    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Nonnull
    @Override
    public InvocationOutcome getOutcome() {
        return outcome;
    }

    @Override
    public int getStatusCode() {
        return statusCode;
    }

    private String describe(InvocationOutcome resultKind, WebhookHttpResponse response) {
        switch (resultKind) {
            case FAILURE:
            case SUCCESS:
                return Integer.toString(response.getStatusCode());
            default:
                return "Unknown";
        }
    }

    private String loadBody(WebhookHttpResponse response) throws IOException {
        WebhookResponseBody responseBody = response.getBody();
        String contentType = responseBody.getContentType().orElse(null);

        if (!BodyUtils.isTextContent(contentType)) {
            return "<binary data>";
        }

        return CharStreams.toString(new BufferedReader(
                new InputStreamReader(responseBody.getContent(), BodyUtils.getCharset(contentType))));
    }
}
