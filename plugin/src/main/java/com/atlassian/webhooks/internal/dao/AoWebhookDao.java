package com.atlassian.webhooks.internal.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.ImmutableMap;

import net.java.ao.Entity;
import net.java.ao.Query;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.webhooks.AbstractWebhookRequest;
import com.atlassian.webhooks.WebhookCreateRequest;
import com.atlassian.webhooks.WebhookCredentials;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.WebhookSearchRequest;
import com.atlassian.webhooks.WebhookUpdateRequest;
import com.atlassian.webhooks.internal.dao.ao.AoWebhook;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookConfigurationEntry;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookEvent;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

public class AoWebhookDao implements WebhookDao {

    private static final Map<Class, String> AO_ALIAS_NAMES = ImmutableMap.of(
            AoWebhookEvent.class, "evt",
            AoWebhookConfigurationEntry.class, "config",
            AoWebhook.class, "webhook");
    private static final AoWebhook[] NO_RESULTS = new AoWebhook[0];
    private final ActiveObjects ao;

    public AoWebhookDao(ActiveObjects ao) {
        this.ao = ao;
    }

    @Override
    @Nonnull
    public AoWebhook create(@Nonnull WebhookCreateRequest request) {
        requireNonNull(request, "request");

        Date now = new Date();
        WebhookScope scope = request.getScope();
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.<String, Object>builder()
                .put(AoWebhook.CREATED_COLUMN, now)
                .put(AoWebhook.UPDATED_COLUMN, now)
                .put(AoWebhook.URL_COLUMN, request.getUrl())
                .put(AoWebhook.NAME_COLUMN, request.getName())
                .put(AoWebhook.ACTIVE_COLUMN, request.isActive())
                .put(AoWebhook.SCOPE_TYPE_COLUMN, scope.getType())
                .put(AoWebhook.SSL_VERIFICATION_REQUIRED_COLUMN, request.isSslVerificationRequired());

        scope.getId().ifPresent(id -> builder.put(AoWebhook.SCOPE_ID_COLUMN, id));

        request.getCredentials().ifPresent(credentials -> {
            credentials.getUsername().ifPresent(username -> builder.put(AoWebhook.USERNAME_COLUMN, username));
            credentials.getPassword().ifPresent(password -> builder.put(AoWebhook.PASSWORD_COLUMN, password));
        });

        return ao.executeInTransaction(() -> {
            AoWebhook aoWebhook = ao.create(AoWebhook.class, builder.build());

            saveForeignObjects(request, aoWebhook.getID());

            return aoWebhook;
        });
    }

    @Override
    public boolean delete(int id) {
        return ao.executeInTransaction(() -> {
            AoWebhook aoWebhook = ao.get(AoWebhook.class, id);
            if (aoWebhook == null) {
                return false;
            }
            ao.delete(aoWebhook.getEvents());
            ao.delete(aoWebhook.getConfiguration());
            ao.delete(aoWebhook);
            return true;
        });
    }

    @Override
    public void delete(AoWebhook[] webhooks) {
        requireNonNull(webhooks, "webhooks");
        if (webhooks.length == 0) {
            return;
        }
        ao.executeInTransaction(() -> {
            List<Integer> ids = Arrays.stream(webhooks).map(Entity::getID).collect(toList());

            String idParams = Collections.nCopies(webhooks.length, "?").stream().collect(Collectors.joining(","));

            ao.deleteWithSQL(
                    AoWebhookEvent.class,
                    AoWebhookEvent.WEBHOOK_COLUMN_QUERY + " IN (" + idParams + ")",
                    ids.toArray());
            ao.deleteWithSQL(
                    AoWebhookConfigurationEntry.class,
                    AoWebhookConfigurationEntry.WEBHOOK_COLUMN_QUERY + " IN (" + idParams + ")",
                    ids.toArray());
            ao.delete(webhooks);
            return null;
        });
    }

    @Override
    @Nullable
    public AoWebhook getById(int id) {
        return ao.get(AoWebhook.class, id);
    }

    @Override
    @Nonnull
    public AoWebhook[] search(@Nonnull WebhookSearchRequest search) {
        return ao.executeInTransaction(() -> {
            requireNonNull(search, "search");
            Query query = Query.select("ID");
            List<String> whereJoinedByAnd = new ArrayList<>();
            List<Object> params = new ArrayList<>();

            AO_ALIAS_NAMES.forEach(query::alias);

            if (search.getId() != null) {
                whereJoinedByAnd.add("ID = ?");
                params.add(search.getId());
            }

            if (!search.getEvents().isEmpty()) {
                addEventBasedQuery(search, query, whereJoinedByAnd, params);
            }

            if (!search.getScopes().isEmpty() || !search.getScopeTypes().isEmpty()) {
                addScopeBasedQuery(search, whereJoinedByAnd, params);
            }

            if (search.getName() != null) {
                whereJoinedByAnd.add(getTableName(AoWebhook.class) + '.' + AoWebhook.NAME_COLUMN + " = ?");
                params.add(search.getName());
            }

            if (search.getActive() != null) {
                whereJoinedByAnd.add(getTableName(AoWebhook.class) + '.' + AoWebhook.ACTIVE_COLUMN + " = ?");
                params.add(search.getActive());
            }

            if (!whereJoinedByAnd.isEmpty()) {
                query.where(String.join(" AND ", whereJoinedByAnd), params.toArray());
            }

            query.limit(search.getLimit());
            query.offset(search.getOffset());
            query.distinct();
            query.order("ID ASC");

            Collection<Integer> ids = new ArrayList<>();
            ao.stream(AoWebhook.class, query, aoWebhook -> ids.add(aoWebhook.getID()));

            if (!ids.isEmpty()) {
                String idParams = Collections.nCopies(ids.size(), "?").stream().collect(Collectors.joining(","));

                Query inQuery = Query.select();
                query.order("ID ASC");
                AO_ALIAS_NAMES.forEach(inQuery::alias);

                return ao.find(
                        AoWebhook.class,
                        inQuery.where(getTableName(AoWebhook.class) + ".ID IN (" + idParams + ")", ids.toArray()));
            }

            return NO_RESULTS;
        });
    }

    @Override
    public AoWebhook update(int id, @Nonnull WebhookUpdateRequest request) {
        requireNonNull(request, "request");
        return ao.executeInTransaction(() -> {
            AoWebhook aoWebhook = ao.get(AoWebhook.class, id);
            if (aoWebhook == null) {
                return null;
            }

            aoWebhook.setName(request.getName());
            aoWebhook.setUrl(request.getUrl());
            aoWebhook.setActive(request.isActive());
            WebhookScope scope = request.getScope();
            aoWebhook.setScopeId(scope.getId().orElse(null));
            aoWebhook.setScopeType(scope.getType());
            aoWebhook.setSslVerificationRequired(request.isSslVerificationRequired());
            aoWebhook.setUpdatedDate(new Date());

            WebhookCredentials credentials = request.getCredentials().orElse(null);
            if (credentials != null) {
                credentials.getUsername().ifPresent(aoWebhook::setUsername);
                credentials.getPassword().ifPresent(aoWebhook::setPassword);
            } else {
                aoWebhook.setUsername(null);
                aoWebhook.setPassword(null);
            }

            ao.delete(aoWebhook.getConfiguration());
            ao.delete(aoWebhook.getEvents());

            saveForeignObjects(request, aoWebhook.getID());

            aoWebhook.save();

            // reload to refresh the relationships for the foreign objects
            return ao.get(AoWebhook.class, id);
        });
    }

    private void addEventBasedQuery(WebhookSearchRequest search, Query query, List<String> where, List<Object> params) {
        query.join(
                AoWebhookEvent.class,
                getTableName(AoWebhookEvent.class) + '.' + AoWebhookEvent.WEBHOOK_COLUMN_QUERY
                        + " = " + getTableName(AoWebhook.class)
                        + ".ID");

        String whereQuery = group(search.getEvents().stream()
                .map(event -> getTableName(AoWebhookEvent.class) + '.' + AoWebhookEvent.EVENT_ID_COLUMN + " = ?")
                .collect(Collectors.joining(" OR ")));

        where.add(whereQuery);
        search.getEvents().stream().map(WebhookEvent::getId).forEach(params::add);
    }

    private void addScopeBasedQuery(WebhookSearchRequest search, List<String> where, List<Object> params) {
        if (!search.getScopes().isEmpty()) {
            addScopeQuery(search, where, params);
        } else {
            addScopeTypeQuery(search, where, params);
        }
    }

    private void addScopeTypeQuery(WebhookSearchRequest search, List<String> where, List<Object> params) {
        where.add(group(search.getScopeTypes().stream()
                .map(type -> AoWebhook.SCOPE_TYPE_COLUMN + " = ?")
                .collect(Collectors.joining(" OR "))));
        params.addAll(search.getScopeTypes());
    }

    private void addScopeQuery(WebhookSearchRequest search, List<String> where, List<Object> params) {
        where.add(group(search.getScopes().stream()
                .map(scope -> {
                    if (scope.getId().isPresent()) {
                        // scope with an ID
                        String whereQuery =
                                group(AoWebhook.SCOPE_TYPE_COLUMN + " = ? AND " + AoWebhook.SCOPE_ID_COLUMN + " = ? ");

                        params.add(scope.getType());
                        params.add(scope.getId().get());

                        return whereQuery;
                    }
                    // no scope ID associated with the scope
                    String whereQuery =
                            group(AoWebhook.SCOPE_TYPE_COLUMN + " = ? AND " + AoWebhook.SCOPE_ID_COLUMN + " IS NULL ");

                    params.add(scope.getType());

                    return whereQuery;
                })
                .collect(Collectors.joining(" OR "))));
    }

    private String getTableName(Class clazz) {
        /*
        Here we are specifically not getting the table name according to AO, but instead we're referring to them as
        aliases. This is due to the fact that ON clauses cannot contain " characters according to the SqlUtil. This
        means that databases such as Postgres will not consider them a table name. If we instead use lowercase aliases
        we can get around this problem.
         */
        return AO_ALIAS_NAMES.get(clazz);
    }

    private String group(String query) {
        return "(" + query + ")";
    }

    private void saveForeignObjects(AbstractWebhookRequest request, int webhookId) {
        request.getConfiguration()
                .forEach((key, value) -> ao.create(
                        AoWebhookConfigurationEntry.class,
                        ImmutableMap.of(
                                AoWebhookConfigurationEntry.KEY_COLUMN, key,
                                AoWebhookConfigurationEntry.WEBHOOK_COLUMN_QUERY, webhookId,
                                AoWebhookConfigurationEntry.VALUE_COLUMN, value)));

        request.getEvents()
                .forEach(event -> ao.create(
                        AoWebhookEvent.class,
                        ImmutableMap.of(
                                AoWebhookEvent.EVENT_ID_COLUMN,
                                event.getId(),
                                AoWebhookEvent.WEBHOOK_COLUMN_QUERY,
                                webhookId)));
    }
}
