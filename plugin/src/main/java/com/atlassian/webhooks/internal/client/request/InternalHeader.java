package com.atlassian.webhooks.internal.client.request;

import javax.annotation.Nonnull;

import com.atlassian.webhooks.request.Header;

import static java.util.Objects.requireNonNull;

public class InternalHeader implements Header {

    private final String name;
    private final String value;

    public InternalHeader(@Nonnull String name, @Nonnull String value) {
        this.name = requireNonNull(name, "name");
        this.value = requireNonNull(value, "value");
    }

    @Nonnull
    @Override
    public String getName() {
        return name;
    }

    @Nonnull
    @Override
    public String getValue() {
        return value;
    }
}
