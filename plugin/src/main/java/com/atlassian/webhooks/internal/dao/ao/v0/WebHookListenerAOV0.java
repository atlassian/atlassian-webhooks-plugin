package com.atlassian.webhooks.internal.dao.ao.v0;

import java.util.Date;

import net.java.ao.Entity;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

@Table("WEB_HOOK_LISTENER_AO")
public interface WebHookListenerAOV0 extends Entity {
    @NotNull
    @StringLength(StringLength.UNLIMITED)
    String getUrl();

    void setUrl(String url);

    @NotNull
    @StringLength(StringLength.UNLIMITED)
    String getName();

    void setName(String name);

    @StringLength(StringLength.UNLIMITED)
    String getDescription();

    void setDescription(String description);

    String getLastUpdatedUser();

    void setLastUpdatedUser(String username);

    @NotNull
    Date getLastUpdated();

    void setLastUpdated(Date updated);

    boolean isExcludeBody();

    void setExcludeBody(boolean excludeBody);

    @StringLength(StringLength.UNLIMITED)
    String getFilters();

    void setFilters(String filtersJson);

    // This is a legacy column, not used anymore. Nothing should be in there, but we leave it just in case
    @StringLength(StringLength.UNLIMITED)
    String getParameters();

    void setParameters(String parameters);

    // Was this created via REST, UI or SERVICE
    @NotNull
    String getRegistrationMethod();

    void setRegistrationMethod(String method);

    @StringLength(StringLength.UNLIMITED)
    String getEvents();

    void setEvents(String events);

    boolean isEnabled();

    void setEnabled(boolean enabled);
}
