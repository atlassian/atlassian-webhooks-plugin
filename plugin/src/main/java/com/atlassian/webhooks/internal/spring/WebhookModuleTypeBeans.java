package com.atlassian.webhooks.internal.spring;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.osgi.framework.ServiceRegistration;

import com.atlassian.plugins.osgi.javaconfig.configs.beans.ModuleFactoryBean;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.internal.Validator;
import com.atlassian.webhooks.internal.module.WebhookModuleDescriptorFactory;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportAsModuleType;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
@Import(ModuleFactoryBean.class)
@SuppressWarnings("rawtypes")
public class WebhookModuleTypeBeans {

    @Bean
    public ApplicationProperties applicationProperties() {
        return importOsgiService(ApplicationProperties.class);
    }

    @Bean
    public Validator validator() {
        return importOsgiService(Validator.class);
    }

    @Bean
    public WebhookService webhookService() {
        return importOsgiService(WebhookService.class);
    }

    @Bean
    public WebhookModuleDescriptorFactory webhookModuleDescriptorFactory() {
        return new WebhookModuleDescriptorFactory();
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportWebhookModuleDescriptorFactory(
            final WebhookModuleDescriptorFactory moduleDescriptorFactory) {
        return exportAsModuleType(moduleDescriptorFactory);
    }
}
