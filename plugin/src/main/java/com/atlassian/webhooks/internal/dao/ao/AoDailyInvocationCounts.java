package com.atlassian.webhooks.internal.dao.ao;

import net.java.ao.Accessor;
import net.java.ao.Mutator;
import net.java.ao.RawEntity;
import net.java.ao.schema.Index;
import net.java.ao.schema.Indexes;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.PrimaryKey;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

@Indexes({
    @Index(
            name = AoDailyInvocationCounts.IDX_WEBHOOK_COUNTS_WH_EV,
            methodNames = {"getWebhookId", "getEventId"})
})
@Table(AoDailyInvocationCounts.TABLE_NAME)
public interface AoDailyInvocationCounts extends RawEntity<String> {

    String COLUMN_DAYS_SINCE_EPOCH = "DAY_SINCE_EPOCH";
    String COLUMN_ERRORS = "ERRORS";
    String COLUMN_EVENT_ID = "EVENT_ID";
    String COLUMN_FAILURES = "FAILURES";
    String COLUMN_ID = "ID";
    String COLUMN_SUCCESSES = "SUCCESSES";
    String COLUMN_WEBHOOK_ID = "WEBHOOK_ID";
    String TABLE_NAME = "DAILY_COUNTS";

    String IDX_WEBHOOK_COUNTS_WH_EV = "IDX_WEBHOOK_COUNTS_WH_EV";

    @Accessor(COLUMN_DAYS_SINCE_EPOCH)
    @NotNull
    long getDaysSinceEpoch();

    @Accessor(COLUMN_ERRORS)
    @NotNull
    int getErrors();

    @Accessor(COLUMN_EVENT_ID)
    @NotNull
    @StringLength(64)
    String getEventId();

    @PrimaryKey(COLUMN_ID)
    @StringLength(88) // 10 for webhook-id (digits) + 64 for event-id + 12 for days-since-epoch (digits) + 2 separators
    @NotNull
    String getId();

    @Accessor(COLUMN_FAILURES)
    @NotNull
    int getFailures();

    @Accessor(COLUMN_SUCCESSES)
    @NotNull
    int getSuccesses();

    @Accessor(COLUMN_WEBHOOK_ID)
    @NotNull
    int getWebhookId();

    @Mutator(COLUMN_ERRORS)
    void setErrors(int count);

    @Mutator(COLUMN_FAILURES)
    void setFailures(int count);

    @Mutator(COLUMN_SUCCESSES)
    void setSuccesses(int count);
}
