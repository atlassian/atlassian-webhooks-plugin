package com.atlassian.webhooks.internal.history;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;
import com.google.common.net.MediaType;

import static com.google.common.net.MediaType.ANY_TEXT_TYPE;
import static com.google.common.net.MediaType.JSON_UTF_8;
import static java.util.Optional.ofNullable;

public class BodyUtils {

    /**
     * @return true if the content type is known to be text. if unspecified, assume it to be text
     */
    public static boolean isTextContent(String contentType) {
        try {
            MediaType mediaType = StringUtils.isBlank(contentType) ? null : MediaType.parse(contentType);
            return mediaType == null || (mediaType.is(ANY_TEXT_TYPE) || mediaType.is(JSON_UTF_8.withoutParameters()));
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    /**
     * @return the charset specified in the content-type of the request or response or UTF-8 otherwise
     */
    @Nonnull
    public static Charset getCharset(String contentType) {
        try {
            return ofNullable(contentType)
                    .map(MediaType::parse)
                    .flatMap(mediaType -> ofNullable(mediaType.charset().orNull()))
                    .orElse(StandardCharsets.UTF_8);
        } catch (IllegalArgumentException e) {
            return StandardCharsets.UTF_8;
        }
    }
}
