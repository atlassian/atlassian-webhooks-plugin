package com.atlassian.webhooks.internal.model;

import java.util.Objects;
import java.util.Optional;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.webhooks.WebhookScope;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

public class SimpleWebhookScope implements WebhookScope {

    private final String id;
    private final String type;

    public SimpleWebhookScope(@Nonnull String type, @Nullable String id) {
        this.id = id;
        this.type = requireNonNull(type, "type");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SimpleWebhookScope that = (SimpleWebhookScope) o;
        return Objects.equals(getType(), that.getType()) && Objects.equals(getId(), that.getId());
    }

    @Nonnull
    @Override
    public Optional<String> getId() {
        return ofNullable(id);
    }

    @Nonnull
    @Override
    public String getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getId());
    }
}
