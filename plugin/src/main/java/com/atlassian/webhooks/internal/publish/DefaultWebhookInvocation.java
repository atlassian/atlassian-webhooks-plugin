package com.atlassian.webhooks.internal.publish;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.annotation.Nonnull;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;

import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookCallback;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookPublishRequest;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.internal.client.request.RawRequest;
import com.atlassian.webhooks.internal.configuration.FeatureFlagService;
import com.atlassian.webhooks.request.Method;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

import static com.atlassian.webhooks.internal.UrlConstructor.constructUrl;

public class DefaultWebhookInvocation implements InternalWebhookInvocation {

    private static final Method DEFAULT_METHOD = Method.POST;

    private final RawRequest.Builder builder;
    private final List<WebhookCallback> callbacks;
    private final WebhookEvent event;
    private final WebhookScope eventScope;
    private final Webhook hook;
    private final Object payload;
    private final String id;

    public DefaultWebhookInvocation(
            @Nonnull FeatureFlagService featureFlagService,
            @Nonnull Webhook hook,
            @Nonnull WebhookPublishRequest webhookRequest) {
        this(featureFlagService, hook, UUID.randomUUID().toString(), webhookRequest);
    }

    @VisibleForTesting
    DefaultWebhookInvocation(
            @Nonnull FeatureFlagService featureFlagService,
            @Nonnull Webhook hook,
            @Nonnull String id,
            @Nonnull WebhookPublishRequest webhookRequest) {
        this.hook = requireNonNull(hook, "hook");
        this.id = requireNonNull(id, "id");

        boolean skipUrlReEncoding = featureFlagService.shouldSkipUrlReEncoding();
        builder = RawRequest.builder(
                        DEFAULT_METHOD,
                        constructUrl(hook.getUrl(), webhookRequest.getContext(), skipUrlReEncoding),
                        hook.isSslVerificationRequired())
                .skipUrlEncoding(skipUrlReEncoding);
        callbacks =
                new ArrayList<>(requireNonNull(webhookRequest, "webhookRequest").getCallbacks());
        event = webhookRequest.getEvent();
        eventScope = webhookRequest.getEventScope().orElse(null);
        payload = webhookRequest.getPayload().orElse(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        // normally you'd not allow equality checks against base classes or interfaces but because of the
        // nature of the id, it's a correct proxy for equality across the family of WebhookInvocation
        if (o == null || !(o instanceof WebhookInvocation)) {
            return false;
        }
        WebhookInvocation that = (WebhookInvocation) o;
        return Objects.equal(id, that.getId());
    }

    @Nonnull
    @Override
    public List<WebhookCallback> getCallbacks() {
        return ImmutableList.copyOf(callbacks);
    }

    @Nonnull
    @Override
    public WebhookEvent getEvent() {
        return event;
    }

    @Nonnull
    @Override
    public String getId() {
        return id;
    }

    @Nonnull
    @Override
    public Optional<Object> getPayload() {
        return ofNullable(payload);
    }

    @Nonnull
    @Override
    public Optional<WebhookScope> getEventScope() {
        return ofNullable(eventScope);
    }

    @Nonnull
    @Override
    public RawRequest.Builder getRequestBuilder() {
        if (builder == null) {
            throw new IllegalStateException("Http context has not been created for this invocation");
        }
        return builder;
    }

    @Nonnull
    @Override
    public Webhook getWebhook() {
        return hook;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public void registerCallback(@Nonnull WebhookCallback callback) {
        callbacks.add(requireNonNull(callback, "callback"));
    }
}
