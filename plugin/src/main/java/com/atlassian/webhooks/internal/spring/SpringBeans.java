package com.atlassian.webhooks.internal.spring;

import java.util.List;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.httpclient.api.factory.HttpClientFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.osgi.javaconfig.configs.beans.PluginAccessorBean;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.webhooks.WebhookPayloadProvider;
import com.atlassian.webhooks.WebhookRequestEnricher;
import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.external.OsgiMigrationToV1;
import com.atlassian.webhooks.internal.BuiltInWebhookEnricher;
import com.atlassian.webhooks.internal.DefaultPayloadManager;
import com.atlassian.webhooks.internal.DefaultValidator;
import com.atlassian.webhooks.internal.DefaultWebhookService;
import com.atlassian.webhooks.internal.DiagnosticsPayloadProvider;
import com.atlassian.webhooks.internal.OsgiWebhookHostAccessor;
import com.atlassian.webhooks.internal.PlatformConfigurer;
import com.atlassian.webhooks.internal.Validator;
import com.atlassian.webhooks.internal.WebhookHostAccessor;
import com.atlassian.webhooks.internal.WebhookPayloadManager;
import com.atlassian.webhooks.internal.WebhooksLifecycle;
import com.atlassian.webhooks.internal.WebhooksLifecycleAware;
import com.atlassian.webhooks.internal.client.RequestExecutor;
import com.atlassian.webhooks.internal.client.request.DefaultRequestExecutor;
import com.atlassian.webhooks.internal.configuration.FeatureFlagService;
import com.atlassian.webhooks.internal.dao.WebhookDao;
import com.atlassian.webhooks.internal.dao.ao.v1.ExternalMigrationTaskInvokerGuard;
import com.atlassian.webhooks.internal.dao.ao.v1.WebhookV1MigrateService;
import com.atlassian.webhooks.internal.history.WebhookInvocationHistorian;
import com.atlassian.webhooks.internal.jmx.JmxBootstrap;
import com.atlassian.webhooks.internal.publish.DefaultWebhookDispatcher;
import com.atlassian.webhooks.internal.publish.WebhookDispatcher;
import com.atlassian.webhooks.migration.WebhooksDataUpgrade;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

/**
 * The Spring bean definitions for Atlassian Webhooks Plugin
 */
@Configuration
@Import({
    DaoSpringBeans.class,
    HistorySpringBeans.class,
    PluginAccessorBean.class,
    WebhookModuleTypeBeans.class,
})
@SuppressWarnings("rawtypes")
public class SpringBeans {

    @Bean
    public ExternalMigrationTaskInvokerGuard externalMigrationTaskInvokerService() {
        return new ExternalMigrationTaskInvokerGuard();
    }

    @Bean
    public FeatureFlagService featureFlagService(final ApplicationProperties applicationProperties) {
        return new FeatureFlagService(applicationProperties);
    }

    @Bean
    public JmxBootstrap jmxBootstrap(WebhookDispatcher webhookDispatcher, WebhookService webhookService) {
        return new JmxBootstrap(webhookDispatcher, webhookService);
    }

    @Bean
    public OsgiMigrationToV1 osgiMigrationToV1(
            final WebhookV1MigrateService webhookV1MigrateService,
            final ExternalMigrationTaskInvokerGuard externalMigrationTaskInvokerService) {
        return new OsgiMigrationToV1(webhookV1MigrateService, externalMigrationTaskInvokerService);
    }

    @Bean
    public PlatformConfigurer platformConfigurer(WebhookInvocationHistorian historian, BundleContext bundleContext) {
        return new PlatformConfigurer(historian, bundleContext);
    }

    @Bean
    public RequestExecutor requestExecutor(HttpClientFactory httpClientFactory) {
        return new DefaultRequestExecutor(httpClientFactory);
    }

    @Bean
    public Validator validator(BundleContext bundleContext) {
        return new DefaultValidator(bundleContext);
    }

    @Bean
    public WebhookDispatcher webhookDispatcher(RequestExecutor requestExecutor) {
        return new DefaultWebhookDispatcher(requestExecutor);
    }

    @Bean
    public WebhookHostAccessor webhookHostAccessor(BundleContext bundleContext) {
        return new OsgiWebhookHostAccessor(bundleContext);
    }

    @Bean
    public WebhookPayloadManager webhookPayloadManager(WebhookHostAccessor hostAccessor) {
        return new DefaultPayloadManager(hostAccessor);
    }

    @Bean
    public WebhookPayloadProvider webhookPayloadProvider() {
        return new DiagnosticsPayloadProvider();
    }

    @Bean
    public BuiltInWebhookEnricher webhookRequestEnricher() {
        return new BuiltInWebhookEnricher();
    }

    @Bean
    public WebhookService webhookService(
            WebhookDao webhookDao,
            WebhookDispatcher webhookDispatcher,
            EventPublisher eventPublisher,
            WebhookHostAccessor hostAccessor,
            PluginAccessor pluginAccessor,
            TransactionTemplate txTemplate,
            Validator validator,
            WebhookPayloadManager webhookPayloadManager,
            FeatureFlagService featureFlagService) {
        return new DefaultWebhookService(
                webhookDao,
                webhookDispatcher,
                eventPublisher,
                hostAccessor,
                pluginAccessor,
                txTemplate,
                validator,
                webhookPayloadManager,
                featureFlagService);
    }

    @Bean
    public WebhooksLifecycle webhooksLifecycle(
            WebhookHostAccessor hostAccessor, List<WebhooksLifecycleAware> services, WebhookService webhookService) {
        return new WebhooksLifecycle(hostAccessor, services, webhookService);
    }

    @Bean
    public WebhookV1MigrateService webhookV1MigrateService() {
        return new WebhookV1MigrateService();
    }

    // Imports

    @Bean
    public ActiveObjects activeObjects() {
        return importOsgiService(ActiveObjects.class);
    }

    @Bean
    public ApplicationProperties applicationProperties() {
        return importOsgiService(ApplicationProperties.class);
    }

    @Bean
    public EventPublisher eventPublisher() {
        return importOsgiService(EventPublisher.class);
    }

    @Bean
    public HttpClientFactory httpClientFactory() {
        return importOsgiService(HttpClientFactory.class);
    }

    @Bean
    public SchedulerService schedulerService() {
        return importOsgiService(SchedulerService.class);
    }

    @Bean
    public TransactionTemplate transactionTemplate() {
        return importOsgiService(TransactionTemplate.class);
    }

    // Exports

    @Bean
    public FactoryBean<ServiceRegistration> exportWebhookHostAccessor(final WebhookHostAccessor webhookHostAccessor) {
        return exportOsgiService(webhookHostAccessor, as(LifecycleAware.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportWebhookPayloadProvider(
            final WebhookPayloadProvider webhookPayloadProvider) {
        return exportOsgiService(webhookPayloadProvider, as(WebhookPayloadProvider.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportWebhookRequestEnricher(
            final BuiltInWebhookEnricher builtInWebhookEnricher) {
        return exportOsgiService(builtInWebhookEnricher, as(WebhookRequestEnricher.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportWebhookService(final WebhookService webhookService) {
        return exportOsgiService(webhookService, as(WebhookService.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportWebhooksLifecycle(final WebhooksLifecycle webhooksLifecycle) {
        return exportOsgiService(webhooksLifecycle, as(LifecycleAware.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportJiraMigration(final OsgiMigrationToV1 osgiMigrationToV1) {
        return exportOsgiService(osgiMigrationToV1, as(WebhooksDataUpgrade.class));
    }
}
