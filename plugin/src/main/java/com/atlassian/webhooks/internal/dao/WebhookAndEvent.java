package com.atlassian.webhooks.internal.dao;

import javax.annotation.Nonnull;

import com.google.common.base.Objects;

import static java.util.Objects.requireNonNull;

class WebhookAndEvent {

    private final String eventId;
    private final int webhookId;

    WebhookAndEvent(int webhookId, @Nonnull String eventId) {
        this.eventId = eventId;
        this.webhookId = requireNonNull(webhookId, "webhookId");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WebhookAndEvent that = (WebhookAndEvent) o;
        return webhookId == that.webhookId && Objects.equal(eventId, that.eventId);
    }

    public String getEventId() {
        return eventId;
    }

    public int getWebhookId() {
        return webhookId;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(webhookId, eventId);
    }
}
