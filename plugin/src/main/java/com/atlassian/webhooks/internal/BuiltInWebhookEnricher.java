package com.atlassian.webhooks.internal;

import javax.annotation.Nonnull;

import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookRequestEnricher;

public class BuiltInWebhookEnricher implements WebhookRequestEnricher {

    @Override
    public void enrich(@Nonnull WebhookInvocation invocation) {
        invocation
                .getRequestBuilder()
                .header("X-Event-Key", invocation.getEvent().getId())
                .header("X-Request-Id", invocation.getId());
    }

    @Override
    public int getWeight() {
        return 0;
    }
}
