package com.atlassian.webhooks.internal;

import java.util.concurrent.atomic.AtomicLong;
import javax.annotation.Nonnull;

import com.atlassian.webhooks.DispatchFailedException;
import com.atlassian.webhooks.WebhookCallback;
import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookStatistics;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

public class SimpleWebhooksStatistics implements WebhookStatistics {

    private final WebhookCallback callback;
    private final AtomicLong errorCount;
    private final AtomicLong failureCount;
    private final AtomicLong publishCount;
    private final AtomicLong rejectedCount;
    private final AtomicLong successCount;

    SimpleWebhooksStatistics() {
        callback = new StatisticsCallback();
        errorCount = new AtomicLong();
        failureCount = new AtomicLong();
        publishCount = new AtomicLong();
        rejectedCount = new AtomicLong();
        successCount = new AtomicLong();
    }

    @Override
    public long getDispatchErrorCount() {
        return errorCount.get();
    }

    @Override
    public long getDispatchFailureCount() {
        return failureCount.get();
    }

    @Override
    public long getDispatchRejectedCount() {
        return rejectedCount.get();
    }

    @Override
    public long getDispatchSuccessCount() {
        return successCount.get();
    }

    @Override
    public long getDispatchedCount() {
        // don't include the rejected count, because they were not really dispatched
        return successCount.get() + errorCount.get() + failureCount.get();
    }

    @Override
    public long getPublishedCount() {
        return publishCount.get();
    }

    WebhookCallback asCallback() {
        return callback;
    }

    void onPublish() {
        publishCount.incrementAndGet();
    }

    class StatisticsCallback implements WebhookCallback {
        @Override
        public void onError(
                WebhookHttpRequest request, @Nonnull Throwable error, @Nonnull WebhookInvocation invocation) {
            if (error instanceof DispatchFailedException) {
                rejectedCount.incrementAndGet();
            } else {
                errorCount.incrementAndGet();
            }
        }

        @Override
        public void onFailure(
                @Nonnull WebhookHttpRequest request,
                @Nonnull WebhookHttpResponse response,
                @Nonnull WebhookInvocation invocation) {
            failureCount.incrementAndGet();
        }

        @Override
        public void onSuccess(
                @Nonnull WebhookHttpRequest request,
                @Nonnull WebhookHttpResponse response,
                @Nonnull WebhookInvocation invocation) {
            successCount.incrementAndGet();
        }
    }
}
