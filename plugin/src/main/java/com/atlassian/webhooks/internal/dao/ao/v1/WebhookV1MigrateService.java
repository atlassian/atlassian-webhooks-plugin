package com.atlassian.webhooks.internal.dao.ao.v1;

import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import javax.annotation.Nonnull;

import org.springframework.stereotype.Component;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.internal.dao.ao.v0.WebHookListenerAOV0;
import com.atlassian.webhooks.migration.WebhookDTO;

@Component
public class WebhookV1MigrateService {
    private static final Logger log = LoggerFactory.getLogger(WebhookV1MigrateService.class);

    public void migrate(ActiveObjects ao, WebHookListenerAOV0 webHookListenerAOV0) {
        migrate(ao, convert(webHookListenerAOV0));
    }

    public int migrate(ActiveObjects ao, WebhookDTO webhookDTO) {
        Date now = new Date();
        ImmutableMap<String, Object> hook = ImmutableMap.<String, Object>builder()
                .put(AoWebhookV1.CREATED_COLUMN, now)
                .put(AoWebhookV1.UPDATED_COLUMN, now)
                .put(AoWebhookV1.URL_COLUMN, webhookDTO.getUrl())
                .put(AoWebhookV1.NAME_COLUMN, webhookDTO.getName())
                .put(AoWebhookV1.ACTIVE_COLUMN, webhookDTO.isEnabled())
                .put(AoWebhookV1.SCOPE_TYPE_COLUMN, WebhookScope.GLOBAL.getType())
                .build();

        AoWebhookV1 aoWebhook = ao.create(AoWebhookV1.class, hook);

        createConfiguration(webhookDTO)
                .forEach((key, value) -> ao.create(
                        AoWebhookConfigurationEntryV1.class,
                        ImmutableMap.of(
                                AoWebhookConfigurationEntryV1.KEY_COLUMN,
                                key,
                                AoWebhookConfigurationEntryV1.WEBHOOK_COLUMN_QUERY,
                                aoWebhook.getID(),
                                AoWebhookConfigurationEntryV1.VALUE_COLUMN,
                                value)));

        getEventsFor(webhookDTO)
                .forEach(event -> ao.create(
                        AoWebhookEventV1.class,
                        ImmutableMap.of(
                                AoWebhookEventV1.EVENT_ID_COLUMN,
                                event.getId(),
                                AoWebhookEventV1.WEBHOOK_COLUMN_QUERY,
                                aoWebhook.getID())));

        return aoWebhook.getID();
    }

    private WebhookDTO convert(WebHookListenerAOV0 webHookListenerAOV0) {
        return WebhookDTO.builder()
                .url(webHookListenerAOV0.getUrl())
                .name(webHookListenerAOV0.getName())
                .description(webHookListenerAOV0.getDescription())
                .lastUpdated(webHookListenerAOV0.getLastUpdated())
                .lastUpdatedUser(webHookListenerAOV0.getLastUpdatedUser())
                .filters(webHookListenerAOV0.getFilters())
                .registrationMethod(webHookListenerAOV0.getRegistrationMethod())
                .enabled(webHookListenerAOV0.isEnabled())
                .excludeBody(webHookListenerAOV0.isExcludeBody())
                .parameters(webHookListenerAOV0.getParameters())
                .jsonEvents(webHookListenerAOV0.getEvents())
                .createWebhookDTO();
    }

    private Map<String, String> createConfiguration(WebhookDTO webhookDTO) {
        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        if (StringUtils.isNotEmpty(webhookDTO.getFilters())) {
            builder.put("FILTERS", webhookDTO.getFilters());
        }
        if (StringUtils.isNotEmpty(webhookDTO.getRegistrationMethod())) {
            builder.put("REGISTRATION_METHOD", webhookDTO.getRegistrationMethod());
        }
        if (StringUtils.isNotEmpty(webhookDTO.getParameters())) {
            builder.put("PARAMETERS", webhookDTO.getParameters());
        }
        if (StringUtils.isNotEmpty(webhookDTO.getDescription())) {
            builder.put("DESCRIPTION", webhookDTO.getDescription());
        }
        if (StringUtils.isNotEmpty(webhookDTO.getLastUpdatedUser())) {
            builder.put("LAST_UPDATED_USER", webhookDTO.getLastUpdatedUser());
        }
        if (webhookDTO.getLastUpdated() != null) {
            builder.put(
                    "LAST_UPDATED",
                    webhookDTO
                            .getLastUpdated()
                            .toInstant()
                            .atOffset(ZoneOffset.UTC)
                            .format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        }
        builder.put("EXCLUDE_BODY", Boolean.toString(webhookDTO.isExcludeBody()));
        return builder.build();
    }

    private Iterable<WebhookEvent> getEventsFor(WebhookDTO webhookDTO) {
        if (StringUtils.isEmpty(webhookDTO.getJsonEvents()) && StringUtils.isEmpty(webhookDTO.getEvents())) {
            return ImmutableList.of();
        }

        // events is a JSON array, we could bring
        ImmutableList.Builder<WebhookEvent> builder = ImmutableList.builder();

        if (!StringUtils.isEmpty(webhookDTO.getJsonEvents())) {
            try {
                JSONArray jsonArray = new JSONArray(webhookDTO.getJsonEvents());
                for (int i = 0; i < jsonArray.length(); i++) {
                    String event = jsonArray.getString(i);
                    builder.add(new MigrateWebhookEvent(event));
                }
            } catch (JSONException e) {
                log.warn("A webhook was unable to migrate the events for id:[{}]", webhookDTO.getId());
            }
        }

        // get String separated with commas
        if (!StringUtils.isEmpty(webhookDTO.getEvents())) {
            for (String event : webhookDTO.getEvents().split("\n")) {
                builder.add(new MigrateWebhookEvent(event));
            }
        }

        return builder.build();
    }

    private static class MigrateWebhookEvent implements WebhookEvent {

        private final String id;

        MigrateWebhookEvent(String id) {
            this.id = id;
        }

        @Nonnull
        @Override
        public String getId() {
            return id;
        }

        @Nonnull
        @Override
        public String getI18nKey() {
            return id;
        }
    }
}
