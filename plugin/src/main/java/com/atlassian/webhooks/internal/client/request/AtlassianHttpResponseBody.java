package com.atlassian.webhooks.internal.client.request;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import javax.annotation.Nonnull;

import org.apache.commons.io.input.BoundedInputStream;

import com.atlassian.httpclient.api.Response;
import com.atlassian.webhooks.request.WebhookResponseBody;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

public class AtlassianHttpResponseBody implements WebhookResponseBody {

    private static final InputStream EMPTY = new ByteArrayInputStream(new byte[0]);

    private final String contentType;
    private final long maxBytes;
    private final Response result;

    public AtlassianHttpResponseBody(@Nonnull Response response, long maxBytes) {
        this.result = requireNonNull(response, "response");
        this.contentType = response.getContentType();
        this.maxBytes = maxBytes;
    }

    @Nonnull
    @Override
    public InputStream getContent() throws IOException {
        InputStream content = result.getEntityStream();
        return content == null ? EMPTY : new BoundedInputStream(content, maxBytes);
    }

    @Nonnull
    @Override
    public Optional<String> getContentType() {
        return ofNullable(contentType);
    }
}
