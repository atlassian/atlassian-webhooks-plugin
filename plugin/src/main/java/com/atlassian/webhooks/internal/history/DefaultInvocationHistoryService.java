package com.atlassian.webhooks.internal.history;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.*;
import javax.annotation.Nonnull;
import javax.net.ssl.SSLException;

import org.springframework.beans.factory.annotation.Qualifier;
import org.slf4j.Logger;
import com.google.common.collect.Multimap;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventListenerRegistrar;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.webhooks.NoSuchWebhookException;
import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.WebhooksConfiguration;
import com.atlassian.webhooks.event.WebhookDeletedEvent;
import com.atlassian.webhooks.history.DetailedInvocation;
import com.atlassian.webhooks.history.DetailedInvocationResult;
import com.atlassian.webhooks.history.HistoricalInvocation;
import com.atlassian.webhooks.history.HistoricalInvocationRequest;
import com.atlassian.webhooks.history.InvocationCounts;
import com.atlassian.webhooks.history.InvocationHistory;
import com.atlassian.webhooks.history.InvocationHistoryByEventRequest;
import com.atlassian.webhooks.history.InvocationHistoryRequest;
import com.atlassian.webhooks.history.InvocationOutcome;
import com.atlassian.webhooks.history.InvocationRequest;
import com.atlassian.webhooks.history.InvocationResult;
import com.atlassian.webhooks.internal.WebhooksLifecycleAware;
import com.atlassian.webhooks.internal.dao.InvocationHistoryDao;
import com.atlassian.webhooks.internal.dao.ao.AoHistoricalInvocation;
import com.atlassian.webhooks.internal.model.SimpleWebhookScope;
import com.atlassian.webhooks.internal.model.UnknownWebhookEvent;
import com.atlassian.webhooks.request.Method;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Strings.emptyToNull;
import static java.util.Collections.emptyMap;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.slf4j.LoggerFactory.getLogger;

import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static com.atlassian.webhooks.history.InvocationOutcome.ERROR;
import static com.atlassian.webhooks.history.InvocationOutcome.FAILURE;
import static com.atlassian.webhooks.history.InvocationOutcome.SUCCESS;

public class DefaultInvocationHistoryService implements InternalInvocationHistoryService, WebhooksLifecycleAware {

    static final int COUNTS_DAYS = 30;

    private static final Logger log = getLogger(DefaultInvocationHistoryService.class);

    private static final Duration COUNTS_DURATION = Duration.of(COUNTS_DAYS, ChronoUnit.DAYS);
    private static final InvocationCounts NO_INVOCATIONS = new SimpleInvocationCounts(COUNTS_DURATION, 0, 0, 0);
    private static final InvocationHistory EMPTY_HISTORY =
            new SimpleInvocationHistory(NO_INVOCATIONS, null, null, null);
    private static final JobId JOB_ID = JobId.of("webhooks.history.daily.cleanup.job");
    private static final JobRunnerKey JOB_RUNNER_KEY = JobRunnerKey.of("webhooks.history.daily.cleanup.runner");
    private static final int MAX_ATTEMPTS = 3;

    private final InvocationHistoryDao dao;
    private final EventListenerRegistrar eventListenerRegistrar;
    private final SchedulerService schedulerService;
    private final TransactionTemplate txTemplate;
    private final WebhookService webhookService;

    public DefaultInvocationHistoryService(
            @Qualifier("asyncInvocationHistoryDao") InvocationHistoryDao dao,
            EventListenerRegistrar eventListenerRegistrar,
            SchedulerService schedulerService,
            TransactionTemplate txTemplate,
            WebhookService webhookService) {
        this.dao = dao;
        this.eventListenerRegistrar = eventListenerRegistrar;
        this.schedulerService = schedulerService;
        this.txTemplate = txTemplate;
        this.webhookService = webhookService;
    }

    @Nonnull
    @Override
    public InvocationHistory get(@Nonnull InvocationHistoryRequest request) {
        int webhookId = request.getWebhookId();
        String eventId = request.getEventId().orElse(null);
        return getInvocationHistory(webhookId, eventId, null);
    }

    @Nonnull
    @Override
    public Map<WebhookEvent, InvocationHistory> getByEvent(@Nonnull InvocationHistoryByEventRequest request) {
        return txTemplate.execute(() -> {
            // verify the webhook exists
            int webhookId = request.getWebhookId();
            Webhook webhook = getWebhookOrThrow(webhookId);
            Set<String> eventIds =
                    webhook.getEvents().stream().map(WebhookEvent::getId).collect(Collectors.toSet());

            Multimap<String, AoHistoricalInvocation> latestByEvent =
                    dao.getLatestInvocationsByEvent(webhookId, eventIds);
            Map<String, InvocationCounts> countsByEvent =
                    dao.getCountsByEvent(webhookId, latestByEvent.keySet(), COUNTS_DAYS);

            return webhook.getEvents().stream()
                    .collect(Collectors.toMap(
                            Function.identity(),
                            ev -> toInvocationHistory(latestByEvent.get(ev.getId()), countsByEvent.get(ev.getId()))));
        });
    }

    @Nonnull
    @Override
    public Map<Integer, InvocationHistory> getByWebhook(Collection<Integer> webhookIds) {
        return getByWebhookForDays(webhookIds, COUNTS_DAYS);
    }

    @Nonnull
    @Override
    public Map<Integer, InvocationHistory> getByWebhookAndScope(Collection<Integer> webhookIds, WebhookScope scope) {
        return getByWebhookAndScopeForDays(webhookIds, scope, COUNTS_DAYS);
    }

    @Nonnull
    @Override
    public Map<Integer, InvocationHistory> getByWebhookAndScopeForDays(
            Collection<Integer> webhookIds, WebhookScope scope, int days) {
        return webhookIds.isEmpty()
                ? emptyMap()
                : txTemplate.execute(() -> {
                    Multimap<Integer, AoHistoricalInvocation> latestByWebhookIdAndScope =
                            dao.getLatestInvocationsByWebhookAndScope(webhookIds, scope);
                    Map<Integer, InvocationCounts> countsByWebhook = dao.getCountsByWebhook(webhookIds, days);
                    return webhookIds.stream()
                            .collect(Collectors.toMap(
                                    Function.identity(),
                                    id -> toInvocationHistory(
                                            latestByWebhookIdAndScope.get(id), countsByWebhook.get(id))));
                });
    }

    @Nonnull
    @Override
    public Map<Integer, InvocationHistory> getByWebhookForDays(Collection<Integer> webhookIds, int days) {
        return webhookIds.isEmpty()
                ? emptyMap()
                : txTemplate.execute(() -> {
                    Multimap<Integer, AoHistoricalInvocation> latestByWebhookId =
                            dao.getLatestInvocationsByWebhook(webhookIds);
                    Map<Integer, InvocationCounts> countsByWebhook = dao.getCountsByWebhook(webhookIds, days);
                    return webhookIds.stream()
                            .collect(Collectors.toMap(
                                    Function.identity(),
                                    id -> toInvocationHistory(latestByWebhookId.get(id), countsByWebhook.get(id))));
                });
    }

    @Override
    @Nonnull
    public Optional<DetailedInvocation> getLatestInvocation(@Nonnull HistoricalInvocationRequest request) {
        return txTemplate.execute(() -> {
            int webhookId = request.getWebhookId();
            String eventId = request.getEventId().orElse(null);
            AoHistoricalInvocation invocation = dao.getLatestInvocation(webhookId, eventId, request.getOutcomes());
            if (invocation != null) {
                return of(toDetailedInvocation(invocation));
            }

            // verify the webhook exists
            getWebhookOrThrow(webhookId);
            return empty();
        });
    }

    @Override
    public void logInvocationError(
            @Nonnull WebhookHttpRequest request,
            @Nonnull Throwable error,
            @Nonnull WebhookInvocation invocation,
            @Nonnull Instant start,
            @Nonnull Instant finish) {
        SimpleDetailedInvocation detailedInvocation = new SimpleDetailedInvocation(
                invocation.getId(),
                invocation.getEvent(),
                invocation.getEventScope().orElse(null),
                new SimpleDetailedRequest(invocation, request),
                getErrorInvocation(request, error),
                start,
                finish);
        saveInvocation(invocation.getWebhook().getId(), detailedInvocation, 1, 0, 0);
    }

    @Override
    public void logInvocationFailure(
            @Nonnull WebhookHttpRequest request,
            @Nonnull WebhookHttpResponse response,
            @Nonnull WebhookInvocation invocation,
            @Nonnull Instant start,
            @Nonnull Instant finish) {
        saveInvocation(
                invocation.getWebhook().getId(),
                new SimpleDetailedInvocation(invocation, FAILURE, request, response, start, finish),
                0,
                1,
                0);
    }

    @Override
    public void logInvocationSuccess(
            @Nonnull WebhookHttpRequest request,
            @Nonnull WebhookHttpResponse response,
            @Nonnull WebhookInvocation invocation,
            @Nonnull Instant start,
            @Nonnull Instant finish) {
        saveInvocation(
                invocation.getWebhook().getId(),
                new SimpleDetailedInvocation(invocation, SUCCESS, request, response, start, finish),
                0,
                0,
                1);
    }

    @Override
    public void onStart(WebhooksConfiguration configuration) {
        eventListenerRegistrar.register(this);
        schedulerService.registerJobRunner(JOB_RUNNER_KEY, new DailyCleanupJobRunner());
        JobConfig config = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY)
                .withRunMode(RUN_ONCE_PER_CLUSTER)
                .withSchedule(Schedule.forCronExpression("0 22 4 1/1 * ? *")); // daily at 04:22

        try {
            schedulerService.scheduleJob(JOB_ID, config);
        } catch (SchedulerServiceException e) {
            log.error("Could not schedule audit log cleanup job", e);
        }
    }

    @Override
    public void onStop() {
        // unregister the job runner, but not the job. In a cluster, another node may still be able to run it
        schedulerService.unregisterJobRunner(JOB_RUNNER_KEY);
        eventListenerRegistrar.unregister(this);
    }

    @EventListener
    public void onWebhookDeleted(@Nonnull WebhookDeletedEvent event) {
        Webhook webhook = event.getWebhook();
        dao.deleteForWebhook(webhook.getId());
        if (log.isDebugEnabled()) {
            log.debug(
                    "[{}{}] Deleted all invocation history for deleted webhook to '{}' (id={})",
                    webhook.getScope().getType(),
                    webhook.getScope().getId().map(id -> ":" + id).orElse(""),
                    webhook.getUrl(),
                    webhook.getId());
        }
    }

    private InvocationRequest createRequest(AoHistoricalInvocation invocation) {
        return new SimpleDetailedRequest(
                invocation.getRequestBody(),
                dao.decodeHeaders(invocation.getId(), invocation.getRequestHeaders()),
                Method.valueOf(invocation.getRequestMethod()),
                invocation.getRequestUrl());
    }

    private InvocationHistory getInvocationHistory(
            int webhookId, String eventId, Collection<InvocationOutcome> outcomes) {
        List<AoHistoricalInvocation> invocations = dao.getLatestInvocations(webhookId, eventId, outcomes);
        if (invocations.isEmpty()) {
            // no invocations recorded, check whether the webhook exists
            getWebhookOrThrow(webhookId);
            // webhook exists, but no invocations have been recorded
            return EMPTY_HISTORY;
        }

        InvocationCounts counts = dao.getCounts(webhookId, eventId, COUNTS_DAYS);
        return toInvocationHistory(invocations, counts);
    }

    private Webhook getWebhookOrThrow(int webhookId) {
        return webhookService
                .findById(webhookId)
                .orElseThrow(() -> new NoSuchWebhookException("Webhook with ID " + webhookId + " not found"));
    }

    private InvocationResult getErrorInvocation(WebhookHttpRequest request, Throwable error) {
        return new SimpleDetailedError(error.getClass().getName(), getErrorMessage(request.getUrl(), error));
    }

    private String getErrorMessage(String url, Throwable error) {
        // We're going to try be nice, if it's a known error we'll translate it to a human readable string
        // Note: We don't use the I18nResolver here because we store it in the database so we don't want it stored
        // in multiple different languages
        if (error.getCause() instanceof UnknownHostException) {
            return String.format("Unknown host specified in the webhook URL: %s", url);
        }
        if (error.getCause() instanceof SocketException) {
            return "Unable to connect to the URL specified within the timeout, please check the host and port are correct and that the URL is accessible from the server running this request.";
        }
        if (error.getCause() instanceof SSLException) {
            return "An SSL error occurred, please check that the provided resource uses https.";
        }
        // Never return the error from error.getMessage() to the user because it might result in showing the user
        // more information than they would normally be able to see, since the request is being made from a
        // privileged instance (SSRF)
        log.warn("Request to {} resulted in an error: {}", url, error.getLocalizedMessage());
        return "The request to the specified URL failed. For more details, please ask the Administrator to check the server logs.";
    }

    private HistoricalInvocation getInvocation(
            Collection<AoHistoricalInvocation> invocations, InvocationOutcome outcome) {
        HistoricalInvocation result = null;
        for (AoHistoricalInvocation invocation : invocations) {
            if (invocation.getOutcome() == outcome) {
                if (result == null || result.getFinish().isBefore(Instant.ofEpochMilli(invocation.getFinish()))) {
                    result = toInvocation(invocation);
                }
            }
        }
        return result;
    }

    private void saveInvocation(int webhookId, DetailedInvocation invocation, int errors, int failures, int successes) {
        try {
            txTemplate.execute(() -> {
                dao.saveInvocation(webhookId, invocation);
                return null;
            });
        } catch (RuntimeException e) {
            log.warn(
                    "Failed to record history for webhook {}/{} and event {} in scope {}",
                    webhookId,
                    invocation.getRequest().getUrl(),
                    invocation.getEvent().getId(),
                    invocation.getEventScope(),
                    e);
        }

        for (int attempt = 1; attempt <= MAX_ATTEMPTS; ++attempt) {
            try {
                txTemplate.execute(() -> {
                    dao.addCounts(
                            webhookId,
                            invocation.getEvent().getId(),
                            new Date(invocation.getFinish().toEpochMilli()),
                            errors,
                            failures,
                            successes);
                    return null;
                });
                break;
            } catch (RuntimeException e) {
                if (attempt == MAX_ATTEMPTS) {
                    throw e;
                } else {
                    log.debug(
                            "Update of invocation counts for {}:{} failed. Retrying",
                            webhookId,
                            invocation.getEvent().getId(),
                            e);
                }
            }
        }
    }

    private DetailedInvocation toDetailedInvocation(AoHistoricalInvocation invocation) {
        DetailedInvocationResult result = invocation.getOutcome() == ERROR
                ? new SimpleDetailedError(invocation.getErrorContent(), invocation.getResultDescription())
                : new SimpleDetailedResponse(
                        invocation.getResponseBody(),
                        invocation.getResultDescription(),
                        dao.decodeHeaders(invocation.getId(), invocation.getResponseHeaders()),
                        invocation.getOutcome(),
                        invocation.getStatusCode());

        return new SimpleDetailedInvocation(
                invocation.getRequestId(),
                toEvent(invocation.getEventId()),
                toEventScope(invocation.getEventScopeType(), invocation.getEventScopeId()),
                createRequest(invocation),
                result,
                Instant.ofEpochMilli(invocation.getStart()),
                Instant.ofEpochMilli(invocation.getFinish()));
    }

    private WebhookEvent toEvent(String eventId) {
        return webhookService.getEvent(eventId).orElseGet(() -> new UnknownWebhookEvent(eventId));
    }

    private HistoricalInvocation toInvocation(AoHistoricalInvocation invocation) {
        return new SimpleHistoricalInvocation(
                invocation.getRequestId(),
                toEvent(invocation.getEventId()),
                toEventScope(invocation.getEventScopeType(), invocation.getEventScopeId()),
                Instant.ofEpochMilli(invocation.getStart()),
                Instant.ofEpochMilli(invocation.getFinish()),
                new SimpleInvocationRequest(Method.valueOf(invocation.getRequestMethod()), invocation.getRequestUrl()),
                new SimpleInvocationResult(invocation.getResultDescription(), invocation.getOutcome()));
    }

    private InvocationHistory toInvocationHistory(
            Collection<AoHistoricalInvocation> invocations, InvocationCounts counts) {
        return new SimpleInvocationHistory(
                firstNonNull(counts, NO_INVOCATIONS),
                getInvocation(invocations, ERROR),
                getInvocation(invocations, FAILURE),
                getInvocation(invocations, SUCCESS));
    }

    private WebhookScope toEventScope(String type, String id) {
        return type != null ? new SimpleWebhookScope(type, emptyToNull(id)) : null;
    }

    private class DailyCleanupJobRunner implements JobRunner {

        @Override
        public JobRunnerResponse runJob(@Nonnull JobRunnerRequest request) {
            int rowsDeleted = txTemplate.execute(() -> dao.deleteDailyCountsOlderThan(COUNTS_DAYS));
            log.debug("Deleted {} rows of webhooks daily invocation counts", rowsDeleted);
            return JobRunnerResponse.success();
        }
    }
}
