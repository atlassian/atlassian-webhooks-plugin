package com.atlassian.webhooks.internal.module;

import com.atlassian.plugins.osgi.javaconfig.moduletypes.PluginContextModuleDescriptorFactory;

public class WebhookModuleDescriptorFactory
        extends PluginContextModuleDescriptorFactory<SimpleWebhookModuleDescriptor> {

    public WebhookModuleDescriptorFactory() {
        super("webhook", SimpleWebhookModuleDescriptor.class);
    }
}
