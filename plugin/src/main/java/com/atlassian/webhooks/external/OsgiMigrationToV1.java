package com.atlassian.webhooks.external;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.webhooks.internal.dao.ao.v1.ExternalMigrationTaskInvokerGuard;
import com.atlassian.webhooks.internal.dao.ao.v1.WebhookV1MigrateService;
import com.atlassian.webhooks.migration.WebhookDTO;
import com.atlassian.webhooks.migration.WebhooksDataUpgrade;

@Component
public class OsgiMigrationToV1 implements WebhooksDataUpgrade {
    private static final Logger log = LoggerFactory.getLogger(OsgiMigrationToV1.class);

    private final WebhookV1MigrateService webhookV1MigrateService;

    private final ExternalMigrationTaskInvokerGuard externalMigrationTaskInvokerService;

    public OsgiMigrationToV1(
            final WebhookV1MigrateService webhookV1MigrateService,
            final ExternalMigrationTaskInvokerGuard externalMigrationTaskInvokerService) {
        this.webhookV1MigrateService = webhookV1MigrateService;
        this.externalMigrationTaskInvokerService = externalMigrationTaskInvokerService;
    }

    @Override
    public void registerExternalMigrationTask(
            List<WebhookDTO> webhookDTOs, Consumer<Map<Integer, Integer>> resultConsumer) {

        externalMigrationTaskInvokerService.migrationTask(ao -> {
            Map<Integer, Integer> result = new HashMap<>();

            webhookDTOs.forEach(hook -> result.put(hook.getId(), webhookV1MigrateService.migrate(ao, hook)));
            log.debug("Events migration completed successfully.");

            resultConsumer.accept(result);
        });
    }
}
