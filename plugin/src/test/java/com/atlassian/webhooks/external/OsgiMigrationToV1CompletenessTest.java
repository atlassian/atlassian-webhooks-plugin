package com.atlassian.webhooks.external;

import java.util.stream.Collectors;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.junit.Test;

import com.atlassian.webhooks.internal.dao.ao.v1.MigrateToV1Task;
import com.atlassian.webhooks.internal.dao.ao.v2.MigrateToV2Task;

import static org.junit.Assert.assertArrayEquals;

public class OsgiMigrationToV1CompletenessTest {

    @Test
    public void verifyThatTheUpgradeTaskAreCompatibleWithExternalModuleMigration() throws DocumentException {
        Document document = new SAXReader().read(getClass().getResourceAsStream("/atlassian-plugin.xml"));
        String[] upgradeTasks = document.selectNodes("//atlassian-plugin/ao/upgradeTask").stream()
                .map(e -> e.getText())
                .collect(Collectors.toList())
                .toArray(new String[0]);

        assertArrayEquals(
                """
                        Apparently, the list of migration tasks has been modified. Please note, that is is assumed that the
                        migration performed in OsgiMigrationToV1 class is all that is needed to migrate data from
                        Jira 9.x to current atlassian-webhooks version. If that's still true, please modify the task list
                        in this test, if not, please modify OsgiMigrationToV1, so that required entities are created.
                        """,
                new String[] {MigrateToV1Task.class.getName(), MigrateToV2Task.class.getName()},
                upgradeTasks);
    }
}
