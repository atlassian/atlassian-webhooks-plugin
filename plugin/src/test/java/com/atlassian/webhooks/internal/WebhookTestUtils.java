package com.atlassian.webhooks.internal;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import com.atlassian.webhooks.internal.dao.ao.AoWebhook;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookConfigurationEntry;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookEvent;
import com.atlassian.webhooks.internal.model.SimpleWebhookCredentials;
import com.atlassian.webhooks.internal.model.SimpleWebhookScope;
import com.atlassian.webhooks.*;

import static java.time.temporal.ChronoUnit.MINUTES;
import static java.util.Arrays.stream;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.webhooks.internal.TestEvent.*;

public class WebhookTestUtils {

    public static final String DEFAULT_URL = "http://something.com/%25";
    private static final WebhookCredentials DEFAULT_CREDENTIALS = new SimpleWebhookCredentials("test", "test");
    private static final List<WebhookEvent> DEFAULT_EVENTS =
            ImmutableList.of(PROJ_CREATE, PROJ_DELETE, PROJ_UPDATE, USER_CREATE);
    private static final String DEFAULT_NAME = "Hook name";
    private static final WebhookScope DEFAULT_SCOPE = new SimpleWebhookScope("DEFAULT_TYPE", "scope-id");
    private static final Instant CREATED_DATE = Instant.ofEpochMilli(1712205037248L);
    private static final Instant UPDATED_DATE = CREATED_DATE.plus(1, MINUTES);
    private static final Map<String, String> DEFAULT_CONTEXT = ImmutableMap.of(
            "hi there", "all",
            "no value", "value");

    public static void assertStandardWebhook(AoWebhook aoWebhook) {
        assertWebhook(aoWebhook, DEFAULT_SCOPE);
    }

    public static void assertStandardWebhook(Webhook webhook) {

        assertThat(webhook.getName(), equalTo(DEFAULT_NAME));
        assertThat(webhook.getUrl(), equalTo(DEFAULT_URL));
        assertThat(webhook.isSslVerificationRequired(), equalTo(true));

        assertThat(webhook.getEvents(), containsInAnyOrder(DEFAULT_EVENTS.toArray()));
        assertThat(webhook.getEvents(), hasSize(DEFAULT_EVENTS.size()));

        WebhookScope scope = webhook.getScope();
        assertThat(scope, notNullValue());
        assertThat(scope.getType(), equalTo("DEFAULT_TYPE"));
        assertThat(scope.getId().orElse(null), equalTo("scope-id"));

        assertThat(webhook.getConfiguration().entrySet(), hasSize(DEFAULT_CONTEXT.size()));
        assertThat(webhook.getConfiguration().entrySet(), everyItem(isIn(DEFAULT_CONTEXT.entrySet())));

        assertThat(webhook.getCreatedDate(), equalTo(Date.from(CREATED_DATE)));
        assertThat(webhook.getUpdatedDate(), equalTo(Date.from(UPDATED_DATE)));

        assertThat(webhook.getCredentials().orElse(null), equalTo(DEFAULT_CREDENTIALS));
    }

    private static void assertWebhook(AoWebhook aoWebhook, WebhookScope scope) {
        assertThat(aoWebhook.getName(), equalTo(DEFAULT_NAME));
        assertThat(aoWebhook.getUrl(), equalTo(DEFAULT_URL));
        assertThat(aoWebhook.isSslVerificationRequired(), equalTo(true));

        assertThat(aoWebhook.getEvents().length, equalTo(DEFAULT_EVENTS.size()));
        assertThat(
                stream(aoWebhook.getEvents()).map(AoWebhookEvent::getEventId).collect(Collectors.toList()),
                containsInAnyOrder(
                        DEFAULT_EVENTS.stream().map(WebhookEvent::getId).toArray()));

        assertThat(aoWebhook.getScopeType(), equalTo(scope.getType()));
        assertThat(aoWebhook.getScopeId(), equalTo(scope.getId().orElse(null)));

        Map<String, String> configMap = stream(aoWebhook.getConfiguration())
                .collect(Collectors.toMap(AoWebhookConfigurationEntry::getKey, AoWebhookConfigurationEntry::getValue));
        assertThat(configMap.entrySet(), everyItem(isIn(DEFAULT_CONTEXT.entrySet())));

        assertThat(
                aoWebhook.getUsername(),
                equalTo(DEFAULT_CREDENTIALS.getUsername().orElse(null)));
        assertThat(
                aoWebhook.getPassword(),
                equalTo(DEFAULT_CREDENTIALS.getPassword().orElse(null)));

        assertThat(aoWebhook.isSslVerificationRequired(), equalTo(true));
        assertThat(aoWebhook.getUrl(), equalTo(DEFAULT_URL));

        // AoWebHooks created from the test will have created/updated date set as current time
        Date now = new Date();
        assertThat(aoWebhook.getCreatedDate(), lessThanOrEqualTo(now));
        assertThat(aoWebhook.getUpdatedDate(), lessThanOrEqualTo(now));
    }

    public static void assertGlobalWebhook(AoWebhook aoWebhook) {
        assertWebhook(aoWebhook, WebhookScope.GLOBAL);
    }

    public static AoWebhook mockStandardAoWebhook() {
        AoWebhook webhook = mock(AoWebhook.class);

        AoWebhookEvent[] events = new AoWebhookEvent[DEFAULT_EVENTS.size()];
        for (int i = 0; i < DEFAULT_EVENTS.size(); i++) {
            events[i] = mock(AoWebhookEvent.class);
            when(events[i].getEventId()).thenReturn(DEFAULT_EVENTS.get(i).getId());
            when(events[i].getWebhook()).thenReturn(webhook);
        }

        AoWebhookConfigurationEntry[] context = DEFAULT_CONTEXT.entrySet().stream()
                .map(entry -> {
                    AoWebhookConfigurationEntry contextEntry = mock(AoWebhookConfigurationEntry.class);
                    when(contextEntry.getKey()).thenReturn(entry.getKey());
                    when(contextEntry.getValue()).thenReturn(entry.getValue());
                    when(contextEntry.getWebhook()).thenReturn(webhook);
                    return contextEntry;
                })
                .toArray(AoWebhookConfigurationEntry[]::new);

        when(webhook.getCreatedDate()).thenReturn(Date.from(CREATED_DATE));
        when(webhook.getConfiguration()).thenReturn(context);
        when(webhook.getName()).thenReturn(DEFAULT_NAME);
        when(webhook.getEvents()).thenReturn(events);
        when(webhook.getPassword()).thenReturn(DEFAULT_CREDENTIALS.getPassword().orElse(null));
        when(webhook.getScopeId()).thenReturn(DEFAULT_SCOPE.getId().orElse(null));
        when(webhook.getScopeType()).thenReturn(DEFAULT_SCOPE.getType());
        when(webhook.getUsername()).thenReturn(DEFAULT_CREDENTIALS.getUsername().orElse(null));
        when(webhook.isSslVerificationRequired()).thenReturn(true);
        when(webhook.getUrl()).thenReturn(DEFAULT_URL);
        when(webhook.getUpdatedDate()).thenReturn(Date.from(UPDATED_DATE));
        return webhook;
    }

    public static WebhookCreateRequest standardWebhookRequest() {
        return WebhookCreateRequest.builder()
                .configuration(DEFAULT_CONTEXT)
                .credentials(DEFAULT_CREDENTIALS)
                .event(DEFAULT_EVENTS)
                .name(DEFAULT_NAME)
                .scope(DEFAULT_SCOPE)
                .sslVerificationRequired(true)
                .url(DEFAULT_URL)
                .build();
    }

    public static WebhookCreateRequest globalWebhookRequest() {
        return WebhookCreateRequest.builder()
                .configuration(DEFAULT_CONTEXT)
                .credentials(DEFAULT_CREDENTIALS)
                .event(DEFAULT_EVENTS)
                .name(DEFAULT_NAME)
                .scope(WebhookScope.GLOBAL)
                .sslVerificationRequired(true)
                .url(DEFAULT_URL)
                .build();
    }
}
