package com.atlassian.webhooks.internal.jmx;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.WebhookStatistics;
import com.atlassian.webhooks.internal.publish.WebhookDispatcher;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WebhooksMXBeanAdapterTest {
    private static final long DISPATCH_COUNT = 1L;
    private static final long ERROR_COUNT = 2L;
    private static final long FAILURE_COUNT = 3L;
    private static final long REJECTED_COUNT = 4L;
    private static final long SUCCESS_COUNT = 5L;
    private static final long PUBLISH_COUNT = 6L;
    private static final int IN_FLIGHT_COUNT = 7;
    private static final long LAST_REJECTED_TIMESTAMP = 8L;
    private static final long UNAVAILABLE = -1L;

    @Mock
    private WebhookDispatcher dispatcher;

    @Mock
    private WebhookService webhookService;

    @InjectMocks
    private WebhooksMXBeanAdapter mbean;

    @Before
    public void setup() {
        when(dispatcher.getInFlightCount()).thenReturn(IN_FLIGHT_COUNT);
        when(dispatcher.getLastRejectedTimestamp()).thenReturn(LAST_REJECTED_TIMESTAMP);
    }

    @Test
    public void testStatisticsAvailable() {
        when(webhookService.getStatistics()).thenReturn(of(new TestStatistics()));

        assertEquals(DISPATCH_COUNT, mbean.getDispatchCount());
        assertEquals(PUBLISH_COUNT, mbean.getPublishCount());
        assertEquals(ERROR_COUNT, mbean.getDispatchErrorCount());
        assertEquals(FAILURE_COUNT, mbean.getDispatchFailureCount());
        assertEquals(REJECTED_COUNT, mbean.getDispatchRejectedCount());
        assertEquals(SUCCESS_COUNT, mbean.getDispatchSuccessCount());
        assertEquals(IN_FLIGHT_COUNT, mbean.getDispatchInFlightCount());
        assertEquals(LAST_REJECTED_TIMESTAMP, mbean.getDispatchLastRejectedTimestamp());
    }

    @Test
    public void testStatisticsUnavailable() {
        when(webhookService.getStatistics()).thenReturn(empty());

        assertEquals(UNAVAILABLE, mbean.getDispatchCount());
        assertEquals(UNAVAILABLE, mbean.getPublishCount());
        assertEquals(UNAVAILABLE, mbean.getDispatchErrorCount());
        assertEquals(UNAVAILABLE, mbean.getDispatchFailureCount());
        assertEquals(UNAVAILABLE, mbean.getDispatchRejectedCount());
        assertEquals(UNAVAILABLE, mbean.getDispatchSuccessCount());
        assertEquals(IN_FLIGHT_COUNT, mbean.getDispatchInFlightCount());
        assertEquals(LAST_REJECTED_TIMESTAMP, mbean.getDispatchLastRejectedTimestamp());
    }

    private static class TestStatistics implements WebhookStatistics {

        @Override
        public long getDispatchedCount() {
            return DISPATCH_COUNT;
        }

        @Override
        public long getPublishedCount() {
            return PUBLISH_COUNT;
        }

        @Override
        public long getDispatchErrorCount() {
            return ERROR_COUNT;
        }

        @Override
        public long getDispatchFailureCount() {
            return FAILURE_COUNT;
        }

        @Override
        public long getDispatchRejectedCount() {
            return REJECTED_COUNT;
        }

        @Override
        public long getDispatchSuccessCount() {
            return SUCCESS_COUNT;
        }
    }
}
