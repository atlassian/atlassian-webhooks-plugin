package com.atlassian.webhooks.internal.client.request;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.atlassian.httpclient.api.Response;
import com.atlassian.webhooks.request.WebhookResponseBody;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AtlassianHttpResponseBodyTest {

    private static final int halfMegabyte = 524_288;

    @Test
    public void testMaxBodySize() throws IOException {
        // Here we have to create a > 0.5 MB stream
        byte[] buffer = new byte[1_000_000];
        Response response = mock(Response.class);
        when(response.getEntityStream()).thenReturn(new ByteArrayInputStream(buffer));
        WebhookResponseBody body = new AtlassianHttpResponseBody(response, halfMegabyte);

        int read = 0;
        InputStream content = body.getContent();
        while (content.available() > 0) {
            content.read();
            read++;
        }

        assertThat(read, equalTo(halfMegabyte));
    }

    @Test
    public void testSmallerBodyRetrievesEntireContent() throws IOException {
        byte[] buffer = new byte[halfMegabyte];
        buffer[halfMegabyte - 1] = 1; // set last byte to ensure all comes through
        Response response = mock(Response.class);
        when(response.getEntityStream()).thenReturn(new ByteArrayInputStream(buffer));
        WebhookResponseBody body = new AtlassianHttpResponseBody(response, halfMegabyte);

        byte[] result = IOUtils.readFully(body.getContent(), halfMegabyte);
        assertThat(result[halfMegabyte - 1], equalTo((byte) 1));
    }
}
