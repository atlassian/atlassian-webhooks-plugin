package com.atlassian.webhooks.internal.dao;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.WebhooksConfiguration;
import com.atlassian.webhooks.history.DetailedInvocation;
import com.atlassian.webhooks.history.InvocationOutcome;
import com.atlassian.webhooks.internal.WebhookHostAccessor;
import com.atlassian.webhooks.internal.history.SimpleDetailedInvocation;
import com.atlassian.webhooks.internal.history.SimpleDetailedRequest;
import com.atlassian.webhooks.internal.history.SimpleDetailedResponse;
import com.atlassian.webhooks.internal.model.SimpleWebhookScope;
import com.atlassian.webhooks.request.Method;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import static com.atlassian.webhooks.diagnostics.WebhookDiagnosticsEvent.PING;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AsyncInvocationHistoryDaoTest {

    @Mock
    private WebhooksConfiguration configuration;

    @Mock
    private InvocationHistoryDao delegate;

    @Mock
    private ScheduledExecutorService executorService;

    @Mock
    private WebhookHostAccessor hostAccessor;

    @Mock
    private ScheduledFuture future;

    @Mock
    private TransactionTemplate txTemplate;

    private AsyncInvocationHistoryDao dao;

    @Before
    public void setup() {
        when(configuration.getStatisticsFlushInterval()).thenReturn(Duration.ofSeconds(30L));
        when(hostAccessor.getExecutorService()).thenReturn(executorService);
        when(executorService.schedule(any(Runnable.class), anyLong(), any())).thenReturn(future);
        when(txTemplate.execute(any())).thenAnswer(invocation -> {
            TransactionCallback<?> callback = invocation.getArgument(0);
            return callback.doInTransaction();
        });
        dao = new AsyncInvocationHistoryDao(delegate, hostAccessor, txTemplate);
    }

    @Test
    public void testLifecycle() {
        verifyZeroInteractions(executorService);

        dao.onStart(configuration);

        verify(executorService).schedule(any(Runnable.class), anyLong(), any());
        verifyZeroInteractions(future);

        dao.onStop();
        verify(future).cancel(anyBoolean());
    }

    @Test
    public void testAddCounts() {
        dao.onStart(configuration);

        dao.addCounts(99, "some:event", new Date(), 1, 2, 3);
        dao.addCounts(99, "some:event", new Date(), 2, 3, 4);

        verifyZeroInteractions(delegate);

        dao.flush();
        verify(delegate).addCounts(eq(99), eq("some:event"), any(Date.class), eq(3), eq(5), eq(7));
    }

    @Test
    public void testGetCountsFlushes() {
        dao.onStart(configuration);

        dao.addCounts(99, "some:event", new Date(), 1, 2, 3);

        verifyZeroInteractions(delegate);

        // retrieving the counts should trigger auto-flush
        dao.getCounts(99, "some:event", 10);

        InOrder inOrder = inOrder(delegate);
        inOrder.verify(delegate).addCounts(eq(99), eq("some:event"), any(), eq(1), eq(2), eq(3));
        inOrder.verify(delegate).getCounts(99, "some:event", 10);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testGetCountsByEventIdFlushes() {
        dao.onStart(configuration);

        dao.addCounts(99, "some:event1", new Date(), 1, 2, 3);
        dao.addCounts(99, "some:event2", new Date(), 2, 3, 4);
        dao.addCounts(99, "some:event3", new Date(), 3, 4, 5);

        verifyZeroInteractions(delegate);

        // retrieving the counts should trigger auto-flush
        Set<String> events = ImmutableSet.of("some:event1", "some:event2");
        dao.getCountsByEvent(99, events, 10);

        verify(delegate).addCounts(eq(99), eq("some:event1"), any(), eq(1), eq(2), eq(3));
        verify(delegate).addCounts(eq(99), eq("some:event2"), any(), eq(2), eq(3), eq(4));
        verify(delegate).getCountsByEvent(99, events, 10);
        verifyNoMoreInteractions(delegate);
    }

    @Test
    public void testGetCountsByWebhooksFlushes() {
        dao.onStart(configuration);

        dao.addCounts(99, "some:event", new Date(), 1, 2, 3);
        dao.addCounts(100, "some:event", new Date(), 2, 3, 4);
        dao.addCounts(101, "some:event", new Date(), 3, 4, 5);

        verifyZeroInteractions(delegate);

        // retrieving the counts should trigger auto-flush
        Set<Integer> ids = ImmutableSet.of(99, 101);
        dao.getCountsByWebhook(ids, 10);

        verify(delegate).addCounts(eq(99), eq("some:event"), any(), eq(1), eq(2), eq(3));
        verify(delegate).addCounts(eq(101), eq("some:event"), any(), eq(3), eq(4), eq(5));
        verify(delegate).getCountsByWebhook(ids, 10);
        verifyNoMoreInteractions(delegate);
    }

    @Test
    public void testGetLatestInvocationFlushes() {
        dao.onStart(configuration);

        dao.addCounts(99, "some:event", new Date(), 1, 2, 3);

        verifyZeroInteractions(delegate);

        // retrieving the counts should trigger auto-flush
        dao.getLatestInvocation(99, "some:event", null);

        InOrder inOrder = inOrder(delegate);
        inOrder.verify(delegate).addCounts(eq(99), eq("some:event"), any(), eq(1), eq(2), eq(3));
        inOrder.verify(delegate).getLatestInvocation(99, "some:event", null);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testGetLatestInvocationsFlushes() {
        dao.onStart(configuration);

        dao.addCounts(99, "some:event", new Date(), 1, 2, 3);

        verifyZeroInteractions(delegate);

        // retrieving the counts should trigger auto-flush
        dao.getLatestInvocations(99, "some:event", null);

        InOrder inOrder = inOrder(delegate);
        inOrder.verify(delegate).addCounts(eq(99), eq("some:event"), any(), eq(1), eq(2), eq(3));
        inOrder.verify(delegate).getLatestInvocations(99, "some:event", null);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testGetLatestByEventIdFlushes() {
        dao.onStart(configuration);

        dao.addCounts(99, "some:event1", new Date(), 1, 2, 3);
        dao.addCounts(99, "some:event2", new Date(), 2, 3, 4);
        dao.addCounts(99, "some:event3", new Date(), 3, 4, 5);

        verifyZeroInteractions(delegate);

        // retrieving the counts should trigger auto-flush
        Set<String> events = ImmutableSet.of("some:event1", "some:event2");
        dao.getLatestInvocationsByEvent(99, events);

        verify(delegate).addCounts(eq(99), eq("some:event1"), any(), eq(1), eq(2), eq(3));
        verify(delegate).addCounts(eq(99), eq("some:event2"), any(), eq(2), eq(3), eq(4));
        verify(delegate).getLatestInvocationsByEvent(99, events);
        verifyNoMoreInteractions(delegate);
    }

    @Test
    public void testGetLatestByWebhooksFlushes() {
        dao.onStart(configuration);

        dao.addCounts(99, "some:event", new Date(), 1, 2, 3);
        dao.addCounts(100, "some:event", new Date(), 2, 3, 4);
        dao.addCounts(101, "some:event", new Date(), 3, 4, 5);

        verifyZeroInteractions(delegate);

        // retrieving the counts should trigger auto-flush
        Set<Integer> ids = ImmutableSet.of(99, 101);
        dao.getLatestInvocationsByWebhook(ids);

        verify(delegate).addCounts(eq(99), eq("some:event"), any(), eq(1), eq(2), eq(3));
        verify(delegate).addCounts(eq(101), eq("some:event"), any(), eq(3), eq(4), eq(5));
        verify(delegate).getLatestInvocationsByWebhook(ids);
        verifyNoMoreInteractions(delegate);
    }

    @Test
    public void testSaveInvocation() {
        dao.onStart(configuration);

        // save multiple invocations, only the latest one should be flushed
        dao.saveInvocation(101, createSuccessInvocation(2000));
        dao.saveInvocation(101, createSuccessInvocation(2002));
        dao.saveInvocation(101, createSuccessInvocation(2001));

        verifyZeroInteractions(delegate);

        dao.flush();

        ArgumentCaptor<DetailedInvocation> captor = ArgumentCaptor.forClass(DetailedInvocation.class);
        verify(delegate).saveInvocation(eq(101), captor.capture());

        DetailedInvocation actual = captor.getValue();
        assertThat(actual.getStart().toEpochMilli(), equalTo(2002L));
    }

    @Test
    public void testSaveGlobalInvocation() {
        dao.onStart(configuration);

        dao.saveInvocation(101, createSuccessInvocation(2000, WebhookScope.GLOBAL));

        verifyZeroInteractions(delegate);

        dao.flush();

        ArgumentCaptor<DetailedInvocation> captor = ArgumentCaptor.forClass(DetailedInvocation.class);
        verify(delegate).saveInvocation(eq(101), captor.capture());

        DetailedInvocation actual = captor.getValue();
        assertThat(actual.getStart().toEpochMilli(), equalTo(2000L));
    }

    private DetailedInvocation createSuccessInvocation(long startTimestamp) {
        return createSuccessInvocation(startTimestamp, new SimpleWebhookScope("repository", "1"));
    }

    private DetailedInvocation createSuccessInvocation(long startTimestamp, WebhookScope scope) {
        return new SimpleDetailedInvocation(
                "request-id",
                PING,
                scope,
                new SimpleDetailedRequest("request-body", ImmutableMap.of("Header-1", "Value-1"), Method.POST, "url"),
                new SimpleDetailedResponse(
                        "response-body",
                        "200 OK",
                        ImmutableMap.of("Header-2", "Value-2"),
                        InvocationOutcome.SUCCESS,
                        200),
                Instant.ofEpochMilli(startTimestamp),
                Instant.ofEpochMilli(startTimestamp + 2));
    }
}
