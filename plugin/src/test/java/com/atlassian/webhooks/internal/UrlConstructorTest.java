package com.atlassian.webhooks.internal;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.atlassian.webhooks.internal.UrlConstructorTest.BackslashEdgeCaseTests;
import com.atlassian.webhooks.internal.UrlConstructorTest.DollarEdgeCaseTests;
import com.atlassian.webhooks.internal.UrlConstructorTest.IndividualTests;
import com.atlassian.webhooks.internal.UrlConstructorTest.SimpleCharactersTests;

import static java.util.Map.entry;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(Suite.class)
@SuiteClasses({
    IndividualTests.class,
    SimpleCharactersTests.class,
    DollarEdgeCaseTests.class,
    BackslashEdgeCaseTests.class
})
public class UrlConstructorTest {

    static boolean skipUrlEncoding = true;

    public static class IndividualTests {

        @Test
        public void replacesParametersMarkedWithCurlyBraces() {
            Map<String, Object> urlReplacements = Collections.singletonMap("key", "value");

            String url = UrlConstructor.constructUrl("http://localhost/?param={key}", urlReplacements, skipUrlEncoding);

            assertEquals("http://localhost/?param=value", url);
        }

        @Test
        public void replacesParametersMarkedWithEscapedCurlyBraces() {
            Map<String, Object> urlReplacements = Collections.singletonMap("key", "value");

            String url =
                    UrlConstructor.constructUrl("http://localhost/?param=%7bkey%7d", urlReplacements, skipUrlEncoding);

            assertEquals("http://localhost/?param=value", url);
        }

        @Test
        public void canEscapeCurlyBraces() {
            String escapedUrl = UrlConstructor.escapeCurlyBraces("http://localhost/?param={value}");

            assertEquals("http://localhost/?param=%7bvalue%7d", escapedUrl);
        }
    }

    /**
     * Test cases for simple characters that are edge cases.
     *
     * @see DollarEdgeCaseTests
     * @see BackslashEdgeCaseTests
     */
    @RunWith(Parameterized.class)
    public static class SimpleCharactersTests {

        // based on RFC 1738 and RFC 3986
        private static final Map<String, String> reservedCharactersToEncodedValues = Map.ofEntries(
                entry(";", "%3B"),
                entry("/", "%2F"),
                entry("?", "%3F"),
                entry(":", "%3A"),
                entry("@", "%40"),
                entry("=", "%3D"),
                entry("&", "%26"),
                entry(",", "%2C"),
                entry("+", "%2B"));

        // based on RFC 1738 and RFC 3986
        private static final Map<String, String> unsafeCharactersToEncodedValues = Map.ofEntries(
                entry(" ", "%20"),
                entry("\"", "%22"),
                entry("<", "%3C"),
                entry(">", "%3E"),
                entry("#", "%23"),
                entry("%", "%25"),
                entry("{", "%7B"),
                entry("}", "%7D"),
                entry("|", "%7C"),
                entry("^", "%5E"),
                entry("~", "%7E"),
                entry("[", "%5B"),
                entry("]", "%5D"),
                entry("`", "%60"));

        @Parameterized.Parameter(0)
        public String character;

        @Parameterized.Parameter(1)
        public String encodedValue;

        @Parameterized.Parameters(name = "{index}: testCharacter={0}, encodedValue={1}")
        public static Iterable<Object[]> data() {
            // Combine both maps into a single list of test cases
            return Stream.of(reservedCharactersToEncodedValues, unsafeCharactersToEncodedValues)
                    .flatMap(map -> map.entrySet().stream())
                    .map(entry -> new Object[] {entry.getKey(), entry.getValue()})
                    .toList();
        }

        @Test
        public void testReservedAndUnsafeCharactersInParamsAreEscaped() {
            Map<String, Object> urlReplacements = Collections.singletonMap("key", character);

            String url = UrlConstructor.constructUrl("http://localhost/?param={key}", urlReplacements, skipUrlEncoding);

            assertEquals("http://localhost/?param=" + encodedValue, url);
        }

        @Test
        public void testEscapedCharactersInParamsAreDoubleEscaped() {
            // we expect the parameter to be double escaped, as e.g. %25 is not an encoded '%' but a real value (e.g.
            // some annoying user put it in their username)
            Map<String, Object> urlReplacements = Collections.singletonMap("key", encodedValue);

            String url = UrlConstructor.constructUrl("http://localhost/?param={key}", urlReplacements, skipUrlEncoding);

            assertEquals("http://localhost/?param=" + "%25" + encodedValue.substring(1), url);
        }

        @Test
        public void testReservedAndUnsafeCharactersInURLAreNotEscaped() {
            // Expect the URL to remain unchanged - note that this can produce invalid URLs, but this is an easy to fix
            // user error and attempting to handle this would be a bad idea due to the other edge cases with encoding
            Map<String, Object> urlReplacements = Collections.singletonMap("key", "value");

            String url = UrlConstructor.constructUrl(
                    "http://localhost" + character + "/?param={key}", urlReplacements, skipUrlEncoding);

            assertEquals("http://localhost" + character + "/?param=value", url);
        }

        @Test
        public void testEscapedCharactersInURLStayEscaped() {
            // Expect the URL to remain unchanged - note that this can produce invalid URLs, but this is an easy to fix
            // user error and attempting to handle this would be a bad idea due to the other edge cases with encoding
            Map<String, Object> urlReplacements = Collections.singletonMap("key", "value");

            String url = UrlConstructor.constructUrl(
                    "http://localhost" + encodedValue + "/?param={key}", urlReplacements, skipUrlEncoding);

            assertEquals("http://localhost" + encodedValue + "/?param=value", url);
        }

        @Test
        public void testReservedAndUnsafeCharactersNotChangedWhenUrlsAreEncoded() {
            String urlWithUnsafeCharacter = "http://localhost/?param=" + character;
            Map<String, Object> urlReplacements = Collections.singletonMap("key", character);

            String url = UrlConstructor.constructUrl("http://localhost/?param={key}", urlReplacements, false);

            assertEquals(urlWithUnsafeCharacter, url);
        }
    }

    /**
     * Test cases for edge cases with the '$' character, which can cause issues with RegExp.
     */
    public static class DollarEdgeCaseTests extends SimpleCharactersTests {
        @Parameterized.Parameters(name = "{index}: testCharacter={0}, encodedValue={1}")
        public static Iterable<Object[]> data() {
            return Stream.of(Map.entry("$", "%24"))
                    .map(entry -> new Object[] {entry.getKey(), entry.getValue()})
                    .toList();
        }

        @Test
        @Override
        public void testReservedAndUnsafeCharactersNotChangedWhenUrlsAreEncoded() {
            Map<String, Object> urlReplacements = Collections.singletonMap("key", character);

            try {
                String url = UrlConstructor.constructUrl("http://localhost/?param={key}", urlReplacements, false);
                fail("Expected IllegalArgumentException, got URL: " + url);
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), equalTo("Illegal group reference: group index is missing"));
            }
        }
    }

    /**
     * Test cases for edge cases with the '\' character.
     * <p>
     * Notably, since it is an escape character, it can cause issues with the string encoding.
     */
    public static class BackslashEdgeCaseTests extends SimpleCharactersTests {
        @Parameterized.Parameters(name = "{index}: testCharacter={0}, encodedValue={1}")
        public static Iterable<Object[]> data() {
            return Stream.of(Map.entry("\\", "%5C"))
                    .map(entry -> new Object[] {entry.getKey(), entry.getValue()})
                    .toList();
        }

        @Test
        @Override
        public void testReservedAndUnsafeCharactersNotChangedWhenUrlsAreEncoded() {
            Map<String, Object> urlReplacements = Collections.singletonMap("key", character);

            try {
                String url = UrlConstructor.constructUrl("http://localhost/?param={key}", urlReplacements, false);
                fail("Expected IllegalArgumentException, got URL: " + url);
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), equalTo("character to be escaped is missing"));
            }
        }
    }
}
