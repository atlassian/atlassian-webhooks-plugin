package com.atlassian.webhooks.internal.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import net.java.ao.ActiveObjectsException;
import net.java.ao.EntityManager;
import net.java.ao.Transaction;

import com.atlassian.activeobjects.internal.EntityManagedActiveObjects;
import com.atlassian.activeobjects.internal.TransactionManager;
import com.atlassian.activeobjects.spi.DatabaseType;
import com.atlassian.sal.api.transaction.TransactionCallback;

public class TestActiveObjectsWithOuterTx extends EntityManagedActiveObjects {

    private static final Map<String, DatabaseType> DATABASE_PRODUCT_TO_TYPE_MAP =
            ImmutableMap.<String, DatabaseType>builder()
                    .put("H2", DatabaseType.H2)
                    .put("HSQL Database Engine", DatabaseType.HSQL)
                    .put("MySQL", DatabaseType.MYSQL)
                    .put("PostgreSQL", DatabaseType.POSTGRESQL)
                    .put("Oracle", DatabaseType.ORACLE)
                    .put("Microsoft SQL Server", DatabaseType.MS_SQL)
                    .put("DB2", DatabaseType.DB2)
                    .build();

    public TestActiveObjectsWithOuterTx(EntityManager entityManager) {
        super(
                entityManager,
                new TransactionManager() {
                    public <T> T doInTransaction(final TransactionCallback<T> callback) {
                        try {
                            return new PropagationAwareTransaction<T>(entityManager) {
                                public T run() {
                                    return callback.doInTransaction();
                                }
                            }.execute();
                        } catch (SQLException var3) {
                            throw new ActiveObjectsException(var3);
                        }
                    }
                },
                findDatabaseType(entityManager));
    }

    private static DatabaseType findDatabaseType(EntityManager entityManager) {
        Connection connection = null;

        DatabaseType databaseType;
        try {
            connection = entityManager.getProvider().getConnection();
            String dbName = connection.getMetaData().getDatabaseProductName();
            Iterator it = DATABASE_PRODUCT_TO_TYPE_MAP.entrySet().iterator();

            Map.Entry entry;
            do {
                if (!it.hasNext()) {
                    return DatabaseType.UNKNOWN;
                }

                entry = (Map.Entry) it.next();
            } while (!dbName.startsWith((String) entry.getKey()));

            databaseType = (DatabaseType) entry.getValue();
        } catch (SQLException var15) {
            throw new ActiveObjectsException(var15);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException var14) {
                    throw new ActiveObjectsException(var14);
                }
            }
        }

        return databaseType;
    }

    public abstract static class PropagationAwareTransaction<T> extends Transaction<T> {

        public PropagationAwareTransaction(EntityManager manager) {
            super(manager);
        }

        @Override
        public T execute() throws SQLException {
            return run();
        }
    }
}
