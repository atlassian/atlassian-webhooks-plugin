package com.atlassian.webhooks.internal.client.request;

import java.util.Map;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Suite;

import com.atlassian.webhooks.internal.client.request.RawRequestTest.CharactersNotAllowedInJavaUrisTests;
import com.atlassian.webhooks.internal.client.request.RawRequestTest.HashEdgeCaseTests;
import com.atlassian.webhooks.internal.client.request.RawRequestTest.IndividualTests;
import com.atlassian.webhooks.internal.client.request.RawRequestTest.PercentageEdgeCaseTests;
import com.atlassian.webhooks.internal.client.request.RawRequestTest.SimpleCharactersTests;
import com.atlassian.webhooks.request.Method;

import static java.util.Map.entry;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.fail;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    IndividualTests.class,
    SimpleCharactersTests.class,
    CharactersNotAllowedInJavaUrisTests.class,
    PercentageEdgeCaseTests.class,
    HashEdgeCaseTests.class
})
public class RawRequestTest {

    public static class IndividualTests {

        @Test
        public void testAddingQueryParamsInUrlAndInBuilder() {
            RawRequest req = RawRequest.builder(Method.POST, "http://example.com?a=b")
                    .skipUrlEncoding(true)
                    .parameter("b", "c", "d", "e")
                    .build();

            String url = req.getUrl();
            assertThat(url, equalTo("http://example.com?a=b&b=c&b=d&b=e"));
        }

        @Test
        public void testUrlOfJustDomainHasNoQuery() {
            RawRequest req = RawRequest.builder(Method.POST, "http://example.com")
                    .skipUrlEncoding(true)
                    .build();

            assertThat(req.getUrl(), equalTo("http://example.com"));
        }

        @Test
        public void testAddingUnicode() {
            RawRequest req = RawRequest.builder(Method.POST, "http://example.com?a=b")
                    .skipUrlEncoding(true)
                    .parameter("👍", "🔥", "d", "e")
                    .build();

            String url = req.getUrl();
            assertThat(url, equalTo("http://example.com?a=b&%F0%9F%91%8D=%F0%9F%94%A5&%F0%9F%91%8D=d&%F0%9F%91%8D=e"));
        }

        @Test
        public void testRawRequest() {
            RawRequest req = RawRequest.builder(Method.POST, "http://example.com?a=b")
                    .skipUrlEncoding(true)
                    .build();

            String url = req.getUrl();
            assertThat(url, equalTo("http://example.com?a=b"));
        }

        @Test
        public void testNoDoubleEscapeInUrl() {
            RawRequest req = RawRequest.builder(Method.POST, "http://example.com?a=%25b")
                    .skipUrlEncoding(true)
                    .parameter("key", "%25value")
                    .build();

            String url = req.getUrl();
            // we expect the parameter to be double escaped, as %25 is not an encoded '%' but a real value (e.g.
            // it's a part of a username or a comment)
            assertThat(url, equalTo("http://example.com?a=%25b&key=%2525value"));
        }

        @Test
        public void testEqualSignInsideParameters() {
            RawRequest req = RawRequest.builder(Method.POST, "http://example.com/")
                    .skipUrlEncoding(true)
                    .parameter("=e", "=f")
                    .build();

            String url = req.getUrl();
            assertThat(url, equalTo("http://example.com/?%3De=%3Df"));
        }

        @Test
        public void testHandlingMultipleEncodedCharacters() {
            RawRequest req = RawRequest.builder(Method.POST, "http://example.com?token=%3D%3D&space=%20")
                    .skipUrlEncoding(true)
                    .parameter("token", "= =%")
                    .build();

            String url = req.getUrl();
            assertThat(url, equalTo("http://example.com?token=%3D%3D&space=%20&token=%3D%20%3D%25"));
        }
    }

    /**
     * Test cases for simple characters that are not edge cases.
     *
     * @see CharactersNotAllowedInJavaUrisTests
     * @see PercentageEdgeCaseTests
     * @see HashEdgeCaseTests
     */
    @RunWith(Parameterized.class)
    public static class SimpleCharactersTests {

        // based on RFC 1738 and RFC 3986
        private static final Map<String, String> reservedCharactersToEncodedValues = Map.ofEntries(
                entry(";", "%3B"),
                entry("/", "%2F"),
                entry("?", "%3F"),
                entry(":", "%3A"),
                entry("@", "%40"),
                entry("=", "%3D"),
                entry("&", "%26"),
                entry("$", "%24"),
                entry(",", "%2C"),
                entry("+", "%2B"));

        // based on RFC 1738 and RFC 3986
        private static final Map<String, String> unsafeCharactersToEncodedValues = Map.ofEntries(entry("~", "%7E"));

        @Parameterized.Parameter(0)
        public String character;

        @Parameterized.Parameter(1)
        public String encodedValue;

        @Parameterized.Parameters(name = "{index}: testCharacter={0}, encodedValue={1}")
        public static Iterable<Object[]> data() {
            // Combine both maps into a single list of test cases
            return Stream.of(reservedCharactersToEncodedValues, unsafeCharactersToEncodedValues)
                    .flatMap(map -> map.entrySet().stream())
                    .map(entry -> new Object[] {entry.getKey(), entry.getValue()})
                    .toList();
        }

        @Test
        public void testReservedAndUnsafeCharactersInParamsAreEscaped() {
            RawRequest req = RawRequest.builder(Method.POST, "http://example.com")
                    .skipUrlEncoding(true)
                    .parameter("key", character)
                    .build();

            String url = req.getUrl();
            assertThat(url, equalTo("http://example.com?key=" + encodedValue));
        }

        @Test
        public void testEscapedCharactersInParamsAreDoubleEscaped() {
            RawRequest req = RawRequest.builder(Method.POST, "http://example.com")
                    .skipUrlEncoding(true)
                    .parameter("key", encodedValue)
                    .build();

            String url = req.getUrl();
            // we expect the parameter to be double escaped, as e.g. %25 is not an encoded '%' but a real value (e.g.
            // some annoying user put it in their username)
            assertThat(url, equalTo("http://example.com?key=" + "%25" + encodedValue.substring(1)));
        }

        @Test
        public void testReservedAndUnsafeCharactersInURLAreNotEscaped() {
            String urlWithSpecialChars = "http://example.com/path/with" + character + "special";
            RawRequest req = RawRequest.builder(Method.POST, urlWithSpecialChars)
                    .skipUrlEncoding(true)
                    .build();

            // Expect the URL to remain unchanged - note that this can produce invalid URLs, but this is an easy to fix
            // user error and attempting to handle this would be a bad idea due to the other edge cases with encoding
            try {
                String url = req.getUrl();
                assertThat(url, equalTo(urlWithSpecialChars));
            } catch (IllegalArgumentException e) {
                // This is expected for some characters that the JDK refuses to parse
                assertThat(
                        e.getMessage(),
                        startsWith("Illegal character in path at index " + urlWithSpecialChars.indexOf(character)));
            }
        }

        @Test
        public void testEscapedCharactersInURLStayEscaped() {
            String urlWithEncodedChars = "http://example.com/path/with" + encodedValue + "special";
            RawRequest req = RawRequest.builder(Method.POST, urlWithEncodedChars)
                    .skipUrlEncoding(true)
                    .build();

            // Expect the URL to remain unchanged - note that this can produce invalid URLs, but this is an easy to fix
            // user error and attempting to handle this would be a bad idea due to the other edge cases with encoding
            String url = req.getUrl();
            assertThat(url, equalTo(urlWithEncodedChars));
        }

        @Test
        public void testDoesNotEscapeCharactersWhenUrlsAreEncoded() {
            String urlWithSpecialChars = "http://example.com/path/with" + character + "special";

            RawRequest req =
                    RawRequest.builder(Method.POST, urlWithSpecialChars).build();

            String url = req.getUrl();
            assertThat(url, equalTo(urlWithSpecialChars));
        }
    }

    /**
     * Test cases for characters that trigger illegal character exception when used in Java URIs.
     */
    public static class CharactersNotAllowedInJavaUrisTests extends SimpleCharactersTests {

        private static final Map<String, String> charactersNotAllowedByJavaUris = Map.ofEntries(
                entry(" ", "%20"),
                entry("\"", "%22"),
                entry("<", "%3C"),
                entry(">", "%3E"),
                entry("{", "%7B"),
                entry("}", "%7D"),
                entry("|", "%7C"),
                entry("\\", "%5C"),
                entry("^", "%5E"),
                entry("[", "%5B"),
                entry("]", "%5D"),
                entry("`", "%60"));

        @Parameterized.Parameters(name = "{index}: testCharacter={0}, encodedValue={1}")
        public static Iterable<Object[]> data() {
            return Stream.of(charactersNotAllowedByJavaUris)
                    .flatMap(map -> map.entrySet().stream())
                    .map(entry -> new Object[] {entry.getKey(), entry.getValue()})
                    .toList();
        }

        @Test
        @Override
        public void testDoesNotEscapeCharactersWhenUrlsAreEncoded() {
            String urlWithSpecialChars = "http://example.com/path/with" + character + "special";

            RawRequest req =
                    RawRequest.builder(Method.POST, urlWithSpecialChars).build();

            try {
                String url = req.getUrl();
                fail("Expected an IllegalArgumentException, but got: " + url);
            } catch (IllegalArgumentException e) {
                assertThat(
                        e.getMessage(),
                        equalTo("Illegal character in path at index " + urlWithSpecialChars.indexOf(character) + ": "
                                + urlWithSpecialChars));
            }
        }
    }

    /**
     * Test cases for edge cases with the '%' character.
     * <p>
     * Notably, since '%' is used for encoding, it can cause malformed escape pairs.
     */
    public static class PercentageEdgeCaseTests extends SimpleCharactersTests {

        @Parameterized.Parameters(name = "{index}: testCharacter={0}, encodedValue={1}")
        public static Iterable<Object[]> data() {
            return Stream.of(Map.entry("%", "%25"))
                    .map(entry -> new Object[] {entry.getKey(), entry.getValue()})
                    .toList();
        }

        @Test
        @Override
        public void testReservedAndUnsafeCharactersInURLAreNotEscaped() {
            String urlWithSpecialChars = "http://example.com/path/with" + character + "special";
            RawRequest req = RawRequest.builder(Method.POST, urlWithSpecialChars)
                    .skipUrlEncoding(true)
                    .build();

            // Expect the URL to remain unchanged - note that this can produce invalid URLs, but this is an easy to fix
            // user error and attempting to handle this would be a bad idea due to the other edge cases with encoding
            try {
                String url = req.getUrl();
                assertThat(url, equalTo(urlWithSpecialChars));
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), startsWith("Malformed escape pair"));
            }
        }

        @Test
        @Override
        public void testDoesNotEscapeCharactersWhenUrlsAreEncoded() {
            String urlWithSpecialChars = "http://example.com/path/with" + character + "special";

            RawRequest req =
                    RawRequest.builder(Method.POST, urlWithSpecialChars).build();

            try {
                String url = req.getUrl();
                fail("Expected an IllegalArgumentException, but got: " + url);
            } catch (IllegalArgumentException e) {
                assertThat(
                        e.getMessage(),
                        equalTo("Malformed escape pair at index 28: http://example.com/path/with%special"));
            }
        }
    }

    /**
     * Test cases for edge cases with the '#' character.
     * <p>
     * Notably, Java's URI class treats '#' as a fragment identifier, so it needs a special treatment.
     */
    public static class HashEdgeCaseTests extends SimpleCharactersTests {

        @Parameterized.Parameters(name = "{index}: testCharacter={0}, encodedValue={1}")
        public static Iterable<Object[]> data() {
            return Stream.of(Map.entry("#", "%23"))
                    .map(entry -> new Object[] {entry.getKey(), entry.getValue()})
                    .toList();
        }

        @Test
        @Override
        public void testDoesNotEscapeCharactersWhenUrlsAreEncoded() {
            String urlWithSpecialChars = "http://example.com/path/with" + character + "special";

            RawRequest req =
                    RawRequest.builder(Method.POST, urlWithSpecialChars).build();

            String url = req.getUrl();
            assertThat(url, equalTo("http://example.com/path/with"));
        }
    }
}
