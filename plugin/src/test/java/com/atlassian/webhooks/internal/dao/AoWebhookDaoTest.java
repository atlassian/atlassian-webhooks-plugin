package com.atlassian.webhooks.internal.dao;

import javax.annotation.Nonnull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.jdbc.Jdbc;
import net.java.ao.test.jdbc.NonTransactional;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;

import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.webhooks.internal.TestEvent;
import com.atlassian.webhooks.internal.dao.ao.AoWebhook;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookConfigurationEntry;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookEvent;
import com.atlassian.webhooks.internal.model.SimpleWebhookCredentials;
import com.atlassian.webhooks.internal.model.SimpleWebhookEvent;
import com.atlassian.webhooks.internal.model.SimpleWebhookScope;
import com.atlassian.webhooks.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import static com.atlassian.webhooks.internal.WebhookTestUtils.assertGlobalWebhook;
import static com.atlassian.webhooks.internal.WebhookTestUtils.assertStandardWebhook;
import static com.atlassian.webhooks.internal.WebhookTestUtils.globalWebhookRequest;
import static com.atlassian.webhooks.internal.WebhookTestUtils.standardWebhookRequest;

@RunWith(ActiveObjectsJUnitRunner.class)
@Jdbc(SystemPropertyJdbcConfiguration.class)
@Data(AoWebhookDaoTest.InternalUpdater.class)
public class AoWebhookDaoTest {

    // To be used to specify a field that should not match. The standard webhook has no fields that will match INVALID
    private static final String INVALID_FIELD = "INVALID";

    private AoWebhookDao dao;
    private EntityManager entityManager;
    private AoWebhook standardWebhook;

    @Before
    public void setup() {
        dao = new AoWebhookDao(new TestActiveObjects(entityManager));
        standardWebhook = dao.create(standardWebhookRequest());
    }

    @Test
    @NonTransactional
    public void testCreatedIsCorrectlyMapped() {
        assertStandardWebhook(standardWebhook);
    }

    @Test
    @NonTransactional
    public void testGlobalCreatedIsCorrectlyMapped() {
        AoWebhook globalWebhook = dao.create(globalWebhookRequest());
        assertGlobalWebhook(globalWebhook);
    }

    @Test
    @NonTransactional
    public void testDelete() {
        dao.delete(standardWebhook.getID());

        assertThat(dao.getById(standardWebhook.getID()), is(nullValue()));
    }

    @Test
    @NonTransactional
    public void testSearchWithNoResults() {
        AoWebhook[] is = dao.search(WebhookSearchRequest.builder()
                .scope(new SimpleWebhookScope("I", "dont-exist"))
                .build());

        assertThat(is.length, equalTo(0));
    }

    @Test
    @NonTransactional
    public void testDeleteOnNonExistentEntity() {
        assertThat(dao.delete(-1), is(false));
    }

    @Test
    @NonTransactional
    public void testLargerSearch() {
        int createdAmount = 200;
        for (int i = 0; i < createdAmount; i++) {
            dao.create(standardWebhookRequest());
        }

        assertThat(
                dao.search(WebhookSearchRequest.builder().build()).length,
                is(createdAmount + 1)); // +1 for @Before created one
    }

    @Test
    @NonTransactional
    public void testListOfScopeKeysSearch() {
        AoWebhook[] search = dao.search(WebhookSearchRequest.builder()
                .scopeType(standardWebhook.getScopeType(), INVALID_FIELD)
                .build());

        assertThat(search.length, is(1));
        assertStandardWebhook(search[0]);
    }

    @Test
    @NonTransactional
    public void testListOfScopesSearch() {
        WebhookScope validScope = new SimpleWebhookScope(standardWebhook.getScopeType(), standardWebhook.getScopeId());

        SimpleWebhookScope invalidScope = new SimpleWebhookScope(INVALID_FIELD, INVALID_FIELD);

        AoWebhook[] singleSearchValid =
                dao.search(WebhookSearchRequest.builder().scope(validScope).build());

        AoWebhook[] notFound =
                dao.search(WebhookSearchRequest.builder().scope(invalidScope).build());

        AoWebhook[] search = dao.search(
                WebhookSearchRequest.builder().scope(validScope, invalidScope).build());

        AoWebhook[] reverseOrderSearch = dao.search(
                WebhookSearchRequest.builder().scope(invalidScope, validScope).build());

        // Even though the first scope matches
        assertThat(notFound.length, is(0));

        assertThat(singleSearchValid.length, is(1));
        assertStandardWebhook(singleSearchValid[0]);

        assertThat(search.length, is(1));
        assertStandardWebhook(search[0]);

        assertThat(reverseOrderSearch.length, is(1));
        assertStandardWebhook(reverseOrderSearch[0]);
    }

    @Test
    @NonTransactional
    public void testLoadById() {
        AoWebhook webhook = dao.getById(standardWebhook.getID());

        assertStandardWebhook(webhook);
    }

    @Test
    @NonTransactional
    public void testBulkDelete() {
        AoWebhook a = dao.create(WebhookCreateRequest.builder()
                .url("http://a")
                .event(new SimpleWebhookEvent("array"))
                .build());
        AoWebhook b = dao.create(WebhookCreateRequest.builder()
                .url("http://b")
                .event(new SimpleWebhookEvent("array"))
                .build());

        WebhookSearchRequest allArrayWebhooks = WebhookSearchRequest.builder()
                .event(new SimpleWebhookEvent("array"))
                .build();
        int countBefore = dao.search(allArrayWebhooks).length;
        assertThat(countBefore, is(2));

        dao.delete(new AoWebhook[] {a, b});

        int countAfter = dao.search(allArrayWebhooks).length;
        assertThat(countAfter, is(0));
    }

    @Test
    @NonTransactional
    public void testSearchWebhookWithMultipleEventsToJoinTo() {
        AoWebhook created = dao.create(WebhookCreateRequest.builder()
                .url("http://example.com")
                .event(new SimpleWebhookEvent("one"), new SimpleWebhookEvent("two"))
                .build());

        AoWebhook[] results = dao.search(WebhookSearchRequest.builder()
                .event(new SimpleWebhookEvent("one"))
                .build());

        assertThat(results.length, is(1));
        assertThat(results[0].getID(), equalTo(created.getID()));
    }

    @Test
    @NonTransactional
    public void testSearch() {
        assertStandardWebhook(dao.search(WebhookSearchRequest.builder()
                .id(standardWebhook.getID())
                .build())[0]);

        assertStandardWebhook(dao.search(WebhookSearchRequest.builder()
                .event(toEventType(standardWebhook.getEvents()[0]))
                .build())[0]);

        assertStandardWebhook(dao.search(WebhookSearchRequest.builder()
                .scope(toScope(standardWebhook))
                .build())[0]);

        assertStandardWebhook(dao.search(WebhookSearchRequest.builder()
                .scopeType(standardWebhook.getScopeType())
                .build())[0]);

        assertStandardWebhook(dao.search(WebhookSearchRequest.builder()
                .name(standardWebhook.getName())
                .build())[0]);

        assertStandardWebhook(dao.search(WebhookSearchRequest.builder()
                .event(toEventType(standardWebhook.getEvents()[0]))
                .scopeType(standardWebhook.getScopeType())
                .name(standardWebhook.getName())
                .build())[0]);
    }

    @Test
    @NonTransactional
    public void testSearchForMultipleEvents() {
        AoWebhook[] search = dao.search(WebhookSearchRequest.builder()
                .event(
                        toEventType(standardWebhook.getEvents()[0]),
                        toEventType(standardWebhook.getEvents()[1]),
                        new SimpleWebhookEvent(INVALID_FIELD))
                .build());

        assertThat(search.length, is(1));
        assertStandardWebhook(search[0]);
    }

    @Test
    @NonTransactional
    public void testSearchInactive() {
        AoWebhook inactive = dao.create(WebhookCreateRequest.builder()
                .active(false)
                .name("NAME")
                .url("URL")
                .build());

        AoWebhook[] result =
                dao.search(WebhookSearchRequest.builder().active(false).build());

        AoWebhook[] allResults = dao.search(WebhookSearchRequest.builder().build());

        AoWebhook[] active =
                dao.search(WebhookSearchRequest.builder().active(true).build());

        assertThat(result.length, is(1));
        assertThat(result[0].isActive(), is(false));
        assertThat(result[0].getID(), equalTo(inactive.getID()));

        assertThat(allResults.length, is(2));

        assertThat(active.length, is(1));
        assertStandardWebhook(active[0]);
    }

    @Test
    @NonTransactional
    public void testSearchPages() {
        WebhookEvent testEvent = new SimpleWebhookEvent("test:event");
        AoWebhook webhook1 = dao.create(WebhookCreateRequest.builder()
                .event(testEvent)
                .name("hook-1")
                .url("http://callbacks.com/1")
                .build());
        AoWebhook webhook2 = dao.create(WebhookCreateRequest.builder()
                .event(testEvent)
                .name("hook-2")
                .url("http://callbacks.com/2")
                .build());
        AoWebhook webhook3 = dao.create(WebhookCreateRequest.builder()
                .event(testEvent)
                .name("hook-3")
                .url("http://callbacks.com/3")
                .build());

        WebhookSearchRequest request =
                WebhookSearchRequest.builder().event(testEvent).limit(2).build();

        // first search should return webhook1 and webhook2
        AoWebhook[] search = dao.search(request);
        assertThat(search.length, is(2));
        assertThat(search[0].getName(), equalTo(webhook1.getName()));
        assertThat(search[1].getName(), equalTo(webhook2.getName()));

        // second page should return webhook3
        search = dao.search(WebhookSearchRequest.builder(request).offset(2).build());
        assertThat(search.length, is(1));
        assertThat(search[0].getName(), equalTo(webhook3.getName()));

        // third page should be empty
        search = dao.search(WebhookSearchRequest.builder(request).offset(4).build());
        assertThat(search.length, is(0));
    }

    @Test
    @NonTransactional
    public void testUpdate() {
        dao.update(
                standardWebhook.getID(),
                WebhookUpdateRequest.builder()
                        .scope(new SimpleWebhookScope("updated-scope-type", "updated-scope-id"))
                        .event(new SimpleWebhookEvent("updated-event-id"))
                        .configuration("updated-key", "updated-value")
                        .credentials(new SimpleWebhookCredentials("test", "test"))
                        .name("updated-name")
                        .url("updated-url")
                        .sslVerificationRequired(false)
                        .build());

        AoWebhook ending = dao.getById(standardWebhook.getID());

        assertThat(ending.getName(), equalTo("updated-name"));
        assertThat(ending.getUrl(), equalTo("updated-url"));
        assertThat(ending.getEvents()[0].getEventId(), equalTo("updated-event-id"));
        assertThat(ending.getEvents().length, is(1));
        assertThat(ending.getScopeId(), equalTo("updated-scope-id"));
        assertThat(ending.getScopeType(), equalTo("updated-scope-type"));
        assertThat(ending.getConfiguration()[0].getKey(), equalTo("updated-key"));
        assertThat(ending.getConfiguration()[0].getValue(), equalTo("updated-value"));
        assertThat(ending.getConfiguration().length, is(1));
        assertThat(ending.getUsername(), equalTo("test"));
        assertThat(ending.getPassword(), equalTo("test"));
        assertThat(ending.isSslVerificationRequired(), equalTo(false));
    }

    @SuppressWarnings("ConstantConditions")
    @Nonnull
    private static WebhookEvent toEventType(AoWebhookEvent event) {
        return TestEvent.fromId(event.getEventId());
    }

    @Nonnull
    private static WebhookScope toScope(AoWebhook webhook) {
        return new SimpleWebhookScope(webhook.getScopeType(), webhook.getScopeId());
    }

    public static class InternalUpdater implements DatabaseUpdater {
        @Override
        public void update(EntityManager entityManager) throws Exception {
            //noinspection unchecked
            entityManager.migrate(AoWebhook.class, AoWebhookConfigurationEntry.class, AoWebhookEvent.class);
        }
    }
}
