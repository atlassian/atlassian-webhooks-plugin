package com.atlassian.webhooks.internal.jmx;

import javax.management.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableList;

import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.WebhooksConfiguration;
import com.atlassian.webhooks.internal.publish.WebhookDispatcher;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class JmxBootstrapTest {

    private static final String JMX_DOMAIN = "example.domain";

    private JmxBootstrap bootstrap;

    @Mock
    private WebhooksConfiguration configuration;

    @Mock
    private WebhookDispatcher dispatcher;

    @Captor
    private ArgumentCaptor<WebhooksMXBeanAdapter> mBeanCaptor;

    @Captor
    private ArgumentCaptor<ObjectName> mBeanNameCaptor;

    @Mock
    private MBeanServer mBeanServer;

    @Mock
    private WebhookService webhookService;

    @Before
    public void setup() {
        when(configuration.getJmxDomain()).thenReturn(JMX_DOMAIN);
        bootstrap = new JmxBootstrap(dispatcher, webhookService) {
            @Override
            MBeanServer getMBeanServer() {
                return mBeanServer;
            }
        };
    }

    @Test
    public void testOnStartRegistersMXBean() throws Exception {
        bootstrap.onStart(configuration);

        verify(mBeanServer).registerMBean(mBeanCaptor.capture(), mBeanNameCaptor.capture());

        ObjectName name = mBeanNameCaptor.getValue();
        assertEquals(JMX_DOMAIN, name.getDomain());
        assertEquals("Webhooks", name.getKeyProperty("name"));

        WebhooksMXBeanAdapter mbean = mBeanCaptor.getValue();
        assertNotNull(mbean);
    }

    @Test
    public void testOnStartHandlesExceptions() throws Exception {
        Iterable<Class<? extends Exception>> exceptions = ImmutableList.of(
                InstanceAlreadyExistsException.class,
                MBeanRegistrationException.class,
                NotCompliantMBeanException.class);

        for (Class<? extends Exception> exception : exceptions) {
            doThrow(exception).when(mBeanServer).registerMBean(any(), any());
            try {
                bootstrap.onStart(configuration);
            } catch (Exception e) {
                fail(exception.getName() + " was not handled");
            }
        }
    }

    @Test
    public void testOnStopUnregistersMXBean() throws Exception {
        bootstrap.onStart(configuration);
        bootstrap.onStop();

        verify(mBeanServer).unregisterMBean(mBeanNameCaptor.capture());
        ObjectName name = mBeanNameCaptor.getValue();
        assertEquals(JMX_DOMAIN, name.getDomain());
        assertEquals("Webhooks", name.getKeyProperty("name"));
    }

    @Test
    public void testOnStopHandlesExceptions() throws Exception {
        Iterable<Class<? extends Exception>> exceptions =
                ImmutableList.of(InstanceNotFoundException.class, MBeanRegistrationException.class);

        bootstrap.onStart(configuration);
        for (Class<? extends Exception> exception : exceptions) {
            doThrow(exception).when(mBeanServer).unregisterMBean(any());
            try {
                bootstrap.onStop();
            } catch (Exception e) {
                fail(exception.getName() + " was not handled");
            }
        }
    }
}
