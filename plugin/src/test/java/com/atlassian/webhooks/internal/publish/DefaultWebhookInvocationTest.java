package com.atlassian.webhooks.internal.publish;

import java.util.List;
import javax.annotation.Nonnull;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookCallback;
import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookPublishRequest;
import com.atlassian.webhooks.internal.WebhookTestUtils;
import com.atlassian.webhooks.internal.configuration.FeatureFlagService;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultWebhookInvocationTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private FeatureFlagService featureFlagService;

    @Test
    public void testDesiredMutableReferencesAreChangeable() {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getUrl()).thenReturn(WebhookTestUtils.DEFAULT_URL);

        DefaultWebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, webhook, mock(WebhookPublishRequest.class));

        invocation
                .getRequestBuilder()
                .parameter("One", "Two")
                .header("Fish", "Fob")
                .url("http://");
    }

    @Test
    public void testLeakedReferencesAreImmutable() {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getUrl()).thenReturn(WebhookTestUtils.DEFAULT_URL);

        InternalWebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, webhook, mock(WebhookPublishRequest.class));

        invocation.registerCallback(new WebhookCallback() {
            @Override
            public void onError(
                    WebhookHttpRequest request, @Nonnull Throwable error, @Nonnull WebhookInvocation webhook) {}

            @Override
            public void onFailure(
                    @Nonnull WebhookHttpRequest request,
                    @Nonnull WebhookHttpResponse response,
                    @Nonnull WebhookInvocation webhook) {}

            @Override
            public void onSuccess(
                    @Nonnull WebhookHttpRequest request,
                    @Nonnull WebhookHttpResponse response,
                    @Nonnull WebhookInvocation webhook) {}
        });

        List<WebhookCallback> callbacks = invocation.getCallbacks();
        expectedException.expect(UnsupportedOperationException.class);
        callbacks.remove(0);
    }

    @Test
    public void testUuidIsGeneratedForEachInvocation() {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getUrl()).thenReturn(WebhookTestUtils.DEFAULT_URL);

        WebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, webhook, mock(WebhookPublishRequest.class));
        WebhookInvocation secondInvocation =
                new DefaultWebhookInvocation(featureFlagService, webhook, mock(WebhookPublishRequest.class));

        assertThat(invocation.getId(), is(not(nullValue())));
        // This can fail, but if it does then submit to hacker-news, they'll be really impressed at the odds
        assertThat(secondInvocation.getId(), not(equalTo(invocation.getId())));
    }

    @Test
    public void testSkipUrlEncodingFeatureFlagEnabled() {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getUrl()).thenReturn(WebhookTestUtils.DEFAULT_URL);
        when(featureFlagService.shouldSkipUrlReEncoding()).thenReturn(true);

        DefaultWebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, webhook, mock(WebhookPublishRequest.class));

        // Verify that URL is constructed with skipUrlReEncoding = true and base URL is not encoded
        assertThat(invocation.getRequestBuilder().isSkipUrlEncoding(), is(true));
        assertThat(invocation.getRequestBuilder().getUrl(), is(WebhookTestUtils.DEFAULT_URL));
    }

    @Test
    public void testSkipUrlEncodingFeatureFlagDisabled() {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getUrl()).thenReturn(WebhookTestUtils.DEFAULT_URL);
        when(featureFlagService.shouldSkipUrlReEncoding()).thenReturn(false);

        DefaultWebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, webhook, mock(WebhookPublishRequest.class));

        // Verify that URL is constructed with skipUrlReEncoding = false and base URL is not encoded
        assertThat(invocation.getRequestBuilder().isSkipUrlEncoding(), is(false));
        assertThat(invocation.getRequestBuilder().getUrl(), is(WebhookTestUtils.DEFAULT_URL));
    }

    @Test
    public void testEqualityAndHashcode() {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getUrl()).thenReturn(WebhookTestUtils.DEFAULT_URL);

        WebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, webhook, "id", mock(WebhookPublishRequest.class));
        WebhookInvocation invocationPrime =
                new DefaultWebhookInvocation(featureFlagService, webhook, "id", mock(WebhookPublishRequest.class));
        WebhookInvocation differentInvocation = new DefaultWebhookInvocation(
                featureFlagService, webhook, "different-id", mock(WebhookPublishRequest.class));

        assertEquals(invocation, invocation);
        assertEquals(invocation, invocationPrime);
        assertEquals(invocation.hashCode(), invocation.hashCode());
        assertEquals(invocation.hashCode(), invocationPrime.hashCode());

        assertNotEquals(invocation, differentInvocation);
        // we can't make general claims about what hashCode is when equality doesn't hold but we do know that it
        // won't be equal when id is "id" vs "different-id"
        assertNotEquals(invocation.hashCode(), differentInvocation.hashCode());
    }
}
