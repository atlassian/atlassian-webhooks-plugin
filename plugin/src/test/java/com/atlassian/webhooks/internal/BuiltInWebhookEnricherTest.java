package com.atlassian.webhooks.internal;

import org.junit.Test;

import com.atlassian.webhooks.diagnostics.WebhookDiagnosticsEvent;
import com.atlassian.webhooks.internal.configuration.FeatureFlagService;
import com.atlassian.webhooks.internal.publish.DefaultWebhookInvocation;
import com.atlassian.webhooks.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BuiltInWebhookEnricherTest {

    @Test
    public void testIdIsAddedToPublishRequest() {
        WebhookRequestEnricher enricher = new BuiltInWebhookEnricher();
        FeatureFlagService featureFlagService = mock(FeatureFlagService.class);
        Webhook standardWebhook = getStandardWebhook();
        WebhookPublishRequest request = WebhookPublishRequest.builder(WebhookDiagnosticsEvent.PING, null)
                .build();
        WebhookInvocation invocation = new DefaultWebhookInvocation(featureFlagService, standardWebhook, request);

        enricher.enrich(invocation);

        assertThat(
                invocation.getRequestBuilder().getHeaders(),
                hasEntry("X-Event-Key", WebhookDiagnosticsEvent.PING.getId()));
    }

    private Webhook getStandardWebhook() {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getId()).thenReturn(1);
        when(webhook.getUrl()).thenReturn(WebhookTestUtils.DEFAULT_URL);
        return webhook;
    }
}
