package com.atlassian.webhooks.internal;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import com.google.gson.Gson;

import com.atlassian.webhooks.internal.configuration.FeatureFlagService;
import com.atlassian.webhooks.internal.publish.DefaultWebhookInvocation;
import com.atlassian.webhooks.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class DefaultPayloadManagerTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Captor
    private ArgumentCaptor<byte[]> bodyCaptor;

    @Mock
    private WebhookPayloadBuilder builder;

    @Captor
    private ArgumentCaptor<String> contentTypeCaptor;

    @Mock
    private FeatureFlagService featureFlagService;

    @Mock
    private WebhookHostAccessor hostAccessor;

    @Mock
    private Webhook webhook;

    @Before
    public void setup() {
        when(webhook.getUrl()).thenReturn("https://www.example.com/callback?arg=something");
    }

    @Test
    public void testStandardBody() {
        WebhookPayloadManager payloadManager = createPayloadManager(jsonMarshaller(true, null));

        WebhookInvocation invocation = new DefaultWebhookInvocation(
                featureFlagService,
                webhook,
                WebhookPublishRequest.builder(TestEvent.PROJ_DELETE, new TestObject())
                        .build());

        payloadManager.setPayload(invocation, builder);

        assertBodyEquals("{\"i\":0,\"st\":\"hi\"}");
    }

    @Test
    public void testSupportExcludesFromProcessing() {
        WebhookPayloadManager payloadManager = createPayloadManager(jsonMarshaller(false, null));

        WebhookInvocation invocation = new DefaultWebhookInvocation(
                featureFlagService,
                webhook,
                WebhookPublishRequest.builder(TestEvent.PROJ_DELETE, new TestObject())
                        .build());

        payloadManager.setPayload(invocation, builder);

        verifyZeroInteractions(builder);
    }

    @Test
    public void testOrderingIsRespected() {
        String testBody = "returned";
        WebhookPayloadManager payloadManager =
                createPayloadManager(jsonMarshaller(true, testBody), jsonMarshaller(true, null));

        WebhookInvocation invocation = new DefaultWebhookInvocation(
                featureFlagService,
                webhook,
                WebhookPublishRequest.builder(TestEvent.PROJ_DELETE, new TestObject())
                        .build());

        payloadManager.setPayload(invocation, builder);

        assertBodyEquals(testBody);
    }

    @Test
    public void testZeroMarshallersDefined() {
        WebhookPayloadManager payloadManager = createPayloadManager();

        WebhookInvocation invocation = new DefaultWebhookInvocation(
                featureFlagService,
                webhook,
                WebhookPublishRequest.builder(TestEvent.PROJ_DELETE, this).build());

        payloadManager.setPayload(invocation, builder);

        verifyZeroInteractions(builder);
    }

    private void assertBodyEquals(String expectedBody) {
        verify(builder).body(bodyCaptor.capture(), contentTypeCaptor.capture());

        byte[] body = bodyCaptor.getValue();
        if (expectedBody == null) {
            assertThat(body, nullValue());
        } else {
            assertThat(body, notNullValue());
            assertThat(new String(body), equalTo(expectedBody));
        }
        assertThat(contentTypeCaptor.getValue(), equalTo("application/json"));
    }

    private WebhookPayloadManager createPayloadManager(WebhookPayloadProvider... marshallers) {
        when(hostAccessor.getPayloadProviders()).thenReturn(Arrays.asList(marshallers));
        return new DefaultPayloadManager(hostAccessor);
    }

    private WebhookPayloadProvider jsonMarshaller(boolean support, @Nullable String hardCodedReturn) {
        return new WebhookPayloadProvider() {

            @Override
            public void setPayload(@Nonnull WebhookInvocation invocation, @Nonnull WebhookPayloadBuilder builder) {
                byte[] body = hardCodedReturn == null
                        ? new Gson()
                                .toJson(invocation.getPayload().orElse(null))
                                .getBytes(StandardCharsets.UTF_8)
                        : hardCodedReturn.getBytes(StandardCharsets.UTF_8);

                builder.body(body, "application/json");
            }

            @Override
            public int getWeight() {
                return 0;
            }

            @Override
            public boolean supports(@Nonnull WebhookInvocation invocation) {
                return support;
            }
        };
    }

    private static class TestObject {
        int i = 0;
        String st = "hi";
    }
}
