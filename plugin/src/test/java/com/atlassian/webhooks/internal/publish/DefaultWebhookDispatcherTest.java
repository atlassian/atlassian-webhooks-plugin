package com.atlassian.webhooks.internal.publish;

import java.time.Clock;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import javax.annotation.Nonnull;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.verification.VerificationWithTimeout;

import com.atlassian.webhooks.diagnostics.WebhookDiagnosticsEvent;
import com.atlassian.webhooks.internal.TestEvent;
import com.atlassian.webhooks.internal.WebhookTestUtils;
import com.atlassian.webhooks.internal.client.RequestExecutor;
import com.atlassian.webhooks.internal.configuration.FeatureFlagService;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;
import com.atlassian.webhooks.*;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultWebhookDispatcherTest {

    private static final VerificationWithTimeout STANDARD_TIMEOUT = timeout(1000);

    private final WebhookEvent event = TestEvent.PROJ_CREATE;
    private final Object eventTrigger = new Object();
    private int availableTickets;

    @Mock
    private WebhookCallback callback;

    @Mock
    private WebhookCallback callbackThatThrows;

    @Mock
    private Clock clock;

    @Mock(answer = Answers.CALLS_REAL_METHODS)
    private WebhooksConfiguration configuration;

    private DefaultWebhookDispatcher dispatcher;

    @Mock
    private RequestExecutor requestExecutor;

    @Mock
    private FeatureFlagService featureFlagService;

    @Mock
    private WebhookHttpResponse response;

    @Before
    public void setup() {
        when(response.getStatusCode()).thenReturn(200);
        when(clock.millis()).thenAnswer(invocation -> System.currentTimeMillis());
        TestException exception = new TestException("testing callback exception handling");
        doThrow(exception).when(callbackThatThrows).onError(any(), any(), any());
        doThrow(exception).when(callbackThatThrows).onFailure(any(), any(), any());
        doThrow(exception).when(callbackThatThrows).onSuccess(any(), any(), any());
        dispatcher = createDispatcher();
    }

    @After
    public void tearDown() {
        assertThat(
                "Should have an equal amount of tickets after a test finished",
                dispatcher.getAvailableTickets(),
                equalTo(availableTickets));
    }

    @Test
    public void testCallbacksCalledOnRequestExecutorError() {
        WebhookPublishRequest publishRequest = getStandardPublishRequest();
        TestException exception = new TestException("testing executor error");
        when(requestExecutor.execute(any())).thenThrow(exception);

        DefaultWebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, getStandardWebhook(), publishRequest);
        try {
            dispatcher.dispatch(invocation);
            fail("TestException was expected");
        } catch (TestException e) {
            // expected
        }
        verify(callbackThatThrows, STANDARD_TIMEOUT)
                .onError(any(WebhookHttpRequest.class), eq(exception), eq(invocation));
        verify(callback, STANDARD_TIMEOUT).onError(any(WebhookHttpRequest.class), eq(exception), eq(invocation));
    }

    @Test
    public void testCallbackOnError() {
        CompletableFuture<WebhookHttpResponse> future = setupFutureOnExecutor();
        WebhookPublishRequest publishRequest = getStandardPublishRequest();

        DefaultWebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, getStandardWebhook(), publishRequest);
        dispatcher.dispatch(invocation);

        TestException exception = new TestException("Testing executor error during dispatch");
        future.completeExceptionally(exception);

        verify(callbackThatThrows, STANDARD_TIMEOUT)
                .onError(any(WebhookHttpRequest.class), eq(exception), eq(invocation));
        verify(callback, STANDARD_TIMEOUT).onError(any(WebhookHttpRequest.class), eq(exception), eq(invocation));
    }

    @Test
    public void testCallbackOnFailure() {
        CompletableFuture<WebhookHttpResponse> future = setupFutureOnExecutor();
        WebhookPublishRequest publishRequest = getStandardPublishRequest();
        when(response.getStatusCode()).thenReturn(500);

        DefaultWebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, getStandardWebhook(), publishRequest);
        dispatcher.dispatch(invocation);

        future.complete(response);

        verify(callbackThatThrows, STANDARD_TIMEOUT)
                .onFailure(any(WebhookHttpRequest.class), eq(response), eq(invocation));
        verify(callback, STANDARD_TIMEOUT).onFailure(any(WebhookHttpRequest.class), eq(response), eq(invocation));
    }

    @Test
    public void testCallbackOnSuccess() {
        CompletableFuture<WebhookHttpResponse> future = setupFutureOnExecutor();

        WebhookPublishRequest publishRequest = getStandardPublishRequest();

        DefaultWebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, getStandardWebhook(), publishRequest);
        dispatcher.dispatch(invocation);

        future.complete(response);

        verify(callbackThatThrows, STANDARD_TIMEOUT)
                .onSuccess(any(WebhookHttpRequest.class), eq(response), eq(invocation));
        verify(callback, STANDARD_TIMEOUT).onSuccess(any(WebhookHttpRequest.class), eq(response), eq(invocation));
    }

    @Test
    public void testCircuitBreakingReleasesTickets() throws InterruptedException {
        when(configuration.getBackoffTriggerCount()).thenReturn(1);
        when(configuration.getBackoffExponent()).thenReturn(10.0d);
        when(configuration.getBackoffInitialDelay()).thenReturn(Duration.of(1, ChronoUnit.HOURS));

        dispatcher = createDispatcher();

        Webhook webhook = getStandardWebhook();
        WebhookPublishRequest publishRequest = getStandardPublishRequest();

        CompletableFuture<WebhookHttpResponse> future1 = new CompletableFuture<>();

        when(requestExecutor.execute(any())).thenReturn(future1);
        when(clock.millis()).thenReturn(1000L);

        future1.completeExceptionally(new TestException());

        // At this point, the webhook should be locked out for the next hour. Lets now try exhaust the tickets.
        AtomicInteger errors = new AtomicInteger();

        // Now we have to give it a moment to register the circuit breaker
        InternalWebhookInvocation firstInvocation =
                new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest);
        dispatcher.dispatch(firstInvocation);

        Thread.sleep(100);

        for (int i = 0; i < 999; i++) {
            InternalWebhookInvocation invocation =
                    new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest);
            invocation.registerCallback(new WebhookCallback() {
                @Override
                public void onError(
                        WebhookHttpRequest request, @Nonnull Throwable error, @Nonnull WebhookInvocation invocation) {
                    DispatchFailedException exception = (DispatchFailedException) error;
                    assertThat(
                            "This invocation *should* fail, but only due to being timed out, not due to in-flight webhooks",
                            exception.getMessage(),
                            not(containsString("Too many webhook dispatches already in flight")));
                    errors.incrementAndGet();
                }

                @Override
                public void onFailure(
                        @Nonnull WebhookHttpRequest request,
                        @Nonnull WebhookHttpResponse response,
                        @Nonnull WebhookInvocation invocation) {}

                @Override
                public void onSuccess(
                        @Nonnull WebhookHttpRequest request,
                        @Nonnull WebhookHttpResponse response,
                        @Nonnull WebhookInvocation invocation) {}
            });
            dispatcher.dispatch(invocation);
        }

        assertThat("There should be an error call for each dispatch after the first", errors.get(), is(999));
    }

    @Test
    public void testCircuitBreaking() throws InterruptedException {
        when(configuration.getBackoffTriggerCount()).thenReturn(2);
        when(configuration.getBackoffExponent()).thenReturn(10.0d);
        when(configuration.getBackoffInitialDelay()).thenReturn(Duration.of(25, ChronoUnit.SECONDS));
        when(configuration.getBackoffMaxDelay()).thenReturn(Duration.of(1000, ChronoUnit.SECONDS));

        dispatcher = createDispatcher();

        Webhook webhook = getStandardWebhook();
        WebhookPublishRequest publishRequest = getStandardPublishRequest();
        TestException toThrow = new TestException();

        CompletableFuture<WebhookHttpResponse> future1 = new CompletableFuture<>();
        CompletableFuture<WebhookHttpResponse> future2 = new CompletableFuture<>();
        CompletableFuture<WebhookHttpResponse> future3 = new CompletableFuture<>();
        CompletableFuture<WebhookHttpResponse> future4 = new CompletableFuture<>();
        CompletableFuture<WebhookHttpResponse> future5 = new CompletableFuture<>();
        CompletableFuture<WebhookHttpResponse> future6 = new CompletableFuture<>();
        CompletableFuture<WebhookHttpResponse> future7 = new CompletableFuture<>();
        CompletableFuture<WebhookHttpResponse> future8 = new CompletableFuture<>();
        when(requestExecutor.execute(any()))
                .thenReturn(future1, future2, future3, future4, future5, future6, future7, future8);
        when(clock.millis()).thenReturn(1000L);

        // make the first 2 invocations fail
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        future1.completeExceptionally(toThrow);
        future2.completeExceptionally(toThrow);
        verify(callback, STANDARD_TIMEOUT.times(2)).onError(any(), eq(toThrow), any());

        // the second invocation should cause the system to skip dispatching of webhooks for 25s -> until 26000L
        reset(callback);
        when(clock.millis()).thenReturn(2000L);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        when(clock.millis()).thenReturn(10000L);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        when(clock.millis()).thenReturn(25999L);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        verify(callback, STANDARD_TIMEOUT.times(3)).onError(any(), any(DispatchFailedException.class), any());

        // test that the dispatch happens after 25s and that the wait interval is increased after another failure (250s)
        reset(callback);
        when(clock.millis()).thenReturn(26000L);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        future3.completeExceptionally(toThrow);
        verify(callback, STANDARD_TIMEOUT).onError(any(), eq(toThrow), any());
        when(clock.millis()).thenReturn(275999L);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        verify(callback, STANDARD_TIMEOUT).onError(any(), any(DispatchFailedException.class), any());

        // test that the next dispatch attempt increases the wait interval yet again and that the wait interval is
        // capped at maxDelay (1000s)
        reset(callback);
        when(clock.millis()).thenReturn(276000L);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        future4.completeExceptionally(toThrow);
        verify(callback, STANDARD_TIMEOUT).onError(any(), eq(toThrow), any());
        when(clock.millis()).thenReturn(1275999L);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        verify(callback, STANDARD_TIMEOUT).onError(any(), any(DispatchFailedException.class), any());

        // test that a successful attempt clears the circuit breaker
        reset(callback);
        when(clock.millis()).thenReturn(1276000L);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        future5.complete(response);
        verify(callback, STANDARD_TIMEOUT).onSuccess(any(), any(), any());
        // after 2 failures backoff should start again with the initial delay
        when(clock.millis()).thenReturn(1277000L);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        future6.completeExceptionally(toThrow);
        when(clock.millis()).thenReturn(1278000L);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        future7.completeExceptionally(toThrow);
        verify(callback, STANDARD_TIMEOUT.times(2)).onError(any(), eq(toThrow), any());
        // verify the circuit breaker skips dispatch at 24.999s after previous failure
        when(clock.millis()).thenReturn(1302999L);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        verify(callback, STANDARD_TIMEOUT).onError(any(), any(DispatchFailedException.class), any());
        // verify the circuit breaker retries after 25s
        when(clock.millis()).thenReturn(1303000L);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        future8.complete(response);
        verify(callback, STANDARD_TIMEOUT.times(2)).onSuccess(any(), any(), any());
    }

    @Test
    public void testDiagnosticEventsDontTripCircuitBreaker() {
        when(configuration.getBackoffTriggerCount()).thenReturn(2);
        when(configuration.getBackoffExponent()).thenReturn(10.0d);
        when(configuration.getBackoffInitialDelay()).thenReturn(Duration.of(25, ChronoUnit.SECONDS));
        when(configuration.getBackoffMaxDelay()).thenReturn(Duration.of(1000, ChronoUnit.SECONDS));

        dispatcher = createDispatcher();

        Webhook webhook = getStandardWebhook();
        WebhookPublishRequest pingRequest = WebhookPublishRequest.builder(webhook, WebhookDiagnosticsEvent.PING, null)
                .callback(callback)
                .build();
        TestException toThrow = new TestException();

        CompletableFuture<WebhookHttpResponse> future1 = new CompletableFuture<>();
        CompletableFuture<WebhookHttpResponse> future2 = new CompletableFuture<>();
        CompletableFuture<WebhookHttpResponse> future3 = new CompletableFuture<>();

        when(requestExecutor.execute(any())).thenReturn(future1, future2, future3);
        when(clock.millis()).thenReturn(1000L);

        // make the first 2 invocations fail
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, pingRequest));
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, pingRequest));
        future1.completeExceptionally(toThrow);
        future2.completeExceptionally(toThrow);
        verify(callback, STANDARD_TIMEOUT.times(2)).onError(any(), eq(toThrow), any());

        // 3rd invocation should still trigger toThrow and not DispatchFailedException
        reset(callback);
        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, pingRequest));
        future3.completeExceptionally(toThrow);
        verify(callback, STANDARD_TIMEOUT).onError(any(), eq(toThrow), any());
    }

    @Test
    public void testMaxInFlightDispatchesEnforced() {
        when(configuration.getDispatchTimeout()).thenReturn(Duration.of(100, ChronoUnit.MILLIS));
        when(configuration.getMaxInFlightDispatches()).thenReturn(2);

        dispatcher = createDispatcher();

        // mock the executor to return a bunch of futures for subsequent invocations
        CompletableFuture<WebhookHttpResponse> future1 = new CompletableFuture<>();
        CompletableFuture<WebhookHttpResponse> future2 = new CompletableFuture<>();
        CompletableFuture<WebhookHttpResponse> future3 = new CompletableFuture<>();
        when(requestExecutor.execute(any())).thenReturn(future1, future2, future3);

        // dispatch 3 webhooks
        Webhook webhook = getStandardWebhook();
        WebhookPublishRequest publishRequest = getStandardPublishRequest();
        for (int i = 0; i < 3; ++i) {
            dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        }

        // verify that 2 webhooks got dispatched, but the 3rd was rejected because the 2 webhooks were still in flight
        verify(requestExecutor, times(2)).execute(any());
        verify(callback).onError(any(), any(DispatchFailedException.class), any());
        assertEquals(2, dispatcher.getInFlightCount());
        // the 2 webhooks haven't completed yet, so no further callbacks are expected yet
        verifyNoMoreInteractions(callback);
        assertThat(System.currentTimeMillis() - dispatcher.getLastRejectedTimestamp(), lessThan(1000L));

        // complete 1 webhook, this should make room for another webhook to be dispatched
        future1.complete(response);
        assertThatWithin(dispatcher::getInFlightCount, equalTo(1), 500);

        dispatcher.dispatch(new DefaultWebhookInvocation(featureFlagService, webhook, publishRequest));
        verify(requestExecutor, times(3)).execute(any());

        future2.complete(response);
        future3.complete(response);

        assertThatWithin(dispatcher::getInFlightCount, equalTo(0), 500);
    }

    @Test
    public void testNotInitializedHandledAsSkipped() {
        // create a fresh dispatcher so it's onStart is not called
        DefaultWebhookDispatcher dispatcher = new DefaultWebhookDispatcher(clock, requestExecutor);

        // mock that the requestExecutor hasn't been initialized either and returns WebhooksNotInitializedExceptions
        WebhooksNotInitializedException toThrow = new WebhooksNotInitializedException("test reason");
        CompletableFuture<WebhookHttpResponse> future = setupFutureOnExecutor();

        DefaultWebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, getStandardWebhook(), getStandardPublishRequest());

        // call dispatch a lot to verify that we don't hit exponential backoff or throttling at any point
        for (int i = 0; i < 1000; ++i) {
            dispatcher.dispatch(invocation);
        }
        future.completeExceptionally(toThrow);

        ArgumentCaptor<DispatchFailedException> exceptionCaptor =
                ArgumentCaptor.forClass(DispatchFailedException.class);
        verify(callback, STANDARD_TIMEOUT.times(1000))
                .onError(any(WebhookHttpRequest.class), exceptionCaptor.capture(), eq(invocation));
        exceptionCaptor.getAllValues().stream()
                .map(Exception::getMessage)
                .forEach(message -> assertThat(message, equalTo("test reason")));
    }

    @Test
    public void testRequestExecutorExceptionHandled() {
        TestException toThrow = new TestException();
        CompletableFuture<WebhookHttpResponse> future = setupFutureOnExecutor();

        WebhookPublishRequest publishRequest = getStandardPublishRequest();

        DefaultWebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, getStandardWebhook(), publishRequest);

        dispatcher.dispatch(invocation);
        future.completeExceptionally(toThrow);

        verify(callback, STANDARD_TIMEOUT).onError(any(WebhookHttpRequest.class), eq(toThrow), eq(invocation));
    }

    @Test
    public void testRequestTriggered() {
        CompletableFuture<WebhookHttpResponse> future = setupFutureOnExecutor();

        WebhookPublishRequest request = getStandardPublishRequest();

        DefaultWebhookInvocation invocation =
                new DefaultWebhookInvocation(featureFlagService, getStandardWebhook(), request);
        dispatcher.dispatch(invocation);

        verify(requestExecutor, STANDARD_TIMEOUT).execute(any());

        future.complete(response);

        verify(callback, STANDARD_TIMEOUT).onSuccess(any(WebhookHttpRequest.class), eq(response), eq(invocation));
    }

    private static <T> void assertThatWithin(Supplier<T> actual, Matcher<? super T> matcher, long timeoutMs) {
        long deadline = System.currentTimeMillis() + timeoutMs;
        long delay = 1;
        do {
            if (matcher.matches(actual.get())) {
                return;
            }
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                // ignore
            }
            ++delay;
        } while (System.currentTimeMillis() < deadline);

        assertThat(actual.get(), matcher);
    }

    private DefaultWebhookDispatcher createDispatcher() {
        DefaultWebhookDispatcher dispatcher = new DefaultWebhookDispatcher(clock, requestExecutor);
        dispatcher.onStart(configuration);

        availableTickets = dispatcher.getAvailableTickets();

        return dispatcher;
    }

    private WebhookPublishRequest getStandardPublishRequest() {
        return WebhookPublishRequest.builder(event, eventTrigger)
                .callback(callbackThatThrows, callback)
                .build();
    }

    private Webhook getStandardWebhook() {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getId()).thenReturn(1);
        when(webhook.getUrl()).thenReturn(WebhookTestUtils.DEFAULT_URL);
        return webhook;
    }

    private CompletableFuture<WebhookHttpResponse> setupFutureOnExecutor() {
        CompletableFuture<WebhookHttpResponse> future = new CompletableFuture<>();

        when(requestExecutor.execute(any())).thenReturn(future);
        return future;
    }

    private static class TestException extends RuntimeException {
        public TestException() {}

        public TestException(String message) {
            super(message);
        }
    }
}
