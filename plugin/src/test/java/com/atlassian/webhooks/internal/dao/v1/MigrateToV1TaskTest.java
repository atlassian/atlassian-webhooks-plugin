package com.atlassian.webhooks.internal.dao.v1;

import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.json.JSONArray;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.common.collect.ImmutableMap;

import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.jdbc.Jdbc;
import net.java.ao.test.jdbc.NonTransactional;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;

import com.atlassian.activeobjects.external.ModelVersion;
import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.webhooks.internal.dao.SystemPropertyJdbcConfiguration;
import com.atlassian.webhooks.internal.dao.ao.v0.WebHookListenerAOV0;
import com.atlassian.webhooks.internal.dao.ao.v1.AoWebhookConfigurationEntryV1;
import com.atlassian.webhooks.internal.dao.ao.v1.AoWebhookEventV1;
import com.atlassian.webhooks.internal.dao.ao.v1.AoWebhookV1;
import com.atlassian.webhooks.internal.dao.ao.v1.ExternalMigrationTaskInvokerGuard;
import com.atlassian.webhooks.internal.dao.ao.v1.MigrateToV1Task;
import com.atlassian.webhooks.internal.dao.ao.v1.WebhookV1MigrateService;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import static com.atlassian.webhooks.internal.dao.v1.MigrateToV1TaskTest.ConfigurationMatcher.containsConfiguration;

@RunWith(ActiveObjectsJUnitRunner.class)
@Jdbc(SystemPropertyJdbcConfiguration.class)
@Data(MigrateToV1TaskTest.VersionZeroUpdater.class)
public class MigrateToV1TaskTest {

    private static Map<String, Object> nullableData = ImmutableMap.<String, Object>builder()
            .put("URL", "http://example.com")
            .put("NAME", "exampleName")
            .put("REGISTRATION_METHOD", "service")
            .put("LAST_UPDATED", new Date())
            .build();
    private static Map<String, Object> standardHook = ImmutableMap.<String, Object>builder()
            .put("LAST_UPDATED_USER", "pault")
            .put("URL", "http://example.com")
            .put("NAME", "I am a name")
            .put("LAST_UPDATED", new Date())
            .put("DESCRIPTION", "I am a description")
            .put("EXCLUDE_BODY", true)
            .put("FILTERS", "some_string_here")
            .put("REGISTRATION_METHOD", "SERVICE")
            .put("EVENTS", "[\"project:created\", \"project:deleted\"]")
            .put("ENABLED", true)
            .build();
    protected EntityManager entityManager;
    private TestActiveObjects ao;
    private MigrateToV1Task upgradeTask;
    private WebhookV1MigrateService webhookV1MigrateService;

    @Before
    public void setup() {
        ao = new TestActiveObjects(entityManager);
        webhookV1MigrateService = new WebhookV1MigrateService();
        upgradeTask = new MigrateToV1Task(webhookV1MigrateService, new ExternalMigrationTaskInvokerGuard());
    }

    @NonTransactional
    @Test
    public void testAllNullableMigration() throws Exception {
        entityManager.create(WebHookListenerAOV0.class, nullableData);
        upgradeTask.upgrade(ModelVersion.valueOf("0"), ao);

        AoWebhookV1[] aoWebhooks = ao.find(AoWebhookV1.class);
        assertThat(aoWebhooks.length, is(1));
        assertWebhookEqual(aoWebhooks[0], nullableData);
    }

    @Test
    @NonTransactional
    public void testMigrationOfMultiple() throws Exception {
        for (int i = 0; i < 1000; i++) {
            entityManager.create(WebHookListenerAOV0.class, standardHook);
        }
        upgradeTask.upgrade(ModelVersion.valueOf("0"), ao);
        assertThat(ao.find(AoWebhookV1.class).length, is(1000));
    }

    @NonTransactional
    @Test
    public void testNoWebhooks() throws Exception {
        entityManager.delete(ao.find(WebHookListenerAOV0.class));
        upgradeTask.upgrade(ModelVersion.valueOf("0"), ao);
    }

    @NonTransactional
    @Test
    public void testSingleMigration() throws Exception {
        entityManager.create(WebHookListenerAOV0.class, standardHook);
        upgradeTask.upgrade(ModelVersion.valueOf("0"), ao);
        AoWebhookV1[] webhooks = ao.find(AoWebhookV1.class);
        assertThat(webhooks.length, is(1));

        assertWebhookEqual(webhooks[0], standardHook);
    }

    private void assertIfConfigurationExists(
            AoWebhookConfigurationEntryV1[] configuration, String configKey, Map<String, Object> oldData) {
        if (oldData.containsKey(configKey)) {
            assertThat(configuration, containsConfiguration(configKey, oldData.get(configKey)));
        } else {
            System.out.println("Skipping assert for " + configKey + " as it is not in the older data");
        }
    }

    private void assertWebhookEqual(AoWebhookV1 webhook, Map<String, Object> oldData) throws Exception {
        assertThat(webhook.getUrl(), equalTo(oldData.get("URL")));
        assertThat(webhook.getName(), equalTo(oldData.get("NAME")));
        assertWebhookEvents(webhook, (String) oldData.get("EVENTS"));

        if (oldData.get("ENABLED") != null) {
            assertThat(webhook.isActive(), equalTo(oldData.get("ENABLED")));
        } else {
            assertThat(webhook.isActive(), is(false));
        }

        assertIfConfigurationExists(webhook.getConfiguration(), "FILTERS", oldData);
        assertIfConfigurationExists(webhook.getConfiguration(), "REGISTRATION_METHOD", oldData);
        assertIfConfigurationExists(webhook.getConfiguration(), "PARAMETERS", oldData);
        assertIfConfigurationExists(webhook.getConfiguration(), "DESCRIPTION", oldData);
        assertIfConfigurationExists(webhook.getConfiguration(), "LAST_UPDATED_USER", oldData);
        assertIfConfigurationExists(webhook.getConfiguration(), "EXCLUDE_ISSUE_DETAILS", oldData);

        if (oldData.containsKey("LAST_UPDATED")) {
            assertThat(
                    webhook.getConfiguration(),
                    containsConfiguration(
                            "LAST_UPDATED",
                            ((Date) oldData.get("LAST_UPDATED"))
                                    .toInstant()
                                    .atOffset(ZoneOffset.UTC)
                                    .format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)));
        }

        if (oldData.containsKey("EXCLUDE_BODY")) {
            assertThat(
                    webhook.getConfiguration(),
                    containsConfiguration("EXCLUDE_BODY", "" + oldData.get("EXCLUDE_BODY")));
        }
    }

    private void assertWebhookEvents(AoWebhookV1 webhook, String oldEvents) throws Exception {
        if (oldEvents == null) {
            assertThat(webhook.getEvents().length, is(0));
        } else {
            JSONArray jsonArray = new JSONArray(oldEvents);
            for (int i = 0; i < jsonArray.length(); i++) {
                String event = jsonArray.getString(i);
                List<AoWebhookEventV1> matches = Arrays.stream(webhook.getEvents())
                        .filter(x -> x.getEventId().equals(event))
                        .collect(Collectors.toList());

                assertThat(matches, hasSize(1));
                assertThat(matches.get(0).getEventId(), equalTo(event));
                assertThat(matches.get(0).getWebhook().getID(), equalTo(webhook.getID()));
            }
        }
    }

    public static class ConfigurationMatcher extends BaseMatcher<AoWebhookConfigurationEntryV1[]> {

        private final String key;
        private final Object value;
        private boolean contained = false;
        private String foundValue;

        private ConfigurationMatcher(String key, Object value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public void describeTo(Description description) {
            description
                    .appendText("a configuration entry with key ")
                    .appendValue(key)
                    .appendText(" and the value ")
                    .appendValue(value);
        }

        @Override
        public void describeMismatch(Object item, Description description) {
            if (contained) {
                description
                        .appendText("value for configuration did not match. \nExpected: ")
                        .appendValue(value)
                        .appendText("\nActual: ")
                        .appendValue(foundValue);
            } else {
                description.appendText("no configuration matched the key ").appendValue(key);
            }
        }

        @Override
        public boolean matches(Object item) {
            if (!(item instanceof AoWebhookConfigurationEntryV1[])) {
                return false;
            }

            AoWebhookConfigurationEntryV1[] entries = (AoWebhookConfigurationEntryV1[]) item;
            for (AoWebhookConfigurationEntryV1 entry : entries) {
                if (entry.getKey().equals(key)) {
                    contained = true;
                    foundValue = entry.getValue();
                    if (foundValue.equals(value)) {
                        return true;
                    }
                }
            }
            return false;
        }

        static ConfigurationMatcher containsConfiguration(String key, Object value) {
            return new ConfigurationMatcher(key, value);
        }
    }

    public static class VersionZeroUpdater implements DatabaseUpdater {
        @Override
        public void update(EntityManager entityManager) throws Exception {
            //noinspection unchecked
            entityManager.migrate(
                    WebHookListenerAOV0.class,
                    AoWebhookV1.class,
                    AoWebhookConfigurationEntryV1.class,
                    AoWebhookEventV1.class);
        }
    }
}
