package com.atlassian.webhooks.internal.dao.v2;

import java.util.Date;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.google.common.collect.ImmutableMap;

import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.jdbc.Jdbc;
import net.java.ao.test.jdbc.NonTransactional;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;

import com.atlassian.activeobjects.external.ModelVersion;
import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.history.InvocationOutcome;
import com.atlassian.webhooks.internal.dao.SystemPropertyJdbcConfiguration;
import com.atlassian.webhooks.internal.dao.ao.AoHistoricalInvocation;
import com.atlassian.webhooks.internal.dao.ao.AoWebhook;
import com.atlassian.webhooks.internal.dao.ao.AoWebhookEvent;
import com.atlassian.webhooks.internal.dao.ao.v1.AoHistoricalInvocationV1;
import com.atlassian.webhooks.internal.dao.ao.v2.MigrateToV2Task;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(ActiveObjectsJUnitRunner.class)
@Jdbc(SystemPropertyJdbcConfiguration.class)
@Data(MigrateToV2TaskTest.VersionZeroUpdater.class)
public class MigrateToV2TaskTest {

    private static Map<String, Object> originalInvocation = ImmutableMap.<String, Object>builder()
            .put(AoHistoricalInvocation.COLUMN_ID, "1.repo:modified.S")
            .put(AoHistoricalInvocation.COLUMN_EVENT_ID, "repo:modified")
            .put(AoHistoricalInvocation.COLUMN_FINISH, 1674088938L)
            .put(AoHistoricalInvocation.COLUMN_OUTCOME, InvocationOutcome.SUCCESS)
            .put(AoHistoricalInvocation.COLUMN_REQUEST_ID, "710f8369-8eaf-4a54-8880-6751bf3986f0")
            .put(AoHistoricalInvocation.COLUMN_REQUEST_METHOD, "POST")
            .put(AoHistoricalInvocation.COLUMN_REQUEST_URL, "http://example.com")
            .put(AoHistoricalInvocation.COLUMN_RESULT_DESCRIPTION, "200")
            .put(AoHistoricalInvocation.COLUMN_START, 1673990095L)
            .put(AoHistoricalInvocation.COLUMN_WEBHOOK_ID, 1)
            .build();

    private static Map<String, Object> updatedInvocation = ImmutableMap.<String, Object>builder()
            .put(AoHistoricalInvocation.COLUMN_ID, "1.repo:modified.S")
            .put(AoHistoricalInvocation.COLUMN_EVENT_ID, "repo:modified")
            .put(AoHistoricalInvocation.COLUMN_FINISH, 1674088938L)
            .put(AoHistoricalInvocation.COLUMN_OUTCOME, InvocationOutcome.SUCCESS)
            .put(AoHistoricalInvocation.COLUMN_REQUEST_ID, "710f8369-8eaf-4a54-8880-6751bf3986f0")
            .put(AoHistoricalInvocation.COLUMN_REQUEST_METHOD, "POST")
            .put(AoHistoricalInvocation.COLUMN_REQUEST_URL, "http://example.com")
            .put(AoHistoricalInvocation.COLUMN_RESULT_DESCRIPTION, "200")
            .put(AoHistoricalInvocation.COLUMN_START, 1673990095L)
            .put(AoHistoricalInvocation.COLUMN_WEBHOOK_ID, 1)
            .put(AoHistoricalInvocation.COLUMN_EVENT_SCOPE_TYPE, "repository")
            .put(AoHistoricalInvocation.COLUMN_EVENT_SCOPE_ID, "1")
            .build();

    private static Map<String, Object> updatedGlobalInvocation = ImmutableMap.<String, Object>builder()
            .put(AoHistoricalInvocation.COLUMN_ID, "1.repo:modified.S")
            .put(AoHistoricalInvocation.COLUMN_EVENT_ID, "repo:modified")
            .put(AoHistoricalInvocation.COLUMN_FINISH, 1674088938L)
            .put(AoHistoricalInvocation.COLUMN_OUTCOME, InvocationOutcome.SUCCESS)
            .put(AoHistoricalInvocation.COLUMN_REQUEST_ID, "710f8369-8eaf-4a54-8880-6751bf3986f0")
            .put(AoHistoricalInvocation.COLUMN_REQUEST_METHOD, "POST")
            .put(AoHistoricalInvocation.COLUMN_REQUEST_URL, "http://example.com")
            .put(AoHistoricalInvocation.COLUMN_RESULT_DESCRIPTION, "200")
            .put(AoHistoricalInvocation.COLUMN_START, 1673990095L)
            .put(AoHistoricalInvocation.COLUMN_WEBHOOK_ID, 1)
            .put(AoHistoricalInvocation.COLUMN_EVENT_SCOPE_TYPE, WebhookScope.TYPE_GLOBAL)
            .put(AoHistoricalInvocation.COLUMN_EVENT_SCOPE_ID, "")
            .build();

    private static Map<String, Object> webhook = ImmutableMap.<String, Object>builder()
            .put(AoWebhook.URL_COLUMN, "http://example.com")
            .put(AoWebhook.NAME_COLUMN, "My webhook")
            .put(AoWebhook.CREATED_COLUMN, new Date())
            .put(AoWebhook.UPDATED_COLUMN, new Date())
            .put(AoWebhook.ACTIVE_COLUMN, true)
            .put(AoWebhook.SSL_VERIFICATION_REQUIRED_COLUMN, true)
            .put(AoWebhook.SCOPE_ID_COLUMN, "1")
            .put(AoWebhook.SCOPE_TYPE_COLUMN, "repository")
            .put("ID", 1)
            .build();

    private static Map<String, Object> globalWebhook = ImmutableMap.<String, Object>builder()
            .put(AoWebhook.URL_COLUMN, "http://example.com")
            .put(AoWebhook.NAME_COLUMN, "My webhook")
            .put(AoWebhook.CREATED_COLUMN, new Date())
            .put(AoWebhook.UPDATED_COLUMN, new Date())
            .put(AoWebhook.ACTIVE_COLUMN, true)
            .put(AoWebhook.SSL_VERIFICATION_REQUIRED_COLUMN, true)
            .put(AoWebhook.SCOPE_ID_COLUMN, "")
            .put(AoWebhook.SCOPE_TYPE_COLUMN, WebhookScope.TYPE_GLOBAL)
            .put("ID", 1)
            .build();

    private static Map<String, Object> webhookEvent = ImmutableMap.<String, Object>builder()
            .put(AoWebhookEvent.EVENT_ID_COLUMN, "repo:modified")
            .put(AoWebhookEvent.WEBHOOK_COLUMN_QUERY, 1)
            .build();

    protected EntityManager entityManager;
    private TestActiveObjects ao;
    private MigrateToV2Task upgradeTask;

    @Before
    public void setup() {
        ao = new TestActiveObjects(entityManager);
        upgradeTask = new MigrateToV2Task();
    }

    @NonTransactional
    @Test
    public void testMigration() throws Exception {
        entityManager.create(AoHistoricalInvocationV1.class, originalInvocation);
        entityManager.create(AoWebhook.class, webhook);
        entityManager.create(AoWebhookEvent.class, webhookEvent);

        upgradeTask.upgrade(ModelVersion.valueOf("1"), ao);

        AoHistoricalInvocation[] aoHistoricalInvocations = ao.find(AoHistoricalInvocation.class);
        assertThat(aoHistoricalInvocations.length, is(1));

        assertInvocations(aoHistoricalInvocations[0], updatedInvocation);
    }

    @NonTransactional
    @Test
    public void testMigrationOfGlobalWebhook() throws Exception {
        entityManager.create(AoHistoricalInvocationV1.class, originalInvocation);
        entityManager.create(AoWebhook.class, globalWebhook);
        entityManager.create(AoWebhookEvent.class, webhookEvent);

        upgradeTask.upgrade(ModelVersion.valueOf("1"), ao);

        AoHistoricalInvocation[] aoHistoricalInvocations = ao.find(AoHistoricalInvocation.class);
        assertThat(aoHistoricalInvocations.length, is(1));

        assertInvocations(aoHistoricalInvocations[0], updatedGlobalInvocation);
    }

    private void assertInvocations(AoHistoricalInvocation updatedInvocation, Map<String, Object> updatedInvocationData)
            throws Exception {
        assertThat(updatedInvocation.getId(), equalTo(updatedInvocationData.get(AoHistoricalInvocation.COLUMN_ID)));
        assertThat(
                updatedInvocation.getFinish(),
                equalTo(updatedInvocationData.get(AoHistoricalInvocation.COLUMN_FINISH)));
        assertThat(
                updatedInvocation.getEventId(),
                equalTo(updatedInvocationData.get(AoHistoricalInvocation.COLUMN_EVENT_ID)));
        assertThat(
                updatedInvocation.getOutcome(),
                equalTo(updatedInvocationData.get(AoHistoricalInvocation.COLUMN_OUTCOME)));
        assertThat(
                updatedInvocation.getRequestId(),
                equalTo(updatedInvocationData.get(AoHistoricalInvocation.COLUMN_REQUEST_ID)));
        assertThat(
                updatedInvocation.getRequestMethod(),
                equalTo(updatedInvocationData.get(AoHistoricalInvocation.COLUMN_REQUEST_METHOD)));
        assertThat(
                updatedInvocation.getRequestUrl(),
                equalTo(updatedInvocationData.get(AoHistoricalInvocation.COLUMN_REQUEST_URL)));
        assertThat(
                updatedInvocation.getResultDescription(),
                equalTo(updatedInvocationData.get(AoHistoricalInvocation.COLUMN_RESULT_DESCRIPTION)));
        assertThat(
                updatedInvocation.getStart(), equalTo(updatedInvocationData.get(AoHistoricalInvocation.COLUMN_START)));
        assertThat(
                updatedInvocation.getWebhookId(),
                equalTo(updatedInvocationData.get(AoHistoricalInvocation.COLUMN_WEBHOOK_ID)));
        assertThat(
                updatedInvocation.getEventScopeId(),
                equalTo(updatedInvocationData.get(AoHistoricalInvocation.COLUMN_EVENT_SCOPE_ID)));
        assertThat(
                updatedInvocation.getEventScopeType(),
                equalTo(updatedInvocationData.get(AoHistoricalInvocation.COLUMN_EVENT_SCOPE_TYPE)));
    }

    public static class VersionZeroUpdater implements DatabaseUpdater {
        @Override
        public void update(EntityManager entityManager) throws Exception {
            //noinspection unchecked
            entityManager.migrate(
                    AoWebhook.class,
                    AoWebhookEvent.class,
                    AoHistoricalInvocationV1.class,
                    AoHistoricalInvocation.class);
        }
    }
}
