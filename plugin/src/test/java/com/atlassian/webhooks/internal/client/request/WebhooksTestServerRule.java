package com.atlassian.webhooks.internal.client.request;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.io.IOUtils;
import org.junit.rules.ExternalResource;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import static org.junit.Assert.fail;

public class WebhooksTestServerRule extends ExternalResource {

    private ExecutorService executor = Executors.newFixedThreadPool(50);
    private HttpServer httpServer;

    public WebhooksTestServerRule() {
        try {
            ServerSocket serverSocket = new ServerSocket(0);
            int port = serverSocket.getLocalPort();
            serverSocket.close();

            httpServer = HttpServer.create(new InetSocketAddress("localhost", port), 0);
            httpServer.createContext("/echo", this::createStandardResponse);
            httpServer.createContext("/no-content", this::createNoContentResponse);
            httpServer.createContext(
                    "/slow",
                    httpExchange -> executor.submit(() -> {
                        try {
                            Thread.sleep(1000);
                            createStandardResponse(httpExchange);
                        } catch (InterruptedException | IOException e) {
                            // ignored
                        }
                    }));
            httpServer.start();
        } catch (IOException e) {
            e.printStackTrace();
            fail("Unable to allocate an echo server");
        }
    }

    public HttpServer getServer() {
        return httpServer;
    }

    @Override
    protected void after() {
        if (httpServer != null) {
            httpServer.stop(0);
        }
    }

    @Override
    protected void before() throws Throwable {}

    private void createNoContentResponse(HttpExchange httpExchange) throws IOException {
        httpExchange.sendResponseHeaders(204, 0);
        httpExchange.getResponseBody().close();
    }

    private void createStandardResponse(HttpExchange httpExchange) throws IOException {
        httpExchange
                .getRequestHeaders()
                .forEach((s, strings) -> httpExchange.getResponseHeaders().add(s, strings.get(0)));
        httpExchange.sendResponseHeaders(200, 0);
        IOUtils.copy(httpExchange.getRequestBody(), httpExchange.getResponseBody());
        httpExchange.getResponseBody().close();
    }
}
