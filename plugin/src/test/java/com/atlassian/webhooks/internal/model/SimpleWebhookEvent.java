package com.atlassian.webhooks.internal.model;

import javax.annotation.Nonnull;

import com.atlassian.webhooks.WebhookEvent;

import static java.util.Objects.requireNonNull;

public class SimpleWebhookEvent implements WebhookEvent {

    private final String id;
    private final String i18nKey;

    public SimpleWebhookEvent(@Nonnull String id) {
        this(id, "i18n." + id);
    }

    public SimpleWebhookEvent(@Nonnull String id, @Nonnull String i18nKey) {
        this.id = requireNonNull(id, "id");
        this.i18nKey = requireNonNull(i18nKey, "i18nKey");
    }

    @Nonnull
    @Override
    public String getId() {
        return id;
    }

    @Nonnull
    @Override
    public String getI18nKey() {
        return i18nKey;
    }
}
