## [7.1.1] - 2023-04-21
* Includes [WEBHOOKS-172](https://ecosystem.atlassian.net/browse/WEBHOOKS-172) fix.
* Includes [WEBHOOKS-174](https://ecosystem.atlassian.net/browse/WEBHOOKS-174) fix.

## [7.1.0] - 2023-01-30
* Add new `credentials` and `sslVerificationRequired` attributes to `Webhook` and relevant request classes
* Add new `WebhookCredentials` interface 
* Add new `isSslVerificationRequired` attribute to `WebhookHttpRequest`
* Add new `eventScope` attribute to `HistoricalInvocation`
* Add new `getEventScope` method to `WebhookInvocation`
* Add new `getByWebhookAndScope` and `getByWebhookAndScopeForDays` methods to `InvocationHistoryService`

## [7.0.1] - Unreleased
* Update Postgresql version to 42.3.7 (BSP-4422)

## [6.2.0] - 2020-12-01
* Add new `getByWebhookForDays` method to `InvocationHistoryService`
* Fix when trying to call `getByWebhook` with an empty collection

## [6.1.2] - 2020-06-11

* 6.1.1 included all the changes in 6.2 by mistake. This removes the 6.2 changes.
* Includes [WEBHOOKS-155](https://ecosystem.atlassian.net/browse/WEBHOOKS-155) fix.

## [6.1.1] - 2020-06-02

DO NOT USE. Includes changes from 6.2 by mistake.

### Security

* [WEBHOOKS-155](https://ecosystem.atlassian.net/browse/WEBHOOKS-155): Don't save or display webhook error messages.