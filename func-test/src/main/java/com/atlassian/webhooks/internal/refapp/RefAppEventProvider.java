package com.atlassian.webhooks.internal.refapp;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookEventProvider;

public class RefAppEventProvider implements WebhookEventProvider {

    @Nullable
    @Override
    public WebhookEvent forId(@Nonnull String id) {
        return RefappEvent.fromId(id);
    }

    @Nonnull
    @Override
    public List<WebhookEvent> getEvents() {
        return Arrays.asList(RefappEvent.values());
    }

    @Override
    public int getWeight() {
        return 100;
    }
}
