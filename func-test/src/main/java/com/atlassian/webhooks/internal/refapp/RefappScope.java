package com.atlassian.webhooks.internal.refapp;

import java.util.Optional;
import javax.annotation.Nonnull;

import com.atlassian.webhooks.WebhookScope;

import static java.util.Optional.ofNullable;

public class RefappScope implements WebhookScope {

    private final String id;
    private final String type;

    public RefappScope(String type, String id) {
        this.id = id;
        this.type = type;
    }

    public RefappScope(WebhookScope scope) {
        id = scope.getId().orElse(null);
        type = scope.getType();
    }

    @Nonnull
    @Override
    public Optional<String> getId() {
        return ofNullable(id);
    }

    @Override
    public String getType() {
        return type;
    }
}
