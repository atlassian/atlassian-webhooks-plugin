package com.atlassian.webhooks.internal.refapp;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.webhooks.WebhookEvent;

/**
 * Webhook event types used for testing
 */
public enum RefappEvent implements WebhookEvent {
    PROJECT_CREATE("project:create"),
    PROJECT_DELETE("project:delete"),
    PROJECT_MODIFIED("project:modified"),
    MOONBASE_CREATED("moon-base:created"),
    MOONBASE_DESTROYED("moon-base:destroyed");

    private static final String I18N_PREFIX = "refapp.webhook.event.";
    private final String id;

    RefappEvent(String id) {
        this.id = id;
    }

    @Nonnull
    @Override
    public String getId() {
        return id;
    }

    @Nonnull
    @Override
    public String getI18nKey() {
        return I18N_PREFIX + id;
    }

    @Nullable
    public static RefappEvent fromId(String id) {
        for (RefappEvent event : values()) {
            if (event.id.equalsIgnoreCase(id)) {
                return event;
            }
        }
        return null;
    }
}
