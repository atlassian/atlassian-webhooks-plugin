package com.atlassian.webhooks.internal.refapp;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookPayloadBuilder;
import com.atlassian.webhooks.WebhookPayloadProvider;

/**
 * Simple payload marshaller that uses Jackson to generate a JSON from the webhook payload
 */
public class RefAppPayloadProvider implements WebhookPayloadProvider {

    private final ObjectMapper mapper;

    public RefAppPayloadProvider() {
        mapper = new ObjectMapper();
    }

    @Override
    public int getWeight() {
        return 100;
    }

    @Override
    public void setPayload(@Nonnull WebhookInvocation invocation, @Nonnull WebhookPayloadBuilder builder) {
        Object payload = invocation.getPayload().orElse(null);
        byte[] body;
        try {
            body = mapper.writeValueAsString(payload).getBytes(StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        builder.body(body, "application/json");
    }

    @Override
    public boolean supports(@Nonnull WebhookInvocation invocation) {
        return true;
    }
}
