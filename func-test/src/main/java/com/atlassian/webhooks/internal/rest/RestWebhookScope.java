package com.atlassian.webhooks.internal.rest;

import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.atlassian.webhooks.WebhookScope;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class RestWebhookScope {

    private String id;
    private String type;

    public RestWebhookScope() {}

    public RestWebhookScope(WebhookScope scope) {
        this(scope.getType(), scope.getId().orElse(null));
    }

    public RestWebhookScope(String type, String id) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static RestWebhookScope valueOf(Object value) {
        if (value instanceof RestWebhookScope) {
            return (RestWebhookScope) value;
        }
        if (value instanceof WebhookScope) {
            return new RestWebhookScope((WebhookScope) value);
        }
        if (value instanceof Map) {
            Map<String, String> map = (Map<String, String>) value;
            return new RestWebhookScope(map.get("type"), map.get("id"));
        }
        return null;
    }
}
