package com.atlassian.webhooks.internal.rest;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;
import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookCreateRequest;
import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.internal.refapp.RefappScope;

/**
 * REST resource for subscribing and unsubscribing to webhooks for the tests.
 */
@UnrestrictedAccess
@Consumes(MediaType.APPLICATION_JSON)
@Path("webhooks")
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class WebhookResource {

    private final WebhookService webhookService;

    @Inject
    public WebhookResource(WebhookService webhookService) {
        this.webhookService = webhookService;
    }

    @POST
    public Response createWebhook(RestWebhook webhook) {
        Webhook result = webhookService.create(WebhookCreateRequest.builder()
                .active(webhook.isActive())
                .configuration(webhook.getConfiguration())
                .event(toEvents(webhook.getEvents()))
                .name(webhook.getName())
                .scope(toScope(webhook.getScope()))
                .sslVerificationRequired(webhook.isSslVerificationRequired())
                .url(webhook.getUrl())
                .build());

        return Response.ok().entity(new RestWebhook(result)).build();
    }

    @DELETE
    @Path("/{webhookId}")
    public Response delete(@PathParam("webhookId") int id) {
        if (webhookService.delete(id)) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    private List<WebhookEvent> toEvents(List<String> eventIds) {
        return eventIds.stream()
                .map(eventId -> webhookService.getEvent(eventId).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private WebhookScope toScope(RestWebhookScope scope) {
        return scope == null ? null : new RefappScope(scope.getType(), scope.getId());
    }
}
