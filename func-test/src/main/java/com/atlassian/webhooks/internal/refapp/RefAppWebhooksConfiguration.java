package com.atlassian.webhooks.internal.refapp;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import javax.annotation.Nonnull;

import com.atlassian.webhooks.WebhooksConfiguration;

public class RefAppWebhooksConfiguration implements WebhooksConfiguration {

    @Override
    public int getMaxInFlightDispatches() {
        return 100_000;
    }

    @Nonnull
    @Override
    public Duration getDispatchTimeout() {
        return Duration.of(10, ChronoUnit.SECONDS);
    }
}
