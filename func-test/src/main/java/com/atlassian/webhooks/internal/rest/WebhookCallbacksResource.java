package com.atlassian.webhooks.internal.rest;

import java.util.concurrent.atomic.AtomicInteger;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.rest.api.security.annotation.UnrestrictedAccess;

@UnrestrictedAccess
@Consumes(MediaType.APPLICATION_JSON)
@Path("callbacks")
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class WebhookCallbacksResource {

    private final AtomicInteger counter;

    @Inject
    public WebhookCallbacksResource() {
        counter = new AtomicInteger();
    }

    @DELETE
    public Response clearStatistics() {
        counter.set(0);

        return Response.ok().build();
    }

    @GET
    public Response getCallbackCounts() {
        return Response.ok(counter.get()).build();
    }

    @POST
    public Response webhookCallback() {
        int count = counter.incrementAndGet();
        return Response.ok(count).build();
    }
}
