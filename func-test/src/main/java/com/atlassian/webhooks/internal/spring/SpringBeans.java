package com.atlassian.webhooks.internal.spring;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.osgi.framework.ServiceRegistration;

import com.atlassian.webhooks.WebhookEventProvider;
import com.atlassian.webhooks.WebhookPayloadProvider;
import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.WebhooksConfiguration;
import com.atlassian.webhooks.internal.refapp.RefAppEventProvider;
import com.atlassian.webhooks.internal.refapp.RefAppPayloadProvider;
import com.atlassian.webhooks.internal.refapp.RefAppWebhooksConfiguration;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

/**
 * The Spring bean definitions for Atlassian Webhooks Integration Tests
 */
@Configuration
@SuppressWarnings("rawtypes")
public class SpringBeans {

    @Bean
    public WebhookEventProvider webhookEventProvider() {
        return new RefAppEventProvider();
    }

    @Bean
    public WebhookPayloadProvider webhookPayloadProvider() {
        return new RefAppPayloadProvider();
    }

    @Bean
    public WebhooksConfiguration webhooksConfiguration() {
        return new RefAppWebhooksConfiguration();
    }

    // Imports

    @Bean
    public WebhookService webhookService() {
        return importOsgiService(WebhookService.class);
    }

    // Exports

    @Bean
    public FactoryBean<ServiceRegistration> exportWebhookEventProvider(
            final WebhookEventProvider webhookEventProvider) {
        return exportOsgiService(webhookEventProvider, as(WebhookEventProvider.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportWebhookPayloadProvider(
            final WebhookPayloadProvider webhookPayloadProvider) {
        return exportOsgiService(webhookPayloadProvider, as(WebhookPayloadProvider.class));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportWebhooksConfiguration(
            final WebhooksConfiguration webhooksConfiguration) {
        return exportOsgiService(webhooksConfiguration, as(WebhooksConfiguration.class));
    }
}
