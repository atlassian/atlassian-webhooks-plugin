package it.com.atlassian.webhooks;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.matching.RequestPattern;
import com.github.tomakehurst.wiremock.matching.StringValuePattern;
import com.github.tomakehurst.wiremock.verification.FindRequestsResult;
import com.github.tomakehurst.wiremock.verification.LoggedRequest;

import com.atlassian.webhooks.WebhookCreateRequest;
import com.atlassian.webhooks.WebhookScope;
import com.atlassian.webhooks.internal.WebhookTestClient;
import com.atlassian.webhooks.internal.refapp.RefappEvent;
import com.atlassian.webhooks.internal.rest.RestWebhook;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.fail;

// The ProjectCreatedEvent (and project:created event) is used to test dispatches to webhooks registered
// through the API. Since <webhook> modules cannot easily be disabled, the tests for verifying dispatches
// to webhook listeners registered through the API and atlassian-plugin.xml use different events to be
// independent.
public class ApiWebhooksDispatchTest {

    private final WebhookTestClient client = new WebhookTestClient();

    private List<Integer> webhookIds;

    private List<WireMockServer> callbackServers;

    private static final int delayedTime = 20;

    @Before
    public void setup() {
        callbackServers = new ArrayList<>();
        webhookIds = new ArrayList<>();
    }

    @After
    public void teardown() {
        for (int id : webhookIds) {
            try {
                client.delete(id);
            } catch (Exception e) {
                // ignore
            }
        }
        // Added thread sleep to 20 seconds as Delayed Callbacks for response before Shutdown all the servers
        try {
            Thread.sleep(TimeUnit.SECONDS.toMillis(delayedTime));
        } catch (InterruptedException e) {
            // ignore
        }
        client.resetCallbackCounter();
        callbackServers.forEach(WireMockServer::shutdownServer);
    }

    @Test
    public void testDispatch() {
        WireMockServer server = startCallbackServer(5, 1);
        String callbackUrl = mockCallback(server, randomId(), 200, "{\"hello\": \"world\"");
        registerProjectCreateListener("test-hook", callbackUrl);

        client.triggerProjectCreate("TEST-PROJECT");

        assertPostsReceived(
                server, callbackUrl, equalToJson("{\"projectKey\": \"TEST-PROJECT\"}"), 1, 2, TimeUnit.SECONDS);
    }

    // This test will go green when atlassian-httpclient is upgraded and the maxTotalConnections is set to a value > 55
    @Test
    public void testSlowListenerDoesNotBlockOtherListeners() {
        // Tests whether 10 slow listeners and 1 listener that is down prevents webhooks from being delivered to
        // a listener that performs normally
        String callId = randomId();
        String callbackUrlDelayed;
        for (int i = 0; i < 10; i++) {
            WireMockServer slowServer = startCallbackServer(15, 3);
            // added fixed delay to 20 seconds for response callback
            callbackUrlDelayed =
                    mockDelayedCallbacks(slowServer, "DELAYED" + callId, TimeUnit.SECONDS.toMillis(delayedTime));
            registerProjectCreateListener("slow-hook-" + i, callbackUrlDelayed);
        }

        // configure a listener that is down
        WireMockServer shutdownServer = startCallbackServer(5, 1);
        String callbackUrlShutdown = mockCallback(shutdownServer, "SHUTDOWN" + callId, 200, "{}");
        registerProjectCreateListener("shutdown-hook", callbackUrlShutdown);
        shutdownServer.removeStub(shutdownServer.getStubMappings().get(0));
        shutdownServer.shutdown();

        // configure a listener that responds normally
        WireMockServer okServer = startCallbackServer(15, 3);
        String callbackUrlOkServer = mockCallback(okServer, "OK" + callId, 200, "{}");
        registerProjectCreateListener("ok-hook", callbackUrlOkServer);

        int eventCount = 200;
        client.triggerProjectCreate("SLOW-LISTENER", eventCount);

        assertPostsReceived(
                okServer,
                callbackUrlOkServer,
                equalToJson("{\"projectKey\": \"SLOW-LISTENER\"}"),
                eventCount,
                60,
                TimeUnit.SECONDS);
    }

    private static String getRelativePath(String url) {
        try {
            return new URL(url).getPath();
        } catch (MalformedURLException e) {
            return url;
        }
    }

    private static String randomId() {
        return UUID.randomUUID().toString();
    }

    private void assertPostsReceived(
            WireMockServer server,
            String callbackUrl,
            StringValuePattern bodyPattern,
            int count,
            long to,
            TimeUnit unit) {

        RequestPattern pattern = postRequestedFor(urlEqualTo(getRelativePath(callbackUrl)))
                .withRequestBody(bodyPattern)
                .build();
        long timeout = System.currentTimeMillis() + unit.toMillis(to);
        FindRequestsResult requests = null;
        while (System.currentTimeMillis() < timeout) {
            requests = server.findRequestsMatching(pattern);
            if (requests.getRequests().size() == count) {
                break;
            }
            try {
                Thread.sleep(100L);
            } catch (InterruptedException e) {
                // ignore
            }
        }
        if (requests != null && requests.getRequests().size() != count) {
            FindRequestsResult result = server.findRequestsMatching(
                    postRequestedFor(urlEqualTo(callbackUrl)).build());
            String bodies = result.getRequests().stream()
                    .map(LoggedRequest::getBodyAsString)
                    .collect(Collectors.joining("\n\n"));

            fail("expected " + count + " calls to " + callbackUrl + " but got "
                    + requests.getRequests().size() + "\n\n" + "Actual requests to the endpoint:\n " + bodies);
        }
    }

    private String mockCallback(WireMockServer server, String id, int status, String body) {
        String relativePath = "/callbacks/" + id;
        server.stubFor(post(urlEqualTo(relativePath))
                .withHeader("Content-Type", WireMock.equalTo("application/json"))
                .willReturn(aResponse()
                        .withBody(body)
                        .withHeader("Content-Type", "application/json")
                        .withStatus(status)));

        return "http://localhost:" + server.port() + relativePath;
    }

    private String mockDelayedCallbacks(WireMockServer server, String id, long delayMs) {
        String pathPrefix = "/slow-callbacks/" + id + "/";
        server.stubFor(post(urlMatching(pathPrefix + ".*"))
                .withHeader("Content-Type", WireMock.equalTo("application/json"))
                .willReturn(aResponse()
                        .withFixedDelay((int) delayMs)
                        .withBody("{\"hello\": \"world\"}")
                        .withHeader("Content-Type", "application/json")
                        .withStatus(200)));

        return "http://localhost:" + server.port() + pathPrefix;
    }

    private void registerProjectCreateListener(String name, String url) {
        registerWebhookListener(WebhookCreateRequest.builder()
                .event(RefappEvent.PROJECT_CREATE)
                .name(name)
                .scope(WebhookScope.GLOBAL)
                .sslVerificationRequired(true)
                .url(url)
                .build());
    }

    private void registerWebhookListener(WebhookCreateRequest request) {
        RestWebhook webhook = client.create(request);
        webhookIds.add(webhook.getId());
    }

    private WireMockServer startCallbackServer(int threads, int acceptors) {
        WireMockServer server = new WireMockServer(WireMockConfiguration.options()
                .dynamicPort()
                .jettyAcceptors(acceptors)
                .containerThreads(threads));
        callbackServers.add(server);
        server.start();

        return server;
    }
}
