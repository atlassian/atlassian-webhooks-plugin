package it.com.atlassian.webhooks;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.webhooks.internal.WebhookTestClient;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

// The MoonBaseCreatedEvent (and moon-base:created event) is used to test dispatches to webhooks registered
// through a <webhook> module in atlassian-plugin.xml. Since <webhook> modules cannot easily be disabled, the
// tests for verifying dispatches to webhook listeners registered through the API and atlassian-plugin.xml use
// different events to be independent.
public class DescriptorWebhooksDispatchTest {

    private final WebhookTestClient client = new WebhookTestClient();

    @Before
    public void setup() {
        client.resetCallbackCounter();
    }

    @Test
    public void testDispatch() {
        client.triggerMoonBaseCreated("base1");

        // verify that the dispatch was received
        assertThat(client.getCallbackCount(), equalTo(1));
    }
}
