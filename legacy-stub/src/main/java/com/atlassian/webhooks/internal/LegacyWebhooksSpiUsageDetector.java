package com.atlassian.webhooks.internal;

import java.util.Dictionary;
import java.util.Map;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LegacyWebhooksSpiUsageDetector {

    private final Logger log = LoggerFactory.getLogger(LegacyWebhooksSpiUsageDetector.class);

    private final ServiceTracker<Object, Object> legacySpiTracker;

    public LegacyWebhooksSpiUsageDetector(BundleContext bundleContext) {

        ServiceTracker<Object, Object> tracker = null;
        try {
            Filter filter = bundleContext.createFilter("(objectClass=com.atlassian.webhooks.spi.*)");
            tracker = new ServiceTracker<>(bundleContext, filter, new ServiceTrackerCustomizer<Object, Object>() {
                @Override
                public Object addingService(ServiceReference<Object> serviceReference) {
                    log.warn(
                            "The API/SPI for atlassian-webhooks has changed. Please update the {} "
                                    + "implementation in the {} bundle to use the new API/SPI.",
                            serviceReference.getProperty("objectClass"),
                            serviceReference.getBundle().getSymbolicName());

                    return null;
                }

                @Override
                public void modifiedService(ServiceReference<Object> serviceReference, Object o) {}

                @Override
                public void removedService(ServiceReference<Object> serviceReference, Object o) {}
            });
            tracker.open();
        } catch (InvalidSyntaxException e) {
            //
        }
        legacySpiTracker = tracker;
    }

    public void destroy() {
        if (legacySpiTracker != null) {
            legacySpiTracker.close();
        }
    }

    private static class LegacySpiFilter implements Filter {
        @Override
        public boolean match(ServiceReference<?> serviceReference) {
            Object objectClass = serviceReference.getProperty("objectClass");
            return objectClass != null && objectClass.toString().startsWith("com.atlassian.webhooks.spi.");
        }

        @Override
        public boolean match(Dictionary<String, ?> dictionary) {
            return false;
        }

        @Override
        public boolean matchCase(Dictionary<String, ?> dictionary) {
            return false;
        }

        @Override
        public boolean matches(Map<String, ?> map) {
            return false;
        }
    }
}
