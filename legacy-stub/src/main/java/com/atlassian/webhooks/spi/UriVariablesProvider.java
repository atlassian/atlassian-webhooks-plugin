package com.atlassian.webhooks.spi;

import java.util.Map;

/**
 * Implementations of this API are supposed to extract the variables from the event, so that they can be
 * used in URI-s specified by listeners.
 *
 * @param <E> event type.
 */
@Deprecated
public interface UriVariablesProvider<E> {
    Map<String, Object> uriVariables(E event);
}
