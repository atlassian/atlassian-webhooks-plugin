package com.atlassian.webhooks.spi;

import java.net.URI;

import io.atlassian.fugue.Option;

import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;

@Deprecated
public interface UriResolver {
    /**
     * Gets a fully constructed URI for a relative path defined in the plugin with the given key.
     *
     * @param listenerOriginDetails registration details of web hook listener
     * @param path                  the relative path
     * @return an absolute URI to the plugin path.
     */
    Option<URI> getUri(WebHookListenerRegistrationDetails listenerOriginDetails, URI path);
}
