package com.atlassian.webhooks.spi;

import java.net.URI;

import com.atlassian.httpclient.api.Request;
import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;

/**
 * Signs outgoing requests when publishing webhooks
 *
 * @deprecated Please provide {@link RequestSigner2} instead.
 */
@Deprecated
public interface RequestSigner {
    /**
     * Signs the requests
     *
     * @param uri                 The request target uri.
     * @param registrationDetails Information about how the listener was registered.
     * @param request             The request to sign.
     */
    void sign(URI uri, WebHookListenerRegistrationDetails registrationDetails, Request.Builder request);
}
