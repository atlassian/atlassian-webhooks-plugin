package com.atlassian.webhooks.spi;

import com.atlassian.webhooks.api.register.WebHookPluginRegistration;

/**
 * A class of objects that can produce webhook plugin registrations.
 */
@Deprecated
public interface WebHookPluginRegistrationFactory {

    WebHookPluginRegistration createPluginRegistration();

    /**
     * Function that can tell whether the application is running in the cloud or not.
     */
    @Deprecated
    interface CloudCondition {
        /**
         * @return true if application is running in the cloud, false otherwise.
         */
        boolean isCloud();
    }
}
