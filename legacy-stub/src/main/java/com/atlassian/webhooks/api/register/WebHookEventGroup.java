package com.atlassian.webhooks.api.register;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

@Deprecated
@Immutable
public class WebHookEventGroup {
    private final String nameI18nKey;
    private final List<RegisteredWebHookEvent> events;

    private WebHookEventGroup(final String nameI18nKey, final Iterable<RegisteredWebHookEvent> events) {
        this.nameI18nKey = nameI18nKey;
        this.events = ImmutableList.copyOf(events);
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getNameI18nKey() {
        return nameI18nKey;
    }

    public List<RegisteredWebHookEvent> getEvents() {
        return events;
    }

    public static class Builder {
        private final Set<RegisteredWebHookEvent> events = new LinkedHashSet<RegisteredWebHookEvent>();
        private String nameI18nKey;

        /**
         * Add a group name. If not provided then the group will not be shown in the UI,
         * but webhooks contained within will still work.
         *
         * @param nameI18nKey message key, if the key is not found it will be printed verbatim
         * @return this builder
         */
        public Builder nameI18nKey(final String nameI18nKey) {
            this.nameI18nKey = nameI18nKey;
            return this;
        }

        /**
         * Add webhook event to the group.
         *
         * @param event event that belongs to the group
         * @return this builder
         */
        public Builder addEvent(@Nonnull final RegisteredWebHookEvent event) {
            events.add(Preconditions.checkNotNull(event));
            return this;
        }

        public WebHookEventGroup build() {
            return new WebHookEventGroup(nameI18nKey, events);
        }
    }
}
