package com.atlassian.webhooks.api.publish;

import javax.annotation.concurrent.Immutable;

import com.atlassian.webhooks.spi.EventMatcher;

@Deprecated
@Immutable
public class WebHookEvent<T> {
    private final String id;
    private final T event;
    private final EventMatcher<? super T> eventMatcher;

    public WebHookEvent(String id, T event, EventMatcher eventMatcher) {
        this.id = id;
        this.eventMatcher = eventMatcher;
        this.event = event;
    }

    /**
     * Id of the WebHookEvent in WebHook plugin. For instance, jira:issue_updated.
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the actual event.
     */
    public T getEvent() {
        return event;
    }

    /**
     * Returns the {@link com.atlassian.webhooks.spi.EventMatcher} for the event.
     */
    public EventMatcher<? super T> getEventMatcher() {
        return eventMatcher;
    }
}
