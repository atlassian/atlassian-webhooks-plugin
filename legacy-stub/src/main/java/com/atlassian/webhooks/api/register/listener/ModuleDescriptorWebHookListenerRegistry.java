package com.atlassian.webhooks.api.register.listener;

@Deprecated
public interface ModuleDescriptorWebHookListenerRegistry {
    void register(String webhookId, WebHookListener webHookListener);

    void unregister(String webhookId, WebHookListener webHookListener);
}
