package com.atlassian.webhooks.api.register.listener;

@Deprecated
public enum RegistrationMethod {
    REST,
    UI,
    SERVICE
}
