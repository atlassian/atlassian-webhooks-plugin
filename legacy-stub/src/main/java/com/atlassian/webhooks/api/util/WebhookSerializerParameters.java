package com.atlassian.webhooks.api.util;

import java.util.List;

import com.atlassian.webhooks.spi.EventSerializer;

/**
 * Parameters from WebhookListener registration, passed to {@link EventSerializer#serialize(java.lang.Object, com.atlassian.webhooks.api.util.WebhookSerializerParameters)} method
 */
@Deprecated
public interface WebhookSerializerParameters {

    /**
     * Returns list of property keys which WebhookListener has subcribed to.
     * Serializer is supposed to return requested properties of the entity it serializes.
     *
     * @return list of property keys
     */
    List<String> getPropertyKeys();
}
