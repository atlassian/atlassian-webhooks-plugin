package com.atlassian.webhooks.api.register;

import java.util.List;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import com.atlassian.webhooks.spi.WebHooksHtmlPanel;

import static com.google.common.base.Strings.emptyToNull;

@Deprecated
@Immutable
public final class WebHookEventSection {
    private final String key;
    private final String nameI18nKey;
    private final String descriptionI18nKey;
    private final Set<WebHookEventGroup> groups;
    private final List<WebHooksHtmlPanel> panels;

    private WebHookEventSection(
            final String key,
            final String nameI18nKey,
            final String descriptionI18nKey,
            final Set<WebHookEventGroup> groups,
            final List<WebHooksHtmlPanel> panels) {
        this.key = key;
        this.nameI18nKey = nameI18nKey;
        this.descriptionI18nKey = descriptionI18nKey;
        this.groups = ImmutableSet.copyOf(groups);
        this.panels = ImmutableList.copyOf(panels);
    }

    /**
     * Returns a builder for a section identified by key.
     *
     * @param key must not be null nor empty
     * @return a new builder.
     */
    public static Builder section(@Nonnull String key) {
        Preconditions.checkNotNull(emptyToNull(key), "section key must not be empty");
        return new Builder(key);
    }

    public String getKey() {
        return key;
    }

    public String getNameI18nKey() {
        return nameI18nKey;
    }

    public String getDescriptionI18nKey() {
        return descriptionI18nKey;
    }

    public Set<WebHookEventGroup> getGroups() {
        return groups;
    }

    public List<WebHooksHtmlPanel> getPanels() {
        return panels;
    }

    public static class Builder {
        private final String key;
        private final Set<WebHookEventGroup> groups = Sets.newHashSet();
        private String nameI18nKey = "";
        private String descriptionI18nKey = "";
        private List<WebHooksHtmlPanel> panels = Lists.newArrayList();

        private Builder(final String key) {
            this.key = key;
        }

        /**
         * Add section name. If not present then the section won't be shown in the UI
         * but the webhooks contained within will still work, i.e. listeners will be
         * able to register for them and they will be fired.
         *
         * @param nameI18nKey message key, if the key is not found it will be printed verbatim
         * @return this builder
         */
        public Builder nameI18nKey(final String nameI18nKey) {
            this.nameI18nKey = nameI18nKey;
            return this;
        }

        /**
         * Add an optional section description.
         *
         * @param descriptionI18nKey message key, if the key is not found it will be printed verbatim
         * @return this builder
         */
        public Builder descriptionI18nKey(final String descriptionI18nKey) {
            this.descriptionI18nKey = descriptionI18nKey;
            return this;
        }

        /**
         * Add a group to the section.
         *
         * @param group a group that the section will contain.
         * @return this builder
         */
        public Builder addGroup(final WebHookEventGroup group) {
            this.groups.add(group);
            return this;
        }

        /**
         * Add an HTML panel that will be displayed in this section.
         *
         * @param panel function rendering an HTML panel
         * @return this builder
         */
        public Builder panel(final WebHooksHtmlPanel panel) {
            this.panels.add(panel);
            return this;
        }

        public WebHookEventSection build() {
            return new WebHookEventSection(key, nameI18nKey, descriptionI18nKey, groups, panels);
        }
    }
}
