package com.atlassian.webhooks.api.util;

import com.atlassian.webhooks.api.register.listener.RegistrationMethod;

/**
 * A channel of communication.
 */
@Deprecated
public enum Channel {
    /**
     * For requests to REST endpoints
     */
    REST(RegistrationMethod.REST),
    /**
     * For requests made from plugins and services
     */
    SERVICE(RegistrationMethod.SERVICE),
    /**
     * For requests made from the web UI
     */
    UI(RegistrationMethod.UI);

    private final RegistrationMethod registrationMethod;

    Channel(RegistrationMethod registrationMethod) {
        this.registrationMethod = registrationMethod;
    }

    public RegistrationMethod toRegistrationMethod() {
        return registrationMethod;
    }
}
