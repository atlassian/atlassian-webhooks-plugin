package com.atlassian.webhooks.api.events;

import java.util.Map;

import com.atlassian.webhooks.api.register.listener.RegistrationMethod;

@Deprecated
public class WebHookListenerDisabledEvent extends AbstractdWebHookListenerEvent {
    public WebHookListenerDisabledEvent(
            String name,
            String url,
            Iterable<String> events,
            Map<String, String> parameters,
            RegistrationMethod registrationMethod) {
        super(name, url, events, parameters, registrationMethod);
    }
}
