package com.atlassian.webhooks.api.register.listener;

@Deprecated
public enum WebHookListenerOrigin {
    MODULE_DESCRIPTOR,
    PERSISTENT_STORE
}
