package com.atlassian.webhooks.api.util;

import java.io.Serializable;

import com.atlassian.sal.api.message.Message;

@Deprecated
public class ErrorMessage implements Message {
    private final String message;
    private final Serializable[] arguments;

    public ErrorMessage(String message, Serializable... arguments) {
        this.message = message;
        this.arguments = arguments;
    }

    public ErrorMessage(String message) {
        this(message, null);
    }

    @Override
    public String getKey() {
        return message;
    }

    @Override
    public Serializable[] getArguments() {
        return arguments;
    }
}
