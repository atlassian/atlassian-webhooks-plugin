package com.atlassian.webhooks.api.register.listener;

import java.util.Collection;
import java.util.Map;

import io.atlassian.fugue.Option;

import com.atlassian.webhooks.api.util.MessageCollection;

import static io.atlassian.fugue.Option.option;

/**
 * This WebHook listener service is responsible for adding, updating, removing and getting WebHookListeners registered
 * in atlassian-webhooks-plugin.  It may also be used to enforce cache clearing by the product-specific plugin.
 *
 * @since v1.0
 */
@Deprecated
public interface WebHookListenerService {
    /**
     * Returns all WebHook listeners.
     *
     * @return a collection of WebHook listeners.
     */
    Iterable<PersistentWebHookListener> getAllWebHookListeners();

    /**
     * Returns a WebHook listener with given id.
     *
     * @param id The WebHook listener id.
     * @return The WebHook listener for given id, else None.
     */
    Option<PersistentWebHookListener> getWebHookListener(int id);

    /**
     * Registers a new WebHook listener.
     *
     * @param registrationParameters The parameters of WebHook listener to register.
     * @param registrationMethod     REST, SERVICE or UI.
     * @return parameters of the registered WebHook listener or message collection.
     * @throws IllegalArgumentException if required fields are illegal.
     */
    WebHookListenerServiceResponse registerWebHookListener(
            PersistentWebHookListener registrationParameters, RegistrationMethod registrationMethod);

    /**
     * Registers a new WebHook listener.
     *
     * @param registrationParameters The parameters of WebHook listener to register.
     * @return parameters of the registered WebHook listener or a message collection.
     * @throws IllegalArgumentException if required fields are illegal.
     */
    WebHookListenerServiceResponse registerWebHookListener(PersistentWebHookListener registrationParameters);

    /**
     * Updates a WebHook listener with given id.
     *
     * @param registrationParameters The parameters of WebHook listener to update.
     * @return parameters of the updated WebHook listener.
     * @throws IllegalArgumentException if required fields are illegal or webhook with given id doesn't exist.
     */
    WebHookListenerServiceResponse updateWebHookListener(int id, WebHookListenerUpdateInput registrationParameters);

    /**
     * Deletes a WebHook listener with given id.
     *
     * @param id Id of WebHook listener to remove.
     * @return a message collection with validation errors.
     * @throws IllegalArgumentException if WebHook with given id doesn't exist.
     */
    MessageCollection deleteWebHookListener(int id);

    class WebHookListenerUpdateInput {
        private final Option<String> name;
        private final Option<String> url;
        private final Option<String> description;
        private final Option<Boolean> excludeBody;
        private final Option<Map<String, String>> filters;
        private final Option<Collection<String>> events;
        private final Option<Boolean> enabled;

        private WebHookListenerUpdateInput(
                final Option<String> name,
                final Option<String> url,
                final Option<String> description,
                final Option<Boolean> excludeBody,
                final Option<Map<String, String>> filters,
                final Option<Collection<String>> events,
                final Option<Boolean> enabled) {
            this.name = name;
            this.url = url;
            this.description = description;
            this.excludeBody = excludeBody;
            this.filters = filters;
            this.events = events;
            this.enabled = enabled;
        }

        public static Builder builder() {
            return new Builder();
        }

        public Option<String> getName() {
            return name;
        }

        public Option<String> getUrl() {
            return url;
        }

        public Option<String> getDescription() {
            return description;
        }

        public Option<Boolean> getExcludeBody() {
            return excludeBody;
        }

        public Option<Map<String, String>> getFilters() {
            return filters;
        }

        public Option<Collection<String>> getEvents() {
            return events;
        }

        public Option<Boolean> getEnabled() {
            return enabled;
        }

        public static class Builder {
            private String name;
            private String url;
            private String description;
            private Boolean excludeBody;
            private Map<String, String> filters;
            private Collection<String> events;
            private Boolean enabled;

            public Builder setName(final String name) {
                this.name = name;
                return this;
            }

            public Builder setUrl(final String url) {
                this.url = url;
                return this;
            }

            public Builder setDescription(final String description) {
                this.description = description;
                return this;
            }

            public Builder setExcludeBody(final Boolean excludeBody) {
                this.excludeBody = excludeBody;
                return this;
            }

            public Builder setFilters(final Map<String, String> filters) {
                this.filters = filters;
                return this;
            }

            public Builder setEvents(final Collection<String> events) {
                this.events = events;
                return this;
            }

            public Builder setEnabled(final Boolean enabled) {
                this.enabled = enabled;
                return this;
            }

            public WebHookListenerUpdateInput build() {
                return new WebHookListenerUpdateInput(
                        option(name),
                        option(url),
                        option(description),
                        option(excludeBody),
                        option(filters),
                        option(events),
                        option(enabled));
            }
        }
    }
}
