package com.atlassian.webhooks.api.events;

import java.util.Map;

import com.atlassian.webhooks.api.register.listener.RegistrationMethod;

@Deprecated
public abstract class AbstractdWebHookListenerEvent {
    private final String name;
    private final String url;
    private final Iterable<String> events;
    private final Map<String, String> parameters;
    private final RegistrationMethod registrationMethod;

    public AbstractdWebHookListenerEvent(
            String name,
            String url,
            Iterable<String> events,
            Map<String, String> parameters,
            RegistrationMethod registrationMethod) {
        this.name = name;
        this.url = url;
        this.events = events;
        this.parameters = parameters;
        this.registrationMethod = registrationMethod;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public Iterable<String> getEvents() {
        return events;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public RegistrationMethod getRegistrationMethod() {
        return registrationMethod;
    }
}
