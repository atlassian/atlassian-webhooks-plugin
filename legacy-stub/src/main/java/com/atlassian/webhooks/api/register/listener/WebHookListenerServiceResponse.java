package com.atlassian.webhooks.api.register.listener;

import javax.annotation.concurrent.Immutable;

import com.google.common.base.Function;
import io.atlassian.fugue.Option;

import com.atlassian.webhooks.api.util.MessageCollection;

import static com.google.common.base.Preconditions.checkNotNull;

@Deprecated
@Immutable
public class WebHookListenerServiceResponse {
    private final MessageCollection messageCollection;
    private final Option<PersistentWebHookListener> registeredListener;

    private WebHookListenerServiceResponse(
            MessageCollection messageCollection, Option<PersistentWebHookListener> webHookListenerParameters) {
        this.messageCollection = checkNotNull(messageCollection);
        this.registeredListener = checkNotNull(webHookListenerParameters);
    }

    public static WebHookListenerServiceResponse ok(final PersistentWebHookListener registeredListener) {
        return new WebHookListenerServiceResponse(MessageCollection.empty(), Option.some(registeredListener));
    }

    public static WebHookListenerServiceResponse error(final MessageCollection messageCollection) {
        return new WebHookListenerServiceResponse(messageCollection, Option.none(PersistentWebHookListener.class));
    }

    public MessageCollection getMessageCollection() {
        return messageCollection;
    }

    public Option<PersistentWebHookListener> getRegisteredListener() {
        return registeredListener;
    }

    public <T> T fold(Function<MessageCollection, T> onError, Function<PersistentWebHookListener, T> onSuccess) {
        if (messageCollection.isEmpty() && registeredListener.isDefined()) {
            return onSuccess.apply(registeredListener.get());
        } else if (!messageCollection.isEmpty()) {
            return onError.apply(messageCollection);
        } else {
            throw new IllegalStateException();
        }
    }
}
