package com.atlassian.webhooks.migration;

import java.util.Date;

public class WebhookDTO {
    private final int id;
    private final String url;
    private final String name;
    private final String description;
    private final String lastUpdatedUser;
    private final Date lastUpdated;
    private final boolean excludeBody;
    private final String filters;
    private final String parameters;
    private final String registrationMethod;
    private final String events;
    private final String jsonEvents;
    private final boolean enabled;

    public WebhookDTO(
            int id,
            String url,
            String name,
            String description,
            String lastUpdatedUser,
            Date lastUpdated,
            boolean excludeBody,
            String filters,
            String parameters,
            String registrationMethod,
            String events,
            String jsonEvents,
            boolean enabled) {
        this.id = id;
        this.url = url;
        this.name = name;
        this.description = description;
        this.lastUpdatedUser = lastUpdatedUser;
        this.lastUpdated = lastUpdated;
        this.excludeBody = excludeBody;
        this.filters = filters;
        this.parameters = parameters;
        this.registrationMethod = registrationMethod;
        this.events = events;
        this.jsonEvents = jsonEvents;
        this.enabled = enabled;
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLastUpdatedUser() {
        return lastUpdatedUser;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public boolean isExcludeBody() {
        return excludeBody;
    }

    public String getFilters() {
        return filters;
    }

    public String getParameters() {
        return parameters;
    }

    public String getRegistrationMethod() {
        return registrationMethod;
    }

    public String getEvents() {
        return events;
    }

    public String getJsonEvents() {
        return jsonEvents;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public static WebhookDTOBuilder builder() {
        return new WebhookDTOBuilder();
    }

    public static class WebhookDTOBuilder {
        private int id;
        private String url;
        private String name;
        private String description;
        private String lastUpdatedUser;
        private Date lastUpdated;
        private boolean excludeBody;
        private String filters;
        private String parameters;
        private String registrationMethod;
        private String events;
        private String jsonEvents;
        private boolean enabled;

        public WebhookDTOBuilder id(int id) {
            this.id = id;
            return this;
        }

        public WebhookDTOBuilder url(String url) {
            this.url = url;
            return this;
        }

        public WebhookDTOBuilder name(String name) {
            this.name = name;
            return this;
        }

        public WebhookDTOBuilder description(String description) {
            this.description = description;
            return this;
        }

        public WebhookDTOBuilder lastUpdatedUser(String lastUpdatedUser) {
            this.lastUpdatedUser = lastUpdatedUser;
            return this;
        }

        public WebhookDTOBuilder lastUpdated(Date lastUpdated) {
            this.lastUpdated = lastUpdated;
            return this;
        }

        public WebhookDTOBuilder excludeBody(boolean excludeBody) {
            this.excludeBody = excludeBody;
            return this;
        }

        public WebhookDTOBuilder filters(String filters) {
            this.filters = filters;
            return this;
        }

        public WebhookDTOBuilder parameters(String parameters) {
            this.parameters = parameters;
            return this;
        }

        public WebhookDTOBuilder registrationMethod(String registrationMethod) {
            this.registrationMethod = registrationMethod;
            return this;
        }

        public WebhookDTOBuilder events(String events) {
            this.events = events;
            return this;
        }

        public WebhookDTOBuilder jsonEvents(String jsonEvents) {
            this.jsonEvents = jsonEvents;
            return this;
        }

        public WebhookDTOBuilder enabled(boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public WebhookDTO createWebhookDTO() {
            return new WebhookDTO(
                    id,
                    url,
                    name,
                    description,
                    lastUpdatedUser,
                    lastUpdated,
                    excludeBody,
                    filters,
                    parameters,
                    registrationMethod,
                    events,
                    jsonEvents,
                    enabled);
        }
    }
}
