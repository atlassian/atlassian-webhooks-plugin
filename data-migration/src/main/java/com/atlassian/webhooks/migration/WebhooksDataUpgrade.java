package com.atlassian.webhooks.migration;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public interface WebhooksDataUpgrade {
    void registerExternalMigrationTask(List<WebhookDTO> webhookDTOs, Consumer<Map<Integer, Integer>> resultConsumer);
}
