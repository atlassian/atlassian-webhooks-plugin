package com.atlassian.webhooks;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;

/**
 * Configuration of the webhooks feature - allows the host application to customize the webhooks settings.
 *
 * @since 5.0
 */
public interface WebhooksConfiguration {

    /**
     * The default configuration that will be used if the host application does not provide one.
     */
    WebhooksConfiguration DEFAULT = new WebhooksConfiguration() {};

    /**
     * @return the rate at which the system should increase the time between dispatch webhooks to a listener after the
     *         listener has failed more than {@link #getBackoffTriggerCount()} times in a row
     */
    default double getBackoffExponent() {
        return 1.2d;
    }

    /**
     * @return the time the system should skip dispatching webhooks to a webhook listener, when it has failed
     *         {@link #getBackoffTriggerCount()} times in a row
     */
    @Nonnull
    default Duration getBackoffInitialDelay() {
        return Duration.of(10, ChronoUnit.SECONDS);
    }

    /**
     * @return the maximum time the system is allowed to skip a webhook listener after dispatching failed more than
     *         {@link #getBackoffTriggerCount()} times in a row
     */
    @Nonnull
    default Duration getBackoffMaxDelay() {
        return Duration.of(2, ChronoUnit.HOURS);
    }

    /**
     * @return the number of times delivery to a webhook listener needs to fail consecutively before the system starts
     *         backing of webhook delivery (skipping the webhook for some time)
     */
    default int getBackoffTriggerCount() {
        return 5;
    }

    /**
     * @return IP addresses that a webhook may not be sent to. Requests to these address will fail with an exception.
     *         Addresses can be ipv4 or v6. They can be specific IPs, or CIDR ranges.
     * @since 5.1
     */
    @Nonnull
    default List<String> getBlacklistedAddresses() {
        return ImmutableList.of();
    }

    /**
     * @return how long to wait for a TCP connection when making a connection to webhook listener
     */
    @Nonnull
    default Duration getConnectionTimeout() {
        return Duration.ofSeconds(20);
    }

    /**
     * Dispatching of webhooks goes through 2 levels of asynchronous processing:
     * <ol>
     *     <li>{@link WebhookService#publish} schedules a webhook request to be prepared and then dispatched</li>
     *     <li>The webhook dispatcher schedules a task to asynchronously create an HTTP connection, send the webhook
     *         request and notify any callbacks of the result</li>
     * </ol>
     * This setting controls the queue size for the first level. Preparing webhooks (generating the body + applying the
     * enrichers) should be fast; this queue is used to handle short peaks where webhook are being triggered at a higher
     * rate than they can be prepared.
     *
     * @return the maximum number of dispatches that can be queued before being sent to the dispatcher.
     */
    default int getDispatchQueueSize() {
        return 250;
    }

    /**
     * @return how long the dispatcher should wait to start dispatching a webhook if {@link #getMaxInFlightDispatches()}
     *         webhook dispatches are already in flight.
     */
    @Nonnull
    default Duration getDispatchTimeout() {
        return Duration.ofMillis(250);
    }

    /**
     * @return the number of I/O dispatch threads to be used by the I/O reactor of the HTTP client used for dispatching
     *         webhooks
     */
    default int getIoThreadCount() {
        return 3;
    }

    /**
     * @return the JMX domain under which webhooks should publish its statistics
     */
    @Nonnull
    default String getJmxDomain() {
        return "com.atlassian.webhooks";
    }

    /**
     * @return the maximum number of threads that can be used for executing callbacks
     */
    default int getMaxCallbackThreads() {
        return 10;
    }

    /**
     * @return how many simultaneous connections are allowed in total
     */
    default int getMaxHttpConnections() {
        return 200;
    }

    /**
     * @return how many connections are allowed per host
     */
    default int getMaxHttpConnectionsPerHost() {
        return 5;
    }

    /**
     * Dispatching of webhooks goes through 2 levels of asynchronous processing:
     * <ol>
     *     <li>{@link WebhookService#publish} schedules a webhook request to be prepared and then dispatched</li>
     *     <li>The webhook dispatcher schedules a task to asynchronously create an HTTP connection, send the webhook
     *         request and notify any callbacks of the result</li>
     * </ol>
     * This setting controls the queue size for the second level.
     *
     * @return the maximum number of dispatches that can be in flight (send request, receive response and notify
     *         callbacks) at a given time
     */
    default int getMaxInFlightDispatches() {
        return 500;
    }

    /**
     * @return the maximum size of the response body in bytes. Any further content will be truncated.
     */
    default long getMaxResponseBodySize() {
        return 16_384;
    }

    /**
     * @return the maximum allowed size for response header lines in bytes.
     *         Responses with headers that have a line size exceeding this limit will fail with an exception.
     * @since 7.2.0
     */
    default int getMaxResponseHeaderLineSize() {
        return 8192;
    }

    /**
     * @return how long the http client should wait for data
     */
    @Nonnull
    default Duration getSocketTimeout() {
        return Duration.ofSeconds(20);
    }

    /**
     * @return the statistics flush interval
     * @since 6.1
     */
    @Nonnull
    default Duration getStatisticsFlushInterval() {
        return Duration.ofSeconds(30L);
    }

    /**
     * @return whether webhook invocation history should be recorded (last successful and last failed invocation)
     * @since 6.1
     */
    default boolean isInvocationHistoryEnabled() {
        return false;
    }

    /**
     * @return whether webhooks statistics should be recorded
     */
    default boolean isStatisticsEnabled() {
        return true;
    }
}
