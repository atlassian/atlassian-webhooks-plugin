package com.atlassian.webhooks;

import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * The host application and/or plugins can register a {@code WebhookEventProvider} to make atlassian-webhooks
 * aware of the supported webhook event types.
 *
 * @since 5.0
 */
public interface WebhookEventProvider {

    /**
     * @param id the {@link WebhookEvent#getId() event type ID}
     * @return the corresponding {@link WebhookEvent event type}, or {@code null} if the ID could not be mapped
     */
    @Nullable
    WebhookEvent forId(@Nonnull String id);

    /**
     * @return a list of the provided {@link WebhookEvent events}
     */
    @Nonnull
    List<WebhookEvent> getEvents();

    /**
     * @return the weight of the provider, used to order different providers
     */
    int getWeight();
}
