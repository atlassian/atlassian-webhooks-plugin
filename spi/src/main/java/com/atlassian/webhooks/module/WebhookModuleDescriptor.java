package com.atlassian.webhooks.module;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.StateAware;
import com.atlassian.webhooks.Webhook;

/**
 * {@link ModuleDescriptor} for {@link Webhook}
 */
public interface WebhookModuleDescriptor extends ModuleDescriptor<Webhook>, StateAware {}
