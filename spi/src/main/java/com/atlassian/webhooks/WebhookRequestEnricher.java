package com.atlassian.webhooks;

import javax.annotation.Nonnull;

import com.atlassian.webhooks.request.WebhookHttpRequest;

/**
 * @since 5.0
 */
public interface WebhookRequestEnricher {

    /**
     * This is called on a webhook invocation that has been passed through filtering and is now ready to perform a HTTP
     * request.
     * <p>
     * This can be implemented to provide additional details to the request, or change those that already exist.
     * {@link WebhookRequestEnricher}s will be called in a chain, passing the result of one onto the next.
     * <p>
     * Changes to the upcoming HTTP request can be made by retrieving a reference to the
     * {@link WebhookHttpRequest.Builder} via {@link WebhookInvocation#getRequestBuilder()}
     *
     * @param invocation an invocation of a webhook
     */
    void enrich(@Nonnull WebhookInvocation invocation);

    /**
     * Gets the weight used to order the enrichers. Lower weights are processed first
     *
     * Values below 1000 are reserved for internal use.
     *
     * @return the weight
     */
    int getWeight();
}
